import { object, string } from 'yup';

const LoginSchema = object().shape({
  email: string()
    .email('Digite um e-mail válido.')
    .required('Por favor, digite o e-mail que deseja entrar'),
  password: string()
    .min(6, 'Sua senha deve ter no mínimo 6 dígitos')
    .required('Por favor, digite a sua senha de no minimo 6 dígitos')
});

export default LoginSchema;
