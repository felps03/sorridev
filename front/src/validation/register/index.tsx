import { isValid as isValidCPF } from '@fnando/cpf';
import * as Yup from 'yup';

const InitialRegisterSchema = Yup.object().shape({
  name: Yup.string().required('Digite o nome que deseja cadastrar'),
  cpf: Yup.string()
    .transform((value) => {
      const cpf: string = value.replace(/\.|-/g, '');

      return isValidCPF(cpf) ? cpf : '';
    })
    .min(11, 'O CPF digitado é inválido')
    .max(11, 'O CPF digitado é inválido')
    .required('Digite o CPF que deseja cadastrar'),
  email: Yup.string()
    .email()
    .required('Digite um email válido para o cadastro'),
  rg: Yup.string().required('Digite o seu rg'),
  phone: Yup.string().required('Digite o seu telefone'),
  birthdate: Yup.string().required('Digite a data de nascimento'),
  gender: Yup.string().required('Selecione o seu gênero'),
  password: Yup.string().required('Digite uma senha'),
  confirmPassword: Yup.string().oneOf(
    [Yup.ref('password'), null],
    'As senhas não combinam'
  )
});

const RegisterSchema = Yup.object().shape({
  name: Yup.string().required('Digite o nome que deseja cadastrar'),
  cpf: Yup.string()
    .transform((value) => {
      const cpf: string = value.replace(/\.|-/g, '');

      return isValidCPF(cpf) ? cpf : '';
    })
    .min(11, 'O CPF digitado é inválido')
    .max(11, 'O CPF digitado é inválido')
    .required('Digite o CPF que deseja cadastrar'),
  email: Yup.string()
    .email('Digite um email válido')
    .required('Digite um email válido para o cadastro'),
  rg: Yup.string().required('Digite o seu rg'),
  phone: Yup.string().required('Digite o seu telefone'),
  birthdate: Yup.string().required('Digite a data de nascimento'),
  gender: Yup.string().required('Selecione o seu gênero')
});

const RegisterProfessionalSchema = Yup.object().shape({
  name: Yup.string().required('Digite o nome que deseja cadastrar'),
  cpf: Yup.string()
    .transform((value) => {
      const cpf: string = value.replace(/\.|-/g, '');

      return isValidCPF(cpf) ? cpf : '';
    })
    .min(11, 'O CPF digitado é inválido')
    .max(11, 'O CPF digitado é inválido')
    .required('Digite o CPF que deseja cadastrar'),
  email: Yup.string()
    .email('Digite um email válido')
    .required('Digite um email válido para o cadastro'),
  rg: Yup.string().required('Digite o seu rg'),
  phone: Yup.string().required('Digite o seu telefone'),
  birthdate: Yup.string().required('Digite a data de nascimento'),
  gender: Yup.string().required('Selecione o seu gênero'),
  CRO: Yup.string().required('Digite o CRO do dentista')
});

const RegisterEmployeeSchema = Yup.object().shape({
  name: Yup.string().required('Digite o nome que deseja cadastrar'),
  cpf: Yup.string()
    .transform((value) => {
      const cpf: string = value.replace(/\.|-/g, '');

      return isValidCPF(cpf) ? cpf : '';
    })
    .min(11, 'O CPF digitado é inválido')
    .max(11, 'O CPF digitado é inválido')
    .required('Digite o CPF que deseja cadastrar'),
  email: Yup.string()
    .email('Digite um email válido')
    .required('Digite um email válido para o cadastro'),
  rg: Yup.string().required('Digite o seu rg'),
  phone: Yup.string().required('Digite o seu telefone'),
  birthdate: Yup.string().required('Digite a data de nascimento'),
  gender: Yup.string().required('Selecione o seu gênero')
});

const RegisterServiceSchema = Yup.object().shape({
  name: Yup.string().required('Digite o nome do fornecedor'),
  phone: Yup.string().required('Digite o telefone do fornecedor'),
  supply: Yup.string().required(
    'Digite o tipo de mantimento fornecido pelo fornecedor'
  )
});
const RegisterStockSchema = Yup.object().shape({
  product: Yup.string().required('Digite o nome do produto'),
  price: Yup.string().required('Digite o valor do produto'),
  quantity: Yup.string().required('Digite a quantidade do produto'),
  provider: Yup.string().required('Digite o nome do fornecedor')
});

const RegisterBalanceSchema = Yup.object().shape({
  cash_accounting: Yup.string().required('Digite o balanço de caixa'),
  spending: Yup.string().required('Digite o valor gasto'),
  number_of_appointments: Yup.string().required(
    'Digite o numero de atendimentos'
  )
});

const RegisterScheduleSchema = Yup.object().shape({
  dateStart: Yup.string().required('Digite a data do agendamento'),
  timeStart: Yup.string().required('Digite o horário do agendamento'),
  cpf: Yup.string()
    .transform((value) => {
      const cpf: string = value.replace(/\.|-/g, '');

      return isValidCPF(cpf) ? cpf : '';
    })
    .min(11, 'O CPF digitado é inválido')
    .max(11, 'O CPF digitado é inválido')
    .required('Digite o CPF do paciente')
});

const validationPasswordSchema = Yup.object().shape({
  currentPassword: Yup.string().required('Digite sua senha atual'),
  password: Yup.string().required('Digite a sua nova senha'),
  confirmPassword: Yup.string()
    .oneOf([Yup.ref('password')], 'As senhas não conferem')
    .required('Confirme a sua nova senha')
});

export {
  InitialRegisterSchema,
  RegisterSchema,
  RegisterProfessionalSchema,
  RegisterEmployeeSchema,
  RegisterServiceSchema,
  RegisterStockSchema,
  RegisterBalanceSchema,
  RegisterScheduleSchema,
  validationPasswordSchema
};
