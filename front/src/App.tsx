import React from 'react';

import { Provider } from 'react-redux';
import { ToastContainer } from 'react-toastify';
import { PersistGate } from 'redux-persist/integration/react';
import { ThemeProvider as StyledThemeProvider } from 'styled-components';

import Routes from './routes';
import { configureStore } from './store/configure-store';
import GlobalStyle from './styles/globalTheme';
import defaultTheme from './styles/theme/default.theme';

const { store, persistor } = configureStore();

function App() {
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <StyledThemeProvider theme={defaultTheme}>
          <ToastContainer />
          <GlobalStyle />
          <Routes />
        </StyledThemeProvider>
      </PersistGate>
    </Provider>
  );
}

export default App;
