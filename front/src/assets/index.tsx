/* eslint-disable import/prefer-default-export */
export { ReactComponent as logo } from './icons/logo.svg';
export { ReactComponent as clockIcon } from './icons/clockIcon.svg';
export { ReactComponent as NotificationsIcon } from './icons/notifications.svg';
export { ReactComponent as TeenBoyIcon } from './icons/teen_boy.svg';
export { ReactComponent as ChildGirlIcon } from './icons/child_girl.svg';
export { ReactComponent as NotFoundIcon } from './icons/notfound.svg';
export { ReactComponent as UsersIcon } from './icons/users.svg';
