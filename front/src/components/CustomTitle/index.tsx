import React, { FC, ReactNode } from 'react';

import { CardHeaderForm } from './styles';

interface Props {
  title: string | ReactNode;
}

const CustomTile: FC<Props> = ({ title }: Props) => {
  return <CardHeaderForm>{title}</CardHeaderForm>;
};

export default CustomTile;
