import styled from 'styled-components';

const CardHeaderForm = styled.section`
  /* padding: 0.25rem 0.5rem; */
  margin-bottom: 0;
  background-color: #fff;
  border-bottom: 1px solid rgba(0, 0, 0, 0.05);
  > h3 {
    font-size: 1.0625rem;
    font-family: inherit;
    font-weight: 600;
    line-height: 1.5;
    color: #32325d;
  }
`;

export { CardHeaderForm };
