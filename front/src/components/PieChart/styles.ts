import { ResponsivePie } from '@nivo/pie';
import styled from 'styled-components';

const ResponsivePieChart = styled(ResponsivePie)`
  && {
    height: 300px;
  }
`;

export { ResponsivePieChart };
