import React, { FC, ReactNode } from 'react';

import { AiFillCloseCircle } from 'react-icons/ai';

import { CardHeaderForm, IconWrapper } from './styles';

interface Props {
  title: string | ReactNode;
  onClose: () => void;
}

const CustomModalHeader: FC<Props> = ({ title, onClose }: Props) => {
  return (
    <CardHeaderForm>
      <span>{title}</span>
      <IconWrapper onClick={onClose}>
        <AiFillCloseCircle />
      </IconWrapper>
    </CardHeaderForm>
  );
};

export default CustomModalHeader;
