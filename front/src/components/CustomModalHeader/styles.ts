import { IconButton } from '@material-ui/core';
import styled from 'styled-components';

const CardHeaderForm = styled.section`
  margin-bottom: 0;
  background-color: #fff;
  border-bottom: 1px solid rgba(0, 0, 0, 0.05);
  display: flex;
  justify-content: space-between;
  align-items: center;
  > h3 {
    font-size: 1.0625rem;
    font-family: inherit;
    font-weight: 600;
    line-height: 1.5;
    color: #32325d;
  }
`;

const IconWrapper = styled(IconButton)`
  && {
    .MuiIconButton-label {
      font-size: 25px;
      font-family: inherit;
      font-weight: 600;
      line-height: 1.5;
      color: ${(props) => props.theme.colors.primary};
    }
  }
`;

export { CardHeaderForm, IconWrapper };
