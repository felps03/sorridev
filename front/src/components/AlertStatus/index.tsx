/* eslint-disable consistent-return */
import React, { useEffect } from 'react';

import { Alert } from '@material-ui/lab';

interface StatusInfo {
  type: 'success' | 'info' | 'warning' | 'error' | undefined;
  message: string;
  onClose: (status: boolean) => void;
  isVisible: boolean;
  duration?: number;
}

const AlertStatus = ({
  type,
  message,
  onClose,
  isVisible,
  duration
}: StatusInfo) => {
  useEffect(() => {
    // message is empty (meaning no errors). Adjust as needed
    if (!message) {
      onClose(false);
      return;
    }
    // error exists. Display the message and hide after 5 secs
    onClose(true);
    const timer = setTimeout(() => {
      onClose(false);
    }, duration || 5000);
    return () => clearTimeout(timer);
  }, [duration, message, onClose]);

  if (!isVisible) return null;
  return <Alert severity={type}>This is an error message!</Alert>;
};

AlertStatus.defaultProps = {
  duration: 5000
};

export default AlertStatus;
