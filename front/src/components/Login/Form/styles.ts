import { Button, TextField } from '@material-ui/core';
import styled from 'styled-components';

const Form = styled.form`
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
  h3 {
    color: #666666;
    margin: 2rem 0rem;
  }
`;

const Input = styled(TextField)`
  && {
    margin-bottom: 15px;
    border: none;
    background-color: #f5f5f5;
    box-shadow: 0px 14px 9px -15px rgba(0, 0, 0, 0.25);
    border-radius: 8px;
    .MuiFormLabel-root.Mui-focused,
    .MuiInputLabel-outlined.MuiInputLabel-shrink {
      color: #4477eef2;
      font-size: ${(props) => props.theme.font.size.medium};
      font-weight: ${(props) => props.theme.font.weight.bold};
    }

    .MuiOutlinedInput-root {
      fieldset {
        border-color: #f6f6f6;
      }
      &:hover fieldset {
        border-color: #e3e3e3;
      }
      &.Mui-focused fieldset {
        border-color: #e3e3e3;
      }
    }

    .MuiFormLabel-root.Mui-error {
      color: ${(props) => props.theme.colors.error};
    }
  }
`;

const SmallButton = styled(Button)`
  && {
    height: 54px;
    width: 60px;
    border: 1px solid rgba(255, 177, 103, 0.6);
    margin: 1rem 0;
    box-shadow: 0px 14px 9px -15px rgba(0, 0, 0, 0.25);
    border-radius: 8px;
    background-color: rgba(255, 177, 103, 0.2);
    cursor: pointer;
    transition: all 0.2s ease-in;
    > span {
      width: auto;
      > svg {
        font-size: 2rem;
      }
    }
    &:hover {
      background-color: rgba(255, 177, 103, 0.2);
      transform: translateY(-3px);
    }

    &:first-child {
      background-color: rgba(255, 177, 103, 0.8);
      margin-right: 0.5rem;
    }
  }
`;

const ButtonWrapper = styled.section`
  display: flex;
  flex-direction: row;
  width: 100%;
`;

const StyledButton = styled(Button)`
  && {
    &.MuiButton-root {
      height: 54px;
      margin-right: 0.5rem;
      margin-top: 1rem;
      /* max-width: 317px; */
      border: none;
      padding-right: 10px;
      box-shadow: 0px 14px 9px -15px rgba(0, 0, 0, 0.25);
      border-radius: 8px;
      background-color: rgba(255, 177, 103, 0.8);
      font-size: 18px;
      color: #fff;
      font-weight: bold;
      cursor: pointer;
      text-transform: capitalize;
      transition: all 0.2s ease-in;
      &:hover {
        background-color: rgba(255, 177, 103, 0.8);
        transform: translateY(-3px);
      }
    }
  }
`;

export { Input, Form, StyledButton, SmallButton, ButtonWrapper };
