/* eslint-disable @typescript-eslint/no-unused-vars */
import React from 'react';

import { Formik } from 'formik';
import GoogleLogin from 'react-google-login';
import { FcGoogle } from 'react-icons/fc';

import { LoginValues } from '../../../interfaces/login';
import LoginSchema from '../../../validation/login';
import {
  Form,
  Input,
  StyledButton,
  ButtonWrapper,
  SmallButton
} from './styles';

interface ILoginFormProps {
  onFormSubmit: (loginValues: LoginValues) => void;
  onSubmitGoogle: (loginValues: LoginValues) => void;
  isSubmitting: boolean;
}

const UserLoginForm = ({
  onFormSubmit,
  isSubmitting,
  onSubmitGoogle
}: ILoginFormProps) => {
  const responseGoogle = (response: any) => {
    onSubmitGoogle(response);
  };
  return (
    <Formik
      initialValues={{ email: '', password: '' }}
      validationSchema={LoginSchema}
      onSubmit={onFormSubmit}
    >
      {({
        errors,
        handleBlur,
        handleChange,
        handleSubmit,
        touched,
        values
      }) => (
        <Form onSubmit={handleSubmit}>
          <h3>Login</h3>
          <div style={{ padding: '0px 40px' }}>
            <Input
              fullWidth
              type="text"
              label="E-mail"
              name="E-mail"
              placeholder="Digite seu e-mail!"
              variant="outlined"
              size="medium"
              value={values.email}
              onChange={handleChange('email')}
              onBlur={handleBlur}
              helperText={errors.email && touched.email && errors.email}
              error={Boolean(errors.email && touched.email)}
            />
            <Input
              fullWidth
              size="medium"
              type="password"
              label="Senha"
              name="Senha"
              placeholder="Digite sua senha"
              variant="outlined"
              value={values.password}
              onChange={handleChange('password')}
              onBlur={handleBlur}
              helperText={
                errors.password && touched.password && errors.password
              }
              error={Boolean(errors.password && touched.password)}
            />
            <ButtonWrapper>
              <StyledButton
                variant="contained"
                color="primary"
                fullWidth
                type="submit"
              >
                Acessar
              </StyledButton>
              <GoogleLogin
                clientId="351741901200-eiuv75vrtt4ri00fca2il40ihp1rdgdt.apps.googleusercontent.com"
                render={(renderProps) => (
                  <SmallButton
                    variant="contained"
                    color="primary"
                    fullWidth
                    type="button"
                    onClick={renderProps.onClick}
                    disabled={renderProps.disabled}
                  >
                    <FcGoogle />
                  </SmallButton>
                )}
                buttonText="Login"
                onSuccess={responseGoogle}
                onFailure={responseGoogle}
                cookiePolicy="single_host_origin"
              />
            </ButtonWrapper>
          </div>
        </Form>
      )}
    </Formik>
  );
};

export default UserLoginForm;
