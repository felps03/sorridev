import React, { useEffect, useState } from 'react';

import { Box, Grid } from '@material-ui/core';
import moment from 'moment';
import { useSelector } from 'react-redux';

import { RootState } from '../../store/configure-store';
import { getMonthTranslation, getCurrentMonth } from '../../utils/getMonth';
import ContentBlock from '../ContentBlock';
import CustomTitle from '../CustomTitle';
import LineChart from '../LineChart';
import PieChart from '../PieChart';

const DashboardCharts = () => {
  const [genderData, setGenderData] = useState<any>([]);
  const [ageData, setAgeData] = useState<any>([]);
  const [monthlyData, setMonthlyData] = useState<any>([]);
  const dashboard = useSelector((state: RootState) => state.dashboard);

  useEffect(() => {
    if (dashboard.gender.status?.isFetchSuccess) {
      if (dashboard.gender.status.noContent) {
        setGenderData([
          {
            id: 'Não há dados',
            value: 0.1,
            color: 'hsl(199, 70%, 50%)'
          }
        ]);
      } else {
        const {
          gender: { data }
        } = dashboard;
        const dados = [
          {
            id: 'Feminino',
            value: data?.female,
            color: 'hsl(199, 70%, 50%)'
          },
          {
            id: 'Masculino',
            value: data?.male,
            color: 'hsl(282, 70%, 50%)'
          },
          {
            id: 'Não Informado',
            value: data?.empty,
            color: 'hsl(32, 92.59259259259258%, 31.764705882352946%)'
          }
        ];

        setGenderData(dados);
      }
    }
    if (dashboard.age.status?.isFetchSuccess) {
      if (dashboard.age?.status.noContent) {
        setAgeData([
          {
            id: 'Não há dados',
            value: 0.1,
            color: 'hsl(199, 70%, 50%)'
          }
        ]);
      } else {
        const {
          age: { data }
        } = dashboard;
        const datas = data.map((ages: any) => ({
          id: ages._id.replace('Under', 'Abaixo').replace('Over', 'Acima'),
          value: ages.count,
          color: 'hsl(199, 70%, 50%)'
        }));
        setAgeData(datas);
      }
    }

    if (
      dashboard.monthly.status?.isFetchSuccess &&
      dashboard.attendance.status?.isFetchSuccess &&
      dashboard.absent.status?.isFetchSuccess
    ) {
      const MonthAccount: any = [];
      const AttendanceAccount: any = [];
      const AbsentAccount: any = [];
      const payload = [];
      const monthly = dashboard.monthly?.data;
      const attendance = dashboard.attendance?.data;
      const absent = dashboard.absent?.data;

      if (dashboard.monthly.status.noContent) {
        payload.push({
          id: 'Contagem',
          color: 'hsl(51, 70%, 50%)',
          data: [{ x: 0, y: 0 }]
        });
      } else {
        Object.entries(monthly).forEach(([key, value]) => {
          const year = key.split('-');
          MonthAccount.push({
            x: `${getMonthTranslation(year[0])} - ${year[1]}`,
            y: value
          });
        });
        payload.push({
          id: 'Novos Pacientes',
          color: 'hsl(300, 70.19607843137256%, 50%)',
          data: MonthAccount
        });
      }

      if (dashboard.attendance.status.noContent) {
        payload.push({
          id: 'Contagem',
          color: 'hsl(51, 70%, 50%)',
          data: [{ x: 0, y: 0 }]
        });
      } else {
        Object.entries(attendance).forEach(([key, value]) => {
          const year = key.split('-');
          AttendanceAccount.push({
            x: `${getMonthTranslation(year[0])} - ${year[1]}`,
            y: value
          });
        });
        payload.push({
          id: 'Atendimentos',
          color: 'hsl(300, 70.19607843137256%, 50%)',
          data: AttendanceAccount
        });
      }

      if (dashboard.absent.status.noContent) {
        payload.push({
          id: 'Pacientes Ausentes',
          color: 'hsl(51, 70%, 50%)',
          data: [
            {
              x: `${getCurrentMonth(moment().month())} - ${moment().year()}`,
              y: 0
            }
          ]
        });
      } else {
        Object.entries(absent).forEach(([key, value]) => {
          const year = key.split('-');
          AbsentAccount.push({
            x: `${getMonthTranslation(year[0])} - ${year[1]}`,
            y: value
          });
        });
        payload.push({
          id: 'Ausentes',
          color: 'hsl(300, 70.19607843137256%, 50%)',
          data: AbsentAccount
        });
      }
      setMonthlyData(payload);
    }
  }, [dashboard]);

  return (
    <Box paddingTop={5}>
      <Grid container spacing={1}>
        <Grid item xs={12} sm={6}>
          <ContentBlock>
            <CustomTitle title="Análise de Gêneros" />
            {!dashboard.gender.status?.isLoading && (
              <PieChart data={genderData} />
            )}
          </ContentBlock>
        </Grid>
        <Grid item xs={12} sm={6}>
          <ContentBlock>
            <CustomTitle title="Análise de Idades" />
            {!dashboard.age.status?.isLoading && <PieChart data={ageData} />}
          </ContentBlock>
        </Grid>
        <Grid item xs={12}>
          <ContentBlock>
            <CustomTitle title="Análise Anual" />
            {!dashboard.monthly.status?.isLoading && ageData && monthlyData && (
              <LineChart data={monthlyData} />
            )}
          </ContentBlock>
        </Grid>
      </Grid>
    </Box>
  );
};

export default DashboardCharts;
