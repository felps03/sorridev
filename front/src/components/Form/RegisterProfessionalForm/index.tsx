/* eslint-disable no-restricted-syntax */
import React, { FormEvent, useEffect, useState } from 'react';

import {
  TextField,
  Grid,
  Select,
  MenuItem,
  FormControl,
  InputLabel,
  FormHelperText,
  CircularProgress
} from '@material-ui/core';
import { Alert } from '@material-ui/lab';
import { Formik } from 'formik';
import { BsPersonPlusFill } from 'react-icons/bs';
import Swal from 'sweetalert2';
import { useDebounce } from 'use-debounce';

import { GENDER } from '../../../constants';
import { Specialties } from '../../../data';
import { SearchForCEP } from '../../../store/ducks/Dashboard/services';
import { RegisterProfessionalSchema } from '../../../validation/register';
import { InputCep, InputCellPhone, InputCPF } from '../../InputNumber';
import { CustomChip, SpecialtyWrapper, StyledButton } from './styles';

interface IRegisterProfessionalProps {
  onRegisterNewProfessional: (...args: any[]) => any;
  isLoading: boolean;
  isEdit?: boolean;
  professionalData?: any;
}

const RegisterProfessionalForm = ({
  onRegisterNewProfessional,
  isLoading,
  isEdit,
  professionalData
}: IRegisterProfessionalProps) => {
  const [specialtiesSelected, setSpecialtiesSelected] = useState<any>([]);
  const [cepValue, setCepValue] = useState<string>('');
  const [cepData, setCepData] = useState<any>({
    cep: '',
    street: '',
    neighborhood: '',
    city: '',
    state: '',
    country: ''
  });
  const [debouncedText] = useDebounce(cepValue, 500);

  const handleChangeSpecialty = (event: any) => {
    setSpecialtiesSelected(event.target.value);
  };

  const handleDelete = (chipToDelete: any) => () => {
    setSpecialtiesSelected((chips: any) =>
      chips.filter((chip: any) => chip !== chipToDelete)
    );
  };

  const handleCEPChange = (event: any) => {
    setCepValue(event?.target.value);
  };

  const ShowModal = (type?: string) => {
    if (type === 'error') {
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Não foi possível encontrar o CEP solicitado!'
      });
    }
    Swal.fire({
      title: 'Buscando CEP',
      showCancelButton: false,
      showConfirmButton: false,
      didOpen: () => {
        Swal.showLoading();
      }
    });
  };

  const fetchDebounceCEP = async (search: string) => {
    const response = await SearchForCEP(search);
    const { data, hasError } = response;

    if (hasError) ShowModal('error');
    setCepData({
      cep: data.cep,
      street: data.street,
      neighborhood: data.neighborhood,
      city: data.city,
      state: data.state,
      country: 'Brasil'
    });
    Swal.close();
  };

  useEffect(() => {
    if (isEdit) {
      setSpecialtiesSelected(professionalData.specialist);
      setCepValue(professionalData.address.cep);
      setCepData({
        cep: professionalData.address.cep,
        street: professionalData.address.street,
        neighborhood: professionalData.address.neighborhood,
        city: professionalData.address.city,
        state: professionalData.address.state,
        country: professionalData.address.country
      });
    }
  }, [isEdit, professionalData]);

  useEffect(() => {
    if (debouncedText) {
      ShowModal();
      fetchDebounceCEP(debouncedText);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [debouncedText]);

  return (
    <Formik
      initialValues={{
        name: professionalData?.name || '',
        birthdate: professionalData?.birthdate
          ? professionalData?.birthdate.split('T')[0]
          : '',
        cpf: professionalData?.cpf || '',
        rg: professionalData?.rg || '',
        phone: professionalData?.phone || '',
        gender: professionalData?.gender || '',
        email: professionalData?.email || '',
        CRO: professionalData?.CRO || '',
        specialist: professionalData?.specialist || '',
        cep: professionalData?.address.cep || '',
        street: professionalData?.street || '',
        number: professionalData?.address.number || '',
        neighborhood: professionalData?.neighborhood || '',
        complement: professionalData?.complement || '',
        city: professionalData?.address.city || '',
        state: professionalData?.address.state || '',
        country: professionalData?.address.country || ''
      }}
      validationSchema={RegisterProfessionalSchema}
      onSubmit={(values: any, { setSubmitting }: any) => {
        setSubmitting(false);
        const payload = { ...values };
        payload.address = {
          cep: cepValue,
          street: cepData.street,
          neighborhood: cepData.neighborhood,
          city: cepData.city,
          state: cepData.state,
          country: cepData.country,
          number: values.number,
          ...(values.complement && { complement: values.complement })
        };
        payload.specialist = specialtiesSelected;
        payload.password = '123456';
        delete payload.number;
        delete payload.complement;

        for (const key in payload) {
          if (payload[key] === '') {
            delete payload[key];
          }
        }

        onRegisterNewProfessional({ ...payload });
      }}
    >
      {({
        isValid,
        values,
        errors,
        dirty,
        touched,
        handleBlur,
        handleChange,
        handleSubmit,
        setFieldValue
      }) => (
        <form
          onSubmit={(value: any) =>
            handleSubmit(value as FormEvent<HTMLFormElement>)
          }
        >
          <Grid container spacing={3}>
            <Grid item xs={12}>
              <TextField
                fullWidth
                size="small"
                label="Nome Completo *"
                name="name"
                type="text"
                variant="outlined"
                onBlur={handleBlur}
                value={values.name}
                onChange={handleChange}
                helperText={errors.name && touched.name && errors.name}
                error={Boolean(errors.name && touched.name)}
              />
            </Grid>
            <Grid item xs={4}>
              <TextField
                fullWidth
                size="small"
                type="date"
                name="birthdate"
                label="Data de Nascimento *"
                placeholder="Digite sua data de nascimento"
                variant="outlined"
                InputLabelProps={{
                  shrink: true
                }}
                onBlur={handleBlur}
                value={values.birthdate}
                onChange={(event: any) => {
                  setFieldValue('birthdate', event.target.value);
                }}
                helperText={
                  errors.birthdate && touched.birthdate && errors.birthdate
                }
                error={Boolean(errors.birthdate && touched.birthdate)}
              />
            </Grid>
            <Grid item xs={4}>
              <InputCPF
                fullWidth
                size="small"
                label="CPF *"
                name="cpf"
                type="text"
                variant="outlined"
                onBlur={handleBlur}
                value={values.cpf}
                onChange={handleChange}
                helperText={errors.cpf && touched.cpf && errors.cpf}
                error={Boolean(errors.cpf && touched.cpf)}
              />
            </Grid>
            <Grid item xs={4}>
              <TextField
                fullWidth
                size="small"
                label="RG *"
                name="rg"
                type="text"
                variant="outlined"
                onBlur={handleBlur}
                value={values.rg}
                onChange={handleChange}
                helperText={errors.rg && touched.rg && errors.rg}
                error={Boolean(errors.rg && touched.rg)}
              />
            </Grid>

            <Grid item xs={4}>
              <InputCellPhone
                fullWidth
                size="small"
                label="Telefone *"
                name="phone"
                type="text"
                variant="outlined"
                onBlur={handleBlur}
                value={values.phone}
                onChange={handleChange}
                helperText={errors.phone && touched.phone && errors.phone}
                error={Boolean(errors.phone && touched.phone)}
              />
            </Grid>
            <Grid item xs={4}>
              <FormControl variant="outlined" fullWidth size="small">
                <InputLabel id="demo-simple-select-outlined-label">
                  Gênero *
                </InputLabel>
                <Select
                  labelId="demo-simple-select-outlined-label"
                  id="demo-simple-select-outlined"
                  label="Gênero *"
                  name="gender"
                  value={values.gender}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  error={Boolean(errors.gender && touched.gender)}
                >
                  {GENDER.map((element, key) => (
                    <MenuItem key={Number(key)} value={element}>
                      {element}
                    </MenuItem>
                  ))}
                </Select>
                {Boolean(errors.gender && touched.gender) && (
                  <FormHelperText>
                    {errors.gender && touched.gender && errors.gender}
                  </FormHelperText>
                )}
              </FormControl>
            </Grid>
            <Grid item xs={4}>
              <TextField
                fullWidth
                size="small"
                label="E-mail *"
                name="email"
                type="text"
                variant="outlined"
                onBlur={handleBlur}
                value={values.email}
                onChange={handleChange}
                helperText={errors.email && touched.email && errors.email}
                error={Boolean(errors.email && touched.email)}
              />
            </Grid>

            <Grid item xs={2}>
              <InputCep
                fullWidth
                size="small"
                label="CEP *"
                name="cep"
                type="text"
                variant="outlined"
                onBlur={handleBlur}
                value={cepValue}
                onChange={handleCEPChange}
                helperText={errors.cep && touched.cep && errors.cep}
                error={Boolean(errors.cep && touched.cep)}
              />
            </Grid>
            <Grid item xs={5}>
              <TextField
                fullWidth
                size="small"
                label="Rua *"
                name="street"
                type="text"
                variant="outlined"
                value={cepData.street}
                onChange={handleChange}
                InputProps={{
                  readOnly: true
                }}
                helperText={errors?.street && touched?.street && errors?.street}
                error={Boolean(errors?.street && touched?.street)}
              />
            </Grid>
            <Grid item xs={2}>
              <TextField
                fullWidth
                size="small"
                label="Numero *"
                name="number"
                type="text"
                variant="outlined"
                onBlur={handleBlur}
                value={values.number}
                onChange={handleChange}
                helperText={errors?.number && touched?.number && errors?.number}
                error={Boolean(errors?.number && touched?.number)}
              />
            </Grid>

            <Grid item xs={3}>
              <TextField
                fullWidth
                size="small"
                label="Complemento"
                name="complement"
                type="text"
                variant="outlined"
                onBlur={handleBlur}
                value={values.complement}
                onChange={handleChange}
                helperText={
                  errors?.complement &&
                  touched?.complement &&
                  errors?.complement
                }
                error={Boolean(errors?.complement && touched?.complement)}
              />
            </Grid>
            <Grid item xs={4}>
              <TextField
                fullWidth
                size="small"
                label="Cidade *"
                name="city"
                type="text"
                variant="outlined"
                value={cepData.city}
                onChange={handleChange}
                InputProps={{
                  readOnly: true
                }}
                helperText={errors?.city && touched?.city && errors?.city}
                error={Boolean(errors?.city && touched?.city)}
              />
            </Grid>
            <Grid item xs={4}>
              <TextField
                fullWidth
                size="small"
                label="Estado *"
                name="state"
                type="text"
                variant="outlined"
                value={cepData.state}
                onChange={handleChange}
                InputProps={{
                  readOnly: true
                }}
                helperText={errors?.state && touched?.state && errors?.state}
                error={Boolean(errors?.state && touched?.state)}
              />
            </Grid>
            <Grid item xs={4}>
              <TextField
                fullWidth
                size="small"
                label="Pais *"
                name="country"
                type="text"
                variant="outlined"
                value={cepData.country}
                onChange={handleChange}
                InputProps={{
                  readOnly: true
                }}
                helperText={
                  errors?.country && touched?.country && errors?.country
                }
                error={Boolean(errors?.country && touched?.country)}
              />
            </Grid>

            <Grid item xs={6}>
              <TextField
                fullWidth
                size="small"
                label="CRO *"
                name="CRO"
                type="text"
                variant="outlined"
                onBlur={handleBlur}
                value={values.CRO}
                onChange={handleChange}
                helperText={errors.CRO && touched.CRO && errors.CRO}
                error={Boolean(errors.CRO && touched.CRO)}
              />
            </Grid>
            <Grid item xs={6}>
              <FormControl variant="outlined" fullWidth size="small">
                <InputLabel id="demo-simple-select-outlined-label">
                  Especialidade *
                </InputLabel>
                <Select
                  labelId="demo-simple-select-outlined-label"
                  id="demo-simple-select-outlined"
                  label="Especialidade *"
                  multiple
                  name="specialist"
                  value={specialtiesSelected}
                  onChange={handleChangeSpecialty}
                  error={Boolean(errors.specialist && touched.specialist)}
                >
                  {Specialties.map((element, key) => (
                    <MenuItem key={Number(key)} value={element}>
                      {element}
                    </MenuItem>
                  ))}
                </Select>
                {Boolean(errors.specialist && touched.specialist) && (
                  <FormHelperText>
                    {errors.specialist &&
                      touched.specialist &&
                      errors.specialist}
                  </FormHelperText>
                )}
              </FormControl>
            </Grid>
          </Grid>
          {specialtiesSelected.length > 0 && (
            <Grid
              container
              justify="flex-end"
              alignContent="flex-end"
              alignItems="flex-end"
            >
              <SpecialtyWrapper>
                <span>Especialidades seleciondas:</span>
                {specialtiesSelected.map((data: any, key: any) => {
                  return (
                    <CustomChip
                      key={Number(key)}
                      variant="outlined"
                      label={data}
                      onDelete={handleDelete(data)}
                    />
                  );
                })}
              </SpecialtyWrapper>
            </Grid>
          )}
          <Grid container spacing={0}>
            <Alert className="no-background" severity="warning">
              * Itens obrigatórios
            </Alert>
          </Grid>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <StyledButton
                variant="contained"
                color="primary"
                fullWidth
                type="submit"
                disabled={
                  !(isValid && dirty) && !(specialtiesSelected.length > 0)
                }
              >
                {isLoading ? (
                  <CircularProgress size={14} />
                ) : (
                  <>
                    {isEdit
                      ? 'Editar profissional'
                      : 'Cadastrar novo profissional'}
                    <BsPersonPlusFill />
                  </>
                )}
              </StyledButton>
            </Grid>
          </Grid>
        </form>
      )}
    </Formik>
  );
};

RegisterProfessionalForm.defaultProps = {
  isEdit: false,
  professionalData: undefined
};

export default RegisterProfessionalForm;
