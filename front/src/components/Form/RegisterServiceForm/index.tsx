import React, { FormEvent } from 'react';

import { TextField, Grid, CircularProgress } from '@material-ui/core';
import { Alert } from '@material-ui/lab';
import { Formik } from 'formik';
import { BsPersonPlusFill } from 'react-icons/bs';

import { RegisterServiceSchema } from '../../../validation/register';
import { InputCellPhone } from '../../InputNumber';
import { StyledButton } from './styles';

interface IRegisterServiceProps {
  onClickSendButton: (...args: any[]) => any;
  isLoading: boolean;
  isEdit?: boolean;
  serviceData?: any;
}

const RegisterServiceForm = ({
  onClickSendButton,
  isLoading,
  isEdit,
  serviceData
}: IRegisterServiceProps) => {
  return (
    <Formik
      initialValues={{
        name: serviceData?.name || '',
        phone: serviceData?.phone || '',
        supply: serviceData?.supply || ''
      }}
      validationSchema={RegisterServiceSchema}
      onSubmit={(values: any, { setSubmitting }: any) => {
        setSubmitting(false);
        const payload = { ...values };

        onClickSendButton({ ...payload });
      }}
    >
      {({
        isValid,
        dirty,
        values,
        errors,
        touched,
        handleBlur,
        handleChange,
        handleSubmit,
        setFieldValue
      }) => (
        <form
          onSubmit={(value: any) =>
            handleSubmit(value as FormEvent<HTMLFormElement>)
          }
        >
          <Grid container spacing={3}>
            <Grid item xs={12}>
              <TextField
                fullWidth
                size="small"
                label="Nome do Forcenedor *"
                name="name"
                type="text"
                variant="outlined"
                onBlur={handleBlur}
                value={values.name}
                onChange={handleChange}
                helperText={errors.name && touched.name && errors.name}
                error={Boolean(errors.name && touched.name)}
              />
            </Grid>
            <Grid item xs={4}>
              <InputCellPhone
                fullWidth
                size="small"
                type="text"
                name="birthdate"
                label="Telefone do Fornecedor *"
                variant="outlined"
                onBlur={handleBlur}
                value={values.phone}
                onChange={(event: any) => {
                  setFieldValue('phone', event.target.value);
                }}
                helperText={errors.phone && touched.phone && errors.phone}
                error={Boolean(errors.phone && touched.phone)}
              />
            </Grid>
            <Grid item xs={4}>
              <TextField
                fullWidth
                size="small"
                label="Produto fornecido *"
                name="supply"
                type="text"
                variant="outlined"
                onBlur={handleBlur}
                value={values.supply}
                onChange={handleChange}
                helperText={errors.supply && touched.supply && errors.supply}
                error={Boolean(errors.supply && touched.supply)}
              />
            </Grid>
          </Grid>
          <Grid container spacing={0}>
            <Alert className="no-background" severity="warning">
              * Itens obrigatórios
            </Alert>
          </Grid>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <StyledButton
                variant="contained"
                color="primary"
                fullWidth
                type="submit"
                disabled={!(isValid && dirty)}
              >
                {isLoading ? (
                  <CircularProgress size={14} />
                ) : (
                  <>
                    {isEdit ? `Editar fornecedor` : `Cadastrar novo fornecedor`}{' '}
                    <BsPersonPlusFill />
                  </>
                )}
              </StyledButton>
            </Grid>
          </Grid>
        </form>
      )}
    </Formik>
  );
};

RegisterServiceForm.defaultProps = {
  isEdit: false,
  serviceData: undefined
};

export default RegisterServiceForm;
