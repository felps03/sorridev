import React, { FormEvent, useEffect, useState } from 'react';

import {
  TextField,
  Grid,
  Select,
  MenuItem,
  FormControl,
  InputLabel,
  FormHelperText,
  CircularProgress
} from '@material-ui/core';
import { Alert } from '@material-ui/lab';
import { Formik } from 'formik';
import { BsPersonPlusFill } from 'react-icons/bs';
import Swal from 'sweetalert2';
import { useDebounce } from 'use-debounce';

import { GENDER } from '../../../constants';
import { SearchForCEP } from '../../../store/ducks/Dashboard/services';
import { RegisterEmployeeSchema } from '../../../validation/register';
import { InputCellPhone, InputCep, InputCPF } from '../../InputNumber';
import { StyledButton } from './styles';

interface IRegisterEmployeeProps {
  onClickSendButton: (...args: any[]) => any;
  isLoading: boolean;
  isEdit?: boolean;
  employeeData?: any;
}

const RegisterEmployeeForm = ({
  onClickSendButton,
  isLoading,
  isEdit,
  employeeData
}: IRegisterEmployeeProps) => {
  const [cepValue, setCepValue] = useState<string>('');
  const [cepData, setCepData] = useState<any>({
    cep: '',
    street: '',
    neighborhood: '',
    city: '',
    state: '',
    country: ''
  });
  const [debouncedText] = useDebounce(cepValue, 500);

  const handleCEPChange = (event: any) => {
    setCepValue(event?.target.value);
  };

  const ShowModal = (type?: string) => {
    if (type === 'error') {
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Não foi possível encontrar o CEP solicitado!'
      });
    }
    Swal.fire({
      title: 'Buscando CEP',
      showCancelButton: false,
      showConfirmButton: false,
      didOpen: () => {
        Swal.showLoading();
      }
    });
  };

  const fetchDebounceCEP = async (search: string) => {
    const response = await SearchForCEP(search);
    const { data, hasError } = response;

    if (hasError) ShowModal('error');
    setCepData({
      cep: data.cep,
      street: data.street,
      neighborhood: data.neighborhood,
      city: data.city,
      state: data.state,
      country: 'Brasil'
    });
    Swal.close();
  };

  useEffect(() => {
    if (debouncedText) {
      ShowModal();
      fetchDebounceCEP(debouncedText);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [debouncedText]);

  useEffect(() => {
    if (isEdit) {
      setCepValue(employeeData.address.cep);
      setCepData({
        cep: employeeData.address.cep,
        street: employeeData.address.street,
        neighborhood: employeeData.address.neighborhood,
        city: employeeData.address.city,
        state: employeeData.address.state,
        country: employeeData.address.country
      });
    }
  }, [isEdit, employeeData]);

  return (
    <Formik
      initialValues={{
        name: employeeData?.name || '',
        birthdate: employeeData?.birthdate
          ? employeeData?.birthdate.split('T')[0]
          : '',
        cpf: employeeData?.cpf || '',
        rg: employeeData?.rg || '',
        phone: employeeData?.phone || '',
        gender: employeeData?.gender || '',
        email: employeeData?.email || '',
        cep: employeeData?.address.cep || '',
        street: employeeData?.street || '',
        number: employeeData?.address.number || '',
        neighborhood: employeeData?.neighborhood || '',
        complement: employeeData?.complement || '',
        city: employeeData?.address.city || '',
        state: employeeData?.address.state || '',
        country: employeeData?.address.country || ''
      }}
      validationSchema={RegisterEmployeeSchema}
      onSubmit={(values: any, { setSubmitting }: any) => {
        setSubmitting(false);

        const payload = { ...values };
        payload.address = {
          cep: cepValue,
          street: cepData.street,
          neighborhood: cepData.neighborhood,
          city: cepData.city,
          state: cepData.state,
          country: cepData.country,
          number: values.number,
          ...(values.complement && { complement: values.complement })
        };
        payload.password = '123456';
        const data = Object.fromEntries(
          Object.entries(payload).filter(([_, v]) => v !== '')
        );
        delete payload.complement;
        delete data.number;

        onClickSendButton({ ...data });
      }}
    >
      {({
        isValid,
        dirty,
        values,
        errors,
        touched,
        handleBlur,
        handleChange,
        handleSubmit,
        setFieldValue
      }) => (
        <form
          onSubmit={(value: any) =>
            handleSubmit(value as FormEvent<HTMLFormElement>)
          }
        >
          <Grid container spacing={3}>
            <Grid item xs={12}>
              <TextField
                // className="custom-input"
                fullWidth
                size="small"
                label="Nome Completo *"
                name="name"
                type="text"
                placeholder="Digite seu nome completo"
                variant="outlined"
                onBlur={handleBlur}
                value={values.name}
                onChange={handleChange}
                helperText={errors.name && touched.name && errors.name}
                error={Boolean(errors.name && touched.name)}
              />
            </Grid>
            <Grid item xs={4}>
              <TextField
                fullWidth
                size="small"
                type="date"
                name="birthdate"
                label="Data de Nascimento *"
                placeholder="Digite sua data de nascimento"
                variant="outlined"
                InputLabelProps={{
                  shrink: true
                }}
                onBlur={handleBlur}
                value={values.birthdate}
                onChange={(event: any) => {
                  setFieldValue('birthdate', event.target.value);
                }}
                helperText={
                  errors.birthdate && touched.birthdate && errors.birthdate
                }
                error={Boolean(errors.birthdate && touched.birthdate)}
              />
            </Grid>
            <Grid item xs={4}>
              <InputCPF
                // className="custom-input"
                fullWidth
                size="small"
                label="CPF *"
                name="cpf"
                type="text"
                placeholder="Digite seu nome completo"
                variant="outlined"
                onBlur={handleBlur}
                value={values.cpf}
                onChange={handleChange}
                helperText={errors.cpf && touched.cpf && errors.cpf}
                error={Boolean(errors.cpf && touched.cpf)}
              />
            </Grid>
            <Grid item xs={4}>
              <TextField
                // className="custom-input"
                fullWidth
                size="small"
                label="RG *"
                name="rg"
                type="text"
                placeholder="Digite seu nome completo"
                variant="outlined"
                onBlur={handleBlur}
                value={values.rg}
                onChange={handleChange}
                helperText={errors.rg && touched.rg && errors.rg}
                error={Boolean(errors.rg && touched.rg)}
              />
            </Grid>

            <Grid item xs={4}>
              <InputCellPhone
                // className="custom-input"
                fullWidth
                size="small"
                label="Telefone *"
                name="phone"
                type="text"
                placeholder="Digite seu nome completo"
                variant="outlined"
                onBlur={handleBlur}
                value={values.phone}
                onChange={handleChange}
                helperText={errors.phone && touched.phone && errors.phone}
                error={Boolean(errors.phone && touched.phone)}
              />
            </Grid>
            <Grid item xs={4}>
              <FormControl variant="outlined" fullWidth size="small">
                <InputLabel id="demo-simple-select-outlined-label">
                  Gênero *
                </InputLabel>
                <Select
                  labelId="demo-simple-select-outlined-label"
                  id="demo-simple-select-outlined"
                  label="Gênero *"
                  name="gender"
                  value={values.gender}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  error={Boolean(errors.gender && touched.gender)}
                >
                  {GENDER.map((element, key) => (
                    <MenuItem key={Number(key)} value={element}>
                      {element}
                    </MenuItem>
                  ))}
                </Select>
                {Boolean(errors.gender && touched.gender) && (
                  <FormHelperText>
                    {errors.gender && touched.gender && errors.gender}
                  </FormHelperText>
                )}
              </FormControl>
            </Grid>
            <Grid item xs={4}>
              <TextField
                // className="custom-input"
                fullWidth
                size="small"
                label="E-mail *"
                name="email"
                type="text"
                placeholder="Digite seu nome completo"
                variant="outlined"
                onBlur={handleBlur}
                value={values.email}
                onChange={handleChange}
                helperText={errors.email && touched.email && errors.email}
                error={Boolean(errors.email && touched.email)}
              />
            </Grid>

            <Grid item xs={2}>
              <InputCep
                fullWidth
                size="small"
                label="CEP *"
                name="cep"
                type="text"
                variant="outlined"
                onBlur={handleBlur}
                value={cepValue}
                onChange={handleCEPChange}
                helperText={errors.cep && touched.cep && errors.cep}
                error={Boolean(errors.cep && touched.cep)}
              />
            </Grid>
            <Grid item xs={5}>
              <TextField
                // className="custom-input"

                fullWidth
                size="small"
                label="Rua *"
                name="street"
                type="text"
                placeholder="Digite seu nome completo"
                variant="outlined"
                value={cepData.street}
                onChange={handleChange}
                InputProps={{
                  readOnly: true
                }}
                helperText={errors?.street && touched?.street && errors?.street}
                error={Boolean(errors?.street && touched?.street)}
              />
            </Grid>
            <Grid item xs={2}>
              <TextField
                // className="custom-input"
                fullWidth
                size="small"
                label="Numero *"
                name="number"
                type="text"
                placeholder="Digite seu nome completo"
                variant="outlined"
                onBlur={handleBlur}
                value={values.number}
                onChange={handleChange}
                helperText={errors?.number && touched?.number && errors?.number}
                error={Boolean(errors?.number && touched?.number)}
              />
            </Grid>

            <Grid item xs={3}>
              <TextField
                // className="custom-input"
                fullWidth
                size="small"
                label="Complemento"
                name="complement"
                type="text"
                placeholder="Digite seu nome completo"
                variant="outlined"
                onBlur={handleBlur}
                value={values.complement}
                onChange={handleChange}
                helperText={
                  errors?.complement &&
                  touched?.complement &&
                  errors?.complement
                }
                error={Boolean(errors?.complement && touched?.complement)}
              />
            </Grid>
            <Grid item xs={4}>
              <TextField
                // className="custom-input"
                fullWidth
                size="small"
                label="Cidade *"
                name="city"
                type="text"
                placeholder="Digite seu nome completo"
                variant="outlined"
                value={cepData.city}
                onChange={handleChange}
                InputProps={{
                  readOnly: true
                }}
                helperText={errors?.city && touched?.city && errors?.city}
                error={Boolean(errors?.city && touched?.city)}
              />
            </Grid>
            <Grid item xs={4}>
              <TextField
                // className="custom-input"
                fullWidth
                size="small"
                label="Estado *"
                name="state"
                type="text"
                placeholder="Digite seu nome completo"
                variant="outlined"
                value={cepData.state}
                onChange={handleChange}
                InputProps={{
                  readOnly: true
                }}
                helperText={errors?.state && touched?.state && errors?.state}
                error={Boolean(errors?.state && touched?.state)}
              />
            </Grid>
            <Grid item xs={4}>
              <TextField
                // className="custom-input"
                fullWidth
                size="small"
                label="Pais *"
                name="country"
                type="text"
                placeholder="Digite seu nome completo"
                variant="outlined"
                value={cepData.country}
                onChange={handleChange}
                InputProps={{
                  readOnly: true
                }}
                helperText={
                  errors?.country && touched?.country && errors?.country
                }
                error={Boolean(errors?.country && touched?.country)}
              />
            </Grid>
          </Grid>
          <Grid container spacing={0}>
            <Alert className="no-background" severity="warning">
              * Itens obrigatórios
            </Alert>
          </Grid>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <StyledButton
                variant="contained"
                color="primary"
                fullWidth
                type="submit"
                disabled={!(isValid && dirty)}
              >
                {isLoading ? (
                  <CircularProgress size={14} />
                ) : (
                  <>
                    Cadastrar novo funcionário <BsPersonPlusFill />
                  </>
                )}
              </StyledButton>
            </Grid>
          </Grid>
        </form>
      )}
    </Formik>
  );
};

RegisterEmployeeForm.defaultProps = {
  isEdit: false,
  employeeData: undefined
};

export default RegisterEmployeeForm;
