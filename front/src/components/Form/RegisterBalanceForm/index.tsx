import React, { FormEvent } from 'react';

import { TextField, Grid, CircularProgress } from '@material-ui/core';
import { Alert } from '@material-ui/lab';
import { Formik } from 'formik';
import { BsPersonPlusFill } from 'react-icons/bs';

import { RegisterBalanceSchema } from '../../../validation/register';
import { StyledButton } from './styles';

interface IRegisterBalanceProps {
  onClickSendButton: (...args: any[]) => any;
  isLoading: boolean;
  isEdit?: boolean;
  balanceData?: any;
}

const RegisterBalanceForm = ({
  onClickSendButton,
  isLoading,
  isEdit,
  balanceData
}: IRegisterBalanceProps) => {
  return (
    <Formik
      initialValues={{
        cash_accounting: balanceData?.cash_accounting || '',
        spending: balanceData?.spending || '',
        number_of_appointments: balanceData?.number_of_appointments || ''
      }}
      validationSchema={RegisterBalanceSchema}
      onSubmit={(values: any, { setSubmitting }: any) => {
        setSubmitting(false);
        const payload = { ...values };

        onClickSendButton({ ...payload });
      }}
    >
      {({
        isValid,
        dirty,
        values,
        errors,
        touched,
        handleBlur,
        handleChange,
        handleSubmit,
        setFieldValue
      }) => (
        <form
          onSubmit={(value: any) =>
            handleSubmit(value as FormEvent<HTMLFormElement>)
          }
        >
          <Grid container spacing={3}>
            <Grid item xs={12}>
              <TextField
                fullWidth
                size="small"
                label="Caixa *"
                name="cash_accounting"
                type="text"
                variant="outlined"
                onBlur={handleBlur}
                value={values.cash_accounting}
                onChange={handleChange}
                helperText={
                  errors.cash_accounting &&
                  touched.cash_accounting &&
                  errors.cash_accounting
                }
                error={Boolean(
                  errors.cash_accounting && touched.cash_accounting
                )}
              />
            </Grid>
            <Grid item xs={4}>
              <TextField
                fullWidth
                size="small"
                type="text"
                name="spending"
                label="Gasto *"
                variant="outlined"
                onBlur={handleBlur}
                value={values.spending}
                onChange={(event: any) => {
                  setFieldValue('spending', event.target.value);
                }}
                helperText={
                  errors.spending && touched.spending && errors.spending
                }
                error={Boolean(errors.spending && touched.spending)}
              />
            </Grid>
            <Grid item xs={4}>
              <TextField
                fullWidth
                size="small"
                label="Numero de Atendimentos *"
                name="number_of_appointments"
                type="text"
                variant="outlined"
                onBlur={handleBlur}
                value={values.number_of_appointments}
                onChange={handleChange}
                helperText={
                  errors.number_of_appointments &&
                  touched.number_of_appointments &&
                  errors.number_of_appointments
                }
                error={Boolean(
                  errors.number_of_appointments &&
                    touched.number_of_appointments
                )}
              />
            </Grid>
          </Grid>
          <Grid container spacing={0}>
            <Alert className="no-background" severity="warning">
              * Itens obrigatórios
            </Alert>
          </Grid>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <StyledButton
                variant="contained"
                color="primary"
                fullWidth
                type="submit"
                disabled={!(isValid && dirty)}
              >
                {isLoading ? (
                  <CircularProgress size={14} />
                ) : (
                  <>
                    {isEdit
                      ? 'Editar movimentação'
                      : 'Lançar nova movimentação'}{' '}
                    <BsPersonPlusFill />
                  </>
                )}
              </StyledButton>
            </Grid>
          </Grid>
        </form>
      )}
    </Formik>
  );
};

RegisterBalanceForm.defaultProps = {
  isEdit: false,
  balanceData: undefined
};

export default RegisterBalanceForm;
