import React, { FormEvent } from 'react';

import { TextField, Grid, CircularProgress } from '@material-ui/core';
import { Alert } from '@material-ui/lab';
import { Formik } from 'formik';
import { BsPersonPlusFill } from 'react-icons/bs';

import { RegisterStockSchema } from '../../../validation/register';
import { StyledButton } from './styles';

interface IRegisterServiceProps {
  onClickSendButton: (...args: any[]) => any;
  isLoading: boolean;
  isEdit?: boolean;
  stockData?: any;
}

const RegisterStockForm = ({
  onClickSendButton,
  isLoading,
  isEdit,
  stockData
}: IRegisterServiceProps) => {
  return (
    <Formik
      initialValues={{
        product: stockData?.product || '',
        price: stockData?.price || '',
        quantity: stockData?.quantity || '',
        provider: stockData?.provider || ''
      }}
      validationSchema={RegisterStockSchema}
      onSubmit={(values: any, { setSubmitting }: any) => {
        setSubmitting(false);
        const payload = { ...values };

        onClickSendButton({ ...payload });
      }}
    >
      {({
        isValid,
        dirty,
        values,
        errors,
        touched,
        handleBlur,
        handleChange,
        handleSubmit,
        setFieldValue
      }) => (
        <form
          onSubmit={(value: any) =>
            handleSubmit(value as FormEvent<HTMLFormElement>)
          }
        >
          <Grid container spacing={3}>
            <Grid item xs={12}>
              <TextField
                fullWidth
                size="small"
                label="Nome do Produto *"
                name="product"
                type="text"
                variant="outlined"
                onBlur={handleBlur}
                value={values.product}
                onChange={handleChange}
                helperText={errors.product && touched.product && errors.product}
                error={Boolean(errors.product && touched.product)}
              />
            </Grid>
            <Grid item xs={4}>
              <TextField
                fullWidth
                size="small"
                type="text"
                name="price"
                label="Preço do produto *"
                variant="outlined"
                onBlur={handleBlur}
                value={values.price}
                onChange={(event: any) => {
                  setFieldValue('price', event.target.value);
                }}
                helperText={errors.price && touched.price && errors.price}
                error={Boolean(errors.price && touched.price)}
              />
            </Grid>
            <Grid item xs={4}>
              <TextField
                fullWidth
                size="small"
                label="Quantidade *"
                name="quantity"
                type="text"
                variant="outlined"
                onBlur={handleBlur}
                value={values.quantity}
                onChange={handleChange}
                helperText={
                  errors.quantity && touched.quantity && errors.quantity
                }
                error={Boolean(errors.quantity && touched.quantity)}
              />
            </Grid>
            <Grid item xs={4}>
              <TextField
                fullWidth
                size="small"
                label="Fornecedor *"
                name="provider"
                type="text"
                variant="outlined"
                onBlur={handleBlur}
                value={values.provider}
                onChange={handleChange}
                helperText={
                  errors.provider && touched.provider && errors.provider
                }
                error={Boolean(errors.provider && touched.provider)}
              />
            </Grid>
          </Grid>
          <Grid container spacing={0}>
            <Alert className="no-background" severity="warning">
              * Itens obrigatórios
            </Alert>
          </Grid>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <StyledButton
                variant="contained"
                color="primary"
                fullWidth
                type="submit"
                disabled={!(isValid && dirty)}
              >
                {isLoading ? (
                  <CircularProgress size={14} />
                ) : (
                  <>
                    {isEdit ? 'Editar stock' : 'Cadastrar novo item'}{' '}
                    <BsPersonPlusFill />
                  </>
                )}
              </StyledButton>
            </Grid>
          </Grid>
        </form>
      )}
    </Formik>
  );
};
RegisterStockForm.defaultProps = {
  isEdit: false,
  stockData: undefined
};
export default RegisterStockForm;
