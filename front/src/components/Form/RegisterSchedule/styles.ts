import { Button, Chip } from '@material-ui/core';
import styled from 'styled-components';

const CardHeaderForm = styled.section`
  padding: 1.25rem 1.5rem;
  margin-bottom: 0;
  background-color: #fff;
  border-bottom: 1px solid rgba(0, 0, 0, 0.05);
`;

const CustomChip = styled(Chip)`
  border: 1px solid ${(props) => props.theme.colors.primary};
  margin: 2px;
  height: 28px;
  background-color: transparent;
  > span {
    color: ${(props) => props.theme.colors.primary};
  }
  > svg {
    color: ${(props) => props.theme.colors.primary};
  }
`;
const SpecialtyWrapper = styled.section`
  padding-bottom: 10px;
  > span {
    font-size: ${(props) => props.theme.font.size.normal};
    margin-right: 10px;
  }
`;

interface Props {
  isEditing: boolean;
}
const StyledButton = styled(Button)<Props>`
  && {
    height: 40px;
    float: right;
    margin-top: 1rem;
    margin-bottom: 1rem;
    max-width: 300px;
    border: none;
    padding-right: 10px;
    box-shadow: 0px 14px 9px -15px rgba(0, 0, 0, 0.25);
    border-radius: 8px;
    background-color: ${(props) => props.theme.colors.primary};
    font-size: ${(props) => props.theme.font.size.small};
    color: #fff;
    font-weight: ${(props) => props.theme.font.weight.regular};
    cursor: pointer;
    text-transform: capitalize;
    transition: all 0.2s ease-in;
    > span > svg {
      margin-left: 10px;
    }
    &:hover {
      background-color: rgba(255, 177, 103, 0.8);
      transform: translateY(-3px);
    }
  }
`;

const DeleteButton = styled(Button)`
  && {
    height: 40px;
    float: right;
    margin-top: 1rem;
    margin-bottom: 1rem;
    max-width: 250px;
    padding-right: 10px;
    box-shadow: none;
    border-radius: 8px;
    background-color: transparent;
    border: 1px solid ${(props) => props.theme.colors.primary};
    font-size: ${(props) => props.theme.font.size.normal};
    color: ${(props) => props.theme.colors.primary};
    font-weight: ${(props) => props.theme.font.weight.regular};
    cursor: pointer;
    text-transform: capitalize;
    transition: all 0.2s ease-in;
    > span > svg {
      margin-left: 10px;
    }
    &:hover {
      background-color: transparent;
      border: 1px solid ${(props) => props.theme.colors.primary};
      box-shadow: none;
    }
  }
`;

export {
  CardHeaderForm,
  CustomChip,
  SpecialtyWrapper,
  StyledButton,
  DeleteButton
};
