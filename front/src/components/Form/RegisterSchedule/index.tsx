import React, { FormEvent, useCallback, useEffect, useState } from 'react';

import {
  TextField,
  Grid,
  CircularProgress,
  RadioGroup,
  FormControl,
  FormControlLabel,
  FormLabel,
  Radio,
  Tooltip
} from '@material-ui/core';
import { Formik } from 'formik';
import moment from 'moment';
import { BsPersonPlusFill } from 'react-icons/bs';
import { useSelector } from 'react-redux';
import Swal from 'sweetalert2';
import { useDebouncedCallback } from 'use-debounce/lib';

import { appApi } from '../../../apis/app-api';
import { ROLES } from '../../../constants';
import { RootState } from '../../../store/configure-store';
import { RegisterScheduleSchema } from '../../../validation/register';
import { InputCPF } from '../../InputNumber';
import { StyledButton, DeleteButton } from './styles';

interface IRegisterScheduleProps {
  onClickSendButton: (...args: any[]) => any;
  onDelete: () => any;
  isLoading: boolean;
  isEditing: boolean;
  dataEdit?: any;
  rangeToEdit?: any;
}

const RegisterSchedule = ({
  onClickSendButton,
  isLoading,
  onDelete,
  dataEdit,
  rangeToEdit,
  isEditing
}: IRegisterScheduleProps) => {
  const [cpfSearch, setSearch] = useState<string>('');
  const [userName, setUserName] = useState<string>('');
  const [userID, setUserID] = useState<string>('');
  const user = useSelector((state: RootState) => state.user);
  const isUser = user.role === ROLES.USER;
  const debounced = useDebouncedCallback((value) => {
    setSearch(value);
  }, 1000);

  const fetchUser = useCallback(async () => {
    const result = await appApi().get(`/user?search=${cpfSearch}`);
    const { data } = result;
    if (result.status === 200) {
      setUserName(data.docs[0].name);
      setUserID(data.docs[0]._id);
    }
    if (result.status === 204) {
      setUserName('');
      Swal.fire('Erro :(', 'O CPF buscado não foi encontrado!', 'error');
    }
  }, [cpfSearch]);

  useEffect(() => {
    if (dataEdit?.client.cpf) {
      setSearch(dataEdit?.client.cpf);
    }
    if (cpfSearch && cpfSearch.length === 14) {
      fetchUser();
    }
  }, [cpfSearch, fetchUser, dataEdit]);
  return (
    <Formik
      initialValues={{
        cpf: dataEdit ? dataEdit?.client.cpf : '',
        name: dataEdit ? dataEdit?.client.name : '',
        health_insurance_name: dataEdit ? dataEdit?.health_insurance_name : '',
        hasReturn: dataEdit ? dataEdit?.hasReturn.toString() : '',
        observation: dataEdit ? dataEdit?.observation : '',
        value: dataEdit ? dataEdit?.value : '',
        dateStart: moment(rangeToEdit?.start).format('YYYY-MM-DD') || '',
        timeStart: moment(rangeToEdit?.start).format('HH:mm') || '',
        dateEnd: moment(rangeToEdit?.end).format('YYYY-MM-DD') || '',
        timeEnd: moment(rangeToEdit?.end).format('HH:mm') || '',
        status: dataEdit ? dataEdit?.status : ''
      }}
      validationSchema={RegisterScheduleSchema}
      onSubmit={(values: any, { setSubmitting }: any) => {
        setSubmitting(false);
        const payload = { ...values };
        payload.name = dataEdit?.client.name || userName;
        payload.hasReturn = payload.hasReturn === 'true';
        payload.client = userID || dataEdit._id;

        if (!isEditing) delete payload.status;

        onClickSendButton({ ...payload });
      }}
    >
      {({
        isValid,
        dirty,
        values,
        errors,
        touched,
        handleBlur,
        handleChange,
        handleSubmit,
        setFieldValue
      }) => (
        <form
          onSubmit={(value: any) =>
            handleSubmit(value as FormEvent<HTMLFormElement>)
          }
        >
          <Grid container spacing={3}>
            <Grid item xs={6}>
              <TextField
                fullWidth
                disabled={isUser}
                size="small"
                type="date"
                name="dateStart"
                label="Data da Consulta *"
                variant="outlined"
                InputLabelProps={{
                  shrink: true
                }}
                onBlur={handleBlur}
                value={values.dateStart}
                onChange={(event: any) => {
                  setFieldValue('dateStart', event.target.value);
                }}
                helperText={
                  errors.dateStart && touched.dateStart && errors.dateStart
                }
                error={Boolean(errors.dateStart && touched.dateStart)}
              />
            </Grid>
            <Grid item xs={3}>
              <TextField
                fullWidth
                disabled={isUser}
                size="small"
                type="time"
                name="timeStart"
                label="Horário de *"
                variant="outlined"
                InputLabelProps={{
                  shrink: true
                }}
                onBlur={handleBlur}
                value={values.timeStart}
                onChange={(event: any) => {
                  setFieldValue('timeStart', event.target.value);
                }}
                helperText={
                  errors.timeStart && touched.timeStart && errors.timeStart
                }
                error={Boolean(errors.timeStart && touched.timeStart)}
              />
            </Grid>
            <Grid item xs={3}>
              <TextField
                fullWidth
                disabled={isUser}
                size="small"
                type="time"
                name="timeEnd"
                label="Até *"
                variant="outlined"
                InputLabelProps={{
                  shrink: true
                }}
                onBlur={handleBlur}
                value={values.timeEnd}
                onChange={(event: any) => {
                  setFieldValue('timeEnd', event.target.value);
                }}
                helperText={errors.timeEnd && touched.timeEnd && errors.timeEnd}
                error={Boolean(errors.timeEnd && touched.timeEnd)}
              />
            </Grid>
            <Grid item xs={6}>
              <InputCPF
                fullWidth
                disabled={isUser}
                size="small"
                label="CPF *"
                name="cpf"
                type="text"
                variant="outlined"
                onBlur={handleBlur}
                value={values.cpf}
                onChange={(event: any) => {
                  setFieldValue('cpf', event.target.value);
                  debounced.callback(event.target.value);
                }}
                helperText={errors.cpf && touched.cpf && errors.cpf}
                error={Boolean(errors.cpf && touched.cpf)}
              />
            </Grid>
            <Grid item xs={6}>
              <Tooltip title="Este valor é preenchido buscando-se pelo CPF">
                <TextField
                  fullWidth
                  disabled={isUser}
                  size="small"
                  label="Nome do usuário *"
                  name="name"
                  type="text"
                  variant="outlined"
                  InputProps={{
                    readOnly: true
                  }}
                  onBlur={handleBlur}
                  value={userName}
                  onChange={() => {
                    setFieldValue('name', userName);
                  }}
                  helperText={errors.name && touched.name && errors.name}
                  error={Boolean(errors.name && touched.name)}
                />
              </Tooltip>
            </Grid>
            <Grid item xs={6}>
              <TextField
                fullWidth
                disabled={isUser}
                size="small"
                label="Convênio"
                name="health_insurance_name"
                type="text"
                variant="outlined"
                onBlur={handleBlur}
                value={values.health_insurance_name}
                onChange={handleChange}
                helperText={
                  errors.health_insurance_name &&
                  touched.health_insurance_name &&
                  errors.health_insurance_name
                }
                error={Boolean(
                  errors.health_insurance_name && touched.health_insurance_name
                )}
              />
            </Grid>
            <Grid item xs={6}>
              <TextField
                fullWidth
                disabled={isUser}
                size="small"
                label="Valor *"
                name="value"
                type="text"
                variant="outlined"
                onBlur={handleBlur}
                value={values.value}
                onChange={handleChange}
                helperText={errors.value && touched.value && errors.value}
                error={Boolean(errors.value && touched.value)}
              />
            </Grid>

            <Grid item xs={12}>
              <TextField
                fullWidth
                disabled={isUser}
                size="small"
                label="Observações"
                name="observation"
                type="text"
                variant="outlined"
                onBlur={handleBlur}
                value={values.observation}
                onChange={handleChange}
                helperText={
                  errors.observation &&
                  touched.observation &&
                  errors.observation
                }
                error={Boolean(errors.observation && touched.observation)}
              />
            </Grid>
            {!isUser && (
              <Grid item xs={6}>
                <FormControl component="fieldset">
                  <FormLabel component="legend">Há retorno?</FormLabel>
                  <RadioGroup
                    row
                    aria-label="hasReturn"
                    name="hasReturn"
                    value={values.hasReturn}
                    onChange={(e) => {
                      setFieldValue('hasReturn', e.target.value);
                    }}
                  >
                    <FormControlLabel
                      value="true"
                      control={<Radio />}
                      label="Sim"
                    />
                    <FormControlLabel
                      value="false"
                      control={<Radio />}
                      label="Não"
                    />
                  </RadioGroup>
                </FormControl>
              </Grid>
            )}
            {isEditing && !isUser && (
              <Grid item xs={6}>
                <FormControl component="fieldset">
                  <FormLabel component="legend">Paciente Compareceu?</FormLabel>
                  <RadioGroup
                    row
                    aria-label="status"
                    name="status"
                    value={values.status}
                    onChange={(e) => {
                      setFieldValue('status', e.target.value);
                    }}
                  >
                    <FormControlLabel
                      value="compareceu"
                      control={<Radio />}
                      label="Sim"
                    />
                    <FormControlLabel
                      value="ausente"
                      control={<Radio />}
                      label="Não"
                    />
                  </RadioGroup>
                </FormControl>
              </Grid>
            )}
          </Grid>
          {!isUser && (
            <Grid container spacing={2} direction="row-reverse">
              <Grid item xs={6}>
                <StyledButton
                  variant="contained"
                  color="primary"
                  fullWidth
                  type="submit"
                  isEditing
                  disabled={!(isValid && dirty) || userName === ''}
                >
                  {isLoading ? (
                    <CircularProgress size={14} />
                  ) : (
                    <>
                      {isEditing
                        ? 'Editar agendamento'
                        : 'Cadastrar novo agendamento'}{' '}
                      <BsPersonPlusFill />
                    </>
                  )}
                </StyledButton>
              </Grid>
              <Grid item xs={6}>
                {isEditing && (
                  <DeleteButton
                    onClick={onDelete}
                    variant="contained"
                    fullWidth
                    type="button"
                  >
                    {isLoading ? (
                      <CircularProgress size={14} />
                    ) : (
                      <>Cancelar agendamento</>
                    )}
                  </DeleteButton>
                )}
              </Grid>
            </Grid>
          )}
        </form>
      )}
    </Formik>
  );
};
RegisterSchedule.defaultProps = {
  dataEdit: undefined,
  rangeToEdit: undefined
};
export default RegisterSchedule;
