import React, { FormEvent, useState } from 'react';

import {
  TextField,
  Grid,
  CircularProgress,
  InputAdornment,
  IconButton
} from '@material-ui/core';
import { Visibility, VisibilityOff } from '@material-ui/icons';
import { Alert } from '@material-ui/lab';
import { Formik } from 'formik';
import { BsPersonPlusFill } from 'react-icons/bs';

import { validationPasswordSchema } from '../../../validation/register';
import { StyledButton } from './styles';

interface IResetPasswordFrom {
  onClickSendButton: (...args: any[]) => any;
  isLoading: boolean;
}

interface Values {
  currentPassword: string;
  password: string;
  confirmPassword: string;
}

const ResetPasswordFrom = ({
  onClickSendButton,
  isLoading
}: IResetPasswordFrom) => {
  const [showPasswordCurrent, setShowPasswordCurrent] = useState<boolean>(
    false
  );
  const [showPassword, setShowPassword] = useState<boolean>(false);
  const [showConfirmPassword, setShowConfirmPassword] = useState<boolean>(
    false
  );

  const handleClickShowCurrentPassword = () => {
    setShowPasswordCurrent(!showPasswordCurrent);
  };

  const handleMouseDownPassword = (event: any) => {
    event.preventDefault();
  };
  const handleMouseDownCurrentPassword = (event: any) => {
    event.preventDefault();
  };
  const handleMouseDownConfirmPassword = (event: any) => {
    event.preventDefault();
  };

  const handleClickShowPassword = () => {
    setShowPassword(!showPassword);
  };

  const handleClickShowConfirmPassword = () => {
    setShowConfirmPassword(!showConfirmPassword);
  };

  const fullSizeChangePasswordHandler = (props: Values) => {
    // eslint-disable-next-line react/prop-types
    const { currentPassword, password, confirmPassword } = props;

    const diffPass = !(password === confirmPassword);
    const emptyValues = !(password && currentPassword && confirmPassword);

    return diffPass || emptyValues;
  };

  return (
    <Formik
      initialValues={{
        currentPassword: '',
        password: '',
        confirmPassword: ''
      }}
      validationSchema={validationPasswordSchema}
      onSubmit={(values: any, { setSubmitting, resetForm }: any) => {
        setSubmitting(false);
        const payload = { ...values };
        resetForm();
        onClickSendButton({ ...payload });
      }}
    >
      {({
        values,
        errors,
        touched,
        handleBlur,
        handleChange,
        handleSubmit,
        setFieldValue
      }) => (
        <form
          onSubmit={(value: any) =>
            handleSubmit(value as FormEvent<HTMLFormElement>)
          }
        >
          <Grid container spacing={2}>
            <Grid item xs={4}>
              <TextField
                fullWidth
                size="small"
                label="Senha Atual *"
                name="currentPassword"
                type={showPasswordCurrent ? 'text' : 'password'}
                variant="outlined"
                onBlur={handleBlur}
                value={values.currentPassword}
                onChange={handleChange}
                InputProps={{
                  endAdornment: (
                    <InputAdornment position="end">
                      <IconButton
                        aria-label="toggle password visibility"
                        onClick={handleClickShowCurrentPassword}
                        onMouseDown={handleMouseDownCurrentPassword}
                      >
                        {showPasswordCurrent && <Visibility />}
                        {!showPasswordCurrent && <VisibilityOff />}
                      </IconButton>
                    </InputAdornment>
                  )
                }}
                helperText={
                  errors.currentPassword &&
                  touched.currentPassword &&
                  errors.currentPassword
                }
                error={Boolean(
                  errors.currentPassword && touched.currentPassword
                )}
              />
            </Grid>
            <Grid item xs={4}>
              <TextField
                fullWidth
                size="small"
                type={showPassword ? 'text' : 'password'}
                name="price"
                label="Nova Senha *"
                variant="outlined"
                onBlur={handleBlur}
                value={values.password}
                onChange={(event: any) => {
                  setFieldValue('password', event.target.value);
                }}
                InputProps={{
                  endAdornment: (
                    <InputAdornment position="end">
                      <IconButton
                        aria-label="toggle password visibility"
                        onClick={handleClickShowPassword}
                        onMouseDown={handleMouseDownPassword}
                      >
                        {showPassword && <Visibility />}
                        {!showPassword && <VisibilityOff />}
                      </IconButton>
                    </InputAdornment>
                  )
                }}
                helperText={
                  errors.password && touched.password && errors.password
                }
                error={Boolean(errors.password && touched.password)}
              />
            </Grid>
            <Grid item xs={4}>
              <TextField
                fullWidth
                size="small"
                label="Confirmar nova senha *"
                name="confirmPassword"
                type={showConfirmPassword ? 'text' : 'password'}
                variant="outlined"
                onBlur={handleBlur}
                value={values.confirmPassword}
                onChange={handleChange}
                InputProps={{
                  endAdornment: (
                    <InputAdornment position="end">
                      <IconButton
                        aria-label="toggle password visibility"
                        onClick={handleClickShowConfirmPassword}
                        onMouseDown={handleMouseDownConfirmPassword}
                      >
                        {showConfirmPassword && <Visibility />}
                        {!showConfirmPassword && <VisibilityOff />}
                      </IconButton>
                    </InputAdornment>
                  )
                }}
                helperText={
                  errors.confirmPassword &&
                  touched.confirmPassword &&
                  errors.confirmPassword
                }
                error={Boolean(
                  errors.confirmPassword && touched.confirmPassword
                )}
              />
            </Grid>
          </Grid>
          <Grid container spacing={0}>
            <Alert
              className="no-background"
              severity="warning"
              style={{ marginTop: 10 }}
            >
              * Itens obrigatórios
            </Alert>
          </Grid>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <StyledButton
                variant="contained"
                color="primary"
                fullWidth
                type="submit"
                disabled={fullSizeChangePasswordHandler(values)}
              >
                {isLoading ? (
                  <CircularProgress size={14} />
                ) : (
                  <>
                    Alterar senha
                    <BsPersonPlusFill />
                  </>
                )}
              </StyledButton>
            </Grid>
          </Grid>
        </form>
      )}
    </Formik>
  );
};

export default ResetPasswordFrom;
