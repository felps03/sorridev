import { AppBar, Button } from '@material-ui/core';
import styled from 'styled-components';

const AppBarWrapper = styled(AppBar)`
  && {
    background: linear-gradient(87deg, #11cdef, #1171ef);
    padding: 1rem;
    @media screen and (max-width: 960px) {
      background: linear-gradient(87deg, #11cdef, #1171ef) !important;
    }
  }
`;

const StyledButton = styled(Button)`
  && {
    height: 40px;
    float: right;
    margin-top: 1rem;
    margin-bottom: 1rem;
    max-width: 300px;
    border: none;
    padding-right: 10px;
    box-shadow: 0px 14px 9px -15px rgba(0, 0, 0, 0.25);
    border-radius: 8px;
    background-color: ${(props) => props.theme.colors.primary};
    font-size: ${(props) => props.theme.font.size.normal};
    color: #fff;
    font-weight: ${(props) => props.theme.font.weight.regular};
    cursor: pointer;
    text-transform: capitalize;
    transition: all 0.2s ease-in;
    > span > svg {
      margin-left: 10px;
    }
    &:hover {
      background-color: rgba(255, 177, 103, 0.8);
      transform: translateY(-3px);
    }
  }
`;

export { AppBarWrapper, StyledButton };
