import React from 'react';

import { Hidden, IconButton, Toolbar, Typography } from '@material-ui/core';
import { BiBellPlus } from 'react-icons/bi';
import { TiThMenu } from 'react-icons/ti';
import { useDispatch, useSelector } from 'react-redux';

import { ROLES } from '../../constants';
import { RootState } from '../../store/configure-store';
import { applicationActions } from '../../store/ducks/Application';
import { AppBarWrapper, StyledButton } from './styles';

const Navbar = () => {
  const dispatch = useDispatch();
  const user = useSelector((state: RootState) => state.user);

  const onClickMenu = () => {
    dispatch(applicationActions.openLeftMenuMobile());
  };

  const handleLogout = () => {
    dispatch({ type: 'USER_LOGOUT' });
  };

  return (
    <AppBarWrapper position="static" elevation={0}>
      <Toolbar>
        <Hidden mdUp implementation="css">
          <IconButton
            edge="start"
            color="inherit"
            aria-label="menu"
            onClick={onClickMenu}
          >
            <TiThMenu />
          </IconButton>
        </Hidden>
        <Typography variant="h6" style={{ flexGrow: 1 }}>
          Dashboard
        </Typography>
        {user.role === ROLES.USER && (
          <Hidden smDown implementation="css">
            <StyledButton aria-label="Logout" onClick={handleLogout}>
              Logout <BiBellPlus />
            </StyledButton>
          </Hidden>
        )}
      </Toolbar>
    </AppBarWrapper>
  );
};

export default Navbar;
