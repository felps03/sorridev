import { Card, CardContent } from '@material-ui/core';
import styled, { css } from 'styled-components';

const Wrapper = styled.section`
  /* padding-top: 4rem;
  padding-bottom: 8rem;*/
  padding-left: 40px;
  padding-right: 40px;
  /* display: flex;
  flex-direction: column; */
  background: linear-gradient(87deg, #11cdef, #1171ef);
`;

const ContentCard = styled(Card)`
  /* width: 401px;
  height: 124px;*/
  border-radius: 0.375rem;
`;

const BodyWrapper = styled(CardContent)`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  padding: 1.5rem 1rem 0.5rem 1.5rem;

  .MuiAvatar-root {
    width: 40px;
    height: 40px;
    &.red {
      background-color: #f5365c;
    }
    &.orange {
      background-color: #fb6340;
    }
    &.yellow {
      background-color: #ffd600;
    }
  }
`;

const Status = styled.section`
  position: relative;
  width: 100%;
  min-height: 1px;
  align-items: baseline;
`;

const Title = styled.h5`
  color: #32325d;
  font-weight: ${(props) => props.theme.font.weight.semiBold};
  text-transform: uppercase !important;
`;

const SubTitle = styled.span`
  font-weight: ${(props) => props.theme.font.weight.bold};
  font-size: ${(props) => props.theme.font.size.small};
  margin-bottom: 0.5rem;
  line-height: 1.5;
  color: #32325d;
`;

const FooterWrapper = styled.section`
  display: flex;
  flex-direction: row;
  padding: 1rem 1.5rem;
  > svg {
    font-size: ${(props) => props.theme.font.size.large};
  }
  > span {
    &:last-child {
      font-weight: ${(props) => props.theme.font.weight.light};
      font-size: ${(props) => props.theme.font.size.small};
      margin-left: 0.5rem !important;
    }
  }
`;

interface Props {
  status?: string;
}
const Percentage = styled.span<Props>`
  align-items: center;
  display: flex;
  font-size: ${(props) => props.theme.font.size.small};
  ${(props) =>
    props.status === 'positive' &&
    css`
      color: #2dce89;
      > svg {
        margin-bottom: 3px;
        color: #2dce89;
      }
    `}
  ${(props) =>
    props.status === 'negative' &&
    css`
      color: #f5365c;
      > svg {
        margin-bottom: 3px;
        color: #f5365c;
      }
    `}
`;

export {
  Wrapper,
  ContentCard,
  BodyWrapper,
  Status,
  Title,
  SubTitle,
  FooterWrapper,
  Percentage
};
