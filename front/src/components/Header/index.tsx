import React, { useEffect, useState } from 'react';

import { Grid, useMediaQuery, Avatar } from '@material-ui/core';
import { Skeleton } from '@material-ui/lab';
import { AiOutlineArrowUp, AiOutlineArrowDown } from 'react-icons/ai';
import { useDispatch, useSelector } from 'react-redux';

import { RootState } from '../../store/configure-store';
import { scheduleActions } from '../../store/ducks/Schedule';
import {
  Wrapper,
  ContentCard,
  BodyWrapper,
  Status,
  Title,
  SubTitle,
  FooterWrapper,
  Percentage
} from './styles';

const Header = () => {
  const breakPoint = useMediaQuery('(max-width:960px)');
  const dispatch = useDispatch();
  const schedule = useSelector((state: RootState) => state.schedule);
  const user = useSelector((state: RootState) => state.user);
  const [loading, setLoading] = useState<boolean>(false);
  const [newUserStatus, setNewUserStatus] = useState<any>({
    percentage: 0,
    status: 'positive'
  });
  const [attendanceStatus, setAttendanceStatus] = useState<any>({
    percentage: 0,
    status: 'positive'
  });
  const [absenceStatus, setAbsenceStatus] = useState<any>({
    percentage: 0,
    status: 'positive'
  });

  const getPercentage = (thisMonth: number, lastMonth: number) => {
    const current = thisMonth === 0 ? 1 : thisMonth;
    const previous = lastMonth === 0 ? 1 : lastMonth;

    const percentage = `${(
      ((current - previous) / previous) *
      100
    ).toLocaleString('fullwide', {
      maximumFractionDigits: 2
    })}%`;
    const status = current >= previous ? 'positive' : 'negative';
    return { percentage, status };
  };

  useEffect(() => {
    if (!schedule.fetched && !schedule.fetching && user.role !== 'ADMIN') {
      setLoading(true);
      dispatch(scheduleActions.fetchAttendanceCount());
    }

    if (schedule.fetched) {
      const newUser = getPercentage(
        schedule.newUserThisMonth,
        schedule.newUserLastMonth
      );
      setNewUserStatus(newUser);
      const attendance = getPercentage(
        schedule.attendanceThisMonth,
        schedule.attendanceLastMonth
      );
      setAttendanceStatus(attendance);
      const absence = getPercentage(
        schedule.absenceThisMonth,
        schedule.absenceLastMonth
      );
      setAbsenceStatus(absence);
      setLoading(false);
    }
  }, [dispatch, schedule, user]);
  return (
    <Wrapper>
      <Grid
        container
        direction="row"
        justify="center"
        alignItems="center"
        spacing={breakPoint ? 4 : 8}
      >
        <Grid item xs={12} sm={12} md={4}>
          <ContentCard>
            <BodyWrapper>
              <Status>
                <Title>Novos Usuários</Title>
                <SubTitle>
                  {loading ? (
                    <Skeleton width="15%" />
                  ) : (
                    <SubTitle>{schedule.newUserThisMonth}</SubTitle>
                  )}
                </SubTitle>
              </Status>

              <Avatar className="yellow">
                <AiOutlineArrowDown />
              </Avatar>
            </BodyWrapper>
            <FooterWrapper>
              {loading ? (
                <Skeleton width="10%" />
              ) : (
                <Percentage status={newUserStatus.status}>
                  <AiOutlineArrowUp /> {newUserStatus.percentage}
                </Percentage>
              )}
              <span>Desde o ultimo mês</span>
            </FooterWrapper>
          </ContentCard>
        </Grid>
        <Grid item xs={12} sm={12} md={4}>
          <ContentCard>
            <BodyWrapper>
              <Status>
                <Title>Atendimentos</Title>
                <SubTitle>
                  {loading ? (
                    <Skeleton width="15%" />
                  ) : (
                    <SubTitle>{schedule.attendanceThisMonth}</SubTitle>
                  )}
                </SubTitle>
              </Status>

              <Avatar className="orange">
                <AiOutlineArrowDown />
              </Avatar>
            </BodyWrapper>
            <FooterWrapper>
              {loading ? (
                <Skeleton width="10%" />
              ) : (
                <Percentage status={attendanceStatus.status}>
                  <AiOutlineArrowUp /> {attendanceStatus.percentage}
                </Percentage>
              )}
              <span>Desde o ultimo mês</span>
            </FooterWrapper>
          </ContentCard>
        </Grid>
        <Grid item xs={12} sm={12} md={4}>
          <ContentCard>
            <BodyWrapper>
              <Status>
                <Title>Não Compareceu</Title>
                <SubTitle>
                  {loading ? (
                    <Skeleton width="15%" />
                  ) : (
                    <SubTitle>{schedule.absenceThisMonth}</SubTitle>
                  )}
                </SubTitle>
              </Status>

              <Avatar className="yellow">
                <AiOutlineArrowDown />
              </Avatar>
            </BodyWrapper>
            <FooterWrapper>
              {loading ? (
                <Skeleton width="10%" />
              ) : (
                <Percentage status={absenceStatus.status}>
                  <AiOutlineArrowUp /> {absenceStatus.percentage}
                </Percentage>
              )}
              <span>Desde o ultimo mês</span>
            </FooterWrapper>
          </ContentCard>
        </Grid>
      </Grid>
    </Wrapper>
  );
};

export default Header;
