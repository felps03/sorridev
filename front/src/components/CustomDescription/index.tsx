import React, { FC, ReactNode } from 'react';

import { CardHeaderForm } from './styles';

interface Props {
  title: string | ReactNode;
}

const CustomDescription: FC<Props> = ({ title }: Props) => {
  return <CardHeaderForm>{title}</CardHeaderForm>;
};

export default CustomDescription;
