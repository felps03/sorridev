import styled from 'styled-components';

const CardHeaderForm = styled.section`
  padding-top: 30px;
  margin-bottom: -20px;
  background-color: #fff;
  > h3 {
    font-size: 1.0625rem;
    font-family: inherit;
    font-weight: 600;
    line-height: 1.5;
    color: #32325d;
  }
`;

export { CardHeaderForm };
