/* eslint-disable react/react-in-jsx-scope */
import { Avatar } from '@material-ui/core';

import getNameInitials from '../../../utils/getNameInitials';
import { UserNameWrapper, AvatarInitials } from './styles';

const column = [
  {
    title: 'Nome',
    field: 'name',
    type: 'string',
    filtering: true,
    render: (rowData: any) => (
      <UserNameWrapper>
        {rowData?.photo ? (
          <Avatar
            variant="rounded"
            src={rowData?.photo}
            style={{ marginRight: 10 }}
          />
        ) : (
          <AvatarInitials variant="rounded">
            {getNameInitials(rowData.name)}
          </AvatarInitials>
        )}
        <span>{rowData.name}</span>
      </UserNameWrapper>
    )
  },
  { title: 'CPF', field: 'cpf', type: 'string' },
  {
    title: 'CRO',
    field: 'CRO',
    type: 'numeric'
  },
  {
    title: 'Telefone',
    field: 'phone',
    type: 'string'
  },
  {
    title: 'E-mail',
    field: 'email',
    type: 'string'
  }
];

export default column;
