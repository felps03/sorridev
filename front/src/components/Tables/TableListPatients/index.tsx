import React, { useEffect, useRef, useState } from 'react';

import { Card, Paper } from '@material-ui/core';
import PerfectScrollbar from 'react-perfect-scrollbar';
import Swal from 'sweetalert2';

import { appApi } from '../../../apis/app-api';
import { TableIcons } from '../../../data';
import locale from '../../../data/table-locale';
import column from './columns';
import { TableWrapper, useStyles } from './styles';

interface Props {
  isAdmin: boolean;
  dentistID: string;
  isEdit?: boolean;
  isDeleting?: boolean;
  onSelected?: (args: any) => void;
  onDelete?: (args: any) => void;
  confirmationModalMessage?: string;
}

const TableListPatients = ({
  dentistID,
  isAdmin,
  isEdit,
  isDeleting,
  onSelected,
  onDelete,
  confirmationModalMessage
}: Props) => {
  const [tableColumn, setTableColumn] = useState<any>([]);
  const [loading, setLoading] = useState<boolean>(false);
  const classes = useStyles();
  const tableRef = useRef<any>();

  const HandleSelection = (query: any) => {
    if (query.length > 0) {
      Swal.fire({
        title: confirmationModalMessage,
        confirmButtonText: 'Sim',
        denyButtonText: 'Não',
        cancelButtonText: 'Cancelar',
        showCancelButton: true,
        showDenyButton: false,
        showConfirmButton: true
      }).then(() => {
        if (onSelected) {
          onSelected(query[0]);
          Swal.close();
        } else {
          Swal.close();
        }
      });
    }
  };

  const HandleDelete = (query: any) => {
    if (query.length > 0) {
      Swal.fire({
        title: confirmationModalMessage,
        text: 'Não é possivel reverter esta ação!',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Sim, remover!',
        cancelButtonText: 'Cancelar'
      }).then((result) => {
        if (result.isConfirmed) {
          if (onDelete) onDelete(query);
        }
      });
    }
  };

  useEffect(() => {
    setTableColumn(column);
  }, []);

  useEffect(() => {
    locale.toolbar.searchPlaceholder = 'Pesquise por um paciente';
    locale.body.emptyDataSourceMessage = 'Não há nenhum paciente cadastrado :(';
  }, []);

  return (
    <Card elevation={0}>
      <div className={classes.root}>
        <PerfectScrollbar>
          <TableWrapper
            icons={TableIcons}
            title=""
            components={{
              Container: (props: any) => <Paper {...props} elevation={0} />
            }}
            tableRef={tableRef}
            columns={tableColumn}
            onSelectionChange={(query: any) => {
              if (isDeleting) {
                HandleDelete(query);
              } else {
                HandleSelection(query);
              }
            }}
            data={(query) =>
              new Promise((resolve, reject) => {
                setLoading(true);
                let url = isAdmin
                  ? `/user?limit=${query.pageSize}&page=${
                      query.page + 1
                    }&dentist=${dentistID}`
                  : `/user?limit=${query.pageSize}&page=${query.page + 1}`;

                if (query.search !== '') {
                  url += `&search=${query.search}`;
                }
                appApi()
                  .get(url)
                  .then((result) => {
                    setLoading(false);
                    if (result.data === '') {
                      setLoading(false);
                      resolve({
                        data: [],
                        page: 0,
                        totalCount: 0
                      });
                    } else {
                      resolve({
                        data: result.data.docs,
                        page: result.data.page - 1,
                        totalCount: result.data.total
                      });
                    }
                  })
                  .catch((err) => {
                    setLoading(false);
                    Swal.fire({
                      icon: 'error',
                      title: 'Oops... Algo deu errado!',
                      text: 'Não foi possivel obter a listagem.'
                    });
                    reject(err);
                  });
              })
            }
            isLoading={loading}
            localization={locale}
            options={{
              ...((isEdit || isDeleting) && { selection: true }),
              minBodyHeight: 500,
              debounceInterval: 3000,
              pageSize: 10,
              pageSizeOptions: [5, 10, 20, 50, 100],
              emptyRowsWhenPaging: false,
              rowStyle: {
                paddingLeft: '1rem'
              },
              searchFieldAlignment: 'left',
              searchFieldStyle: {
                float: 'left'
              }
            }}
          />
        </PerfectScrollbar>
      </div>
    </Card>
  );
};

TableListPatients.defaultProps = {
  isEdit: false,
  isDeleting: false,
  onSelected: () => {},
  onDelete: () => {},
  confirmationModalMessage: 'Tem certeza de que você quer editar este paciente?'
};

export default TableListPatients;
