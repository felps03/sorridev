const column = [
  { title: 'Fechamentos de Caixa', field: 'cash_accounting', type: 'string' },
  {
    title: 'Gastos',
    field: 'spending',
    type: 'numeric'
  },
  {
    title: 'Número de Parcelas',
    field: 'number_of_appointments',
    type: 'string'
  }
];

export default column;
