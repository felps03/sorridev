import { Avatar, CardHeader, makeStyles } from '@material-ui/core';
import MaterialTable from 'material-table';
import styled from 'styled-components';

const Header = styled(CardHeader)`
  padding: 1.25rem 1.5rem;
  .MuiCardHeader-content {
    > span {
      font-size: ${(props) => props.theme.font.size.medium};
    }
  }
`;
const UserNameWrapper = styled.section`
  && {
    display: flex;
    flex-direction: row;
    align-items: center;
    > img {
      border-radius: 4px;
      margin-right: 10px;
      width: 40px;
      height: 40px;
    }
  }
`;

const AvatarInitials = styled(Avatar)`
  && {
    background: ${(props) => props.theme.colors.primary};
    margin-right: 10px;
  }
`;

const TableWrapper = styled(MaterialTable)``;

const useStyles = makeStyles(() => ({
  root: {
    '& .MuiTable-root > thead > tr > th': {
      paddingLeft: '1rem',
      paddingRight: '1.5rem',
      backgroundColor: '#F3F6F9',
      color: '#8898aa',
      '& span': {
        fontWeight: 600
      },
      '&:first-child': {
        borderTopLeftRadius: '8px',
        borderBottomLeftRadius: '8px'
      },
      '&:last-child': {
        borderTopRightRadius: '8px',
        borderBottomRightRadius: '8px'
      }
    },
    '& .MuiTable-root > thead > tr': {
      '& .MuiTableCell-root': {
        borderBottom: 'none'
      }
    },
    '& .MuiTable-root > tbody > tr > td': {
      border: 'none'
    },
    '& .MuiToolbar-gutters': {
      paddingLeft: 0,
      paddingRight: 0,
      background: 'transparent !important'
    },
    '& .MuiTableCell-paddingNone': {
      padding: '15px'
    },
    '& .MuiToolbar-regular': {
      background: 'transparent !important'
    },
    '& .MuiFormControl-root': {
      marginBottom: '15px',
      width: '50%',
      paddingTop: '10px',
      paddingBottom: '10px',
      border: 'none',
      backgroundColor: '#F3F6F9',
      boxShadow: '0px 14px 9px -15px rgba(0, 0, 0, 0.25)',
      borderRadius: '8px',
      '& .MuiInputBase-root': {
        '&:before': {
          borderBottom: 'none !important'
        },
        '&:after': {
          borderBottom: 'none !important'
        },
        '& .MuiInputAdornment-root': {
          '& .MuiSvgIcon-root': {
            fontSize: '25px',
            color: '#5e81f4'
          }
        }
      }
    }
  }
}));

export { Header, TableWrapper, useStyles, UserNameWrapper, AvatarInitials };
