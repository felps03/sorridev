/* eslint-disable react/react-in-jsx-scope */
import { Avatar } from '@material-ui/core';

import { CapitalizeString } from '../../../utils/general';
import getNameInitials from '../../../utils/getNameInitials';
import { UserNameWrapper, AvatarInitials } from './styles';

const column = [
  {
    title: 'Nome',
    field: 'name',
    type: 'string',
    filtering: true,
    render: (rowData: any) => (
      <UserNameWrapper>
        {rowData?.photo ? (
          <Avatar
            variant="rounded"
            src={rowData?.photo}
            style={{ marginRight: 10 }}
          />
        ) : (
          <AvatarInitials variant="rounded">
            {getNameInitials(rowData.name)}
          </AvatarInitials>
        )}
        <span>{rowData.name}</span>
      </UserNameWrapper>
    )
  },
  {
    title: 'Gênero',
    field: 'gender',
    type: 'string',
    render: (rowData: any) =>
      rowData?.gender && CapitalizeString(rowData.gender)
  },
  {
    title: 'Idade',
    render: (rowData: any) => <span>{rowData.age} anos</span>,
    field: 'age',
    type: 'numeric'
  },
  {
    title: 'Telefone',
    field: 'phone',
    type: 'string'
  },
  {
    title: 'E-mail',
    field: 'email',
    type: 'string'
  }
];

export default column;
