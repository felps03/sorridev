const column = [
  {
    title: 'Produto',
    field: 'product',
    type: 'string',
    filtering: true
  },
  {
    title: 'Preço',
    field: 'price',
    type: 'string',
    filtering: true,
    render: (rowData: any) => rowData.price
  },
  {
    title: 'Quantidade em estoque',
    field: 'quantity',
    type: 'numeric',
    filtering: true
  },
  {
    title: 'Provedor',
    field: 'provider',
    type: 'string',
    filtering: true
  }
];

export default column;
