const column = [
  {
    title: 'Fornecedor',
    field: 'name',
    type: 'string',
    filtering: true
  },
  {
    title: 'Telefone de Contato',
    field: 'phone',
    type: 'numeric',
    filtering: true
  },
  {
    title: 'Fornecedor',
    field: 'supply',
    type: 'string',
    filtering: true
  }
];

export default column;
