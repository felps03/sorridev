import { Avatar, Box, LinearProgress, makeStyles } from '@material-ui/core';
import styled from 'styled-components';

const Wrapper = styled.section`
  margin: 16px 0px 16px 0px;
`;

const Title = styled.h4`
  margin: 20px 0px 20px 20px;
  font-size: ${(props) => props.theme.font.size.small};
  color: ${(props) => props.theme.colors.black};
  font-weight: ${(props) => props.theme.font.weight.semiBold};
`;

const AttendanceBox = styled(Box)`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-evenly;
  margin-top: 20px;
  margin-left: 0px;
`;

const AttendanceAvatar = styled(Avatar)`
  height: 40px;
  width: 40px;
`;

const AttendanceProgress = styled(LinearProgress)`
  width: 25%;

  border-radius: 21px;
  height: 8px;
  .MuiLinearProgress-barColorPrimary {
    border-radius: 21px;
  }
`;

const AttendanceText = styled.div`
  display: flex;
  flex-direction: column;
  > span:first-child {
    font-size: ${(props) => props.theme.font.size.small};
    color: ${(props) => props.theme.colors.black};
    font-weight: ${(props) => props.theme.font.weight.semiBold};
  }

  .MuiSkeleton-root,
  > span:not(:first-child) {
    min-width: 62px;
    font-size: ${(props) => props.theme.font.size.extraSmall};
    color: ${(props) => props.theme.colors.textSecondaryColor};
    font-weight: ${(props) => props.theme.font.weight.light};
  }
`;

export default makeStyles({
  yesterday: {
    color: '#5e81f4',
    background: '#eef2fd',
    '> svg': {
      color: '#5e81f4'
    },
    '& > .MuiLinearProgress-barColorPrimary': {
      backgroundColor: '#5e81f4'
    }
  },
  today: {
    color: '#0CC3E7',
    background: '#D5F5FB',
    '> svg': {
      color: '#0CC3E7'
    },
    '& > .MuiLinearProgress-barColorPrimary': {
      backgroundColor: '#0CC3E7'
    }
  },
  tomorrow: {
    color: '#FFAE33',
    background: '#FFEFD6',
    '> svg': {
      color: '#FFAE33'
    },
    '& > .MuiLinearProgress-barColorPrimary': {
      backgroundColor: '#FFAE33'
    }
  }
});

export {
  Wrapper,
  AttendanceText,
  AttendanceBox,
  AttendanceAvatar,
  AttendanceProgress,
  Title
};
