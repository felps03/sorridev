import React, { useEffect, useState } from 'react';

import { Skeleton } from '@material-ui/lab';
import { RiUserVoiceLine } from 'react-icons/ri';
import { VscDebugStepBack, VscDebugStepOver } from 'react-icons/vsc';
import { useDispatch, useSelector } from 'react-redux';

import { RootState } from '../../../store/configure-store';
import { scheduleActions } from '../../../store/ducks/Schedule';
import { ToPercentage } from '../../../utils/general';
import styles, {
  Wrapper,
  AttendanceText,
  AttendanceBox,
  AttendanceAvatar,
  AttendanceProgress,
  Title
} from './styles';

const Attendance = () => {
  const classes = styles();
  const dispatch = useDispatch();
  const schedule = useSelector((state: RootState) => state.schedule);
  const user = useSelector((state: RootState) => state.user);
  const [loading, setLoading] = useState<boolean>(false);
  const [yesterday, setYesterday] = useState<number>(0);
  const [today, setToday] = useState<number>(0);
  const [tomorrow, setTomorrow] = useState<number>(0);

  useEffect(() => {
    if (!schedule.fetched && !schedule.fetching && user.role !== 'ADMIN') {
      setLoading(true);
      dispatch(scheduleActions.fetchScheduleCount());
    }

    if (schedule.fetched) {
      setLoading(false);
      setYesterday(schedule.yesterday);
      setToday(schedule.today);
      setTomorrow(schedule.tomorrow);
    }
  }, [dispatch, schedule, user]);

  return (
    <Wrapper>
      <Title>Atendimentos</Title>
      <AttendanceBox component="div" m={1}>
        <AttendanceAvatar variant="rounded" className={classes.yesterday}>
          <VscDebugStepBack />
        </AttendanceAvatar>
        <AttendanceText>
          <span>Ontem</span>
          {loading ? (
            <Skeleton width="100%" />
          ) : (
            <span>{schedule.yesterday} Atendimentos</span>
          )}
        </AttendanceText>
        <AttendanceProgress
          variant="determinate"
          value={loading ? yesterday : ToPercentage(yesterday)}
          className={classes.yesterday}
        />
      </AttendanceBox>
      <AttendanceBox component="div" m={1}>
        <AttendanceAvatar variant="rounded" className={classes.today}>
          <RiUserVoiceLine />
        </AttendanceAvatar>
        <AttendanceText>
          <span>Hoje</span>
          {loading ? (
            <Skeleton width="100%" />
          ) : (
            <span>{schedule.today} Atendimentos</span>
          )}
        </AttendanceText>
        <AttendanceProgress
          variant="determinate"
          value={loading ? today : ToPercentage(today)}
          className={classes.today}
        />
      </AttendanceBox>
      <AttendanceBox component="div" m={1}>
        <AttendanceAvatar variant="rounded" className={classes.tomorrow}>
          <VscDebugStepOver />
        </AttendanceAvatar>
        <AttendanceText>
          <span>Amanhã</span>
          {loading ? (
            <Skeleton width="100%" />
          ) : (
            <span>{schedule.tomorrow} Atendimentos</span>
          )}
        </AttendanceText>
        <AttendanceProgress
          variant="determinate"
          value={loading ? tomorrow : ToPercentage(tomorrow)}
          className={classes.tomorrow}
        />
      </AttendanceBox>
    </Wrapper>
  );
};

export default Attendance;
