import React from 'react';

import FullCalendar from '@fullcalendar/react';
// eslint-disable-next-line import/order
import dayGridMonth from '@fullcalendar/daygrid';
import { Calendario } from './styles';

const SidebarCalendar = () => {
  return (
    <Calendario>
      <FullCalendar
        height="300px"
        dayHeaderFormat={{
          weekday: 'narrow'
        }}
        locale="pt-br"
        plugins={[dayGridMonth]}
        titleFormat={{ year: 'numeric', month: 'long' }}
        headerToolbar={{
          left: 'prev',
          center: 'title',
          right: 'next'
        }}
        initialView="dayGridMonth"
      />
    </Calendario>
  );
};
export default SidebarCalendar;
