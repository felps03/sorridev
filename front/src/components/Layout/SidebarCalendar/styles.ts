import styled from 'styled-components';

const Calendario = styled.div`
  padding: 20px;
  .fc .fc-daygrid-day-top {
    justify-content: center;
  }
  .fc .fc-col-header-cell-cushion {
    padding-top: 0.95rem;
    font-size: ${(props) => props.theme.font.size.small};
    color: ${(props) => props.theme.colors.primary};
  }
  .fc .fc-toolbar-title {
    font-size: ${(props) => props.theme.font.size.small};
    text-transform: capitalize;
  }
  .fc .fc-toolbar.fc-header-toolbar {
    margin-bottom: -3.5em;
    z-index: 1;
  }
  .fc .fc-view-harness-active > .fc-view {
    background: #ffefd6a3;
    border-radius: 20px;
    padding: 20px;
  }

  .fc .fc-view-harness {
    margin-top: 20px;
    padding: 20px;
  }
  .fc-theme-standard .fc-scrollgrid,
  .fc-theme-standard td,
  .fc-theme-standard th {
    border: none;
  }
  .fc-liquid-hack .fc-daygrid-day-frame {
    position: initial;
  }
  .fc .fc-daygrid-body-unbalanced .fc-daygrid-day-events {
    position: relative;
    min-height: 2%;
  }
  .fc .fc-day-past .fc-daygrid-day-number,
  .fc .fc-day-future .fc-daygrid-day-number {
    color: ${(props) => props.theme.colors.black};
    font-size: ${(props) => props.theme.font.size.small};
  }

  .fc .fc-day-today .fc-daygrid-day-number {
    color: white !important;
    font-size: ${(props) => props.theme.font.size.small};
  }

  .fc .fc-daygrid-day.fc-day-today {
    background-color: ${(props) => props.theme.colors.primary};
    vertical-align: middle;
    /* height: 10px; */
    border-radius: 10px;
  }

  .fc .fc-button:focus {
    box-shadow: none;
  }

  .fc .fc-button-primary,
  .fc .fc-button-primary:not(:disabled):active,
  .fc .fc-button-primary:not(:disabled).fc-button-active {
    background-color: transparent;
    color: ${(props) => props.theme.colors.black};
    border: none;
  }
`;

export { Calendario };
