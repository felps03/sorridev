import React from 'react';

import { Toolbar } from '@material-ui/core';
import { TiThMenu } from 'react-icons/ti';
import { useDispatch } from 'react-redux';

import NotificationsIcon from '../../../../assets/icons/notifications.svg';
import { applicationActions } from '../../../../store/ducks/Application';
import { HiddenSM, HiddenMD, Icon, AppBarWrapper } from './styles';

const TopBar = () => {
  const dispatch = useDispatch();
  const onClickMenu = () => {
    dispatch(applicationActions.openLeftMenuMobile());
  };

  return (
    <AppBarWrapper position="sticky" elevation={0}>
      <Toolbar>
        <HiddenMD smDown implementation="css">
          <Icon src={NotificationsIcon} alt="Notificações" />
        </HiddenMD>
        <HiddenSM mdUp implementation="css">
          <TiThMenu onClick={onClickMenu} />
        </HiddenSM>
      </Toolbar>
    </AppBarWrapper>
  );
};

export default TopBar;
