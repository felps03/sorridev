import { Hidden, AppBar } from '@material-ui/core';
import styled from 'styled-components';

const TopBarWrapper = styled.main`
  flex: 1;
  display: flex;
  flex-direction: column;
  position: relative;
  transition: all 0.3s ease 0s;
`;

const AppBarWrapper = styled(AppBar)`
  background-color: #11cdef !important;
  min-height: 157px;
  z-index: unset;
  @media screen and (max-width: 960px) {
    background-color: ${(props) => props.theme.colors.secondary} !important;
  }
`;

const HiddenSM = styled(Hidden)`
  width: 100%;
`;

const HiddenMD = styled(Hidden)`
  width: 100%;
  @media screen and (min-width: 960px) {
    display: flex;
    justify-content: end;
  }
`;

const Icon = styled.img`
  margin-top: 15px;
  height: 3rem;
  width: 3rem;
`;

export { TopBarWrapper, HiddenSM, HiddenMD, Icon, AppBarWrapper };
