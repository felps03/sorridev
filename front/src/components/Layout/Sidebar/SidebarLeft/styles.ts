import { Drawer, List, ListItem, MenuItem } from '@material-ui/core';
import { Link } from 'react-router-dom';
import styled, { css } from 'styled-components';

const SidebarWrapper = styled.aside``;
const Nav = styled.nav``;
const DrawerWrapper = styled(Drawer)`
  .MuiDrawer-paper {
    max-width: 280px;
    width: 280px;
  }
`;
const Logo = styled.img`
  height: 4rem;
`;
const NavHeaderLink = styled(Link)`
  && {
    display: flex;
    justify-content: space-evenly;
    text-decoration: none;
  }
`;
const NavHeader = styled.div`
  padding: 20px;
  margin-top: 2.5rem;
`;
const CompanyName = styled.h3`
  text-align: center;
  text-transform: ${(props) => props.theme.font.transform.uppercase};
  color: ${(props) => props.theme.colors.secondary};
  font-size: ${(props) => props.theme.font.size.extraLarge};
  font-weight: ${(props) => props.theme.font.weight.bold};
  display: flex;
  flex-direction: column;
  margin-bottom: 1.2rem;

  > span {
    text-align: left;
    color: ${(props) => props.theme.colors.primary};
  }
`;
const LinkWrapper = styled(Link)`
  text-decoration: none;
  width: 100%;
`;
const ListWrapper = styled.div`
  text-decoration: none;
  width: 100%;
`;
const NavList = styled(List)`
  && {
    padding-top: 2rem;
    /* padding-left: 10px; */
    .MuiListItem-root.Mui-selected,
    .MuiListItem-root:hover {
      background: #f6f9fc;
      color: black;
      > a > div {
        > svg {
          font-size: 1.4rem;
          color: ${(props) => props.theme.colors.secondary};
        }
        .MuiListItemText-root {
          > span {
            color: ${(props) => props.theme.colors.black};
            font-weight: ${(props) => props.theme.font.weight.semiBold};
          }
        }
      }
    }
  }
`;
const NavListItemIcon = styled.div`
  && {
    display: flex;
    align-items: center;
    width: 100%;

    .MuiListItemText-root {
      > span {
        color: ${(props) => props.theme.colors.textSecondaryColor};
        font-weight: ${(props) => props.theme.font.weight.semiBold};
      }
    }
    > svg:first-child {
      color: ${(props) => props.theme.colors.textSecondaryColor};
      min-width: 4rem;
      font-size: 1.4rem;
      line-height: 1.5rem;
    }
    > svg:not(:first-child) {
      color: ${(props) => props.theme.colors.textSecondaryColor};
    }

    &.active {
      > svg {
        color: ${(props) => props.theme.colors.secondary};
      }
    }
  }
`;
const NavListItem = styled(MenuItem)`
  && {
    margin-top: 1.2rem;
    ${(props) =>
      props.selected &&
      css`
        border-right: 3px solid ${props.theme.colors.secondary};
      `}
  }
`;

const SubNavListItemIcon = styled(ListItem)`
  && {
    display: flex;
    align-items: center;
    width: 100%;
    padding-left: 3.5rem;

    &:hover {
      .MuiListItemText-root {
        > span {
          color: ${(props) => props.theme.colors.secondary};
          font-weight: ${(props) => props.theme.font.weight.semiBold};
        }
      }
      > svg {
        color: ${(props) => props.theme.colors.secondary};
      }
    }

    .MuiListItemText-root {
      > span {
        color: ${(props) => props.theme.colors.textSecondaryColor};
        font-weight: ${(props) => props.theme.font.weight.regular};
        font-size: ${(props) => props.theme.font.size.small};
      }
    }
    > svg {
      color: ${(props) => props.theme.colors.textSecondaryColor};
      min-width: 2rem;
      font-size: 1.2rem;
      line-height: 1.5rem;
    }
  }
`;

const SubNavList = styled(List)``;

export {
  Nav,
  Logo,
  SidebarWrapper,
  DrawerWrapper,
  CompanyName,
  NavHeaderLink,
  NavHeader,
  NavList,
  LinkWrapper,
  ListWrapper,
  NavListItem,
  NavListItemIcon,
  SubNavList,
  SubNavListItemIcon
};
