import React, { useEffect, useState } from 'react';

import { ListItemText, Hidden, Collapse, ListItem } from '@material-ui/core';
import { ExpandLess, ExpandMore } from '@material-ui/icons';
import { BiHomeAlt } from 'react-icons/bi';
import { BsPeople, BsFolder, BsFillGearFill } from 'react-icons/bs';
import { FaRegMoneyBillAlt } from 'react-icons/fa';
import { RiCustomerService2Line } from 'react-icons/ri';
import PerfectScrollbar from 'react-perfect-scrollbar';
import { useSelector } from 'react-redux';
import { useLocation } from 'react-router-dom';

import logo from '../../../../assets/icons/logo.svg';
import { RootState } from '../../../../store/configure-store';
import Clock from '../../../Clock';
import SidebarMenuMobile from '../Mobile';
import {
  DrawerWrapper,
  Nav,
  SidebarWrapper,
  Logo,
  NavHeaderLink,
  CompanyName,
  NavHeader,
  NavList,
  LinkWrapper,
  ListWrapper,
  NavListItem,
  NavListItemIcon,
  SubNavListItemIcon
} from './styles';

const SidebarLeft = () => {
  const location = useLocation();
  const userMenu = useSelector((state: RootState) => state.user.userMenu);
  const [selectedIndex, setSelectedIndex] = useState<any>();
  // TODO: Check responsiveness to use this hook
  const [activeMenu, setActiveMenu] = useState<number>();
  const [activeMenuPath, setActiveMenuPath] = useState<string>('');

  useEffect(() => {
    setActiveMenuPath(location.pathname);
  }, [location]);

  const handleClick = (index: any) => {
    if (selectedIndex === index) {
      setSelectedIndex('');
    } else {
      setSelectedIndex(index);
    }
  };

  const handleChildClick = () => {
    setSelectedIndex('');
  };

  const getIcon = (icon: string) => {
    switch (icon) {
      case 'BiHomeAlt':
        return <BiHomeAlt />;
        break;
      case 'BsPeople':
        return <BsPeople />;
        break;
      case 'BsFolder':
        return <BsFolder />;
        break;
      case 'BsFillGearFill':
        return <BsFillGearFill />;
        break;
      case 'FaRegMoneyBillAlt':
        return <FaRegMoneyBillAlt />;
        break;
      case 'RiCustomerService2Line':
        return <RiCustomerService2Line />;
        break;
      default:
        return <BiHomeAlt />;
        break;
    }
  };

  const createLinks = () => {
    return (
      <NavList aria-labelledby="nested-list-subheader">
        {userMenu.map((prop: any, key: any) => {
          if (prop.subMenu) {
            return (
              <ListWrapper key={Number(key)}>
                <NavListItem
                  button
                  onClick={() => {
                    handleChildClick();
                    handleClick(key);
                    setActiveMenu(Number(key));
                  }}
                  selected={
                    activeMenu === Number(key) || prop.path === activeMenuPath
                  }
                >
                  <NavListItemIcon
                    className={
                      activeMenu === Number(key) || prop.path === activeMenuPath
                        ? 'active'
                        : ''
                    }
                  >
                    {getIcon(prop.icon)}
                    <ListItemText primary={prop.name} />
                    {key === selectedIndex ? (
                      <ExpandLess />
                    ) : (
                      <ExpandMore />
                    )}{' '}
                  </NavListItemIcon>
                </NavListItem>
                <Collapse
                  in={key === selectedIndex}
                  timeout="auto"
                  unmountOnExit
                >
                  {prop.subMenu.map((child: any, index: number) => {
                    return (
                      <LinkWrapper
                        to={child.path}
                        key={Number(index)}
                        onClick={handleChildClick}
                      >
                        <ListWrapper>
                          <ListItem button>
                            <SubNavListItemIcon button>
                              {/* {getIcon(child.icon)} */}
                              <ListItemText primary={child.name} />
                            </SubNavListItemIcon>
                          </ListItem>
                        </ListWrapper>
                      </LinkWrapper>
                    );
                  })}
                </Collapse>
              </ListWrapper>
            );
          }
          return (
            <LinkWrapper to={prop.path} key={Number(key)}>
              <NavListItem
                button
                onClick={() => {
                  handleClick(key);
                  setActiveMenu(Number(key));
                }}
                selected={
                  activeMenu === Number(key) || prop.path === activeMenuPath
                }
              >
                <NavListItemIcon
                  className={
                    activeMenu === Number(key) || prop.path === activeMenuPath
                      ? 'active'
                      : ''
                  }
                >
                  {getIcon(prop.icon)}
                  <ListItemText primary={prop.name} />
                </NavListItemIcon>
              </NavListItem>
            </LinkWrapper>
          );
        })}
      </NavList>
    );
  };

  const brand = (
    <NavHeader>
      <NavHeaderLink to="/dashboard">
        <Logo src={logo} alt="SorriDevLogo" />
        <CompanyName>
          Sorri <span>Dev</span>
        </CompanyName>
      </NavHeaderLink>
    </NavHeader>
  );

  return (
    <SidebarWrapper>
      <PerfectScrollbar>
        <Hidden smDown implementation="css">
          <DrawerWrapper variant="persistent" anchor="left" open>
            <Nav>
              {brand}
              <Clock />

              <NavList aria-label="sidebar">{createLinks()}</NavList>
            </Nav>
          </DrawerWrapper>
        </Hidden>
      </PerfectScrollbar>
      <Hidden mdUp implementation="css">
        <SidebarMenuMobile />
      </Hidden>
    </SidebarWrapper>
  );
};
export default SidebarLeft;
