import React, { useState, useEffect } from 'react';

import {
  Divider,
  Button,
  ListItemText,
  Collapse,
  ListItem,
  useMediaQuery
} from '@material-ui/core';
import { ExpandLess, ExpandMore } from '@material-ui/icons';
import { BiHomeAlt } from 'react-icons/bi';
import { BsPeople, BsFolder, BsFillGearFill } from 'react-icons/bs';
import { FaRegMoneyBillAlt } from 'react-icons/fa';
import { IoIosCloseCircleOutline } from 'react-icons/io';
import { RiCustomerService2Line } from 'react-icons/ri';
import PerfectScrollbar from 'react-perfect-scrollbar';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory, useLocation } from 'react-router-dom';

import logo from '../../../../assets/icons/logo.svg';
import { RootState } from '../../../../store/configure-store';
import { applicationActions } from '../../../../store/ducks/Application';
import {
  Nav,
  NavList,
  NavListItem,
  NavListItemIcon,
  DialogWrapper,
  CompanyName,
  Logo,
  DialogHeader,
  LinkWrapper,
  ListWrapper,
  SubNavListItemIcon
} from './styles';

const SidebarMenuMobile = () => {
  const location = useLocation();
  const history = useHistory();
  const dispatch = useDispatch();
  const userMenu = useSelector((state: RootState) => state.user.userMenu);
  const matches = useMediaQuery('(min-width:960px)');

  const isDialogOpen = useSelector(
    (state: RootState) => state.app.isLeftMenuOpen
  );
  const [activeMenu, setActiveMenu] = useState<number>();
  const [activeMenuPath, setActiveMenuPath] = useState<string>('');

  const [selectedIndex, setSelectedIndex] = useState<any>();
  const onClickMenu = () => {
    dispatch(applicationActions.closeLeftMenuMobile());
  };

  useEffect(() => {
    setActiveMenuPath(location.pathname);
  }, [location]);
  useEffect(() => {
    if (matches) dispatch(applicationActions.closeLeftMenuMobile());
  }, [dispatch, matches]);

  const handleClick = (index: any) => {
    if (selectedIndex === index) {
      setSelectedIndex('');
    } else {
      setSelectedIndex(index);
    }
  };

  const getIcon = (icon: string) => {
    switch (icon) {
      case 'BiHomeAlt':
        return <BiHomeAlt />;
        break;
      case 'BsPeople':
        return <BsPeople />;
        break;
      case 'BsFolder':
        return <BsFolder />;
        break;
      case 'BsFillGearFill':
        return <BsFillGearFill />;
        break;
      case 'FaRegMoneyBillAlt':
        return <FaRegMoneyBillAlt />;
        break;
      case 'RiCustomerService2Line':
        return <RiCustomerService2Line />;
        break;

      default:
        return <BiHomeAlt />;
        break;
    }
  };

  const createLinks = () => {
    return (
      <NavList aria-labelledby="nested-list-subheader">
        {userMenu.map((prop: any, key: any) => {
          if (prop.subMenu) {
            return (
              <ListWrapper key={Number(key)}>
                <NavListItem
                  button
                  onClick={() => {
                    handleClick(key);
                    setActiveMenu(Number(key));
                  }}
                  selected={
                    activeMenu === Number(key) || prop.path === activeMenuPath
                  }
                >
                  <NavListItemIcon
                    className={
                      activeMenu === Number(key) || prop.path === activeMenuPath
                        ? 'active'
                        : ''
                    }
                  >
                    {getIcon(prop.icon)}
                    <ListItemText primary={prop.name} />
                    {key === selectedIndex ? (
                      <ExpandLess />
                    ) : (
                      <ExpandMore />
                    )}{' '}
                  </NavListItemIcon>
                </NavListItem>
                <Collapse
                  in={key === selectedIndex}
                  timeout="auto"
                  unmountOnExit
                >
                  {prop.subMenu.map((child: any, index: any) => {
                    return (
                      <LinkWrapper
                        to={child.path}
                        onClick={onClickMenu}
                        key={Number(index)}
                      >
                        <ListWrapper>
                          <ListItem button>
                            <SubNavListItemIcon button>
                              {/* {getIcon(child.icon)} */}
                              <ListItemText primary={child.name} />
                            </SubNavListItemIcon>
                          </ListItem>
                        </ListWrapper>
                      </LinkWrapper>
                    );
                  })}
                </Collapse>
              </ListWrapper>
            );
          }
          return (
            <LinkWrapper to={prop.path} onClick={onClickMenu} key={Number(key)}>
              <NavListItem button>
                <NavListItemIcon>
                  {getIcon(prop.icon)}
                  <ListItemText primary={prop.name} />
                </NavListItemIcon>
              </NavListItem>
            </LinkWrapper>
          );
        })}
      </NavList>
    );
  };

  return (
    <DialogWrapper
      onClose={onClickMenu}
      aria-labelledby="mobile-menu"
      open={isDialogOpen}
    >
      <PerfectScrollbar>
        <DialogHeader id="mobile-menu-header">
          <CompanyName
            onClick={() => {
              onClickMenu();
              history.push('/dashboard');
            }}
          >
            <Logo src={logo} alt="SorriDevLogo" /> Sorri <span>Dev</span>
          </CompanyName>
          {isDialogOpen ? (
            <Button onClick={onClickMenu}>
              <IoIosCloseCircleOutline />
            </Button>
          ) : null}
        </DialogHeader>
        <Divider />
        <Nav>
          <NavList aria-label="sidebar">{createLinks()}</NavList>
        </Nav>
      </PerfectScrollbar>
    </DialogWrapper>
  );
};

export default SidebarMenuMobile;
