import { Drawer } from '@material-ui/core';
import styled from 'styled-components';

const SidebarWrapper = styled.aside``;
const DrawerWrapper = styled(Drawer)`
  .MuiDrawer-paper {
    max-width: 300px;
    width: 300px;
  }
`;

export { SidebarWrapper, DrawerWrapper };
