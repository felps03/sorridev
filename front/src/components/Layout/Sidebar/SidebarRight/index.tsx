import React from 'react';

import { Hidden } from '@material-ui/core';
import PerfectScrollbar from 'react-perfect-scrollbar';

import Attendance from '../../Attendance';
import NextAppointments from '../../NextAppointments';
import SidebarCalendar from '../../SidebarCalendar';
import UserMenu from '../../UserMenu';
import { DrawerWrapper, SidebarWrapper } from './styles';

const SidebarRight = () => {
  return (
    <SidebarWrapper>
      <PerfectScrollbar>
        <Hidden smDown implementation="css">
          <DrawerWrapper variant="persistent" anchor="right" open>
            <UserMenu />
            <Attendance />
            <NextAppointments />
            <SidebarCalendar />
          </DrawerWrapper>
        </Hidden>
      </PerfectScrollbar>
    </SidebarWrapper>
  );
};
export default SidebarRight;
