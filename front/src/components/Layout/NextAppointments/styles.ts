import { Avatar, Box, makeStyles } from '@material-ui/core';
import styled from 'styled-components';

const Wrapper = styled.section`
  margin: 16px 0px 16px 0px;
`;

const Title = styled.h4`
  margin: 20px 0px 20px 20px;
  font-size: ${(props) => props.theme.font.size.small};
  color: ${(props) => props.theme.colors.black};
  font-weight: ${(props) => props.theme.font.weight.semiBold};
`;

const AppointmentBox = styled(Box)`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-evenly;
  margin-top: 20px;
  min-width: 223px;
  margin-left: -15px;
`;

const AppointmentAvatar = styled(Avatar)`
  height: 40px;
  width: 40px;
`;

const AppointmentText = styled.div`
  display: flex;
  flex-direction: column;
  float: left;
  > span:first-child {
    min-width: 159px;
    font-size: ${(props) => props.theme.font.size.small};
    color: ${(props) => props.theme.colors.black};
    font-weight: ${(props) => props.theme.font.weight.semiBold};
  }

  > span:not(:first-child) {
    font-size: ${(props) => props.theme.font.size.extraSmall};
    color: ${(props) => props.theme.colors.textSecondaryColor};
    font-weight: ${(props) => props.theme.font.weight.light};
  }
`;

export default makeStyles({
  firstAppointment: {
    color: '#0CC3E7',
    background: '#D5F5FB'
  },
  secondAppointment: {
    color: '#FFAE33',
    background: '#FFEFD6'
  }
});

export { Wrapper, AppointmentText, AppointmentBox, AppointmentAvatar, Title };
