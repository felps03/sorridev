import React, { useCallback, useEffect, useState } from 'react';

import moment from 'moment';

import { appApi } from '../../../apis/app-api';
import { ChildGirlIcon } from '../../../assets';
import styles, {
  Wrapper,
  AppointmentText,
  AppointmentBox,
  AppointmentAvatar,
  Title
} from './styles';

const NextAppointments = () => {
  const classes = styles();
  const [schedules, setSchedules] = useState<any>([]);
  const [nextSchedules, setNextSchedules] = useState<any>([]);
  const [loading, isLoading] = useState<any>([]);

  const getNextAppointment = useCallback(async (events: any) => {
    const sortedSchedules = events.sort((current: any, next: any) => {
      return moment.utc(current.startTime).diff(moment.utc(next.startTime));
    });

    const currentAppointments: any = [];
    // eslint-disable-next-line array-callback-return
    sortedSchedules.map((element: any) => {
      const found = currentAppointments.some(
        (el: any) => el._id === element._id
      );
      const startHour = moment(element.startTime).get('hour');
      const currentHour = moment().get('hour');
      if (
        startHour >= currentHour &&
        currentAppointments.length < 2 &&
        !found
      ) {
        currentAppointments.push(element);
      }
    });

    if (currentAppointments.length === 0) {
      // eslint-disable-next-line array-callback-return
      sortedSchedules.map((element: any) => {
        if (currentAppointments.length < 2) {
          currentAppointments.push(element);
        }
      });
    }

    setNextSchedules(currentAppointments);
  }, []);

  const fetchSchedule = useCallback(async () => {
    isLoading(true);
    const result = await appApi().get(`/schedule?limit=200&page=1`);
    if (result.status === 200) {
      const events = result.data.docs;
      getNextAppointment(events);
    } else {
      setNextSchedules([]);
    }
    isLoading(false);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  useEffect(() => {
    fetchSchedule();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    setInterval(() => {
      fetchSchedule();
    }, 15 * 60 * 1000);
  }, [fetchSchedule]);

  return (
    <Wrapper>
      <Title>Próximas Consultas</Title>
      {nextSchedules.length > 0 ? (
        <>
          {nextSchedules.map((next: any) => (
            <AppointmentBox component="div" m={1}>
              <AppointmentAvatar
                variant="rounded"
                className={classes.firstAppointment}
              >
                <ChildGirlIcon />
              </AppointmentAvatar>
              <AppointmentText>
                <span>{next.client.name} - 10 anos</span>
                <span>
                  {moment(next.startTime).format('DD MMM YYYY - HH:mm')}
                </span>
              </AppointmentText>
            </AppointmentBox>
          ))}
        </>
      ) : (
        <AppointmentBox component="div" m={1}>
          <AppointmentText>
            <span>Nenhum agendamento</span>
          </AppointmentText>
        </AppointmentBox>
      )}
    </Wrapper>
  );
};

export default NextAppointments;
