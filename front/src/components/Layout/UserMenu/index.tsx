import React, { useState } from 'react';

import { Avatar } from '@material-ui/core';
import { AiOutlineLogout } from 'react-icons/ai';
import { MdKeyboardArrowDown } from 'react-icons/md';
import { useDispatch, useSelector } from 'react-redux';

import { RootState } from '../../../store/configure-store';
import { NameLimitter } from '../../../utils/general';
import {
  Wrapper,
  DropdownMenu,
  DropdownMenuItem,
  DropdownMenuItemText
} from './styles';

const UserMenu = () => {
  const dispatch = useDispatch();
  const userInfo = useSelector((state: RootState) => state.user);
  const [anchorEl, setAnchorEl] = useState<any>(null);

  const handleClick = (event: any) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleLogout = () => {
    dispatch({ type: 'USER_LOGOUT' });
  };

  return (
    <>
      <Wrapper component="section" onClick={handleClick}>
        {userInfo?.photo ? (
          <Avatar variant="rounded" src={userInfo?.photo} />
        ) : (
          <img
            src={`https://ui-avatars.com/api/?name=${userInfo?.name}&background=FFB167&color=fff`}
            alt="user"
          />
        )}

        <h4>{NameLimitter(userInfo?.name)}</h4>
        <MdKeyboardArrowDown />
      </Wrapper>
      <DropdownMenu
        disableScrollLock
        id="sidemenu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >
        <DropdownMenuItem onClick={handleLogout}>
          <AiOutlineLogout />
          <DropdownMenuItemText>Logout</DropdownMenuItemText>
        </DropdownMenuItem>
      </DropdownMenu>
    </>
  );
};

export default UserMenu;
