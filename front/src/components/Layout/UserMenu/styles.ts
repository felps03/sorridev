import { Box, Menu, MenuItem } from '@material-ui/core';
import styled from 'styled-components';

const Wrapper = styled(Box)`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-evenly;

  margin: 16px 0px 16px 0px;
  > img {
    border-radius: 4px;
    width: 40px;
    height: 40px;
  }

  > h4 {
    color: ${(props) => props.theme.colors.black};
    font-weight: ${(props) => props.theme.font.weight.semiBold};
  }

  > svg {
    color: ${(props) => props.theme.colors.black};
    font-size: 1.4rem;
    line-height: 1.5rem;
  }
`;

const DropdownMenu = styled(Menu)`
  && {
    .MuiMenu-paper {
      top: 68px !important;
      min-width: 220px !important;
      max-width: 220px !important;
      margin-left: 30px;
    }

    .MuiList-padding {
      padding-top: 0px;
    }
  }
`;

const DropdownMenuItem = styled(MenuItem)`
  padding: 0.5rem 1rem;
  font-size: 0.875rem;
  margin-top: 10px;

  border-right: 4px solid ${(props) => props.theme.colors.primary};

  > svg {
    color: ${(props) => props.theme.colors.primary};
    margin-right: 1rem;
    font-size: 1rem;
    vertical-align: -17%;
  }

  :hover {
    background: #f6f9fc;
  }
`;

const DropdownMenuItemText = styled.span`
  color: ${(props) => props.theme.colors.primary};
  font-weight: ${(props) => props.theme.font.weight.semiBold};
`;

export { Wrapper, DropdownMenu, DropdownMenuItem, DropdownMenuItemText };
