import { ResponsiveLine } from '@nivo/line';
import styled from 'styled-components';

const ResponsiveLineChart = styled(ResponsiveLine)`
  && {
    height: 300px;
  }
`;

export { ResponsiveLineChart };
