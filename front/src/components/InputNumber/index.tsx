import React from 'react';

import { TextField } from '@material-ui/core';
// import { classNames } from 'react-extras';
import NumberFormat from 'react-number-format';

// import './styles.less';

export const InputCurrency = ({ ...props }) => {
  return (
    <NumberFormat
      {...props}
      isNumericString
      decimalSeparator=","
      thousandSeparator="."
      prefix="R$"
      customInput={TextField}
      decimalScale={2}
    />
  );
};

export const InputCellPhone = ({ ...props }) => (
  <NumberFormat {...props} customInput={TextField} format="(##) #####-####" />
);

export const InputTelephone = ({ ...props }) => (
  <NumberFormat {...props} customInput={TextField} format="(##) ####-####" />
);

export const InputCep = ({ ...props }) => (
  <NumberFormat {...props} customInput={TextField} format="#####-###" />
);

export const InputCPF = ({ ...props }) => (
  <NumberFormat {...props} customInput={TextField} format="###.###.###-##" />
);
