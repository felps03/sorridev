import React, { FC, FormEvent } from 'react';

import {
  CircularProgress,
  Grid,
  TextField,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  FormHelperText
} from '@material-ui/core';
import { Formik } from 'formik';
import { BsPersonPlusFill } from 'react-icons/bs';

import { GENDER } from '../../../constants';
import { RegisterSchema } from '../../../validation/register';
import { InputCellPhone, InputCPF } from '../../InputNumber';
import { StyledButton } from './styles';

interface IUserDash {
  onClickSendButton: (...args: any[]) => any;
  isLoading: boolean;
  patientData?: any;
}

const DashRegisterUserForm: FC<IUserDash> = ({
  onClickSendButton,
  isLoading,
  patientData
}: IUserDash) => {
  return (
    <Formik
      initialValues={{
        name: patientData?.name || '',
        birthdate: patientData?.birthdate
          ? patientData?.birthdate.split('T')[0]
          : '',
        cpf: patientData?.cpf || '',
        rg: patientData?.rg || '',
        phone: patientData?.phone || '',
        gender: patientData?.gender || '',
        email: patientData?.email || ''
      }}
      validationSchema={RegisterSchema}
      onSubmit={(values: any, { setSubmitting }: any) => {
        setSubmitting(false);

        onClickSendButton({ ...values });
      }}
    >
      {({
        isValid,
        dirty,
        values,
        errors,
        touched,
        handleBlur,
        handleChange,
        handleSubmit,
        setFieldValue
      }) => (
        <form
          onSubmit={(value: any) =>
            handleSubmit(value as FormEvent<HTMLFormElement>)
          }
        >
          <Grid container style={{ paddingTop: 20 }} spacing={2}>
            <Grid item xs={12} sm={6}>
              <TextField
                fullWidth
                size="small"
                label="Nome Completo"
                name="name"
                type="text"
                placeholder="Digite seu nome completo"
                variant="outlined"
                onBlur={handleBlur}
                value={values.name}
                onChange={handleChange}
                helperText={errors.name && touched.name && errors.name}
                error={Boolean(errors.name && touched.name)}
              />
            </Grid>
            <Grid item xs={12} sm={3}>
              <InputCPF
                fullWidth
                size="small"
                label="CPF *"
                name="cpf"
                type="text"
                placeholder="Digite seu nome completo"
                variant="outlined"
                onBlur={handleBlur}
                value={values.cpf}
                onChange={handleChange}
                helperText={errors.cpf && touched.cpf && errors.cpf}
                error={Boolean(errors.cpf && touched.cpf)}
              />
            </Grid>
            <Grid item xs={12} sm={3}>
              <TextField
                fullWidth
                size="small"
                label="RG *"
                name="rg"
                type="text"
                placeholder="Digite seu RG"
                variant="outlined"
                onBlur={handleBlur}
                value={values.rg}
                onChange={handleChange}
                helperText={errors.rg && touched.rg && errors.rg}
                error={Boolean(errors.rg && touched.rg)}
              />
            </Grid>
            <Grid item xs={12} sm={4}>
              <TextField
                fullWidth
                size="small"
                type="date"
                name="birthdate"
                label="Data de Nascimento *"
                placeholder="Digite sua data de nascimento"
                variant="outlined"
                InputLabelProps={{
                  shrink: true
                }}
                onBlur={handleBlur}
                value={values.birthdate}
                onChange={(event: any) => {
                  setFieldValue('birthdate', event.target.value);
                }}
                helperText={
                  errors.birthdate && touched.birthdate && errors.birthdate
                }
                error={Boolean(errors.birthdate && touched.birthdate)}
              />
            </Grid>
            <Grid item xs={12} sm={4}>
              <FormControl variant="outlined" fullWidth size="small">
                <InputLabel id="demo-simple-select-outlined-label">
                  Gênero *
                </InputLabel>
                <Select
                  labelId="demo-simple-select-outlined-label"
                  id="demo-simple-select-outlined"
                  label="Gênero *"
                  name="gender"
                  value={values.gender}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  error={Boolean(errors.gender && touched.gender)}
                >
                  {GENDER.map((element, key) => (
                    <MenuItem key={Number(key)} value={element}>
                      {element}
                    </MenuItem>
                  ))}
                </Select>
                {Boolean(errors.gender && touched.gender) && (
                  <FormHelperText>
                    {errors.gender && touched.gender && errors.gender}
                  </FormHelperText>
                )}
              </FormControl>
            </Grid>
            <Grid item xs={12} sm={4}>
              <InputCellPhone
                fullWidth
                size="small"
                label="Telefone de Contato *"
                name="phone"
                type="text"
                variant="outlined"
                onBlur={handleBlur}
                value={values.phone}
                onChange={handleChange}
                helperText={errors.phone && touched.phone && errors.phone}
                error={Boolean(errors.phone && touched.phone)}
              />
            </Grid>
            <Grid item xs={12} sm={12}>
              <TextField
                fullWidth
                size="small"
                label="E-mail"
                placeholder="Digite um e-mail"
                variant="outlined"
                type="text"
                name="email"
                onBlur={handleBlur}
                value={values.email}
                onChange={handleChange}
                helperText={errors.email && touched.email && errors.email}
                error={Boolean(errors.email && touched.email)}
              />
            </Grid>
            <Grid item xs={12}>
              <StyledButton
                variant="contained"
                color="primary"
                fullWidth
                type="submit"
                disabled={!(isValid && dirty)}
              >
                {isLoading ? (
                  <CircularProgress size={14} />
                ) : (
                  <>
                    Cadastrar novo usuário <BsPersonPlusFill />
                  </>
                )}
              </StyledButton>
            </Grid>
          </Grid>
        </form>
      )}
    </Formik>
  );
};

DashRegisterUserForm.defaultProps = {
  patientData: undefined
};

export default DashRegisterUserForm;
