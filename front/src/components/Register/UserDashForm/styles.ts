import { Button, TextField } from '@material-ui/core';
import styled from 'styled-components';

const Input = styled(TextField)`
  && {
    margin-bottom: 15px;
    border: none;

    .MuiOutlinedInput-input {
      background-color: #f5f5f5;
      box-shadow: 0px 14px 9px -15px rgba(0, 0, 0, 0.25);
      border-radius: 8px;
    }
    .MuiFormLabel-root.Mui-focused,
    .MuiInputLabel-outlined.MuiInputLabel-shrink {
      color: #4477eef2;
      font-size: ${(props) => props.theme.font.size.medium};
      font-weight: ${(props) => props.theme.font.weight.bold};
    }

    .MuiFormLabel-root.Mui-error {
      color: ${(props) => props.theme.colors.error};
    }
  }
`;

const StyledButton = styled(Button)`
  && {
    height: 54px;
    float: right;
    margin-top: 1rem;
    max-width: 317px;
    border: none;
    padding-right: 10px;
    box-shadow: 0px 14px 9px -15px rgba(0, 0, 0, 0.25);
    border-radius: 8px;
    background-color: ${(props) => props.theme.colors.primary};
    font-size: ${(props) => props.theme.font.size.medium};
    color: #fff;
    font-weight: ${(props) => props.theme.font.weight.regular};
    cursor: pointer;
    text-transform: capitalize;
    transition: all 0.2s ease-in;
    > span > svg {
      margin-left: 10px;
    }
    &:hover {
      background-color: rgba(255, 177, 103, 0.8);
      transform: translateY(-3px);
    }
  }
`;

export { Input, StyledButton };
