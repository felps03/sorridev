/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/no-unused-vars */
import React, { useState } from 'react';

import {
  Button,
  CircularProgress,
  Grid,
  IconButton,
  InputAdornment,
  makeStyles,
  MobileStepper,
  useTheme
} from '@material-ui/core';
import {
  Visibility,
  VisibilityOff,
  CancelOutlined,
  KeyboardArrowLeft,
  KeyboardArrowRight
} from '@material-ui/icons';
import { Formik, FormikProps } from 'formik';
import { FcGoogle } from 'react-icons/fc';
import { IoWarningOutline } from 'react-icons/io5';
import { MdKeyboardArrowLeft, MdKeyboardArrowRight } from 'react-icons/md';
import { useSelector } from 'react-redux';
import { toast } from 'react-toastify';

import { RegisterValues, UserRegisterState } from '../../../interfaces/User';
import { InitialRegisterSchema } from '../../../validation/register';
import ToastMessage from '../../ToastMessage';
import {
  Form,
  Input,
  StyledButton,
  SmallButton,
  ButtonWrapper,
  DotSteps
} from './styles';

interface IRegisterFormProps {
  onFormSubmit: (loginValues: RegisterValues) => void;
  isSubmitting: boolean;
}

interface IRegisterFormData {
  password: string;
  confirmPassword: string;
}

const useStyles = makeStyles({
  root: {
    maxWidth: 400,
    flexGrow: 1
  }
});

const UserRegisterForm = ({
  onFormSubmit,
  isSubmitting
}: IRegisterFormProps) => {
  const isLoading = useSelector((state: UserRegisterState) => state.isLoading);
  const [showPassword, setShowPassword] = useState(false);
  const [showConfirmPassword, setConfirmPassword] = useState(false);

  const handleClickShowPassword = () => setShowPassword(!showPassword);
  const handleMouseDownPassword = () => setShowPassword(!showPassword);
  const handleClickShowConfirmPassword = () =>
    setConfirmPassword(!showConfirmPassword);
  const handleMouseDownConfirmPassword = () =>
    setConfirmPassword(!showConfirmPassword);

  const fullSizeChangePasswordHandler = (props: IRegisterFormData) => {
    const { password, confirmPassword } = props;

    const diffPass = !(password === confirmPassword);
    const emptyValues = !(password && confirmPassword);

    return diffPass || emptyValues;
  };

  const classes = useStyles();
  const theme = useTheme();

  const [activeStep, setActiveStep] = useState<number>(0);

  const handleNext = () => {
    setActiveStep((prevActiveStep: number) => prevActiveStep + 1);
  };

  const handleBack = () => {
    setActiveStep((prevActiveStep: number) => prevActiveStep - 1);
  };

  return (
    <>
      <Formik
        initialValues={{
          name: '',
          birthdate: '',
          cpf: '',
          rg: '',
          phone: '',
          gender: '',
          email: '',
          password: '',
          confirmPassword: ''
        }}
        validationSchema={InitialRegisterSchema}
        onSubmit={async (values: RegisterValues) => {
          onFormSubmit(values);
        }}
      >
        {({
          errors,
          handleBlur,
          handleChange,
          handleSubmit,
          isValid,
          setFieldValue,
          touched,
          values
        }: FormikProps<RegisterValues>) => {
          function handleRegisterValidations() {
            toast.error(
              <ToastMessage
                type="error"
                title="Erro ao enviar cadastro!"
                description=" Por favor,
            verifique todos campos!"
              />,
              {
                position: 'top-right',
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
                autoClose: 5000
              }
            );
          }

          return (
            <Form onSubmit={handleSubmit}>
              <h3>Seja bem vindo, Cadastre-se {isValid}</h3>
              <div style={{ padding: '0px 40px' }}>
                <Grid container>
                  {activeStep === 0 && (
                    <>
                      <Grid item xs={12}>
                        <Input
                          fullWidth
                          size="medium"
                          label="Nome Completo"
                          name="name"
                          type="text"
                          placeholder="Digite seu nome completo"
                          variant="outlined"
                          onBlur={handleBlur}
                          value={values.name}
                          onChange={handleChange}
                          helperText={
                            errors.name && touched.name && errors.name
                          }
                          error={Boolean(errors.name && touched.name)}
                        />
                      </Grid>
                      <Grid item xs={12}>
                        <Input
                          fullWidth
                          size="medium"
                          label="Telefone"
                          placeholder="(DDD) xxxxx-xxxx"
                          variant="outlined"
                          type="text"
                          name="phone"
                          onBlur={handleBlur}
                          value={values.phone}
                          onChange={handleChange}
                          helperText={
                            errors.phone && touched.phone && errors.phone
                          }
                          error={Boolean(errors.phone && touched.phone)}
                        />
                      </Grid>

                      <Grid item xs={12}>
                        <Input
                          fullWidth
                          size="medium"
                          label="Gênero"
                          placeholder="Selecione o genero"
                          variant="outlined"
                          name="gender"
                          onBlur={handleBlur}
                          value={values.gender}
                          onChange={handleChange}
                          helperText={
                            errors.gender && touched.gender && errors.gender
                          }
                          error={Boolean(errors.gender && touched.gender)}
                        />
                      </Grid>
                    </>
                  )}
                  {activeStep === 1 && (
                    <>
                      <Grid item xs={12}>
                        <Input
                          fullWidth
                          size="medium"
                          label="CPF"
                          name="cpf"
                          type="text"
                          placeholder="Digite seu CPF"
                          variant="outlined"
                          onBlur={handleBlur}
                          value={values.cpf}
                          onChange={handleChange}
                          helperText={errors.cpf && touched.cpf && errors.cpf}
                          error={Boolean(errors.cpf && touched.cpf)}
                        />
                      </Grid>
                      <Grid item xs={12}>
                        <Input
                          fullWidth
                          size="medium"
                          label="RG *"
                          name="rg"
                          type="text"
                          placeholder="Digite seu RG"
                          variant="outlined"
                          value={values.rg}
                          onBlur={handleBlur}
                          onChange={handleChange}
                          helperText={errors.rg && touched.rg && errors.rg}
                          error={Boolean(errors.rg && touched.rg)}
                        />
                      </Grid>
                      <Grid item xs={12}>
                        <Input
                          fullWidth
                          size="medium"
                          type="date"
                          name="birthdate"
                          label="Data de Nascimento"
                          placeholder="Digite sua data de nascimento"
                          variant="outlined"
                          InputLabelProps={{
                            shrink: true
                          }}
                          onBlur={handleBlur}
                          value={values.birthdate}
                          onChange={(event: any) => {
                            setFieldValue('birthdate', event.target.value);
                          }}
                          helperText={
                            errors.birthdate &&
                            touched.birthdate &&
                            errors.birthdate
                          }
                          error={Boolean(errors.birthdate && touched.birthdate)}
                        />
                      </Grid>
                    </>
                  )}
                  {activeStep === 2 && (
                    <>
                      <Grid item xs={12}>
                        <Input
                          fullWidth
                          size="medium"
                          label="E-mail"
                          placeholder="Digite um e-mail"
                          variant="outlined"
                          type="text"
                          name="email"
                          onBlur={handleBlur}
                          value={values.email}
                          onChange={handleChange}
                          helperText={
                            errors.email && touched.email && errors.email
                          }
                          error={Boolean(errors.email && touched.email)}
                        />
                      </Grid>

                      <Grid item xs={12}>
                        <Input
                          fullWidth
                          size="medium"
                          label="Senha"
                          placeholder="Digite uma senha"
                          variant="outlined"
                          type={showPassword ? 'text' : 'password'}
                          name="password"
                          onBlur={handleBlur}
                          value={values.password}
                          onChange={handleChange}
                          InputProps={{
                            endAdornment: (
                              <InputAdornment position="end">
                                <IconButton
                                  aria-label="toggle password visibility"
                                  onClick={handleClickShowPassword}
                                  onMouseDown={handleMouseDownPassword}
                                >
                                  {showPassword ? (
                                    <Visibility />
                                  ) : (
                                    <VisibilityOff />
                                  )}
                                </IconButton>
                              </InputAdornment>
                            )
                          }}
                          helperText={
                            errors.password &&
                            touched.password &&
                            errors.password
                          }
                          error={Boolean(errors.password && touched.password)}
                        />
                      </Grid>
                      <Grid item xs={12}>
                        <Input
                          fullWidth
                          size="medium"
                          label="Confirmar Senha"
                          placeholder="Confirme sua senha"
                          variant="outlined"
                          type={showConfirmPassword ? 'text' : 'password'}
                          name="confirmPassword"
                          value={values.confirmPassword}
                          onChange={handleChange}
                          helperText={
                            errors.confirmPassword &&
                            touched.confirmPassword &&
                            errors.confirmPassword
                          }
                          error={Boolean(
                            errors.confirmPassword && touched.confirmPassword
                          )}
                          InputProps={{
                            endAdornment: (
                              <InputAdornment position="end">
                                <IconButton
                                  aria-label="toggle password visibility"
                                  onClick={handleClickShowConfirmPassword}
                                  onMouseDown={handleMouseDownConfirmPassword}
                                >
                                  {showConfirmPassword ? (
                                    <Visibility />
                                  ) : (
                                    <VisibilityOff />
                                  )}
                                </IconButton>
                              </InputAdornment>
                            )
                          }}
                        />
                      </Grid>
                    </>
                  )}
                  <ButtonWrapper>
                    {activeStep !== 0 && (
                      <SmallButton
                        variant="contained"
                        color="primary"
                        fullWidth
                        type="button"
                        onClick={handleBack}
                      >
                        <MdKeyboardArrowLeft />
                      </SmallButton>
                    )}
                    {activeStep !== 2 ? (
                      <StyledButton
                        variant="contained"
                        color="primary"
                        fullWidth
                        type="button"
                        onClick={handleNext}
                      >
                        continuar <MdKeyboardArrowRight />
                      </StyledButton>
                    ) : (
                      <StyledButton
                        variant="contained"
                        color="primary"
                        fullWidth
                        type="submit"
                        onClick={() => {
                          handleRegisterValidations();
                        }}
                      >
                        {isLoading ? (
                          <CircularProgress size={14} />
                        ) : (
                          'Cadastrar'
                        )}
                      </StyledButton>
                    )}
                    <SmallButton
                      variant="contained"
                      color="primary"
                      fullWidth
                      type="button"
                      onClick={handleNext}
                    >
                      <FcGoogle />
                    </SmallButton>
                  </ButtonWrapper>
                </Grid>
              </div>
            </Form>
          );
        }}
      </Formik>

      <DotSteps
        variant="dots"
        steps={3}
        position="static"
        activeStep={activeStep}
        nextButton=""
        backButton=""
        style={{ background: 'transparent' }}
      />
    </>
  );
};

export default UserRegisterForm;
