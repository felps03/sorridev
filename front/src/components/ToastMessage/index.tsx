import React, { FC } from 'react';

import { FiAlertCircle, FiCheckCircle, FiInfo } from 'react-icons/fi';

import { Container } from './styles';

interface ToastMessageProps {
  type: 'success' | 'error' | 'info' | 'warning';
  title: string;
  description: string;
}

const icons = {
  info: <FiInfo size={24} />,
  success: <FiCheckCircle size={24} />,
  error: <FiAlertCircle size={24} />,
  warning: <FiAlertCircle size={24} />
};

const ToastMessage: FC<ToastMessageProps> = ({
  type,
  title,
  description
}: ToastMessageProps) => {
  return (
    <Container alertType={type} hasDescription={Number(!!description)}>
      {icons[type]}

      <div>
        <strong>{title}</strong>
        {description && <p>{description}</p>}
      </div>
    </Container>
  );
};

export default ToastMessage;
