import styled, { css } from 'styled-components';

interface ContainerProps {
  alertType?: 'success' | 'error' | 'info' | 'warning';
  hasDescription: number;
}

export const Container = styled.div<ContainerProps>`
  padding: 16px 30px 16px 16px;
  display: flex;
  background-color: ${({ alertType }) =>
    (alertType === 'success' && '#07bc0c') ||
    (alertType === 'warning' && '#f1c40f') ||
    (alertType === 'info' && '#3498db') ||
    (alertType === 'error' && '#e74c3c')};
  & + div {
    margin-top: 16px;
  }

  color: white;

  > svg {
    margin: 0px 12px 0 0;
  }

  div {
    strong {
      font-weight: ${(props) => props.theme.font.weight.bold};
    }
    p {
      margin-top: 4px;
      font-size: ${(props) => props.theme.font.size.medium};
      font-weight: ${(props) => props.theme.font.weight.medium};
      opacity: 0.8;
      line-height: 20px;
    }
  }

  button {
    position: absolute;
    right: 16px;
    top: 19px;
    background: transparent;
    border: 0;
    color: inherit;
  }

  ${(props) =>
    !props.hasDescription &&
    css`
      align-items: center;

      svg {
        margin-top: 0;
      }
    `}
`;
