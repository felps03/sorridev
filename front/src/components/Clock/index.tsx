import React, { useEffect, useState } from 'react';

import moment from 'moment';

import ClockIcon from '../../assets/icons/clockIcon.svg';
import { ClockWrapper, Logo } from './styles';

const Clock = () => {
  const [time, setTime] = useState<any>(moment(moment.now()).format('HH:mm'));
  useEffect(() => {
    const timer = setInterval(() => {
      // Creates an interval which will update the current data every minute
      // This will trigger a rerender every component that uses the useDate hook.
      setTime(moment(moment.now()).format('HH:mm'));
    }, 60 * 1000);
    return () => {
      clearInterval(timer); // Return a funtion to clear the timer so that it will stop being called on unmount
    };
  }, []);

  return (
    <ClockWrapper>
      <Logo src={ClockIcon} alt="SorriDevLogo" />
      <span>{time}</span>
    </ClockWrapper>
  );
};

export default Clock;
