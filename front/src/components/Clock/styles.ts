import styled from 'styled-components';

const Logo = styled.img`
  height: 3rem;
  padding-right: 1rem;
`;

const ClockWrapper = styled.section`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;

  > span {
    font-size: ${(props) => props.theme.font.size.extraLarge};
    color: ${(props) => props.theme.colors.textSecondaryColor};
    font-weight: ${(props) => props.theme.font.weight.semiBold};
  }
`;

export { ClockWrapper, Logo };
