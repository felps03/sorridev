import React, { FC, ReactNode } from 'react';

import { Paper } from '@material-ui/core';

import { Wrapper, ContainerWrapper } from './styles';

interface Props {
  children: ReactNode;
}

const ContentBlock: FC<Props> = ({ children }: Props) => {
  return (
    <ContainerWrapper>
      <Paper elevation={0}>
        <Wrapper>{children}</Wrapper>
      </Paper>
    </ContainerWrapper>
  );
};

export default ContentBlock;
