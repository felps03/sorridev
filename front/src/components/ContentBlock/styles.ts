import styled from 'styled-components';

const ContainerWrapper = styled.section`
  padding-left: 38px;
  padding-right: 38px;
`;
const Wrapper = styled.section`
  padding: 30px;
  min-height: 200px;
`;

export { Wrapper, ContainerWrapper };
