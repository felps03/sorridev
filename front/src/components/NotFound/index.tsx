import React from 'react';

import { NotFoundIcon } from '../../assets';

const NotFound = () => {
  return (
    <>
      <NotFoundIcon />
    </>
  );
};

export default NotFound;
