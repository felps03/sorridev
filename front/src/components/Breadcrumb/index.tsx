import React, { useEffect, useState } from 'react';

import { Grid } from '@material-ui/core';
import { useLocation } from 'react-router-dom';

import { CapitalizeString } from '../../utils/general';
import {
  Wrapper,
  BreadcrumbsWrapper,
  LinkWrapper,
  CurrentPathWrapper
} from './styles';

const CustomBreadcrumbs = () => {
  const location = useLocation();
  const [initialPath, setInitialPath] = useState<string>('');
  const [wrapperPath, setWrapperPath] = useState<string>('');
  const [currentPath, setCurrentPath] = useState<string>('');

  useEffect(() => {
    const pathName = location.pathname.split('/');
    pathName.shift();
    setInitialPath(pathName[0]);
    setWrapperPath(pathName[1]);
    setCurrentPath(pathName[2]);
  }, [location]);
  const handleClick = () => {};

  return (
    <Wrapper>
      <Grid container direction="row">
        <Grid item xs={12}>
          {wrapperPath ? (
            <BreadcrumbsWrapper aria-label="breadcrumb">
              <LinkWrapper color="inherit" to="/dashboard">
                {CapitalizeString(initialPath)}
              </LinkWrapper>
              <LinkWrapper
                color="inherit"
                to={`/dashboard/${wrapperPath}/lista`}
                onClick={handleClick}
              >
                {CapitalizeString(wrapperPath)}
              </LinkWrapper>
              {currentPath && (
                <CurrentPathWrapper>{`${CapitalizeString(
                  wrapperPath
                )} ${CapitalizeString(currentPath)}`}</CurrentPathWrapper>
              )}
            </BreadcrumbsWrapper>
          ) : null}
        </Grid>
      </Grid>
    </Wrapper>
  );
};

export default CustomBreadcrumbs;
