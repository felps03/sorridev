import { Breadcrumbs, Typography } from '@material-ui/core';
import { Link } from 'react-router-dom';
import styled from 'styled-components';

const Wrapper = styled.section`
  padding: 1rem;
  background: linear-gradient(87deg, #11cdef, #1171ef);
  padding-bottom: 4.5rem !important;
`;

const BreadcrumbsWrapper = styled(Breadcrumbs)`
  padding: 0rem 0rem 1.5rem 1.5rem;
`;
const LinkWrapper = styled(Link)`
  color: white;
  text-decoration: none;
  font-weight: ${(props) => props.theme.font.weight.bold};
`;
const CurrentPathWrapper = styled(Typography)`
  color: white;
  font-weight: ${(props) => props.theme.font.weight.bold};
`;

export { Wrapper, BreadcrumbsWrapper, LinkWrapper, CurrentPathWrapper };
