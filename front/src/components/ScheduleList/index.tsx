/* eslint-disable no-alert */
import React, { useRef, useState } from 'react';

import adaptivePlugin from '@fullcalendar/adaptive';
import dayGridPlugin from '@fullcalendar/daygrid';
import interactionPlugin from '@fullcalendar/interaction'; // needed for dayClick
import FullCalendar, {
  DateSelectArg,
  EventApi,
  EventClickArg,
  EventContentArg
} from '@fullcalendar/react';
import timeGridPlugin from '@fullcalendar/timegrid';
import moment from 'moment';
import { AiFillPrinter } from 'react-icons/ai';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import ReactToPrint from 'react-to-print';
import Swal from 'sweetalert2';
import { v4 as uuid } from 'uuid';

import { handleSuccessResponse, handleErrorResponse } from '../../apis';
import { ROLES } from '../../constants';
import { RootState } from '../../store/configure-store';
import { applicationActions } from '../../store/ducks/Application';
import { scheduleActions } from '../../store/ducks/Schedule';
import {
  fetchAddSchedule,
  fetchUpdateScheduleById,
  fetchDeleteScheduleById
} from '../../store/ducks/Schedule/services';
import { NameLimitter } from '../../utils/general';
import ModalRegisterSchedule from '../ModalRegisterSchedule';
import { ScheduleWrapper, StyledButton, PrintWrapper } from './styles';

interface IProps {
  listOfEvents: any;
}
const ScheduleList = ({ listOfEvents }: IProps) => {
  const history = useHistory();
  const dispatch = useDispatch();
  const user = useSelector((state: RootState) => state.user);
  const [calendarEvents, setCalendarEvents] = useState<any>([]);
  const [registerModal, showRegisterModal] = useState<boolean>(false);
  const [handleTimeData, setHandleTimeData] = useState<any>();
  const [calendarData, setCalendarData] = useState<any>();
  const [holeEventData, setHoleEventData] = useState<any>();
  const [isEdit, setIsEdit] = useState<boolean>(false);
  const [data, setData] = useState<any>();
  const intervalRef = useRef() as React.MutableRefObject<HTMLInputElement>;

  const handleDateSelect = (selectInfo: DateSelectArg) => {
    if (user.role !== ROLES.USER) {
      setData(undefined);
      setHandleTimeData(selectInfo);
      showRegisterModal(true);
      setIsEdit(false);
    }
  };

  const addNewScheduleRequest = async (newEvent: any) => {
    try {
      const result = await fetchAddSchedule(newEvent);
      return handleSuccessResponse(result);
    } catch (error) {
      return handleErrorResponse(error);
    }
  };

  const updateScheduleRequest = async (newEvent: any, editID: string) => {
    try {
      const result = await fetchUpdateScheduleById(newEvent, editID);
      return handleSuccessResponse(result);
    } catch (error) {
      return handleErrorResponse(error);
    }
  };

  const deleteScheduleRequest = async (newEvent: any) => {
    try {
      const result = await fetchDeleteScheduleById(newEvent);
      return handleSuccessResponse(result);
    } catch (error) {
      return handleErrorResponse(error);
    }
  };

  const AddNewSchedule = async (dados: any) => {
    const calendarApi = handleTimeData.view.calendar;
    calendarApi.unselect();
    const response = await addNewScheduleRequest(dados);
    if (response.status === 201) {
      if ('data' in response) {
        dispatch(applicationActions.updateGraphics());
        dispatch(scheduleActions.fetchAttendanceCount());
        const update = calendarEvents.push(response?.data);
        setCalendarEvents(update);
        calendarApi.addEvent({
          id: uuid(),
          title: NameLimitter(dados.name),
          start: handleTimeData.startStr,
          end: handleTimeData.endStr,
          allDay: handleTimeData.allDay
        });
        Swal.fire('Agendado', 'Agendamento realizado com sucesso!', 'success');
        showRegisterModal(false);
        history.push('/');
        history.replace('/dashboard/agenda/agendamentos');
      }
    } else {
      Swal.fire(
        'Agendamento',
        'Não foi possivel realizar o agendamento!',
        'error'
      );
    }
  };

  const UpdateNewSchedule = async (dados: any, editID: string) => {
    const calendarApi = calendarData;
    calendarApi.unselect();
    const response = await updateScheduleRequest(dados, editID);
    if (response.status === 200) {
      if ('data' in response) {
        dispatch(applicationActions.updateGraphics());
        const update = calendarEvents.push(response?.data);
        setCalendarEvents(update);
        calendarApi.addEvent({
          id: uuid(),
          title: NameLimitter(data.client.name),
          start: handleTimeData.startStr,
          end: handleTimeData.endStr,
          allDay: handleTimeData.allDay
        });
        Swal.fire('Agendado', 'Agendamento atualizado com sucesso!', 'success');
        showRegisterModal(false);
      }
      setCalendarData('');
      setIsEdit(false);

      history.push('/');
      history.replace('/dashboard/agenda/agendamentos');
    } else {
      Swal.fire(
        'Agendamento',
        'Não foi possivel atualizar o agendamento!',
        'error'
      );
    }
  };

  const handleDeleteSchedule = async () => {
    const response = await deleteScheduleRequest(data._id);
    if (response.status === 204) {
      if ('data' in response) {
        Swal.fire({
          title: 'Você deseja cancelar este agendamento?',
          showDenyButton: true,
          showCancelButton: false,
          confirmButtonText: `Sim`,
          denyButtonText: `Não`
        }).then((result) => {
          if (result.isConfirmed) {
            holeEventData.event.remove();
            Swal.fire(
              'Agendamentos',
              'O agendamento foi cancelado com sucesso!',
              'success'
            );
          } else if (result.isDenied) {
            Swal.fire('Agendamento não cancelado', '', 'info');
          }
        });
        showRegisterModal(false);
      }
      setCalendarData('');
      setIsEdit(false);
    } else {
      Swal.fire(
        'Agendamento',
        'Não foi possivel atualizar o agendamento!',
        'error'
      );
    }
  };

  const handleEventClick = (clickInfo: EventClickArg) => {
    setIsEdit(true);
    const currentEdit = clickInfo.event._def.extendedProps;
    const currentRange = clickInfo.event._instance?.range;
    const currentCalendar = clickInfo.view.calendar;

    const range = {
      start: moment(currentRange?.start).add(3, 'hour').format(),
      end: moment(currentRange?.end).add(3, 'hour').format()
    };
    setCalendarData(currentCalendar);
    setData(currentEdit);
    setHoleEventData(clickInfo);
    setHandleTimeData(range);
    showRegisterModal(true);
  };

  const handleEvents = (events: EventApi[]) => {
    setCalendarEvents(events);
  };

  const renderEventContent = (eventContent: EventContentArg) => {
    return (
      <>
        {/* <b>{eventContent.timeText}</b> */}
        <b>{eventContent.event.title}</b>
      </>
    );
  };

  return (
    <ScheduleWrapper>
      <PrintWrapper>
        <ReactToPrint
          // eslint-disable-next-line react/button-has-type
          trigger={() => (
            <StyledButton>
              Imprimir Agenda <AiFillPrinter />
            </StyledButton>
          )}
          content={() => intervalRef.current}
        />
      </PrintWrapper>
      <div ref={intervalRef}>
        <FullCalendar
          // eslint-disable-next-line no-return-assign
          plugins={[
            dayGridPlugin,
            timeGridPlugin,
            interactionPlugin,
            adaptivePlugin
          ]}
          headerToolbar={{
            left: 'prev,next today',
            center: 'title',
            right: 'dayGridMonth,timeGridWeek,timeGridDay'
          }}
          slotLabelInterval={{
            minutes: 30
          }}
          slotLabelFormat={{
            hour: 'numeric',
            minute: '2-digit',
            hour12: false
          }}
          dayHeaderFormat={{
            weekday: 'short',
            month: 'numeric',
            day: 'numeric',
            omitCommas: true
          }}
          locale="pt-br"
          titleFormat={{ year: 'numeric', month: 'long' }}
          events={listOfEvents}
          initialView="timeGridWeek"
          editable
          selectable
          selectMirror
          dayMaxEvents
          initialEvents={listOfEvents}
          select={handleDateSelect}
          eventContent={renderEventContent}
          eventClick={handleEventClick}
          eventsSet={handleEvents}
          buttonText={{
            today: 'Hoje',
            month: 'Mensal',
            week: 'Semanal',
            day: 'Diario',
            list: 'Listagem'
          }}
        />
      </div>
      {registerModal && (
        <ModalRegisterSchedule
          dataEdit={data}
          isEditing={isEdit}
          rangeToEdit={handleTimeData}
          isVisible={registerModal}
          onDelete={handleDeleteSchedule}
          onRegisterNewEvent={(args: any) => {
            let payload;

            args.startTime = moment(
              `${args.dateStart} ${args.timeStart}`
            ).format();
            if (isEdit) {
              args.endTime = moment(
                moment(`${args.dateStart} ${args.timeEnd}`)
              ).format();
            } else {
              args.endTime = moment(
                moment(`${args.dateStart} ${args.timeEnd}`).subtract(
                  1,
                  'minute'
                )
              ).format();
            }

            if (moment(args.dateStart).isBefore(moment()) && isEdit) {
              payload = {
                client: args.client,
                status: args.status,
                hasReturn: args.hasReturn,
                startTime: args.startTime,
                endTime: args.endTime
              };
            } else {
              delete args.dateEnd;
              delete args.timeEnd;
              delete args.dateStart;
              delete args.timeStart;

              payload = { ...args };
            }
            if (user.role === ROLES.EMPLOYEE) payload.dentist = user.dentist;
            if (user.role === ROLES.DENTIST) payload.dentist = user._id;
            if (isEdit) {
              UpdateNewSchedule(payload, data._id);
            } else {
              AddNewSchedule(payload);
            }
          }}
          onCancel={() => showRegisterModal(false)}
        />
      )}
    </ScheduleWrapper>
  );
};

export default ScheduleList;
