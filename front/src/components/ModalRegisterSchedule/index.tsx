import React, { FC } from 'react';

import { Dialog, DialogContent, DialogTitle } from '@material-ui/core';

import CustomModalHeader from '../CustomModalHeader';
import RegisterSchedule from '../Form/RegisterSchedule';

interface Props {
  isVisible: boolean;
  onCancel: () => void;
  onRegisterNewEvent: (args: any) => void;
  onDelete: () => void;
  dataEdit: any;
  rangeToEdit: any;
  isEditing: boolean;
}

const ModalRegisterSchedule: FC<Props> = ({
  isVisible,
  onCancel,
  dataEdit,
  rangeToEdit,
  onRegisterNewEvent,
  onDelete,
  isEditing
}: Props) => {
  return (
    <Dialog
      open={isVisible}
      onClose={onCancel}
      aria-labelledby="modalRegister-title"
      aria-describedby="modalRegister-description"
    >
      <DialogTitle id="form-modalRegister">
        <CustomModalHeader title="Cadastrar agendamento" onClose={onCancel} />
      </DialogTitle>
      <DialogContent>
        <RegisterSchedule
          dataEdit={dataEdit}
          isEditing={isEditing}
          rangeToEdit={rangeToEdit}
          onDelete={onDelete}
          onClickSendButton={onRegisterNewEvent}
          isLoading={false}
        />
      </DialogContent>
    </Dialog>
  );
};

export default ModalRegisterSchedule;
