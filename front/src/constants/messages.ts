const MESSAGES = {
  CONFLICT: {
    title: 'Ops, não foi possível prosseguir com o cadastrado',
    description: 'Verifique se o usuário já não está presente no sistema!'
  },
  CONFLICT_PROFESSIONAL: {
    title: 'Ops, não foi possível prosseguir com o cadastrado',
    description: 'Verifique se o profissional já não está presente no sistema!'
  },
  CONFLICT_UPDATE_PROFESSIONAL: {
    title: 'Ops, não foi possível prosseguir com atualização',
    description: 'Verifique os dados do profissional!'
  },
  CONFLICT_EMPLOYEE: {
    title: 'Ops, não foi possível prosseguir com o cadastrado',
    description: 'Verifique se o funcionário já não está presente no sistema!'
  },
  CONFLICT_UPDATE_EMPLOYEE: {
    title: 'Ops, não foi possível prosseguir com atualização',
    description: 'Verifique se o funcionário os dados do profissional!'
  },

  CONFLICT_SERVICE: {
    title: 'Ops, não foi possível prosseguir com o cadastrado',
    description: 'Verifique se o serviço já não está presente no sistema!'
  },
  CONFLICT_BALANCE: {
    title: 'Ops, não foi possível prosseguir com atualização',
    description:
      'Verifique se o fluxo de caixa já não está presente no sistema!'
  },
  CONFLICT_STOCK: {
    title: 'Ops, não foi possível prosseguir com o cadastrado',
    description:
      'Verifique se o estoque em questão já não está presente no sistema!'
  },
  SUCCESS_REGISTER: {
    title: 'Sucesso',
    description: 'O usuário foi cadastrado no sistema!'
  },
  SUCCESS_UPDATE_PROFESSIONAL: {
    title: 'Sucesso',
    description: 'O usuário foi atualizado com êxito!'
  },
  SUCCESS_UPDATE_EMPLOYEE: {
    title: 'Sucesso',
    description: 'O funcionário foi atualizado com êxito!'
  },
  SUCCESS_PROFESSIONAL_REGISTER: {
    title: 'Sucesso',
    description: 'O profissional foi cadastrado no sistema!'
  },
  SUCCESS_EMPLOYEE_REGISTER: {
    title: 'Sucesso',
    description: 'O Funcionário foi cadastrado no sistema!'
  },
  SUCCESS_SERVICE_REGISTER: {
    title: 'Sucesso',
    description: 'O Fornecedor foi cadastrado no sistema!'
  },
  SUCCESS_STOCK_REGISTER: {
    title: 'Sucesso',
    description: 'Nova entrada de estoque cadastrada no sistema!'
  },
  SUCCESS_BALANCE_REGISTER: {
    title: 'Sucesso',
    description: 'Nova entrada de caixa registrada no sistema!'
  },
  INTERNAL_SERVER_REGISTER: {
    title: 'Ops, não foi possível prosseguir com o cadastrado.',
    description: 'Aguarde alguns minutos e tente novamente.'
  }
};

export default Object.freeze(MESSAGES);
