const GENDER = ['FEMININO', 'MASCULINO', 'NÃO INFORMADO'];

export default Object.freeze(GENDER);
