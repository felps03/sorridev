import GENDER from './gender';
import MESSAGES from './messages';
import ROLES from './roles';

const APP_API_ROOT = 'https://sorridev.ue.r.appspot.com';
const AUTH_API_ROOT = 'https://sorridev.ue.r.appspot.com';
const BASE_PATH = '/sorridev';

export { ROLES, GENDER, MESSAGES, APP_API_ROOT, AUTH_API_ROOT, BASE_PATH };
