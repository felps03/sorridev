const ROLES = {
  ADMIN: 'ADMIN',
  DENTIST: 'DENTIST',
  EMPLOYEE: 'EMPLOYEE',
  PROFESSIONAL: 'PROFESSIONAL',
  USER: 'USER'
};

export default Object.freeze(ROLES);
