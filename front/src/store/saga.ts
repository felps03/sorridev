import { all } from 'redux-saga/effects';

import { dashboardSaga } from './ducks/Dashboard';
import { scheduleSaga } from './ducks/Schedule';
import { userSaga } from './ducks/User/Authentication';
import { userRegisterSaga } from './ducks/User/Register';

// APLICAÇÃO SAGA
export function* rootSaga() {
  yield all([userSaga(), userRegisterSaga(), scheduleSaga(), dashboardSaga()]);
}
