/* eslint-disable no-param-reassign */
import { applyMiddleware, combineReducers, compose, createStore } from 'redux';
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import createSagaMiddleware from 'redux-saga';

import { Action } from '../interfaces';
import { applicationReducer } from './ducks/Application';
import { dashboardReducer } from './ducks/Dashboard';
import { scheduleReducer } from './ducks/Schedule';
import { userReducer } from './ducks/User/Authentication';
import { userRegisterReducer } from './ducks/User/Register';
import { rootSaga } from './saga';

const reducers = combineReducers({
  user: userReducer,
  schedule: scheduleReducer,
  register: userRegisterReducer,
  app: applicationReducer,
  dashboard: dashboardReducer
});

export const rootReducer = (state: any, action: Action) => {
  if (action.type === 'USER_LOGOUT') {
    localStorage.clear();
    return reducers(undefined, action);
  }

  return reducers(state, action);
};

const persistedReducer = persistReducer(
  {
    key: 'root',
    storage
  },
  rootReducer
);

const sagaMiddleware = createSagaMiddleware();

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export type RootState = ReturnType<typeof reducers>;
export const configureStore = () => {
  const store = createStore(
    persistedReducer,
    composeEnhancers(applyMiddleware(sagaMiddleware))
  );
  const persistor = persistStore(store);

  sagaMiddleware.run(rootSaga);

  return { store, persistor };
};
