import { Action } from '../../../../interfaces';
import { UserState } from '../../../../interfaces/User';
import { REGISTER_USER_REQUEST, REGISTER_USER_SUCCESS } from './action-types';

export const defaultUserState: UserState | any = {
  register: {
    isRegisterSuccess: false,
    isRegisterError: false,
    registerError: {},
    isLoading: false
  }
};

export function userRegisterReducer(
  state = defaultUserState.register,
  action: Action
): UserState {
  switch (action.type) {
    case REGISTER_USER_REQUEST: {
      return {
        ...state,
        isRegisterSuccess: false,
        isRegisterError: false,
        registerError: false,
        isLoading: true
      };
    }
    case REGISTER_USER_SUCCESS: {
      return {
        ...state,
        isRegisterSuccess: true,
        isRegisterError: false,
        registerError: false,
        isLoading: false
      };
    }
    default:
      return state;
  }
}
