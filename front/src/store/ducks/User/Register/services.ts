import { authApi } from '../../../../apis/auth-api';
import { RegisterValues } from '../../../../interfaces/User';

export const registerUser = async (register: RegisterValues): Promise<any> => {
  const fetch = authApi();

  return fetch.post('/user', register);
};
