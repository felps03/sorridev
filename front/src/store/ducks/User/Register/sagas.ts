import { takeLatest } from 'redux-saga/effects';

import { REGISTER_USER_REQUEST } from './action-types';
import { registerUser } from './operations';

export function* userRegisterSaga() {
  yield takeLatest(REGISTER_USER_REQUEST, registerUser);
}
