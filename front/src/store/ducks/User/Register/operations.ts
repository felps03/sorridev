import { call, put } from 'redux-saga/effects';

import { RegisterAction } from '../../../../interfaces/User';
import CallToaster from '../../../../utils/callToaster';
import { registerUserError, registerUserSuccess } from './actions';
import * as userServices from './services';

export function* registerUser(action: RegisterAction) {
  try {
    yield call(userServices.registerUser, action.payload);
    // eslint-disable-next-line no-console
    yield put(registerUserSuccess());
  } catch (error) {
    CallToaster({
      type: 'error',
      title: 'Ops, não foi possível realizar o cadastro.',
      autoClose: 2000
    });
    yield put(registerUserError(error.response.data || null));
  }
}
