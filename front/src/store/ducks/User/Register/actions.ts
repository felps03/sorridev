import { Action } from '../../../../interfaces';
import { IError } from '../../../../interfaces/Error';
import { RegisterValues } from '../../../../interfaces/User';
import {
  REGISTER_USER_REQUEST,
  REGISTER_USER_ERROR,
  REGISTER_USER_SUCCESS
} from './action-types';

/**
 * REGISTER USUÁRIO
 */
export const registerUserRequest = (registerValues: RegisterValues) => ({
  type: REGISTER_USER_REQUEST,
  payload: registerValues
});

export const registerUserError = (payload?: IError): Action => ({
  type: REGISTER_USER_ERROR,
  payload
});

export const registerUserSuccess = (): Action => ({
  type: REGISTER_USER_SUCCESS
});
