import jwtDecode from 'jwt-decode';
import { call, put } from 'redux-saga/effects';

import { LoginAction, UserJWT } from '../../../../interfaces/User';
import CallToaster from '../../../../utils/callToaster';
import getUserMenu from '../../../../utils/getUserMenu';
import { SetStorage } from '../../../../utils/handleLocalStorage';
import { loginUserError, loginUserSuccess } from './actions';
import * as userServices from './services';

export function* fetchLogin(action: LoginAction) {
  try {
    const response = yield call(userServices.loginUser, action.payload);

    SetStorage('accessToken', response.headers.token);
    const accessTokenDecode: UserJWT = jwtDecode(response.headers.token);
    const { data } = response;
    data.role = accessTokenDecode.owner;
    data.userMenu = getUserMenu(accessTokenDecode.owner);
    yield put(loginUserSuccess(data));
    CallToaster({
      type: 'success',
      title: 'Login Efetuado com sucesso!',
      autoClose: 2000
    });
  } catch (error) {
    CallToaster({
      type: 'error',
      title: 'Ops, não foi possível realizar o login.',
      autoClose: 2000
    });
    yield put(loginUserError(error.response.data || null));
  }
}

export function* fetchLoginByGoogle(action: LoginAction) {
  try {
    const response = yield call(userServices.loginByGoogle, action.payload);

    SetStorage('accessToken', response.headers.token);
    const accessTokenDecode: UserJWT = jwtDecode(response.headers.token);
    const { data } = response;
    data.role = accessTokenDecode.owner;
    data.userMenu = getUserMenu(accessTokenDecode.owner);
    yield put(loginUserSuccess(data));
    CallToaster({
      type: 'success',
      title: 'Login Efetuado com sucesso!',
      autoClose: 2000
    });
  } catch (error) {
    CallToaster({
      type: 'error',
      title: 'Ops, não foi possível realizar o login.',
      autoClose: 2000
    });
    yield put(loginUserError(error.response.data || null));
  }
}
