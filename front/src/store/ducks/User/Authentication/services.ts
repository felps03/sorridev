import { appApi } from '../../../../apis/app-api';
import { authApi } from '../../../../apis/auth-api';
import { LoginValues } from '../../../../interfaces/User';

export const loginUser = async (loginValues: LoginValues): Promise<any> => {
  const fetch = authApi();

  return fetch.post('/auth', loginValues);
};

export const resetPassword = async (data: any): Promise<any> => {
  const fetch = appApi();

  return fetch.patch('/auth/resetPassword', data);
};

export const loginByGoogle = async (register: any): Promise<any> => {
  const fetch = authApi();

  return fetch.post('/auth/google', register);
};
