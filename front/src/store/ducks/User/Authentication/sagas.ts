import { takeLatest } from 'redux-saga/effects';

import { LOGIN_USER_REQUEST, LOGIN_USER_GOOGLE_REQUEST } from './action-types';
import { fetchLogin, fetchLoginByGoogle } from './operations';

export function* userSaga() {
  yield takeLatest(LOGIN_USER_REQUEST, fetchLogin);
  yield takeLatest(LOGIN_USER_GOOGLE_REQUEST, fetchLoginByGoogle);
}
