export const LOGIN_USER_REQUEST = 'LOGIN_USER_REQUEST';
export const LOGIN_USER_ERROR = 'LOGIN_USER_ERROR';
export const LOGIN_USER_SUCCESS = 'LOGIN_USER_SUCCESS';

export const LOGIN_USER_GOOGLE_REQUEST = 'LOGIN_USER_GOOGLE_REQUEST';
export const LOGIN_USER_GOOGLE_ERROR = 'LOGIN_USER_GOOGLE_ERROR';
export const LOGIN_USER_GOOGLE_SUCCESS = 'LOGIN_USER_GOOGLE_SUCCESS';

export const SET_LOADING = 'SET_LOADING';
