import { Action } from '../../../../interfaces';
import { IError } from '../../../../interfaces/Error';
import { LoginValues, UserState } from '../../../../interfaces/User';
import {
  LOGIN_USER_REQUEST,
  LOGIN_USER_ERROR,
  LOGIN_USER_SUCCESS,
  LOGIN_USER_GOOGLE_ERROR,
  LOGIN_USER_GOOGLE_REQUEST,
  LOGIN_USER_GOOGLE_SUCCESS
} from './action-types';

/**
 * LOGIN USUÁRIO
 */
export const loginUserRequest = (loginValues: LoginValues) => ({
  type: LOGIN_USER_REQUEST,
  payload: loginValues
});

export const loginUserError = (payload?: IError): Action => ({
  type: LOGIN_USER_ERROR,
  payload
});

export const loginUserSuccess = (payload: UserState): Action => ({
  type: LOGIN_USER_SUCCESS,
  payload
});

export const loginByGoogleUserRequest = (loginValues: any) => ({
  type: LOGIN_USER_GOOGLE_REQUEST,
  payload: loginValues
});

export const loginByGoogleUserError = (payload?: IError): Action => ({
  type: LOGIN_USER_GOOGLE_ERROR,
  payload
});

export const loginByGoogleUserSuccess = (payload: UserState): Action => ({
  type: LOGIN_USER_GOOGLE_SUCCESS,
  payload
});
