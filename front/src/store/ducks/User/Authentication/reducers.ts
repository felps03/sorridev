import { Action } from '../../../../interfaces';
import { UserPayload, UserState } from '../../../../interfaces/User';
import {
  LOGIN_USER_REQUEST,
  LOGIN_USER_SUCCESS,
  LOGIN_USER_GOOGLE_REQUEST,
  LOGIN_USER_GOOGLE_SUCCESS
} from './action-types';

export const defaultUserState: UserState | any = {
  user: {
    _id: null,
    email: null,
    name: null,
    role: null,
    userMenu: [],
    photo: null,
    isAuthenticating: false,
    isAuthenticated: false,
    isLoginFailed: false,
    dentist: null
  }
};

export function userReducer(
  state = defaultUserState.user,
  action: Action
): UserState {
  switch (action.type) {
    case LOGIN_USER_GOOGLE_REQUEST:
    case LOGIN_USER_REQUEST: {
      return {
        ...state,
        isAuthenticating: true
      };
    }
    case LOGIN_USER_GOOGLE_SUCCESS:
    case LOGIN_USER_SUCCESS: {
      const { payload } = action as UserPayload;
      return {
        ...state,
        isAuthenticated: true,
        isLoginFailed: false,
        ...payload
      };
    }
    default:
      return state;
  }
}
