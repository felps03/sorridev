import { Action } from '../../../interfaces';
import {
  OPEN_MENU_MOBILE_LEFT,
  CLOSE_MENU_MOBILE_LEFT,
  UPDATE_GRAPHICS
} from './action-types';

export const defaultApplicationState = {
  isLeftMenuOpen: false,
  update: 0
};

export function applicationReducer(
  state = defaultApplicationState,
  action: Action
) {
  switch (action.type) {
    case OPEN_MENU_MOBILE_LEFT: {
      return {
        ...state,
        isLeftMenuOpen: true
      };
    }
    case CLOSE_MENU_MOBILE_LEFT: {
      return {
        ...state,
        isLeftMenuOpen: false
      };
    }
    case UPDATE_GRAPHICS: {
      return {
        ...state,
        update: state.update + 1
      };
    }
    default:
      return state;
  }
}
