import { Action } from '../../../interfaces';
import {
  OPEN_MENU_MOBILE_LEFT,
  CLOSE_MENU_MOBILE_LEFT,
  UPDATE_GRAPHICS
} from './action-types';

/**
 * APPLICATION FLOW
 */
export const openLeftMenuMobile = (): Action => ({
  type: OPEN_MENU_MOBILE_LEFT
});
export const closeLeftMenuMobile = (): Action => ({
  type: CLOSE_MENU_MOBILE_LEFT
});

export const updateGraphics = (): Action => ({
  type: UPDATE_GRAPHICS
});
