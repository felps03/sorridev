export const GET_SCHEDULE_COUNT = 'GET_SCHEDULE_COUNT';
export const GET_SCHEDULE_COUNT_SUCCESS = 'GET_SCHEDULE_COUNT_SUCCESS';
export const GET_SCHEDULE_COUNT_ERROR = 'GET_SCHEDULE_COUNT_ERROR';

export const GET_ATTENDANCE_COUNT = 'GET_ATTENDANCE_COUNT';
export const GET_ATTENDANCE_COUNT_SUCCESS = 'GET_ATTENDANCE_COUNT_SUCCESS';
export const GET_ATTENDANCE_COUNT_ERROR = 'GET_ATTENDANCE_COUNT_ERROR';

export const START_REQUESTING_ATTENDANCE_COUNT =
  'IS_REQUESTING_ATTENDANCE_COUNT';
export const FINISH_REQUESTING_ATTENDANCE_COUNT =
  'IS_REQUESTING_ATTENDANCE_COUNT';

export const START_REQUESTING_SCHEDULE_COUNT = 'IS_REQUESTING_SCHEDULE_COUNT';
export const FINISH_REQUESTING_SCHEDULE_COUNT = 'IS_REQUESTING_SCHEDULE_COUNT';
