import { takeLatest } from 'redux-saga/effects';

import { GET_ATTENDANCE_COUNT, GET_SCHEDULE_COUNT } from './action-types';
import { getScheduleCount, getAttendanceCount } from './operations';

export function* scheduleSaga() {
  yield takeLatest(GET_SCHEDULE_COUNT, getScheduleCount);
  yield takeLatest(GET_ATTENDANCE_COUNT, getAttendanceCount);
}
