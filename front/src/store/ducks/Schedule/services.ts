import { appApi } from '../../../apis/app-api';

export const fetchScheduleCount = async (): Promise<any> => {
  const fetch = appApi();

  return fetch.get('/schedule/servicecount');
};

export const fetchSAttendanceCount = async (): Promise<any> => {
  const fetch = appApi();

  return fetch.get('/user/dashboard/attendance');
};

export const fetchSchedule = async (): Promise<any> => {
  const fetch = appApi();

  return fetch.get('/schedule');
};

export const fetchScheduleById = async (id: string): Promise<any> => {
  const fetch = appApi();

  return fetch.get(`/schedule/${id}`);
};

export const fetchUpdateScheduleById = async (
  data: any,
  id: string
): Promise<any> => {
  const fetch = appApi();

  return fetch.put(`/schedule/${id}`, data);
};

export const fetchDeleteScheduleById = async (id: string): Promise<any> => {
  const fetch = appApi();

  return fetch.delete(`/schedule/${id}`);
};

export const fetchAddSchedule = async (data: any): Promise<any> => {
  const fetch = appApi();

  return fetch.post(`/schedule`, data);
};
