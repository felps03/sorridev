import { Action } from '../../../interfaces';
import { ScheduleState } from '../../../interfaces/Schedule';
import {
  GET_ATTENDANCE_COUNT,
  GET_ATTENDANCE_COUNT_ERROR,
  GET_ATTENDANCE_COUNT_SUCCESS,
  GET_SCHEDULE_COUNT,
  GET_SCHEDULE_COUNT_ERROR,
  GET_SCHEDULE_COUNT_SUCCESS
} from './action-types';

export const defaultUserState: ScheduleState | any = {
  schedule: {
    newUserThisMonth: 0,
    newUserLastMonth: 0,
    attendanceThisMonth: 0,
    attendanceLastMonth: 0,
    absenceThisMonth: 0,
    absenceLastMonth: 0,
    yesterday: 0,
    today: 0,
    tomorrow: 0,
    fetched: false,
    fetching: false,
    fetchError: false,
    error: {}
  }
};

export function scheduleReducer(
  state = defaultUserState.schedule,
  action: Action
): ScheduleState {
  switch (action.type) {
    case GET_SCHEDULE_COUNT: {
      return {
        ...state,
        fetched: false,
        fetching: true
      };
    }
    case GET_SCHEDULE_COUNT_SUCCESS: {
      const { payload } = action;
      return {
        ...state,
        fetched: true,
        fetching: false,
        fetchError: false,
        ...payload
      };
    }
    case GET_SCHEDULE_COUNT_ERROR: {
      const { payload } = action;
      return {
        ...state,
        fetched: false,
        fetching: false,
        fetchError: true,
        error: payload
      };
    }

    case GET_ATTENDANCE_COUNT: {
      return {
        ...state,
        fetched: false,
        fetching: true
      };
    }
    case GET_ATTENDANCE_COUNT_SUCCESS: {
      const { payload } = action;
      return {
        ...state,
        fetched: true,
        fetching: false,
        fetchError: false,
        ...payload
      };
    }
    case GET_ATTENDANCE_COUNT_ERROR: {
      const { payload } = action;
      return {
        ...state,
        fetched: false,
        fetching: false,
        fetchError: true,
        error: payload
      };
    }

    default:
      return state;
  }
}
