import { call, put } from 'redux-saga/effects';

import CallToaster from '../../../utils/callToaster';
import {
  fetchAttendanceCountError,
  fetchAttendanceCountSuccess,
  fetchScheduleCountError,
  fetchScheduleCountSuccess
} from './actions';
import { fetchScheduleCount, fetchSAttendanceCount } from './services';
/*
 * Fetch Schedule Count
 */
// export function* PeriodicGtScheduleCount() {
//   while (true) {
//     try {
//       const data = yield call(fetchScheduleCount);
//       yield put(fetchScheduleCountSuccess(data.data));
//       console.log('aqui');
//       yield delay(10000);
//     } catch (error) {
//       yield put(fetchScheduleCountError(error.response.data || null));
//     }
//   }
// }

export function* getScheduleCount() {
  try {
    const data = yield call(fetchScheduleCount);
    yield put(fetchAttendanceCountSuccess(data.data));
  } catch (error) {
    yield put(fetchScheduleCountError(error.response.data || null));
  }
}

export function* getAttendanceCount() {
  try {
    const data = yield call(fetchSAttendanceCount);
    yield put(fetchScheduleCountSuccess(data.data));
  } catch (error) {
    CallToaster({
      type: 'error',
      title: 'Error :(',
      description: 'Não foi possível buscar os dados dos atendimentos'
    });
    yield put(fetchAttendanceCountError(error.response.data || null));
  }
}
