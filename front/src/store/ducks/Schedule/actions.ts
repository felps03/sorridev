import { Action } from '../../../interfaces';
import { IError } from '../../../interfaces/Error';
import { ScheduleState } from '../../../interfaces/Schedule';
import {
  GET_SCHEDULE_COUNT,
  GET_SCHEDULE_COUNT_SUCCESS,
  GET_SCHEDULE_COUNT_ERROR,
  GET_ATTENDANCE_COUNT,
  GET_ATTENDANCE_COUNT_SUCCESS,
  GET_ATTENDANCE_COUNT_ERROR
} from './action-types';

export const fetchScheduleCount = (): Action => ({
  type: GET_SCHEDULE_COUNT
});
export const fetchScheduleCountSuccess = (payload: ScheduleState): Action => ({
  type: GET_SCHEDULE_COUNT_SUCCESS,
  payload
});
export const fetchScheduleCountError = (payload?: IError): Action => ({
  type: GET_SCHEDULE_COUNT_ERROR,
  payload
});

export const fetchAttendanceCount = (): Action => ({
  type: GET_ATTENDANCE_COUNT
});
export const fetchAttendanceCountSuccess = (
  payload: ScheduleState
): Action => ({
  type: GET_ATTENDANCE_COUNT_SUCCESS,
  payload
});
export const fetchAttendanceCountError = (payload?: IError): Action => ({
  type: GET_ATTENDANCE_COUNT_ERROR,
  payload
});
