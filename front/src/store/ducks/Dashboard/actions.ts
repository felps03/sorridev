import { Action } from '../../../interfaces';
import { DashboardState } from '../../../interfaces/Dashboard';
import { IError } from '../../../interfaces/Error';
import {
  GET_ANALYSIS_GENDER_ERROR,
  GET_ANALYSIS_GENDER_REQUEST,
  GET_ANALYSIS_GENDER_SUCCESS,
  GET_ANALYSIS_AGE_ERROR,
  GET_ANALYSIS_AGE_REQUEST,
  GET_ANALYSIS_AGE_SUCCESS,
  GET_ANALYSIS_MONTHLY_REQUEST,
  GET_ANALYSIS_MONTHLY_ERROR,
  GET_ANALYSIS_MONTHLY_SUCCESS,
  GET_ANALYSIS_MONTHLY_ATTENDANCE_REQUEST,
  GET_ANALYSIS_MONTHLY_ATTENDANCE_ERROR,
  GET_ANALYSIS_MONTHLY_ATTENDANCE_SUCCESS,
  GET_ANALYSIS_GENDER_NOT_CONTENT,
  GET_ANALYSIS_AGE_NOT_CONTENT,
  GET_ANALYSIS_MONTHLY_NOT_CONTENT,
  GET_ANALYSIS_MONTHLY_ATTENDANCE_NOT_CONTENT,
  GET_ANALYSIS_ABSENT_REQUEST,
  GET_ANALYSIS_ABSENT_ERROR,
  GET_ANALYSIS_ABSENT_SUCCESS,
  GET_ANALYSIS_ABSENT_NOT_CONTENT
} from './action-types';

export const genderAnalysisRequest = () => ({
  type: GET_ANALYSIS_GENDER_REQUEST
});

export const genderAnalysisError = (payload?: IError): Action => ({
  type: GET_ANALYSIS_GENDER_ERROR,
  payload
});

export const genderAnalysisSuccess = (payload: DashboardState): Action => ({
  type: GET_ANALYSIS_GENDER_SUCCESS,
  payload
});

export const ageAnalysisRequest = () => ({
  type: GET_ANALYSIS_AGE_REQUEST
});

export const ageAnalysisError = (payload?: IError): Action => ({
  type: GET_ANALYSIS_AGE_ERROR,
  payload
});

export const ageAnalysisSuccess = (payload: DashboardState): Action => ({
  type: GET_ANALYSIS_AGE_SUCCESS,
  payload
});

export const monthlyAnalysisRequest = () => ({
  type: GET_ANALYSIS_MONTHLY_REQUEST
});

export const monthlyAnalysisError = (payload?: IError): Action => ({
  type: GET_ANALYSIS_MONTHLY_ERROR,
  payload
});

export const monthlyAnalysisSuccess = (payload: DashboardState): Action => ({
  type: GET_ANALYSIS_MONTHLY_SUCCESS,
  payload
});

export const attendanceAnalysisRequest = () => ({
  type: GET_ANALYSIS_MONTHLY_ATTENDANCE_REQUEST
});

export const attendanceAnalysisError = (payload?: IError): Action => ({
  type: GET_ANALYSIS_MONTHLY_ATTENDANCE_ERROR,
  payload
});

export const attendanceAnalysisSuccess = (payload: DashboardState): Action => ({
  type: GET_ANALYSIS_MONTHLY_ATTENDANCE_SUCCESS,
  payload
});

export const genderAnalysisNoContent = (): Action => ({
  type: GET_ANALYSIS_GENDER_NOT_CONTENT
});

export const ageAnalysisNoContent = (): Action => ({
  type: GET_ANALYSIS_AGE_NOT_CONTENT
});

export const monthlyAnalysisNoContent = (): Action => ({
  type: GET_ANALYSIS_MONTHLY_NOT_CONTENT
});

export const attendanceAnalysisNoContent = (): Action => ({
  type: GET_ANALYSIS_MONTHLY_ATTENDANCE_NOT_CONTENT
});

export const absentAnalysisRequest = () => ({
  type: GET_ANALYSIS_ABSENT_REQUEST
});

export const absentAnalysisError = (payload?: IError): Action => ({
  type: GET_ANALYSIS_ABSENT_ERROR,
  payload
});

export const absentAnalysisSuccess = (payload: DashboardState): Action => ({
  type: GET_ANALYSIS_ABSENT_SUCCESS,
  payload
});
export const absentAnalysisNoContent = (): Action => ({
  type: GET_ANALYSIS_ABSENT_NOT_CONTENT
});
