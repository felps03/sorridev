import { Action } from '../../../interfaces';
import { DashboardState } from '../../../interfaces/Dashboard';
import {
  GET_ANALYSIS_GENDER_ERROR,
  GET_ANALYSIS_GENDER_REQUEST,
  GET_ANALYSIS_GENDER_SUCCESS,
  GET_ANALYSIS_AGE_ERROR,
  GET_ANALYSIS_AGE_REQUEST,
  GET_ANALYSIS_AGE_SUCCESS,
  GET_ANALYSIS_MONTHLY_ERROR,
  GET_ANALYSIS_MONTHLY_REQUEST,
  GET_ANALYSIS_MONTHLY_SUCCESS,
  GET_ANALYSIS_MONTHLY_ATTENDANCE_REQUEST,
  GET_ANALYSIS_MONTHLY_ATTENDANCE_ERROR,
  GET_ANALYSIS_MONTHLY_ATTENDANCE_SUCCESS,
  GET_ANALYSIS_AGE_NOT_CONTENT,
  GET_ANALYSIS_GENDER_NOT_CONTENT,
  GET_ANALYSIS_MONTHLY_ATTENDANCE_NOT_CONTENT,
  GET_ANALYSIS_MONTHLY_NOT_CONTENT,
  GET_ANALYSIS_ABSENT_NOT_CONTENT,
  GET_ANALYSIS_ABSENT_ERROR,
  GET_ANALYSIS_ABSENT_SUCCESS,
  GET_ANALYSIS_ABSENT_REQUEST
} from './action-types';

export const defaultUserState: DashboardState | any = {
  gender: {
    data: {
      male: 0,
      female: 0,
      empty: 0,
      total: 0
    },
    status: {
      isFetching: false,
      isFetchSuccess: false,
      isFetchError: false,
      error: {},
      isLoading: false,
      noContent: false
    }
  },
  age: {
    data: [],
    status: {
      isFetching: false,
      isFetchSuccess: false,
      isFetchError: false,
      error: {},
      isLoading: false,
      noContent: false
    }
  },
  monthly: {
    data: undefined,
    status: {
      isFetching: false,
      isFetchSuccess: false,
      isFetchError: false,
      error: {},
      isLoading: false,
      noContent: false
    }
  },
  attendance: {
    data: undefined,
    status: {
      isFetching: false,
      isFetchSuccess: false,
      isFetchError: false,
      error: {},
      isLoading: false,
      noContent: false
    }
  },
  absent: {
    data: undefined,
    status: {
      isFetching: false,
      isFetchSuccess: false,
      isFetchError: false,
      error: {},
      isLoading: false,
      noContent: false
    }
  }
};

export function dashboardReducer(
  state = defaultUserState,
  action: Action
): DashboardState {
  switch (action.type) {
    case GET_ANALYSIS_AGE_REQUEST:
      return {
        ...state,
        age: {
          isFetching: true,
          isFetchSuccess: false,
          isFetchError: false,
          error: {},
          isLoading: false,
          noContent: false
        }
      };
    case GET_ANALYSIS_GENDER_REQUEST: {
      return {
        ...state,
        gender: {
          isFetching: true,
          isFetchSuccess: false,
          isFetchError: false,
          error: {},
          isLoading: false,
          noContent: false
        }
      };
    }
    case GET_ANALYSIS_ABSENT_REQUEST: {
      return {
        ...state,
        absent: {
          isFetching: true,
          isFetchSuccess: false,
          isFetchError: false,
          error: {},
          isLoading: false,
          noContent: false
        }
      };
    }
    case GET_ANALYSIS_MONTHLY_ATTENDANCE_REQUEST: {
      return {
        ...state,
        attendance: {
          isFetching: true,
          isFetchSuccess: false,
          isFetchError: false,
          error: {},
          isLoading: false,
          noContent: false
        }
      };
    }
    case GET_ANALYSIS_MONTHLY_REQUEST: {
      return {
        ...state,
        monthly: {
          isFetching: true,
          isFetchSuccess: false,
          isFetchError: false,
          error: {},
          isLoading: false,
          noContent: false
        }
      };
    }
    case GET_ANALYSIS_GENDER_SUCCESS: {
      const { payload } = action;
      const data = payload;
      return {
        ...state,
        gender: {
          status: {
            isFetching: false,
            isFetchSuccess: true,
            isFetchError: false,
            error: {},
            isLoading: false,
            noContent: false
          },
          data
        }
      };
    }
    case GET_ANALYSIS_MONTHLY_SUCCESS: {
      const { payload } = action;
      const data = payload;
      return {
        ...state,
        monthly: {
          status: {
            isFetching: false,
            isFetchSuccess: true,
            isFetchError: false,
            error: {},
            isLoading: false,
            noContent: false
          },
          data
        }
      };
    }
    case GET_ANALYSIS_GENDER_ERROR: {
      return {
        ...state,
        gender: {
          status: {
            isFetching: false,
            isFetchSuccess: false,
            isFetchError: true,
            isLoading: false,
            error: action.payload,
            noContent: false
          }
        }
      };
    }
    case GET_ANALYSIS_MONTHLY_ERROR: {
      return {
        ...state,
        monthly: {
          status: {
            isFetching: false,
            isFetchSuccess: false,
            isFetchError: true,
            isLoading: false,
            error: action.payload,
            noContent: false
          }
        }
      };
    }
    case GET_ANALYSIS_MONTHLY_ATTENDANCE_ERROR: {
      return {
        ...state,
        attendance: {
          status: {
            isFetching: false,
            isFetchSuccess: false,
            isFetchError: true,
            isLoading: false,
            error: action.payload,
            noContent: false
          }
        }
      };
    }
    case GET_ANALYSIS_MONTHLY_ATTENDANCE_SUCCESS: {
      const { payload } = action;
      const data = payload;
      return {
        ...state,
        attendance: {
          status: {
            isFetching: false,
            isFetchSuccess: true,
            isFetchError: false,
            error: {},
            isLoading: false,
            noContent: false
          },
          data
        }
      };
    }
    case GET_ANALYSIS_AGE_SUCCESS: {
      const { payload } = action;
      const data = payload;
      return {
        ...state,
        age: {
          status: {
            isFetching: false,
            isFetchSuccess: true,
            isFetchError: false,
            error: {},
            isLoading: false,
            noContent: false
          },
          data
        }
      };
    }
    case GET_ANALYSIS_ABSENT_SUCCESS: {
      const { payload } = action;
      const data = payload;
      return {
        ...state,
        absent: {
          status: {
            isFetching: false,
            isFetchSuccess: true,
            isFetchError: false,
            error: {},
            isLoading: false,
            noContent: false
          },
          data
        }
      };
    }
    case GET_ANALYSIS_AGE_ERROR: {
      return {
        ...state,
        age: {
          status: {
            isFetching: false,
            isFetchSuccess: false,
            isFetchError: true,
            isLoading: false,
            error: action.payload,
            noContent: false
          }
        }
      };
    }
    case GET_ANALYSIS_ABSENT_ERROR: {
      return {
        ...state,
        absent: {
          status: {
            isFetching: false,
            isFetchSuccess: false,
            isFetchError: true,
            isLoading: false,
            error: action.payload,
            noContent: false
          }
        }
      };
    }
    case GET_ANALYSIS_GENDER_NOT_CONTENT: {
      return {
        ...state,
        gender: {
          status: {
            isFetchSuccess: true,
            noContent: true
          }
        }
      };
    }
    case GET_ANALYSIS_AGE_NOT_CONTENT: {
      return {
        ...state,
        age: {
          status: {
            isFetchSuccess: true,
            noContent: true
          }
        }
      };
    }
    case GET_ANALYSIS_MONTHLY_NOT_CONTENT: {
      return {
        ...state,
        monthly: {
          status: {
            isFetchSuccess: true,
            noContent: true
          }
        }
      };
    }
    case GET_ANALYSIS_MONTHLY_ATTENDANCE_NOT_CONTENT: {
      return {
        ...state,
        attendance: {
          status: {
            isFetchSuccess: true,
            noContent: true
          }
        }
      };
    }
    case GET_ANALYSIS_ABSENT_NOT_CONTENT: {
      return {
        ...state,
        absent: {
          status: {
            isFetchSuccess: true,
            noContent: true
          }
        }
      };
    }
    default:
      return state;
  }
}
