import { takeLatest } from 'redux-saga/effects';

import {
  GET_ANALYSIS_GENDER_REQUEST,
  GET_ANALYSIS_AGE_REQUEST,
  GET_ANALYSIS_MONTHLY_REQUEST,
  GET_ANALYSIS_MONTHLY_ATTENDANCE_REQUEST,
  GET_ANALYSIS_ABSENT_REQUEST
} from './action-types';
import {
  FetchGenderAnalysis,
  FetchAgeAnalysis,
  FetchMonthlyAnalysis,
  FetchAttendanceAnalysis,
  FetchAbsentAnalysis
} from './operations';

export function* dashboardSaga() {
  yield takeLatest(GET_ANALYSIS_GENDER_REQUEST, FetchGenderAnalysis);
  yield takeLatest(GET_ANALYSIS_AGE_REQUEST, FetchAgeAnalysis);
  yield takeLatest(GET_ANALYSIS_MONTHLY_REQUEST, FetchMonthlyAnalysis);
  yield takeLatest(
    GET_ANALYSIS_MONTHLY_ATTENDANCE_REQUEST,
    FetchAttendanceAnalysis
  );
  yield takeLatest(GET_ANALYSIS_ABSENT_REQUEST, FetchAbsentAnalysis);
}
