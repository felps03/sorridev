import { handleSuccessResponse, handleErrorResponse } from '../../../apis';
import { appApi, fetchCEP } from '../../../apis/app-api';
import { RegisterDashValues } from '../../../interfaces/User';

export const fetchGenderAnalysis = async (): Promise<any> => {
  const fetch = appApi();

  return fetch.get('/user/analysis/gender');
};
export const fetchAgeAnalysis = async (): Promise<any> => {
  const fetch = appApi();

  return fetch.get('/user/analysis/age');
};

export const fetchMonthlyAnalysis = async (): Promise<any> => {
  const fetch = appApi();

  return fetch.get('/user/dashboard');
};

export const fetchAttendanceAnalysis = async (): Promise<any> => {
  const fetch = appApi();

  return fetch.get('/schedule/dashboard');
};

export const fetchAbsentAnalysis = async (): Promise<any> => {
  const fetch = appApi();

  return fetch.get('/schedule/dashboard?status=ausente');
};

export const RegisterUser = async (data: RegisterDashValues): Promise<any> => {
  try {
    const response = await appApi().post('/user', data);
    return handleSuccessResponse(response);
  } catch (error) {
    return handleErrorResponse(error);
  }
};

export const SearchForCEP = async (cep: string): Promise<any> => {
  try {
    const response = await fetchCEP().get(`/${cep}`);
    return handleSuccessResponse(response);
  } catch (error) {
    return handleErrorResponse(error);
  }
};

export const RegisterProfessionalRequest = async (data: any): Promise<any> => {
  try {
    const response = await appApi().post(`/professional`, data);
    return handleSuccessResponse(response);
  } catch (error) {
    return handleErrorResponse(error);
  }
};

export const UpdateProfessionalRequest = async (
  id: string,
  data: any
): Promise<any> => {
  try {
    const response = await appApi().put(`/professional/${id}`, data);
    return handleSuccessResponse(response);
  } catch (error) {
    return handleErrorResponse(error);
  }
};
export const DeleteProfessionalRequest = async (id: string): Promise<any> => {
  try {
    const response = await appApi().delete(`/professional/${id}`);
    return handleSuccessResponse(response);
  } catch (error) {
    return handleErrorResponse(error);
  }
};
export const DeleteEmployeeRequest = async (id: string): Promise<any> => {
  try {
    const response = await appApi().delete(`/employee/${id}`);
    return handleSuccessResponse(response);
  } catch (error) {
    return handleErrorResponse(error);
  }
};

export const UpdatePatientRequest = async (
  id: string,
  data: any
): Promise<any> => {
  try {
    const response = await appApi().put(`/user/${id}`, data);
    return handleSuccessResponse(response);
  } catch (error) {
    return handleErrorResponse(error);
  }
};

export const DeletePatientRequest = async (id: string): Promise<any> => {
  try {
    const response = await appApi().delete(`/user/${id}`);
    return handleSuccessResponse(response);
  } catch (error) {
    return handleErrorResponse(error);
  }
};

export const DeleteServiceRequest = async (id: string): Promise<any> => {
  try {
    const response = await appApi().delete(`/service/${id}`);
    return handleSuccessResponse(response);
  } catch (error) {
    return handleErrorResponse(error);
  }
};

export const DeleteStockRequest = async (id: string): Promise<any> => {
  try {
    const response = await appApi().delete(`/stock/${id}`);
    return handleSuccessResponse(response);
  } catch (error) {
    return handleErrorResponse(error);
  }
};

export const DeleteBalanceRequest = async (id: string): Promise<any> => {
  try {
    const response = await appApi().delete(`/balance/${id}`);
    return handleSuccessResponse(response);
  } catch (error) {
    return handleErrorResponse(error);
  }
};

export const UpdateEmployeeRequest = async (
  id: string,
  data: any
): Promise<any> => {
  try {
    const response = await appApi().put(`/employee/${id}`, data);
    return handleSuccessResponse(response);
  } catch (error) {
    return handleErrorResponse(error);
  }
};

export const UpdateServiceRequest = async (
  id: string,
  data: any
): Promise<any> => {
  try {
    const response = await appApi().put(`/service/${id}`, data);
    return handleSuccessResponse(response);
  } catch (error) {
    return handleErrorResponse(error);
  }
};
export const UpdateStockRequest = async (
  id: string,
  data: any
): Promise<any> => {
  try {
    const response = await appApi().put(`/stock/${id}`, data);
    return handleSuccessResponse(response);
  } catch (error) {
    return handleErrorResponse(error);
  }
};
export const UpdateBalanceRequest = async (
  id: string,
  data: any
): Promise<any> => {
  try {
    const response = await appApi().put(`/balance/${id}`, data);
    return handleSuccessResponse(response);
  } catch (error) {
    return handleErrorResponse(error);
  }
};

export const RegisterEmployeeRequest = async (data: any): Promise<any> => {
  try {
    const response = await appApi().post(`/employee`, data);
    return handleSuccessResponse(response);
  } catch (error) {
    return handleErrorResponse(error);
  }
};

export const RegisterServiceRequest = async (data: any): Promise<any> => {
  try {
    const response = await appApi().post(`/service`, data);
    return handleSuccessResponse(response);
  } catch (error) {
    return handleErrorResponse(error);
  }
};

export const RegisterStockRequest = async (data: any): Promise<any> => {
  try {
    const response = await appApi().post(`/stock`, data);
    return handleSuccessResponse(response);
  } catch (error) {
    return handleErrorResponse(error);
  }
};

export const RegisterBalanceRequest = async (data: any): Promise<any> => {
  try {
    const response = await appApi().post(`/balance`, data);
    return handleSuccessResponse(response);
  } catch (error) {
    return handleErrorResponse(error);
  }
};
