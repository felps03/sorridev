/* eslint-disable no-console */
import { call, put } from 'redux-saga/effects';

import CallToaster from '../../../utils/callToaster';
import {
  genderAnalysisSuccess,
  genderAnalysisError,
  ageAnalysisSuccess,
  ageAnalysisError,
  monthlyAnalysisSuccess,
  monthlyAnalysisError,
  attendanceAnalysisSuccess,
  attendanceAnalysisError,
  genderAnalysisNoContent,
  ageAnalysisNoContent,
  monthlyAnalysisNoContent,
  attendanceAnalysisNoContent,
  absentAnalysisNoContent,
  absentAnalysisSuccess,
  absentAnalysisError
} from './actions';
import * as analysisService from './services';

export function* FetchGenderAnalysis() {
  try {
    const response = yield call(analysisService.fetchGenderAnalysis);
    if (response.status === 204) yield put(genderAnalysisNoContent());
    if (response.status === 200)
      yield put(genderAnalysisSuccess(response.data));
  } catch (error) {
    CallToaster({
      type: 'error',
      title: 'Ops, não foi possível obter os dados dos gráficos de gênero.',
      autoClose: 2000
    });
    yield put(genderAnalysisError(error.response.data || null));
  }
}

export function* FetchAgeAnalysis() {
  try {
    const response = yield call(analysisService.fetchAgeAnalysis);
    if (response.status === 204) yield put(ageAnalysisNoContent());
    if (response.status === 200) yield put(ageAnalysisSuccess(response.data));
  } catch (error) {
    CallToaster({
      type: 'error',
      title: 'Ops, não foi possível obter os dados dos gráficos de idade.',
      autoClose: 2000
    });
    yield put(ageAnalysisError(error.response.data || null));
  }
}

export function* FetchMonthlyAnalysis() {
  try {
    const response = yield call(analysisService.fetchMonthlyAnalysis);
    if (response.status === 204) yield put(monthlyAnalysisNoContent());
    if (response.status === 200)
      yield put(monthlyAnalysisSuccess(response.data));
  } catch (error) {
    CallToaster({
      type: 'error',
      title: 'Ops, não foi possível obter os dados dos gráficos dos meses.',
      autoClose: 2000
    });
    yield put(monthlyAnalysisError(error.response.data || null));
  }
}

export function* FetchAttendanceAnalysis() {
  try {
    const response = yield call(analysisService.fetchAttendanceAnalysis);
    if (response.status === 204) yield put(attendanceAnalysisNoContent());
    if (response.status === 200)
      yield put(attendanceAnalysisSuccess(response.data));
  } catch (error) {
    CallToaster({
      type: 'error',
      title:
        'Ops, não foi possível obter os dados dos gráficos dos atendimentos.',
      autoClose: 2000
    });
    yield put(attendanceAnalysisError(error.response.data || null));
  }
}
export function* FetchAbsentAnalysis() {
  try {
    const response = yield call(analysisService.fetchAbsentAnalysis);
    if (response.status === 204) yield put(absentAnalysisNoContent());
    if (response.status === 200)
      yield put(absentAnalysisSuccess(response.data));
  } catch (error) {
    CallToaster({
      type: 'error',
      title:
        'Ops, não foi possível obter os dados dos gráficos dos atendimentos.',
      autoClose: 2000
    });
    yield put(absentAnalysisError(error.response.data || null));
  }
}
