/* eslint-disable array-callback-return */
import { Menu } from '../data';

const getUserMenu = (userRole: string) => {
  // eslint-disable-next-line consistent-return
  return Menu.map((element: any) => {
    if (element.permission.includes(userRole)) {
      if ('children' in element) {
        return {
          ...{ element },
          children: element.children.filter((subElement: any) =>
            subElement.permission.includes(userRole)
          )
        };
      }
      return { ...element };
    }
  }).filter((el) => {
    return el != null;
  });
};

export default getUserMenu;
