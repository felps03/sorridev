const formatToMoney = (value: any) => {
  if (typeof value === 'string') {
    return value && value !== ''
      ? parseFloat(value).toLocaleString('pt-BR', {
          maximumFractionDigits: 2,
          minimumFractionDigits: 2
        })
      : '0,00';
  }
  return value && typeof value !== 'string' && value !== ''
    ? value.toLocaleString('pt-BR', {
        maximumFractionDigits: 2,
        minimumFractionDigits: 2
      })
    : '0,00';
};

export { formatToMoney };
