import React from 'react';

import { toast } from 'react-toastify';

import ToastMessage from '../components/ToastMessage';

interface IToaster {
  type: 'success' | 'error' | 'info' | 'warning';
  title: string;
  description?: string;
  autoClose?: number;
}

const CallToaster = ({ type, title, description, autoClose }: IToaster) => {
  toast[type](
    <ToastMessage type={type} title={title} description={description || ''} />,
    {
      position: 'top-right',
      autoClose: autoClose || 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined
    }
  );
};

export default CallToaster;
