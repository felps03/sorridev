const GetFirstLetter = (word: string) => {
  return word.charAt(0).toUpperCase();
};

const NameLimitter = (word: string, limitter?: number) => {
  const currentName = word
    .split(' ')
    .slice(0, limitter || 2)
    .join(' ');

  if (currentName.length > 15) {
    const nameSplit = currentName.split(' ');
    nameSplit[1] = GetFirstLetter(nameSplit[1]).concat('.');
    return nameSplit.join(' ');
  }
  return currentName;
};

const ToPercentage = (value: number) => {
  return (value / 50) * 100;
};

const CapitalizeString = (word: string) => {
  return word.charAt(0).toUpperCase() + word.slice(1).toLowerCase();
};

export { NameLimitter, GetFirstLetter, ToPercentage, CapitalizeString };
