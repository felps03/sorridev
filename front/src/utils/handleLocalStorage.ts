/* eslint-disable @typescript-eslint/no-explicit-any */
export const GetStorage = (key: string): string => {
  return JSON.parse(localStorage.getItem(key) || '');
};

export const SetStorage = (key: string, value?: any): void => {
  if (value) {
    localStorage.setItem(key, JSON.stringify(value));
  } else {
    localStorage.removeItem(key);
  }
};
