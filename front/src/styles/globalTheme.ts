import { createGlobalStyle } from 'styled-components';
import 'react-toastify/dist/ReactToastify.css';
import 'react-perfect-scrollbar/dist/css/styles.css';

export default createGlobalStyle`
  * {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
    outline: 0;
  }
  body {
    background: ${(props) => props.theme.colors.background};
    color: ${(props) => props.theme.colors.text};
    -webkit-font-smoothing: antialiased;
    transition: all 1s;
  }
  body, input, button {
    font-family: ${(props) => props.theme.font.family};
    font-size: ${(props) => props.theme.font.size.normal};
  }
  h1, h2, h3, h4, h5, h6, strong {
    font-weight: 500;
  }
  button{
    cursor: pointer;
  }
  body.modal-open{
    overflow: hidden;
    padding-right: 0px !important;
  }
  .no-background {
    background: transparent;
  }

  .custom-input{
    && {
      margin-bottom: 15px;
      border: none;

      .MuiOutlinedInput-input {
        background-color: #f5f5f5;
        box-shadow: 0px 14px 9px -15px rgba(0, 0, 0, 0.25);
        border-radius: 8px;
      }
      .MuiFormLabel-root.Mui-focused,
      .MuiInputLabel-outlined.MuiInputLabel-shrink {
        color: #4477eef2;
        font-size: ${(props) => props.theme.font.size.medium};
        font-weight: ${(props) => props.theme.font.weight.bold};
      }

      .MuiFormLabel-root.Mui-error {
        color: ${(props) => props.theme.colors.error};
      }
    }
  }

  .custom-button{
    height: 54px;
    float: right;
    margin-top: 1rem;
    max-width: 317px;
    border: none;
    padding-right: 10px;
    box-shadow: 0px 14px 9px -15px rgba(0, 0, 0, 0.25);
    border-radius: 8px;
    background-color: ${(props) => props.theme.colors.primary};
    font-size: ${(props) => props.theme.font.size.medium};
    color: #fff;
    font-weight: ${(props) => props.theme.font.weight.regular};
    cursor: pointer;
    text-transform: capitalize;
    transition: all 0.2s ease-in;
    > span > svg {
      margin-left: 10px;
    }
    &:hover {
      background-color: rgba(255, 177, 103, 0.8);
      transform: translateY(-3px);
    }
  }

  .swal2-container.swal2-center{
    z-index: 1300;
  }

  .fc-license-message{
    display: none;
  }
  .fc-button-group,
  .fc-today-button{
    padding: 0.2em 0.2em !important;
  }
`;
