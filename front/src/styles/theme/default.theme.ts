import Logo from '../../assets/icons/logo.svg';

export default {
  logo: `${Logo}`,

  font: {
    family: "'Poppins', sans-serif",
    size: {
      extraSmall: '12px',
      small: '14px',
      normal: '16px',
      medium: '18px',
      large: '22px',
      extraLarge: '24px'
    },
    weight: {
      thin: 100,
      extraLight: 200,
      light: 300,
      regular: 400,
      medium: 500,
      semiBold: 600,
      bold: 700,
      black: 900
    },
    style: {
      italic: 'italic'
    },
    transform: {
      uppercase: 'uppercase',
      none: 'none',
      capitalize: 'capitalize',
      lowercase: 'lowercase',
      initial: 'initial',
      inherit: 'inherit'
    }
  },
  colors: {
    primary: '#FFB167',
    secondary: '#4A7FFB',
    error: '#e74c3c',
    success: '#07bc0c',
    warning: '#f1c40f',

    background: '#E5E5E5',
    text: '#666666',
    textSecondaryColor: '#8DA1B5',
    textDark: '#808080',
    inputColor: '#EBEBEB',
    black: '#444444'
  }
};
