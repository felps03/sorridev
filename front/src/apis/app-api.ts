import axios, { AxiosRequestConfig } from 'axios';

import { APP_API_ROOT } from '../constants';
import { GetStorage } from '../utils/handleLocalStorage';

export const appApi = () => {
  const accessToken = GetStorage('accessToken');
  const config: AxiosRequestConfig = {
    baseURL: APP_API_ROOT,
    ...(accessToken && {
      headers: {
        authorization: `Bearer ${accessToken}`
      }
    })
  };

  const instance = axios.create(config);

  return instance;
};

export const fetchCEP = () => {
  const config: AxiosRequestConfig = {
    baseURL: 'https://brasilapi.com.br/api/cep/v1'
  };

  const instance = axios.create(config);

  return instance;
};
