import axios, { AxiosRequestConfig } from 'axios';

import { AUTH_API_ROOT } from '../constants';

export const authApi = (accessToken?: string) => {
  const config: AxiosRequestConfig = {
    baseURL: AUTH_API_ROOT
  };

  if (accessToken) {
    config.headers.authorization = `Bearer ${accessToken}`;
  }

  const instance = axios.create(config);

  return instance;
};
