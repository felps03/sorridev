export * from './auth-api';

const getDefaultsResponse = (result: any) => ({
  hasError: result.isAxiosError || false,
  status:
    result.request?.status ||
    result.response?.status ||
    result.request?.statusCode ||
    result.response?.statusCode
});

export const handleErrorResponse = (result: any) => {
  const shortData = result.response?.data;
  const message = shortData?.error || shortData?.message || null;
  return { ...getDefaultsResponse(result), message };
};

export const handleSuccessResponse = (result: any) => ({
  ...getDefaultsResponse(result),
  data: result.data || null,
  hasError: !result.data
});
