import 'styled-components';

declare module 'styled-components' {
  export interface DefaultTheme {
    logo: string;

    font: {
      family: string;
      size: {
        extraSmall: string;
        small: string;
        normal: string;
        medium: string;
        large: string;
        extraLarge: string;
      };
      weight: {
        thin: number;
        extraLight: number;
        light: number;
        regular: number;
        medium: number;
        semiBold: number;
        bold: number;
        black: number;
      };
      style: {
        italic: string;
      };
      transform: {
        uppercase: string;
        none: string;
        capitalize: string;
        lowercase: string;
        initial: string;
        inherit: string;
      };
    };
    colors: {
      primary: string;
      secondary: string;
      error: string;
      success: string;
      warning: string;
      textSecondaryColor: string;
      background: string;
      text: string;
      textDark: string;
      inputColor: string;
      black: string;
    };
  }
}
