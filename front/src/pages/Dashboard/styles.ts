import styled from 'styled-components';

const Wrapper = styled.section`
  margin-left: 280px;
  &.main {
    margin-left: 280px;
    margin-right: 300px;
  }
  @media screen and (max-width: 960px) {
    &.main {
      margin-left: 0px;
      margin-right: 0px;
    }
  }
`;

export { Wrapper };
