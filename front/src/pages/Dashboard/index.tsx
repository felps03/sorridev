import React, { useEffect } from 'react';

import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useDispatch, useSelector } from 'react-redux';
import { Switch, useHistory, useLocation } from 'react-router-dom';

import CustomBreadcrumbs from '../../components/Breadcrumb';
import DashboardCharts from '../../components/DashboardCharts';
import Header from '../../components/Header';
import { SidebarLeft, SidebarRight } from '../../components/Layout/Sidebar';
import Navbar from '../../components/Navbar';
import { ROLES } from '../../constants';
import { ProtectedRoute } from '../../routes/permissions-route';
import { RootState } from '../../store/configure-store';
import { applicationActions } from '../../store/ducks/Application';
import { dashboardActions } from '../../store/ducks/Dashboard';
import { scheduleActions } from '../../store/ducks/Schedule';
import EditBalance from '../Balance/Edit';
import ListBalance from '../Balance/List';
import RegisterBalance from '../Balance/Register';
import RemoveBalance from '../Balance/Remove';
import ResetPassword from '../Configurations/UpdatePassword';
import EditEmployee from '../Employee/Edit';
import ListEmployees from '../Employee/List';
import RegisterEmployee from '../Employee/Register';
import RemoveEmployee from '../Employee/Remove';
import EditPatients from '../Patients/Edit';
import ListPatients from '../Patients/List';
import RegisterPatient from '../Patients/Register';
import RemovePatients from '../Patients/Remove';
import EditProfessionals from '../Professional/Edit';
import ListProfessionals from '../Professional/List';
import RegisterProfessionals from '../Professional/Register';
import RemoveProfessionals from '../Professional/Remove';
import ListSchedule from '../Schedule/List';
import EditService from '../Service/Edit';
import ListService from '../Service/List';
import RegisterService from '../Service/Register';
import RemoveService from '../Service/Remove';
import EditStock from '../Stock/Edit';
import ListStock from '../Stock/List';
import RegisterStock from '../Stock/Register';
import RemoveStock from '../Stock/Remove';
import { Wrapper } from './styles';

const Dashboard = () => {
  const dispatch = useDispatch();
  const history = useHistory();
  const location = useLocation();

  const user = useSelector((state: RootState) => state.user);
  const UpdateOnApp = useSelector((state: RootState) => state.app.update);

  const matches = useMediaQuery('(max-width:960px)');
  useEffect(() => {
    if (location.pathname === '/') {
      if (user.role === ROLES.USER) {
        history.push('/dashboard/agenda/agendamentos');
      } else {
        history.push('/dashboard');
      }
    }
    if (location.pathname === '/dashboard') {
      if (user.role === ROLES.USER) {
        history.push('/dashboard/agenda/agendamentos');
      }
    }
  }, [history, location.pathname, user.role]);

  useEffect(() => {
    if (!matches) dispatch(applicationActions.closeLeftMenuMobile());
    if (![ROLES.ADMIN, ROLES.USER].includes(user.role)) {
      dispatch(dashboardActions.genderAnalysisRequest());
      dispatch(dashboardActions.ageAnalysisRequest());
      dispatch(dashboardActions.monthlyAnalysisRequest());
      dispatch(dashboardActions.attendanceAnalysisRequest());
      dispatch(dashboardActions.absentAnalysisRequest());
      dispatch(scheduleActions.fetchScheduleCount());
      dispatch(scheduleActions.fetchAttendanceCount());
    }
  }, [dispatch, matches, user, UpdateOnApp]);

  return (
    <>
      <SidebarLeft />
      <Wrapper className={user.role === 'USER' ? '' : 'main'}>
        <Navbar />
        {['/dashboard'].includes(location.pathname) &&
        !['ADMIN', 'USER'].includes(user.role) ? (
          <>
            <Header />
            <DashboardCharts />
          </>
        ) : (
          <CustomBreadcrumbs />
        )}

        <section
          className="main-content"
          style={{ marginTop: '-4.5rem', marginBottom: '4.5rem' }}
        >
          <Switch>
            <ProtectedRoute
              path="/dashboard/pacientes/lista"
              component={ListPatients}
              requiredRole={['DENTIST', 'EMPLOYEE', 'ADMIN']}
            />
            <ProtectedRoute
              path="/dashboard/pacientes/criar"
              component={RegisterPatient}
              requiredRole={['DENTIST', 'EMPLOYEE', 'ADMIN']}
            />
            <ProtectedRoute
              path="/dashboard/estoque/lista"
              component={ListStock}
              requiredRole={['DENTIST', 'EMPLOYEE', 'ADMIN']}
            />
            <ProtectedRoute
              path="/dashboard/financeiro/lista"
              component={ListBalance}
              requiredRole={['DENTIST', 'EMPLOYEE', 'ADMIN']}
            />
            <ProtectedRoute
              path="/dashboard/profissionais/lista"
              component={ListProfessionals}
              requiredRole={['ADMIN']}
            />
            <ProtectedRoute
              path="/dashboard/funcionario/lista"
              component={ListEmployees}
              requiredRole={['ADMIN', 'DENTIST']}
            />
            <ProtectedRoute
              path="/dashboard/profissionais/adicionar"
              component={RegisterProfessionals}
              requiredRole={['ADMIN']}
            />
            <ProtectedRoute
              path="/dashboard/funcionario/adicionar"
              component={RegisterEmployee}
              requiredRole={['ADMIN', 'DENTIST']}
            />
            <ProtectedRoute
              path="/dashboard/servicos/lista"
              component={ListService}
              requiredRole={['ADMIN', 'DENTIST', 'EMPLOYEE']}
            />

            <ProtectedRoute
              path="/dashboard/financeiro/cadastrar"
              component={RegisterBalance}
              requiredRole={['ADMIN', 'DENTIST', 'EMPLOYEE']}
            />
            <ProtectedRoute
              path="/dashboard/servicos/cadastrar"
              component={RegisterService}
              requiredRole={['ADMIN', 'DENTIST', 'EMPLOYEE']}
            />
            <ProtectedRoute
              path="/dashboard/estoque/cadastrar"
              component={RegisterStock}
              requiredRole={['ADMIN', 'DENTIST', 'EMPLOYEE']}
            />
            <ProtectedRoute
              path="/dashboard/profissionais/editar"
              component={EditProfessionals}
              requiredRole={['ADMIN']}
            />
            <ProtectedRoute
              path="/dashboard/funcionario/editar"
              component={EditEmployee}
              requiredRole={['ADMIN', 'DENTIST', 'EMPLOYEE']}
            />
            <ProtectedRoute
              path="/dashboard/pacientes/editar"
              component={EditPatients}
              requiredRole={['ADMIN', 'DENTIST', 'EMPLOYEE']}
            />
            <ProtectedRoute
              path="/dashboard/servicos/editar"
              component={EditService}
              requiredRole={['ADMIN', 'DENTIST', 'EMPLOYEE']}
            />

            <ProtectedRoute
              path="/dashboard/estoque/editar"
              component={EditStock}
              requiredRole={['ADMIN', 'DENTIST', 'EMPLOYEE']}
            />
            <ProtectedRoute
              path="/dashboard/financeiro/editar"
              component={EditBalance}
              requiredRole={['ADMIN', 'DENTIST', 'EMPLOYEE']}
            />
            <ProtectedRoute
              path="/dashboard/agenda/agendamentos"
              component={ListSchedule}
              requiredRole={['ADMIN', 'DENTIST', 'EMPLOYEE', 'USER']}
            />

            <ProtectedRoute
              path="/dashboard/pacientes/excluir"
              component={RemovePatients}
              requiredRole={['ADMIN', 'DENTIST', 'EMPLOYEE']}
            />
            <ProtectedRoute
              path="/dashboard/profissionais/excluir"
              component={RemoveProfessionals}
              requiredRole={['ADMIN']}
            />
            <ProtectedRoute
              path="/dashboard/funcionario/excluir"
              component={RemoveEmployee}
              requiredRole={['ADMIN', 'DENTIST']}
            />
            <ProtectedRoute
              path="/dashboard/servicos/excluir"
              component={RemoveService}
              requiredRole={['ADMIN', 'DENTIST', 'EMPLOYEE']}
            />
            <ProtectedRoute
              path="/dashboard/estoque/excluir"
              component={RemoveStock}
              requiredRole={['ADMIN', 'DENTIST', 'EMPLOYEE']}
            />
            <ProtectedRoute
              path="/dashboard/financeiro/excluir"
              component={RemoveBalance}
              requiredRole={['ADMIN', 'DENTIST', 'EMPLOYEE']}
            />
            <ProtectedRoute
              path="/dashboard/configurar"
              component={ResetPassword}
              requiredRole={['ADMIN', 'DENTIST', 'EMPLOYEE', 'USER']}
            />
          </Switch>
        </section>
        {user.role === 'USER' ? null : <SidebarRight />}
      </Wrapper>
    </>
  );
};

export default Dashboard;
