import React, { useState } from 'react';

import { Card } from '@material-ui/core';
import { FaExchangeAlt } from 'react-icons/fa';
import { useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import Swal from 'sweetalert2';

import ContentBlock from '../../../components/ContentBlock';
import CustomDescription from '../../../components/CustomDescription';
import CustomTile from '../../../components/CustomTitle';
import TableListPatients from '../../../components/Tables/TableListPatients';
import TableListProfessional from '../../../components/Tables/TableListProfessional';
import { MESSAGES, ROLES } from '../../../constants';
import { RootState } from '../../../store/configure-store';
import { DeletePatientRequest } from '../../../store/ducks/Dashboard/services';
import CallToaster from '../../../utils/callToaster';
import { ButtonWrapper, DentistWrapper } from './styles';

const RemovePatients = () => {
  const history = useHistory();
  const [patient, setPatient] = useState<any>();
  const [dentistID, setDentistID] = useState<string>('');
  const [dentistName, setDentistName] = useState<string>('');

  const user = useSelector((state: RootState) => state.user);

  const handleDeletePacient = async (query: any) => {
    const response = await DeletePatientRequest(query[0]._id);
    if (response.status === 204) {
      Swal.fire('Sucesso!', 'Usuário removido.', 'success');
      CallToaster({
        type: 'success',
        title: 'Sucesso',
        description: 'Paciente Removido!'
      });
      history.push('/dashboard/pacientes/lista');
    }

    if (response.status === 409) {
      CallToaster({
        type: 'error',
        title: MESSAGES.CONFLICT_UPDATE_EMPLOYEE.title,
        description: MESSAGES.CONFLICT_UPDATE_EMPLOYEE.description
      });
    }
    if (response.status === 500) {
      CallToaster({
        type: 'error',
        title: MESSAGES.INTERNAL_SERVER_REGISTER.title,
        description: MESSAGES.INTERNAL_SERVER_REGISTER.description
      });
    }

    // setLoading(false);
  };

  return (
    <ContentBlock>
      <CustomTile title="Remover pacientes" />
      {user.role === ROLES.ADMIN && (
        <CustomDescription title="Selecione abaixo o profissional ao qual deseja remover um pacientes." />
      )}
      <Card elevation={0}>
        {user.role === ROLES.ADMIN && dentistName === '' && (
          <TableListProfessional
            onDentistChangeSetID={setDentistID}
            onDentistChangeSetName={setDentistName}
            confirmationModalMessage="Deseja remover um paciente deste(a) dentista(a)?"
            isEdit
          />
        )}

        {user.role === ROLES.ADMIN && dentistName !== '' && (
          <DentistWrapper>
            Dentista Selecionado: {dentistName}
            <ButtonWrapper
              variant="outlined"
              onClick={() => {
                setDentistID('');
                setDentistName('');
              }}
            >
              trocar dentista <FaExchangeAlt />
            </ButtonWrapper>
          </DentistWrapper>
        )}

        {((user.role === ROLES.DENTIST && !patient) ||
          (user.role === ROLES.EMPLOYEE && !patient) ||
          (!patient && dentistName !== '')) && (
          <TableListPatients
            confirmationModalMessage="Deseja editar este(a) paciente?"
            onSelected={setPatient}
            onDelete={handleDeletePacient}
            isAdmin={user.role === ROLES.ADMIN}
            dentistID={dentistID}
            isDeleting
          />
        )}
      </Card>
    </ContentBlock>
  );
};

export default RemovePatients;
