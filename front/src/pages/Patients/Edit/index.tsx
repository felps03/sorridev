import React, { useState } from 'react';

import { Box, Card } from '@material-ui/core';
import { FaExchangeAlt } from 'react-icons/fa';
import { useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';

import ContentBlock from '../../../components/ContentBlock';
import CustomDescription from '../../../components/CustomDescription';
import CustomTile from '../../../components/CustomTitle';
import DashRegisterUserForm from '../../../components/Register/UserDashForm';
import TableListPatients from '../../../components/Tables/TableListPatients';
import TableListProfessional from '../../../components/Tables/TableListProfessional';
import { MESSAGES, ROLES } from '../../../constants';
import { RootState } from '../../../store/configure-store';
import { UpdatePatientRequest } from '../../../store/ducks/Dashboard/services';
import CallToaster from '../../../utils/callToaster';
import { ButtonWrapper, DentistWrapper } from './styles';

const EditPatients = () => {
  const history = useHistory();
  const [patient, setPatient] = useState<any>();
  const [dentistID, setDentistID] = useState<string>('');
  const [dentistName, setDentistName] = useState<string>('');
  const [loading, setLoading] = useState<boolean>(false);

  const user = useSelector((state: RootState) => state.user);

  const UpdatePatient = async (values: any) => {
    setLoading(true);

    if (user.role === 'ADMIN') values.dentist = dentistID;
    const response = await UpdatePatientRequest(patient._id, values);

    if (response.status === 200) {
      CallToaster({
        type: 'success',
        title: MESSAGES.SUCCESS_UPDATE_PROFESSIONAL.title,
        description: MESSAGES.SUCCESS_UPDATE_PROFESSIONAL.description
      });
      history.push('/dashboard/pacientes/lista');
    }

    if (response.status === 409) {
      CallToaster({
        type: 'error',
        title: MESSAGES.CONFLICT_UPDATE_EMPLOYEE.title,
        description: MESSAGES.CONFLICT_UPDATE_EMPLOYEE.description
      });
    }
    if (response.status === 500) {
      CallToaster({
        type: 'error',
        title: MESSAGES.INTERNAL_SERVER_REGISTER.title,
        description: MESSAGES.INTERNAL_SERVER_REGISTER.description
      });
    }

    setLoading(false);
  };

  return (
    <ContentBlock>
      <CustomTile title="Edição de pacientes" />
      {user.role === ROLES.ADMIN && (
        <CustomDescription title="Selecione abaixo o profissional ao qual deseja editar algum de seus pacientes." />
      )}
      <Card elevation={0}>
        {user.role === ROLES.ADMIN && dentistName === '' && (
          <TableListProfessional
            onDentistChangeSetID={setDentistID}
            onDentistChangeSetName={setDentistName}
            confirmationModalMessage="Deseja editar um paciente deste(a) dentista(a)?"
            isEdit
          />
        )}

        {user.role === ROLES.ADMIN && dentistName !== '' && (
          <DentistWrapper>
            Dentista Selecionado: {dentistName}
            <ButtonWrapper
              variant="outlined"
              onClick={() => {
                setDentistID('');
                setDentistName('');
              }}
            >
              trocar dentista <FaExchangeAlt />
            </ButtonWrapper>
          </DentistWrapper>
        )}

        {((user.role === ROLES.DENTIST && !patient) ||
          (user.role === ROLES.EMPLOYEE && !patient) ||
          (!patient && dentistName !== '')) && (
          <TableListPatients
            confirmationModalMessage="Deseja editar este(a) paciente?"
            onSelected={setPatient}
            isAdmin={user.role === ROLES.ADMIN}
            dentistID={dentistID}
            isEdit
          />
        )}

        {user.role === ROLES.DENTIST && patient && (
          <DentistWrapper>
            Paciente Selecionado: {patient.name}
            <ButtonWrapper
              variant="outlined"
              onClick={() => {
                setPatient('');
              }}
            >
              trocar paciente <FaExchangeAlt />
            </ButtonWrapper>
          </DentistWrapper>
        )}

        {((user.role === ROLES.DENTIST && patient) ||
          (user.role === ROLES.EMPLOYEE && patient) ||
          (patient && dentistName !== '')) && (
          <Box paddingTop={2}>
            <DashRegisterUserForm
              isLoading={loading}
              patientData={patient}
              onClickSendButton={(values: any) => UpdatePatient(values)}
            />
          </Box>
        )}
      </Card>
    </ContentBlock>
  );
};

export default EditPatients;
