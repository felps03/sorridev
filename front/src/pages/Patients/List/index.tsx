import React, { useEffect, useState } from 'react';

import { Card } from '@material-ui/core';
import { FaExchangeAlt } from 'react-icons/fa';
import { useSelector } from 'react-redux';

import ContentBlock from '../../../components/ContentBlock';
import CustomDescription from '../../../components/CustomDescription';
import CustomTile from '../../../components/CustomTitle';
import TableListPatients from '../../../components/Tables/TableListPatients';
import TableListProfessional from '../../../components/Tables/TableListProfessional';
import { ROLES } from '../../../constants';
import { RootState } from '../../../store/configure-store';
import { ButtonWrapper, DentistWrapper } from './styles';

const ListPatients = () => {
  const [dentistID, setDentistID] = useState<string>('');
  const [dentistName, setDentistName] = useState<string>('');
  const user = useSelector((state: RootState) => state.user);

  useEffect(() => {
    if (user.role === ROLES.DENTIST) {
      setDentistID(user._id);
    }
    if (user.role === ROLES.EMPLOYEE) {
      setDentistID(user.dentist || '');
    }
  }, [user]);

  return (
    <ContentBlock>
      <CustomTile title="Listagem de Pacientes" />
      {user.role === ROLES.ADMIN && (
        <CustomDescription title="Selecione abaixo o profissional ao qual deseja visualizar a lista de pacientes." />
      )}
      <Card elevation={0}>
        {user.role === ROLES.ADMIN && dentistName === '' && (
          <TableListProfessional
            onDentistChangeSetID={setDentistID}
            onDentistChangeSetName={setDentistName}
            confirmationModalMessage="Deseja os pacientes do(a) dentista selecionado (a)?"
            isEdit
          />
        )}

        {user.role === ROLES.ADMIN && dentistName !== '' && (
          <DentistWrapper>
            Dentista Selecionado: {dentistName}
            <ButtonWrapper
              variant="outlined"
              onClick={() => {
                setDentistID('');
                setDentistName('');
              }}
            >
              trocar dentista <FaExchangeAlt />
            </ButtonWrapper>
          </DentistWrapper>
        )}

        {((user.role === ROLES.ADMIN && dentistName !== '') ||
          user.role === ROLES.DENTIST ||
          user.role === ROLES.EMPLOYEE) && (
          <TableListPatients
            isAdmin={user.role === ROLES.ADMIN}
            dentistID={dentistID}
          />
        )}
      </Card>
    </ContentBlock>
  );
};

export default ListPatients;
