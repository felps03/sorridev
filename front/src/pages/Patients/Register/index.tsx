import React, { useEffect, useState } from 'react';

import { Card } from '@material-ui/core';
import { FaExchangeAlt } from 'react-icons/fa';
import { useSelector, useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';

import ContentBlock from '../../../components/ContentBlock';
import CustomDescription from '../../../components/CustomDescription';
import CustomTile from '../../../components/CustomTitle';
import DashRegisterUserForm from '../../../components/Register/UserDashForm';
import TableListProfessional from '../../../components/Tables/TableListProfessional';
import { MESSAGES, ROLES } from '../../../constants';
import { RootState } from '../../../store/configure-store';
import { applicationActions } from '../../../store/ducks/Application';
import { RegisterUser } from '../../../store/ducks/Dashboard/services';
import CallToaster from '../../../utils/callToaster';
import { ButtonWrapper, DentistWrapper } from './styles';

const RegisterPatient = () => {
  const history = useHistory();
  const dispatch = useDispatch();
  const [loading, setLoading] = useState<boolean>(false);
  const [dentistID, setDentistID] = useState<string>('');
  const [dentistName, setDentistName] = useState<string>('');
  const user = useSelector((state: RootState) => state.user);

  const registerNewUser = async (values: any) => {
    setLoading(true);

    values.password = '123456';

    if (user.role === ROLES.EMPLOYEE) values.dentist = user.dentist;
    if (user.role === ROLES.ADMIN) values.dentist = dentistID;
    if (user.role === ROLES.DENTIST) values.dentist = user._id;

    const response = await RegisterUser(values);
    if (response.status === 201) {
      CallToaster({
        type: 'success',
        title: MESSAGES.SUCCESS_REGISTER.title,
        description: MESSAGES.SUCCESS_REGISTER.description
      });
      dispatch(applicationActions.updateGraphics());
      history.push('/dashboard/pacientes/lista');
    }
    if (response.status === 409) {
      CallToaster({
        type: 'error',
        title: MESSAGES.CONFLICT.title,
        description: MESSAGES.CONFLICT.description
      });
    }
    if (response.status === 500) {
      CallToaster({
        type: 'error',
        title: MESSAGES.INTERNAL_SERVER_REGISTER.title,
        description: MESSAGES.INTERNAL_SERVER_REGISTER.description
      });
    }

    setLoading(false);
  };

  useEffect(() => {
    if ([ROLES.DENTIST, ROLES.EMPLOYEE].includes(user.role)) {
      setDentistID(user._id);
    }
  }, [user._id, user.role]);

  return (
    <ContentBlock>
      <CustomTile title="Cadastro de Pacientes" />
      {user.role === ROLES.ADMIN && (
        <CustomDescription title="Selecione abaixo o profissional ao qual deseja atibuir um novo pacientes." />
      )}
      <Card elevation={0}>
        {user.role === ROLES.ADMIN && dentistName === '' && (
          <TableListProfessional
            onDentistChangeSetID={setDentistID}
            onDentistChangeSetName={setDentistName}
            confirmationModalMessage="Deseja registrar um paciente ao dentista selecionado (a)?"
            isEdit
          />
        )}

        {user.role === ROLES.ADMIN && dentistName !== '' && (
          <DentistWrapper>
            Dentista Selecionado: {dentistName}
            <ButtonWrapper
              variant="outlined"
              onClick={() => {
                setDentistID('');
                setDentistName('');
              }}
            >
              trocar dentista <FaExchangeAlt />
            </ButtonWrapper>
          </DentistWrapper>
        )}

        {((user.role === ROLES.ADMIN && dentistName !== '') ||
          user.role === ROLES.DENTIST ||
          user.role === ROLES.EMPLOYEE) && (
          <DashRegisterUserForm
            isLoading={loading}
            onClickSendButton={(values: any) => registerNewUser(values)}
          />
        )}
      </Card>
    </ContentBlock>
  );
};

export default RegisterPatient;
