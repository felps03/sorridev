import { Grid } from '@material-ui/core';
import { Link } from 'react-router-dom';
import styled from 'styled-components';

import background from '../../assets/img/background.png';

export const BackgroundImage = styled(Grid)`
  background-image: url(${background});
  background-position: center;
  background-size: cover;
  background-repeat: no-repeat;
`;

export const Logo = styled.img`
  height: 5rem;
`;

export const CompanyName = styled.h3`
  text-align: center;
  text-transform: ${(props) => props.theme.font.transform.uppercase};
  color: ${(props) => props.theme.colors.primary};
  font-size: ${(props) => props.theme.font.size.extraLarge};
  font-weight: ${(props) => props.theme.font.weight.bold};

  > span {
    color: ${(props) => props.theme.colors.secondary};
  }
`;

export const LogoWrapper = styled.section`
  && {
    margin: 141px 0px;
    display: flex;
    flex-direction: column;
    align-items: center;
    @media (min-height: 800px) {
      margin: 228px 0px;
    }
  }
`;

export const SignUpMessage = styled.span`
  && {
    font-size: ${(props) => props.theme.font.size.small};
    color: ${(props) => props.theme.colors.text};
  }
`;

export const SignUp = styled(Link)`
  && {
    text-decoration: none;
    margin-left: 0.5rem;
    span {
      color: ${(props) => props.theme.colors.primary};
      font-size: inherit;
      &:hover {
        font-weight: ${(props) => props.theme.font.weight.semiBold};
      }
    }
  }
`;

export const SingUpWrapper = styled.section`
  display: flex;
  flex-direction: row;
  align-items: center;
  margin-top: 20px;
`;
