import React from 'react';

import { Grid, Paper } from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';

import logo from '../../assets/icons/logo.svg';
import UserLoginForm from '../../components/Login/Form';
import { LoginValues } from '../../interfaces/login';
import { RootState } from '../../store/configure-store';
import { userActions } from '../../store/ducks/User/Authentication';
import {
  LogoWrapper,
  BackgroundImage,
  Logo,
  CompanyName,
  SignUp,
  SignUpMessage,
  SingUpWrapper
} from './styles';

const Login = () => {
  const dispatch = useDispatch();
  const user = useSelector((state: RootState) => state.user);

  const handleSubmitLogin = (loginValues: LoginValues) => {
    dispatch(userActions.loginUserRequest(loginValues));
  };

  const handleGoogleLogin = async (data: any) => {
    const payload = {
      name: data.profileObj.name,
      provider: 'google',
      email: data.profileObj.email,
      photo: data.profileObj.imageUrl
    };
    dispatch(userActions.loginByGoogleUserRequest(payload));
  };

  return (
    <Grid container component="main" style={{ height: '100vh' }}>
      <Grid item xs={12} sm={3} component={Paper} elevation={6} square>
        <LogoWrapper>
          <Logo src={logo} alt="SorriDevLogo" />
          <CompanyName>
            Sorri <span>Dev</span>
          </CompanyName>
          <UserLoginForm
            onFormSubmit={handleSubmitLogin}
            isSubmitting={Boolean(user.isAuthenticated)}
            onSubmitGoogle={handleGoogleLogin}
          />
          <SingUpWrapper>
            <SignUpMessage>Ainda não tem uma conta? </SignUpMessage>
            <SignUp to="/cadastro">
              <span>Cadastre-se</span>
            </SignUp>
          </SingUpWrapper>
        </LogoWrapper>
      </Grid>
      <BackgroundImage item xs={false} sm={9} />
    </Grid>
  );
};

export default Login;
