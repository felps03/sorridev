import React from 'react';

import { Card } from '@material-ui/core';
import Swal from 'sweetalert2';

import ContentBlock from '../../../components/ContentBlock';
import CustomTile from '../../../components/CustomTitle';
import ResetPasswordFrom from '../../../components/Form/ResetPasswordFrom';
import { resetPassword } from '../../../store/ducks/User/Authentication/services';

const ResetPassword = () => {
  const resetPasswordRequest = async (values: any) => {
    try {
      await resetPassword(values);
      Swal.fire({
        icon: 'success',
        title: 'Senha Alterada com sucesso',
        showConfirmButton: false,
        timer: 1500
      });
    } catch (error) {
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Senha atual inválida!'
      });
    }
  };

  return (
    <ContentBlock>
      <CustomTile title="Redefinir Senha" />
      <Card elevation={0}>
        <div style={{ paddingTop: 30 }}>
          <ResetPasswordFrom
            onClickSendButton={resetPasswordRequest}
            isLoading={false}
          />
        </div>
      </Card>
    </ContentBlock>
  );
};

export default ResetPassword;
