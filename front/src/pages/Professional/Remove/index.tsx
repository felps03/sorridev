import React, { useState } from 'react';

import { useHistory } from 'react-router-dom';
import Swal from 'sweetalert2';

import ContentBlock from '../../../components/ContentBlock';
import CustomTile from '../../../components/CustomTitle';
import TableListProfessional from '../../../components/Tables/TableListProfessional';
import { MESSAGES } from '../../../constants';
import { DeleteProfessionalRequest } from '../../../store/ducks/Dashboard/services';
import CallToaster from '../../../utils/callToaster';

const RemoveProfessionals = () => {
  const history = useHistory();
  const [dentist, setDentist] = useState<any>();

  const handleDeleteProfessional = async (query: any) => {
    const response = await DeleteProfessionalRequest(query[0]._id);
    if (response.status === 204) {
      Swal.fire('Sucesso!', 'Professional removido.', 'success');
      CallToaster({
        type: 'success',
        title: 'Sucesso',
        description: 'Professional Removido!'
      });
      history.push('/dashboard/professional/lista');
    }

    if (response.status === 409) {
      CallToaster({
        type: 'error',
        title: MESSAGES.CONFLICT_UPDATE_EMPLOYEE.title,
        description: MESSAGES.CONFLICT_UPDATE_EMPLOYEE.description
      });
    }
    if (response.status === 500) {
      CallToaster({
        type: 'error',
        title: MESSAGES.INTERNAL_SERVER_REGISTER.title,
        description: MESSAGES.INTERNAL_SERVER_REGISTER.description
      });
    }
  };

  return (
    <ContentBlock>
      <CustomTile title="Remover Profissionais" />

      {!dentist && (
        <TableListProfessional
          onSelected={setDentist}
          onDelete={handleDeleteProfessional}
          isDeleting
        />
      )}
    </ContentBlock>
  );
};

export default RemoveProfessionals;
