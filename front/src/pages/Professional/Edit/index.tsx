import React, { useEffect, useState } from 'react';

import { Card } from '@material-ui/core';
import { useHistory } from 'react-router-dom';

import ContentBlock from '../../../components/ContentBlock';
import CustomTile from '../../../components/CustomTitle';
import RegisterProfessionalForm from '../../../components/Form/RegisterProfessionalForm';
import TableListProfessional from '../../../components/Tables/TableListProfessional';
import { MESSAGES } from '../../../constants';
import { UpdateProfessionalRequest } from '../../../store/ducks/Dashboard/services';
import CallToaster from '../../../utils/callToaster';

const EditProfessionals = () => {
  const history = useHistory();
  const [dentist, setDentist] = useState<any>();
  const [dentistID, setDentistID] = useState<any>();
  const [loading, setLoading] = useState<boolean>(false);

  const updateNewProfessional = async (values: any) => {
    setLoading(true);
    const response = await UpdateProfessionalRequest(dentistID, values);

    if (response.status === 200) {
      CallToaster({
        type: 'success',
        title: MESSAGES.SUCCESS_UPDATE_PROFESSIONAL.title,
        description: MESSAGES.SUCCESS_UPDATE_PROFESSIONAL.description
      });
      history.push('/dashboard/profissionais/lista');
    }
    if (response.status === 409) {
      CallToaster({
        type: 'error',
        title: MESSAGES.CONFLICT_UPDATE_PROFESSIONAL.title,
        description: MESSAGES.CONFLICT_UPDATE_PROFESSIONAL.description
      });
    }
    if (response.status === 500) {
      CallToaster({
        type: 'error',
        title: MESSAGES.INTERNAL_SERVER_REGISTER.title,
        description: MESSAGES.INTERNAL_SERVER_REGISTER.description
      });
    }

    setLoading(false);
  };

  useEffect(() => {
    if (dentist) {
      setDentistID(dentist._id);
    }
  }, [dentist]);

  return (
    <ContentBlock>
      <CustomTile title="Editar de Profissionais" />

      {!dentist && (
        <TableListProfessional
          onSelected={setDentist}
          onDentistChangeSetID={() => {}}
          isEdit
        />
      )}
      {dentist && (
        <Card elevation={0}>
          <div style={{ paddingTop: 30 }}>
            <RegisterProfessionalForm
              isLoading={loading}
              isEdit
              professionalData={dentist}
              onRegisterNewProfessional={(values: any) =>
                updateNewProfessional(values)
              }
            />
          </div>
        </Card>
      )}
    </ContentBlock>
  );
};

export default EditProfessionals;
