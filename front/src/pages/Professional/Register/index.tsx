import React, { useState } from 'react';

import { Card } from '@material-ui/core';
import { useHistory } from 'react-router-dom';

import ContentBlock from '../../../components/ContentBlock';
import CustomTile from '../../../components/CustomTitle';
import RegisterProfessionalForm from '../../../components/Form/RegisterProfessionalForm';
import { MESSAGES } from '../../../constants';
import { RegisterProfessionalRequest } from '../../../store/ducks/Dashboard/services';
import CallToaster from '../../../utils/callToaster';

const RegisterProfessional = () => {
  const history = useHistory();
  const [loading, setLoading] = useState<boolean>(false);

  const registerNewProfessional = async (values: any) => {
    setLoading(true);
    const response = await RegisterProfessionalRequest(values);

    if (response.status === 201) {
      CallToaster({
        type: 'success',
        title: MESSAGES.SUCCESS_PROFESSIONAL_REGISTER.title,
        description: MESSAGES.SUCCESS_PROFESSIONAL_REGISTER.description
      });
      history.push('/dashboard/profissionais/lista');
    }
    if (response.status === 409) {
      CallToaster({
        type: 'error',
        title: MESSAGES.CONFLICT_PROFESSIONAL.title,
        description: MESSAGES.CONFLICT_PROFESSIONAL.description
      });
    }
    if (response.status === 500) {
      CallToaster({
        type: 'error',
        title: MESSAGES.CONFLICT.title,
        description: MESSAGES.CONFLICT.description
      });
    }

    setLoading(false);
  };

  return (
    <ContentBlock>
      <CustomTile title="Registro de Profissionais" />
      <Card elevation={0}>
        <div style={{ paddingTop: 30 }}>
          <RegisterProfessionalForm
            isLoading={loading}
            onRegisterNewProfessional={(values: any) =>
              registerNewProfessional(values)
            }
          />
        </div>
      </Card>
    </ContentBlock>
  );
};

export default RegisterProfessional;
