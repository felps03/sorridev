import React from 'react';

import ContentBlock from '../../../components/ContentBlock';
import CustomTile from '../../../components/CustomTitle';
import TableListProfessional from '../../../components/Tables/TableListProfessional';

const ListProfessionals = () => {
  return (
    <ContentBlock>
      <CustomTile title="Listagem de Profissionais" />

      <TableListProfessional />
    </ContentBlock>
  );
};

export default ListProfessionals;
