import React, { useEffect, useState } from 'react';

import { Box, Card } from '@material-ui/core';
import { FaExchangeAlt } from 'react-icons/fa';
import { useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';

import ContentBlock from '../../../components/ContentBlock';
import CustomDescription from '../../../components/CustomDescription';
import CustomTile from '../../../components/CustomTitle';
import RegisterServiceForm from '../../../components/Form/RegisterServiceForm';
import TableListProfessional from '../../../components/Tables/TableListProfessional';
import { MESSAGES, ROLES } from '../../../constants';
import { RootState } from '../../../store/configure-store';
import { RegisterServiceRequest } from '../../../store/ducks/Dashboard/services';
import CallToaster from '../../../utils/callToaster';
import { DentistWrapper, ButtonWrapper } from './styles';

const RegisterService = () => {
  const history = useHistory();
  const [loading, setLoading] = useState<boolean>(false);
  const [dentistID, setDentistID] = useState<string>('');
  const [dentistName, setDentistName] = useState<string>('');
  const user = useSelector((state: RootState) => state.user);

  const registerNewService = async (values: any) => {
    setLoading(true);

    values.dentist = dentistID;
    const response = await RegisterServiceRequest(values);

    if (response.status === 201) {
      CallToaster({
        type: 'success',
        title: MESSAGES.SUCCESS_SERVICE_REGISTER.title,
        description: MESSAGES.SUCCESS_SERVICE_REGISTER.description
      });
      history.push('/dashboard/servicos/lista');
    }
    if (response.status === 409) {
      CallToaster({
        type: 'error',
        title: MESSAGES.CONFLICT_SERVICE.title,
        description: MESSAGES.CONFLICT_SERVICE.description
      });
    }
    if (response.status === 500) {
      CallToaster({
        type: 'error',
        title: MESSAGES.INTERNAL_SERVER_REGISTER.title,
        description: MESSAGES.INTERNAL_SERVER_REGISTER.description
      });
    }

    setLoading(false);
  };

  useEffect(() => {
    if ([ROLES.DENTIST, ROLES.EMPLOYEE].includes(user.role)) {
      setDentistID(user._id);
    }
  }, [user._id, user.role]);

  return (
    <ContentBlock>
      <CustomTile title="Cadastro de Serviços Gerais" />
      {user.role === ROLES.ADMIN && (
        <CustomDescription title="Selecione abaixo o profissional ao qual deseja cadastrar um serviço geral." />
      )}
      <Card elevation={0}>
        {user.role === ROLES.ADMIN && dentistName === '' && (
          <TableListProfessional
            onDentistChangeSetID={setDentistID}
            onDentistChangeSetName={setDentistName}
            confirmationModalMessage="Deseja cadastrar um serviço geral para o dentista selecionado (a)?"
            isEdit
          />
        )}

        {user.role === ROLES.ADMIN && dentistName !== '' && (
          <DentistWrapper>
            Dentista Selecionado: {dentistName}
            <ButtonWrapper
              variant="outlined"
              onClick={() => {
                setDentistID('');
                setDentistName('');
              }}
            >
              trocar dentista <FaExchangeAlt />
            </ButtonWrapper>
          </DentistWrapper>
        )}

        {((user.role === ROLES.ADMIN && dentistName !== '') ||
          user.role === ROLES.DENTIST ||
          user.role === ROLES.EMPLOYEE) && (
          <Box paddingTop={2}>
            <RegisterServiceForm
              isLoading={loading}
              onClickSendButton={(values: any) => registerNewService(values)}
            />
          </Box>
        )}
      </Card>
    </ContentBlock>
  );
};

export default RegisterService;
