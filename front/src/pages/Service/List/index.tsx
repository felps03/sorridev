import React, { useEffect, useState } from 'react';

import { FaExchangeAlt } from 'react-icons/fa';
import { useSelector } from 'react-redux';

import ContentBlock from '../../../components/ContentBlock';
import CustomDescription from '../../../components/CustomDescription';
import CustomTile from '../../../components/CustomTitle';
import TableListProfessional from '../../../components/Tables/TableListProfessional';
import TableListService from '../../../components/Tables/TableListService';
import { ROLES } from '../../../constants';
import { RootState } from '../../../store/configure-store';
import { ButtonWrapper, DentistWrapper } from './styles';

const ListService = () => {
  const [dentistID, setDentistID] = useState<string>('');
  const [dentistName, setDentistName] = useState<string>('');
  const user = useSelector((state: RootState) => state.user);

  useEffect(() => {
    if ([ROLES.DENTIST, ROLES.EMPLOYEE].includes(user.role)) {
      setDentistID(user._id);
    }
  }, [user._id, user.role]);

  return (
    <ContentBlock>
      <CustomTile title="Lista de Serviços Gerais" />
      {user.role === ROLES.ADMIN && (
        <CustomDescription title="Selecione abaixo o profissional ao qual deseja listar os serviço geral." />
      )}
      {user.role === ROLES.ADMIN && dentistName === '' && (
        <TableListProfessional
          onDentistChangeSetID={setDentistID}
          onDentistChangeSetName={setDentistName}
          confirmationModalMessage="Deseja a lista de serviços gerais do dentista selecionado (a)?"
          isEdit
        />
      )}

      {user.role === ROLES.ADMIN && dentistName !== '' && (
        <DentistWrapper>
          Dentista Selecionado: {dentistName}
          <ButtonWrapper
            variant="outlined"
            onClick={() => {
              setDentistID('');
              setDentistName('');
            }}
          >
            trocar dentista <FaExchangeAlt />
          </ButtonWrapper>
        </DentistWrapper>
      )}

      {((user.role === ROLES.ADMIN && dentistName !== '') ||
        user.role === ROLES.DENTIST ||
        user.role === ROLES.EMPLOYEE) && (
        <TableListService
          isAdmin={user.role === ROLES.ADMIN}
          dentistID={dentistID}
        />
      )}
    </ContentBlock>
  );
};

export default ListService;
