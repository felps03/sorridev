import React, { useState } from 'react';

import { FaExchangeAlt } from 'react-icons/fa';
import { useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import Swal from 'sweetalert2';

import ContentBlock from '../../../components/ContentBlock';
import CustomDescription from '../../../components/CustomDescription';
import CustomTile from '../../../components/CustomTitle';
import TableListProfessional from '../../../components/Tables/TableListProfessional';
import TableListService from '../../../components/Tables/TableListService';
import { MESSAGES, ROLES } from '../../../constants';
import { RootState } from '../../../store/configure-store';
import { DeleteServiceRequest } from '../../../store/ducks/Dashboard/services';
import CallToaster from '../../../utils/callToaster';
import { ButtonWrapper, DentistWrapper } from './styles';

const RemoveService = () => {
  const history = useHistory();
  const [dentistID, setDentistID] = useState<string>('');
  const [service, setService] = useState<any>();
  const [dentistName, setDentistName] = useState<string>('');
  const user = useSelector((state: RootState) => state.user);

  const handleDeleteService = async (query: any) => {
    const response = await DeleteServiceRequest(query[0]._id);
    if (response.status === 204) {
      Swal.fire('Sucesso!', 'Serviço geral removido com sucesso.', 'success');
      CallToaster({
        type: 'success',
        title: 'Sucesso',
        description: 'Serviço geral removido!'
      });
      history.push('/dashboard/professional/lista');
    }

    if (response.status === 409) {
      CallToaster({
        type: 'error',
        title: 'Conflito',
        description: 'Não foi possível remover o serviço geral!'
      });
    }
    if (response.status === 500) {
      CallToaster({
        type: 'error',
        title: MESSAGES.INTERNAL_SERVER_REGISTER.title,
        description: MESSAGES.INTERNAL_SERVER_REGISTER.description
      });
    }
  };
  return (
    <ContentBlock>
      <CustomTile title="Remover Serviços Gerais" />
      {user.role === ROLES.ADMIN && (
        <CustomDescription title="Selecione abaixo o profissional ao qual deseja remover um serviço geral." />
      )}
      {user.role === ROLES.ADMIN && dentistName === '' && (
        <TableListProfessional
          onDentistChangeSetID={setDentistID}
          onDentistChangeSetName={setDentistName}
          confirmationModalMessage="Deseja remover um serviço geral do dentista selecionado (a)?"
          isEdit
        />
      )}

      {user.role === ROLES.ADMIN && dentistName !== '' && (
        <DentistWrapper>
          Dentista Selecionado: {dentistName}
          <ButtonWrapper
            variant="outlined"
            onClick={() => {
              setDentistID('');
              setDentistName('');
            }}
          >
            trocar dentista <FaExchangeAlt />
          </ButtonWrapper>
        </DentistWrapper>
      )}

      {((user.role === ROLES.DENTIST && !service) ||
        (user.role === ROLES.EMPLOYEE && !service) ||
        (!service && dentistName !== '')) && (
        <TableListService
          confirmationModalMessage="Deseja remover este(a) serviço geral?"
          onSelected={setService}
          onDelete={handleDeleteService}
          isAdmin={user.role === ROLES.ADMIN}
          dentistID={dentistID}
          isDeleting
        />
      )}
    </ContentBlock>
  );
};

export default RemoveService;
