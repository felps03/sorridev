/* eslint-disable no-console */
import React from 'react';

import { Grid, Paper } from '@material-ui/core';
import { useDispatch } from 'react-redux';

import logo from '../../assets/icons/logo.svg';
import UserRegisterForm from '../../components/Register/UserForm';
import { RegisterValues } from '../../interfaces/User';
import { userRegisterActions } from '../../store/ducks/User/Register';
import {
  LogoWrapper,
  BackgroundImage,
  Logo,
  CompanyName,
  SignUp,
  SignUpMessage,
  SingUpWrapper,
  Terms
} from './styles';

const Register = () => {
  const dispatch = useDispatch();

  const handleRegister = (payload: RegisterValues) => {
    dispatch(userRegisterActions.registerUserRequest(payload));
  };

  return (
    <Grid container component="main" style={{ height: '100vh' }}>
      <Grid item xs={12} sm={3} component={Paper} elevation={6} square>
        <LogoWrapper>
          <Logo src={logo} alt="SorriDevLogo" />
          <CompanyName>
            Sorri <span>Dev</span>
          </CompanyName>

          <UserRegisterForm
            onFormSubmit={handleRegister}
            isSubmitting={false}
          />

          <Terms>
            Ao me inscrever, concordo com a Política de Privacidade <br /> e os
            Termos de
          </Terms>
          <SingUpWrapper>
            <SignUpMessage>Já tem uma conta? </SignUpMessage>
            <SignUp to="/login">
              <span>Logue-se</span>
            </SignUp>
          </SingUpWrapper>
        </LogoWrapper>
      </Grid>
      <BackgroundImage item xs={false} sm={9} />
    </Grid>
  );
};

export default Register;
