import { Grid } from '@material-ui/core';
import { Link } from 'react-router-dom';
import styled from 'styled-components';

import background from '../../assets/img/background.png';

export const Terms = styled.p`
  padding: 0 1rem;
  text-align: center;
  font-size: ${(props) => props.theme.font.size.extraSmall};
  color: ${(props) => props.theme.colors.textDark};
  font-weight: 300;
`;

export const BackgroundImage = styled(Grid)`
  background-image: url(${background});
  background-position: center;
  background-size: cover;
  background-repeat: no-repeat;
`;

export const Logo = styled.img`
  height: 5rem;
`;

export const CompanyName = styled.h3`
  text-align: center;
  text-transform: ${(props) => props.theme.font.transform.uppercase};
  color: ${(props) => props.theme.colors.primary};
  font-size: ${(props) => props.theme.font.size.extraLarge};
  font-weight: ${(props) => props.theme.font.weight.bold};

  > span {
    color: ${(props) => props.theme.colors.secondary};
  }
`;

export const LogoWrapper = styled.section`
  margin: 150px 0px;
  display: flex;
  flex-direction: column;
  align-items: center;
`;

export const SignUpMessage = styled.span`
  font-size: ${(props) => props.theme.font.size.small};
  color: ${(props) => props.theme.colors.text};
  font-weight: ${(props) => props.theme.font.weight.semiBold};
`;

export const SignUp = styled(Link)`
  text-decoration: none;
  span {
    color: ${(props) => props.theme.colors.secondary};
    padding-left: 2px;
    font-size: inherit;
    &:hover {
      font-weight: ${(props) => props.theme.font.weight.semiBold};
    }
  }
`;

export const SingUpWrapper = styled.section`
  display: flex;
  flex-direction: row;
  align-items: center;
`;
