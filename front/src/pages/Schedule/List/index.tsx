import React, { useCallback, useEffect, useState } from 'react';

import { v4 as uuid } from 'uuid';

import { appApi } from '../../../apis/app-api';
import ContentBlock from '../../../components/ContentBlock';
import CustomTile from '../../../components/CustomTitle';
import ScheduleList from '../../../components/ScheduleList';

const ListSchedule = () => {
  const [calendarEvents, setCalendarEvents] = useState<any>([]);
  const [page, setPage] = useState<any>(0);
  const [loading, isLoading] = useState<boolean>(false);
  const rowsPerPage = 150;

  const getCalendarEvents = (data: any) => {
    const list = data.map((schedules: any) => ({
      id: uuid(),
      title: schedules.client.name,
      start: schedules.startTime,
      end: schedules.endTime,
      allDay: false,
      ...schedules
    }));

    return list;
  };

  const fetchSchedule = useCallback(async () => {
    isLoading(true);
    const result = await appApi().get(
      `/schedule?limit=${rowsPerPage}&page=${page + 1}`
    );
    if (result.status === 200) {
      const events = getCalendarEvents(result.data.docs);
      setCalendarEvents(events);
    } else {
      setCalendarEvents([]);
    }
    isLoading(false);
  }, [page, rowsPerPage]);

  useEffect(() => {
    fetchSchedule();
  }, [fetchSchedule]);

  return (
    <ContentBlock>
      <CustomTile title="Lista de Agendamentos" />
      <ScheduleList listOfEvents={calendarEvents} />
    </ContentBlock>
  );
};

export default ListSchedule;
