import React, { useState } from 'react';

import { Box, Card } from '@material-ui/core';
import { FaExchangeAlt } from 'react-icons/fa';
import { useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';

import ContentBlock from '../../../components/ContentBlock';
import CustomDescription from '../../../components/CustomDescription';
import CustomTile from '../../../components/CustomTitle';
import RegisterEmployeeForm from '../../../components/Form/RegisterEmployeeForm';
import TableListProfessional from '../../../components/Tables/TableListProfessional';
import { MESSAGES, ROLES } from '../../../constants';
import { RootState } from '../../../store/configure-store';
import { RegisterEmployeeRequest } from '../../../store/ducks/Dashboard/services';
import CallToaster from '../../../utils/callToaster';
import { ButtonWrapper, DentistWrapper } from './styles';

const RegisterEmployee = () => {
  const history = useHistory();
  const [dentistID, setDentistID] = useState<string>('');
  const [dentistName, setDentistName] = useState<string>('');
  const [loading, setLoading] = useState<boolean>(false);

  const user = useSelector((state: RootState) => state.user);

  const registerNewEmployee = async (values: any) => {
    setLoading(true);

    if (user.role === ROLES.ADMIN) values.dentist = dentistID;
    const response = await RegisterEmployeeRequest(values);

    if (response.status === 201) {
      CallToaster({
        type: 'success',
        title: MESSAGES.SUCCESS_EMPLOYEE_REGISTER.title,
        description: MESSAGES.SUCCESS_EMPLOYEE_REGISTER.description
      });
      history.push('/dashboard/funcionario/lista');
    }

    if (response.status === 409) {
      CallToaster({
        type: 'error',
        title: MESSAGES.CONFLICT_EMPLOYEE.title,
        description: MESSAGES.CONFLICT_EMPLOYEE.description
      });
    }
    if (response.status === 500) {
      CallToaster({
        type: 'error',
        title: MESSAGES.CONFLICT.title,
        description: MESSAGES.CONFLICT.description
      });
    }

    setLoading(false);
  };

  return (
    <ContentBlock>
      <CustomTile title="Registro de Funcionários" />
      {user.role === ROLES.ADMIN && (
        <CustomDescription title="Selecione abaixo o profissional ao qual deseja atibuir um novo funcionário." />
      )}
      <Card elevation={0}>
        {user.role === ROLES.ADMIN && dentistName === '' && (
          <TableListProfessional
            onDentistChangeSetID={setDentistID}
            onDentistChangeSetName={setDentistName}
            confirmationModalMessage="Deseja cadastrar um funcionário(a) para este(a) dentista?"
            isEdit
          />
        )}

        {user.role === ROLES.ADMIN && dentistName !== '' && (
          <DentistWrapper>
            Dentista Selecionado: {dentistName}
            <ButtonWrapper
              variant="outlined"
              onClick={() => {
                setDentistID('');
                setDentistName('');
              }}
            >
              trocar dentista <FaExchangeAlt />
            </ButtonWrapper>
          </DentistWrapper>
        )}

        {((user.role === ROLES.ADMIN && dentistName !== '') ||
          user.role === ROLES.DENTIST) && (
          <Box paddingTop={2}>
            <RegisterEmployeeForm
              isLoading={false}
              onClickSendButton={(values: any) => registerNewEmployee(values)}
            />
          </Box>
        )}
      </Card>
    </ContentBlock>
  );
};

export default RegisterEmployee;
