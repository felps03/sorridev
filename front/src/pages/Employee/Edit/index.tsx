import React, { useState } from 'react';

import { Box, Card } from '@material-ui/core';
import { FaExchangeAlt } from 'react-icons/fa';
import { useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';

import ContentBlock from '../../../components/ContentBlock';
import CustomDescription from '../../../components/CustomDescription';
import CustomTile from '../../../components/CustomTitle';
import RegisterEmployeeForm from '../../../components/Form/RegisterEmployeeForm';
import TableListEmployee from '../../../components/Tables/TableListEmployee';
import TableListProfessional from '../../../components/Tables/TableListProfessional';
import { MESSAGES, ROLES } from '../../../constants';
import { RootState } from '../../../store/configure-store';
import { UpdateEmployeeRequest } from '../../../store/ducks/Dashboard/services';
import CallToaster from '../../../utils/callToaster';
import { ButtonWrapper, DentistWrapper } from './styles';

const EditEmployee = () => {
  const history = useHistory();
  const [employee, setEmployee] = useState<any>();
  const [dentistID, setDentistID] = useState<string>('');
  const [dentistName, setDentistName] = useState<string>('');
  const [loading, setLoading] = useState<boolean>(false);

  const user = useSelector((state: RootState) => state.user);

  const UpdateEmployee = async (values: any) => {
    setLoading(true);

    if (user.role === 'ADMIN') values.dentist = dentistID;
    const response = await UpdateEmployeeRequest(employee._id, values);

    if (response.status === 200) {
      CallToaster({
        type: 'success',
        title: MESSAGES.SUCCESS_UPDATE_EMPLOYEE.title,
        description: MESSAGES.SUCCESS_UPDATE_EMPLOYEE.description
      });
      history.push('/dashboard/funcionario/lista');
    }

    if (response.status === 409) {
      CallToaster({
        type: 'error',
        title: MESSAGES.CONFLICT_UPDATE_EMPLOYEE.title,
        description: MESSAGES.CONFLICT_UPDATE_EMPLOYEE.description
      });
    }
    if (response.status === 500) {
      CallToaster({
        type: 'error',
        title: MESSAGES.INTERNAL_SERVER_REGISTER.title,
        description: MESSAGES.INTERNAL_SERVER_REGISTER.description
      });
    }

    setLoading(false);
  };

  return (
    <ContentBlock>
      <CustomTile title="Edição de Funcionários" />
      {user.role === ROLES.ADMIN && (
        <CustomDescription title="Selecione abaixo o profissional ao qual deseja editar um funcionário." />
      )}
      <Card elevation={0}>
        {user.role === ROLES.ADMIN && dentistName === '' && (
          <TableListProfessional
            onDentistChangeSetID={setDentistID}
            onDentistChangeSetName={setDentistName}
            confirmationModalMessage="Deseja editar um funcionário(a) deste(a) dentista(a)?"
            isEdit
          />
        )}

        {user.role === ROLES.ADMIN && dentistName !== '' && (
          <DentistWrapper>
            Dentista Selecionado: {dentistName}
            <ButtonWrapper
              variant="outlined"
              onClick={() => {
                setDentistID('');
                setDentistName('');
              }}
            >
              trocar dentista <FaExchangeAlt />
            </ButtonWrapper>
          </DentistWrapper>
        )}

        {((user.role === ROLES.DENTIST && !employee) ||
          (!employee && dentistName !== '')) && (
          <TableListEmployee
            confirmationModalMessage="Deseja editar este(a) funcionário(a)?"
            onSelected={setEmployee}
            isAdmin={user.role === ROLES.ADMIN}
            dentistID={dentistID}
            isEdit
          />
        )}

        {user.role === ROLES.DENTIST && employee && (
          <DentistWrapper>
            Funcionário Selecionado: {employee.name}
            <ButtonWrapper
              variant="outlined"
              onClick={() => {
                setEmployee('');
              }}
            >
              trocar funcionário <FaExchangeAlt />
            </ButtonWrapper>
          </DentistWrapper>
        )}

        {((user.role === ROLES.DENTIST && employee) ||
          (employee && dentistName !== '')) && (
          <Box paddingTop={2}>
            <RegisterEmployeeForm
              isLoading={loading}
              isEdit
              employeeData={employee}
              onClickSendButton={(values: any) => UpdateEmployee(values)}
            />
          </Box>
        )}
      </Card>
    </ContentBlock>
  );
};

export default EditEmployee;
