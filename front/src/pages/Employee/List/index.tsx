import React, { useState } from 'react';

import { Box, Card } from '@material-ui/core';
import { FaExchangeAlt } from 'react-icons/fa';
import { useSelector } from 'react-redux';

import ContentBlock from '../../../components/ContentBlock';
import CustomDescription from '../../../components/CustomDescription';
import CustomTile from '../../../components/CustomTitle';
import TableListEmployee from '../../../components/Tables/TableListEmployee';
import TableListProfessional from '../../../components/Tables/TableListProfessional';
import { ROLES } from '../../../constants';
import { RootState } from '../../../store/configure-store';
import { ButtonWrapper, DentistWrapper } from './styles';

const ListEmployees = () => {
  const [dentistID, setDentistID] = useState<string>('');
  const [dentistName, setDentistName] = useState<string>('');
  const user = useSelector((state: RootState) => state.user);

  return (
    <ContentBlock>
      <CustomTile title="Listagem de Funcionários" />
      {user.role === ROLES.ADMIN && (
        <CustomDescription title="Selecione abaixo o profissional ao qual deseja ver a lista de funcionários." />
      )}
      <Card elevation={0}>
        {user.role === ROLES.ADMIN && dentistName === '' && (
          <TableListProfessional
            onDentistChangeSetID={setDentistID}
            onDentistChangeSetName={setDentistName}
            confirmationModalMessage="Deseja listar os funcionários (as) deste (a) dentista?"
            isEdit
          />
        )}

        {user.role === ROLES.ADMIN && dentistName !== '' && (
          <DentistWrapper>
            Dentista Selecionado: {dentistName}
            <ButtonWrapper
              variant="outlined"
              onClick={() => {
                setDentistID('');
                setDentistName('');
              }}
            >
              trocar dentista <FaExchangeAlt />
            </ButtonWrapper>
          </DentistWrapper>
        )}

        {((user.role === ROLES.ADMIN && dentistName !== '') ||
          user.role === ROLES.DENTIST) && (
          <Box paddingTop={2}>
            <TableListEmployee
              isAdmin={user.role === ROLES.ADMIN}
              dentistID={dentistID}
            />
          </Box>
        )}
      </Card>
    </ContentBlock>
  );
};

export default ListEmployees;
