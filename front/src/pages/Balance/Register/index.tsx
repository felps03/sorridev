import React, { useEffect, useState } from 'react';

import { Box, Card } from '@material-ui/core';
import { FaExchangeAlt } from 'react-icons/fa';
import { useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';

import ContentBlock from '../../../components/ContentBlock';
import CustomDescription from '../../../components/CustomDescription';
import CustomTile from '../../../components/CustomTitle';
import RegisterBalanceForm from '../../../components/Form/RegisterBalanceForm';
import TableListProfessional from '../../../components/Tables/TableListProfessional';
import { MESSAGES, ROLES } from '../../../constants';
import { RootState } from '../../../store/configure-store';
import { RegisterBalanceRequest } from '../../../store/ducks/Dashboard/services';
import CallToaster from '../../../utils/callToaster';
import { DentistWrapper, ButtonWrapper } from '../List/styles';

const RegisterBalance = () => {
  const history = useHistory();
  const [dentistID, setDentistID] = useState<string>('');
  const [dentistName, setDentistName] = useState<string>('');
  const user = useSelector((state: RootState) => state.user);

  const registerNewBalance = async (values: any) => {
    values.dentist = dentistID;
    const response = await RegisterBalanceRequest(values);

    if (response.status === 201) {
      CallToaster({
        type: 'success',
        title: MESSAGES.SUCCESS_BALANCE_REGISTER.title,
        description: MESSAGES.SUCCESS_BALANCE_REGISTER.description
      });
      history.push('/dashboard/financeiro/lista');
    }
    if (response.status === 409) {
      CallToaster({
        type: 'error',
        title: MESSAGES.CONFLICT_STOCK.title,
        description: MESSAGES.CONFLICT_STOCK.description
      });
    }
    if (response.status === 500) {
      CallToaster({
        type: 'error',
        title: MESSAGES.INTERNAL_SERVER_REGISTER.title,
        description: MESSAGES.INTERNAL_SERVER_REGISTER.description
      });
    }
  };

  useEffect(() => {
    if (user.role === ROLES.DENTIST) {
      setDentistID(user._id);
    }
    if (user.role === ROLES.EMPLOYEE) {
      setDentistID(user.dentist || '');
    }
  }, [user]);

  return (
    <ContentBlock>
      <CustomTile title="Cadastro de entrada no fluxo de caixa" />
      {user.role === ROLES.ADMIN && (
        <CustomDescription title="Selecione abaixo o profissional ao qual deseja cadastrar uma entrada no fluxo de caixa." />
      )}
      <Card elevation={0}>
        {user.role === ROLES.ADMIN && dentistName === '' && (
          <TableListProfessional
            onDentistChangeSetID={setDentistID}
            onDentistChangeSetName={setDentistName}
            confirmationModalMessage="Deseja adicionar uma entrada no fluxo de caixa do dentista selecionado (a)?"
            isEdit
          />
        )}

        {user.role === ROLES.ADMIN && dentistName !== '' && (
          <DentistWrapper>
            Dentista Selecionado: {dentistName}
            <ButtonWrapper
              variant="outlined"
              onClick={() => {
                setDentistID('');
                setDentistName('');
              }}
            >
              trocar dentista <FaExchangeAlt />
            </ButtonWrapper>
          </DentistWrapper>
        )}

        {((user.role === ROLES.ADMIN && dentistName !== '') ||
          user.role === ROLES.DENTIST ||
          user.role === ROLES.EMPLOYEE) && (
          <Box paddingTop={2}>
            <RegisterBalanceForm
              isLoading={false}
              onClickSendButton={(values: any) => registerNewBalance(values)}
            />
          </Box>
        )}
      </Card>
    </ContentBlock>
  );
};

export default RegisterBalance;
