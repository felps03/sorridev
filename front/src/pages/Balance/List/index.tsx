import React, { useEffect, useState } from 'react';

import { FaExchangeAlt } from 'react-icons/fa';
import { useSelector } from 'react-redux';

import ContentBlock from '../../../components/ContentBlock';
import CustomDescription from '../../../components/CustomDescription';
import CustomTile from '../../../components/CustomTitle';
import TableListBalance from '../../../components/Tables/TableListBalance';
import TableListProfessional from '../../../components/Tables/TableListProfessional';
import { ROLES } from '../../../constants';
import { RootState } from '../../../store/configure-store';
import { ButtonWrapper, DentistWrapper } from './styles';

const ListBalance = () => {
  const [dentistID, setDentistID] = useState<string>('');
  const [dentistName, setDentistName] = useState<string>('');
  const user = useSelector((state: RootState) => state.user);

  useEffect(() => {
    if ([ROLES.DENTIST, ROLES.EMPLOYEE].includes(user.role)) {
      setDentistID(user._id);
    }
  }, [user._id, user.role]);

  return (
    <ContentBlock>
      <CustomTile title="Listagem de Fechamento de caixa" />
      {user.role === ROLES.ADMIN && (
        <CustomDescription title="Selecione abaixo o profissional ao qual deseja listar itens do fluxo de caixa." />
      )}
      {user.role === ROLES.ADMIN && dentistName === '' && (
        <TableListProfessional
          onDentistChangeSetID={setDentistID}
          onDentistChangeSetName={setDentistName}
          confirmationModalMessage="Deseja ver o fehcamento de caixa do (a) dentista selecionado (a)?"
          isEdit
        />
      )}

      {user.role === ROLES.ADMIN && dentistName !== '' && (
        <DentistWrapper>
          Dentista Selecionado: {dentistName}
          <ButtonWrapper
            variant="outlined"
            onClick={() => {
              setDentistID('');
              setDentistName('');
            }}
          >
            trocar dentista <FaExchangeAlt />
          </ButtonWrapper>
        </DentistWrapper>
      )}

      {((user.role === ROLES.ADMIN && dentistName !== '') ||
        user.role === ROLES.DENTIST ||
        user.role === ROLES.EMPLOYEE) && (
        <TableListBalance
          isAdmin={user.role === ROLES.ADMIN}
          dentistID={dentistID}
        />
      )}
    </ContentBlock>
  );
};

export default ListBalance;
