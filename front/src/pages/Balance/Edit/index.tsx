import React, { useState } from 'react';

import { Box } from '@material-ui/core';
import { FaExchangeAlt } from 'react-icons/fa';
import { useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';

import ContentBlock from '../../../components/ContentBlock';
import CustomDescription from '../../../components/CustomDescription';
import CustomTile from '../../../components/CustomTitle';
import RegisterBalanceForm from '../../../components/Form/RegisterBalanceForm';
import TableListBalance from '../../../components/Tables/TableListBalance';
import TableListProfessional from '../../../components/Tables/TableListProfessional';
import { MESSAGES, ROLES } from '../../../constants';
import { RootState } from '../../../store/configure-store';
import { UpdateBalanceRequest } from '../../../store/ducks/Dashboard/services';
import CallToaster from '../../../utils/callToaster';
import { ButtonWrapper, DentistWrapper } from './styles';

const EditService = () => {
  const history = useHistory();
  const [loading, setLoading] = useState<boolean>(false);
  const [dentistID, setDentistID] = useState<string>('');
  const [balance, setBalance] = useState<any>();
  const [dentistName, setDentistName] = useState<string>('');
  const user = useSelector((state: RootState) => state.user);

  const UpdateBalance = async (values: any) => {
    setLoading(true);

    if (user.role === 'ADMIN') values.dentist = dentistID;
    if (user.role === 'EMPLOYEE') values.dentist = user.dentist;
    const response = await UpdateBalanceRequest(balance._id, values);

    if (response.status === 200) {
      CallToaster({
        type: 'success',
        title: MESSAGES.SUCCESS_BALANCE_REGISTER.title,
        description: MESSAGES.SUCCESS_BALANCE_REGISTER.description
      });
      history.push('/dashboard/financeiro/lista');
    }

    if (response.status === 409) {
      CallToaster({
        type: 'error',
        title: MESSAGES.CONFLICT_BALANCE.title,
        description: MESSAGES.CONFLICT_BALANCE.description
      });
    }
    if (response.status === 500) {
      CallToaster({
        type: 'error',
        title: MESSAGES.INTERNAL_SERVER_REGISTER.title,
        description: MESSAGES.INTERNAL_SERVER_REGISTER.description
      });
    }

    setLoading(false);
  };

  return (
    <ContentBlock>
      <CustomTile title="Editar Entrada no Fluxo de Caixa" />
      {user.role === ROLES.ADMIN && (
        <CustomDescription title="Selecione abaixo o profissional ao qual deseja editar um item do fluxo de caixa." />
      )}
      {user.role === ROLES.ADMIN && dentistName === '' && (
        <TableListProfessional
          onDentistChangeSetID={setDentistID}
          onDentistChangeSetName={setDentistName}
          confirmationModalMessage="Deseja editar o fluxo de caixa do dentista selecionado (a)?"
          isEdit
        />
      )}

      {user.role === ROLES.ADMIN && dentistName !== '' && (
        <DentistWrapper>
          Dentista Selecionado: {dentistName}
          <ButtonWrapper
            variant="outlined"
            onClick={() => {
              setDentistID('');
              setDentistName('');
            }}
          >
            trocar dentista <FaExchangeAlt />
          </ButtonWrapper>
        </DentistWrapper>
      )}
      {((user.role === ROLES.DENTIST && !balance) ||
        (user.role === ROLES.EMPLOYEE && !balance) ||
        (!balance && dentistName !== '')) && (
        <TableListBalance
          confirmationModalMessage="Deseja editar esta entrada?"
          onSelected={setBalance}
          isAdmin={user.role === ROLES.ADMIN}
          dentistID={dentistID}
          isEdit
        />
      )}
      {((user.role === ROLES.DENTIST && balance) ||
        (user.role === ROLES.EMPLOYEE && balance) ||
        (balance && dentistName !== '')) && (
        <Box paddingTop={2}>
          <RegisterBalanceForm
            isLoading={loading}
            isEdit
            balanceData={balance}
            onClickSendButton={(values: any) => UpdateBalance(values)}
          />
        </Box>
      )}
    </ContentBlock>
  );
};

export default EditService;
