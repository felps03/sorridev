import { Button } from '@material-ui/core';
import styled from 'styled-components';

const ButtonWrapper = styled(Button)`
  padding: 1px 8px;
  margin-left: 15px;
  &.MuiButton-outlined {
    border: 1px solid ${(props) => props.theme.colors.primary};
    color: ${(props) => props.theme.colors.primary};
    padding: 1px 8px;
    > span > svg {
      margin-left: 10px;
    }
  }
`;

const DentistWrapper = styled.section`
  padding: 20px 10px 10px 0px;
`;
export { ButtonWrapper, DentistWrapper };
