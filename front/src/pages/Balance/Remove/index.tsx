import React, { useState } from 'react';

import { FaExchangeAlt } from 'react-icons/fa';
import { useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import Swal from 'sweetalert2';

import ContentBlock from '../../../components/ContentBlock';
import CustomDescription from '../../../components/CustomDescription';
import CustomTile from '../../../components/CustomTitle';
import TableListBalance from '../../../components/Tables/TableListBalance';
import TableListProfessional from '../../../components/Tables/TableListProfessional';
import { MESSAGES, ROLES } from '../../../constants';
import { RootState } from '../../../store/configure-store';
import { DeleteBalanceRequest } from '../../../store/ducks/Dashboard/services';
import CallToaster from '../../../utils/callToaster';
import { ButtonWrapper, DentistWrapper } from './styles';

const RemoveBalance = () => {
  const history = useHistory();
  const [dentistID, setDentistID] = useState<string>('');
  const [balance, setBalance] = useState<any>();
  const [dentistName, setDentistName] = useState<string>('');
  const user = useSelector((state: RootState) => state.user);

  const handleDeleteService = async (query: any) => {
    const response = await DeleteBalanceRequest(query[0]._id);
    if (response.status === 204) {
      Swal.fire('Sucesso!', 'Fluxo de caixa removido.', 'success');
      CallToaster({
        type: 'success',
        title: 'Sucesso',
        description: 'Fluxo de caixa removido!'
      });
      history.push('/dashboard/financeiro/lista');
    }

    if (response.status === 409) {
      CallToaster({
        type: 'error',
        title: 'Conflito',
        description: 'Não foi possível registrar o fluxo de caixa!'
      });
    }
    if (response.status === 500) {
      CallToaster({
        type: 'error',
        title: MESSAGES.INTERNAL_SERVER_REGISTER.title,
        description: MESSAGES.INTERNAL_SERVER_REGISTER.description
      });
    }
  };

  return (
    <ContentBlock>
      <CustomTile title="Remover Entrada no Fluxo de Caixa" />
      {user.role === ROLES.ADMIN && (
        <CustomDescription title="Selecione abaixo o profissional ao qual deseja remover uma entrada no fluxo de caixa." />
      )}
      {user.role === ROLES.ADMIN && dentistName === '' && (
        <TableListProfessional
          onDentistChangeSetID={setDentistID}
          onDentistChangeSetName={setDentistName}
          confirmationModalMessage="Deseja remover um fluxo de caixa do dentista selecionado (a)?"
          isEdit
        />
      )}

      {user.role === ROLES.ADMIN && dentistName !== '' && (
        <DentistWrapper>
          Dentista Selecionado: {dentistName}
          <ButtonWrapper
            variant="outlined"
            onClick={() => {
              setDentistID('');
              setDentistName('');
            }}
          >
            trocar dentista <FaExchangeAlt />
          </ButtonWrapper>
        </DentistWrapper>
      )}

      {((user.role === ROLES.DENTIST && !balance) ||
        (user.role === ROLES.EMPLOYEE && !balance) ||
        (!balance && dentistName !== '')) && (
        <TableListBalance
          confirmationModalMessage="Deseja remover esta entrada?"
          onDelete={handleDeleteService}
          onSelected={setBalance}
          isAdmin={user.role === ROLES.ADMIN}
          dentistID={dentistID}
          isDeleting
        />
      )}
    </ContentBlock>
  );
};

export default RemoveBalance;
