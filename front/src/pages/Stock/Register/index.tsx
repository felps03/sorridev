import React, { useState } from 'react';

import { Card } from '@material-ui/core';
import { FaExchangeAlt } from 'react-icons/fa';
import { useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';

import ContentBlock from '../../../components/ContentBlock';
import CustomDescription from '../../../components/CustomDescription';
import CustomTile from '../../../components/CustomTitle';
import RegisterStockForm from '../../../components/Form/RegisterStockForm';
import TableListProfessional from '../../../components/Tables/TableListProfessional';
import { MESSAGES, ROLES } from '../../../constants';
import { RootState } from '../../../store/configure-store';
import { RegisterStockRequest } from '../../../store/ducks/Dashboard/services';
import CallToaster from '../../../utils/callToaster';
import { DentistWrapper, ButtonWrapper } from '../../Balance/List/styles';

const RegisterStock = () => {
  const history = useHistory();
  const [loading, setLoading] = useState<boolean>(false);
  const [dentistID, setDentistID] = useState<string>('');
  const [dentistName, setDentistName] = useState<string>('');
  const user = useSelector((state: RootState) => state.user);

  const registerNewStock = async (values: any) => {
    setLoading(true);

    if (user.role === ROLES.ADMIN) {
      values.dentist = dentistID;
    }

    const response = await RegisterStockRequest(values);

    if (response.status === 201) {
      CallToaster({
        type: 'success',
        title: MESSAGES.SUCCESS_STOCK_REGISTER.title,
        description: MESSAGES.SUCCESS_STOCK_REGISTER.description
      });
      history.push('/dashboard/estoque/lista');
    }
    if (response.status === 409) {
      CallToaster({
        type: 'error',
        title: MESSAGES.CONFLICT_STOCK.title,
        description: MESSAGES.CONFLICT_STOCK.description
      });
    }
    if (response.status === 500) {
      CallToaster({
        type: 'error',
        title: MESSAGES.INTERNAL_SERVER_REGISTER.title,
        description: MESSAGES.INTERNAL_SERVER_REGISTER.description
      });
    }

    setLoading(false);
  };

  return (
    <ContentBlock>
      <CustomTile title="Cadastro de itens no estoque" />
      {user.role === ROLES.ADMIN && (
        <CustomDescription title="Selecione abaixo o profissional ao qual deseja cadastrar um novo item de estoque." />
      )}
      <Card elevation={0}>
        <div style={{ paddingTop: 30 }}>
          {user.role === ROLES.ADMIN && dentistName === '' && (
            <TableListProfessional
              onDentistChangeSetID={setDentistID}
              onDentistChangeSetName={setDentistName}
              confirmationModalMessage="Deseja registrar um item ao estoque do(a) dentista selecionado (a)?"
              isEdit
            />
          )}

          {user.role === ROLES.ADMIN && dentistName !== '' && (
            <DentistWrapper>
              Dentista Selecionado: {dentistName}
              <ButtonWrapper
                variant="outlined"
                onClick={() => {
                  setDentistID('');
                  setDentistName('');
                }}
              >
                trocar dentista <FaExchangeAlt />
              </ButtonWrapper>
            </DentistWrapper>
          )}

          {((user.role === ROLES.ADMIN && dentistName !== '') ||
            user.role === ROLES.DENTIST ||
            user.role === ROLES.EMPLOYEE) && (
            <RegisterStockForm
              isLoading={loading}
              onClickSendButton={(values: any) => registerNewStock(values)}
            />
          )}
        </div>
      </Card>
    </ContentBlock>
  );
};

export default RegisterStock;
