import React, { useState } from 'react';

import { FaExchangeAlt } from 'react-icons/fa';
import { useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import Swal from 'sweetalert2';

import ContentBlock from '../../../components/ContentBlock';
import CustomDescription from '../../../components/CustomDescription';
import CustomTile from '../../../components/CustomTitle';
import TableListProfessional from '../../../components/Tables/TableListProfessional';
import TableListStock from '../../../components/Tables/TableListStock';
import { MESSAGES, ROLES } from '../../../constants';
import { RootState } from '../../../store/configure-store';
import { DeleteStockRequest } from '../../../store/ducks/Dashboard/services';
import CallToaster from '../../../utils/callToaster';
import { ButtonWrapper, DentistWrapper } from './styles';

const RemoveStock = () => {
  const history = useHistory();
  const [dentistID, setDentistID] = useState<string>('');
  const [stock, setStock] = useState<any>();
  const [dentistName, setDentistName] = useState<string>('');
  const user = useSelector((state: RootState) => state.user);

  const handleDeleteStock = async (query: any) => {
    const response = await DeleteStockRequest(query[0]._id);
    if (response.status === 204) {
      Swal.fire('Sucesso!', 'Item de estoque removido.', 'success');
      CallToaster({
        type: 'success',
        title: 'Sucesso',
        description: 'Item de estoque removido!'
      });
      history.push('/dashboard/estoque/lista');
    }

    if (response.status === 409) {
      CallToaster({
        type: 'error',
        title: 'Conflito',
        description: 'Não foi possível remover o item de estoque!'
      });
    }
    if (response.status === 500) {
      CallToaster({
        type: 'error',
        title: MESSAGES.INTERNAL_SERVER_REGISTER.title,
        description: MESSAGES.INTERNAL_SERVER_REGISTER.description
      });
    }
  };

  return (
    <ContentBlock>
      <CustomTile title="Remover itens de estoque" />
      {user.role === ROLES.ADMIN && (
        <CustomDescription title="Selecione abaixo o profissional ao qual deseja remover um item de estoque." />
      )}
      {user.role === ROLES.ADMIN && dentistName === '' && (
        <TableListProfessional
          onDentistChangeSetID={setDentistID}
          onDentistChangeSetName={setDentistName}
          confirmationModalMessage="Deseja remover um estoque do dentista selecionado (a)?"
          isDeleting
        />
      )}

      {user.role === ROLES.ADMIN && dentistName !== '' && (
        <DentistWrapper>
          Dentista Selecionado: {dentistName}
          <ButtonWrapper
            variant="outlined"
            onClick={() => {
              setDentistID('');
              setDentistName('');
            }}
          >
            trocar dentista <FaExchangeAlt />
          </ButtonWrapper>
        </DentistWrapper>
      )}

      {((user.role === ROLES.DENTIST && !stock) ||
        (user.role === ROLES.EMPLOYEE && !stock) ||
        (!stock && dentistName !== '')) && (
        <TableListStock
          confirmationModalMessage="Deseja remover este item de estoque?"
          onSelected={setStock}
          onDelete={handleDeleteStock}
          isAdmin={user.role === ROLES.ADMIN}
          dentistID={dentistID}
          isDeleting
        />
      )}
    </ContentBlock>
  );
};

export default RemoveStock;
