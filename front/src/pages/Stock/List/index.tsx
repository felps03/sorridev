import React, { useEffect, useState } from 'react';

import { Card } from '@material-ui/core';
import { FaExchangeAlt } from 'react-icons/fa';
import { useSelector } from 'react-redux';

import ContentBlock from '../../../components/ContentBlock';
import CustomDescription from '../../../components/CustomDescription';
import CustomTile from '../../../components/CustomTitle';
import TableListProfessional from '../../../components/Tables/TableListProfessional';
import TableListStock from '../../../components/Tables/TableListStock';
import { ROLES } from '../../../constants';
import { RootState } from '../../../store/configure-store';
import { ButtonWrapper, DentistWrapper } from './styles';

const ListStock = () => {
  const [dentistID, setDentistID] = useState<string>('');
  const [dentistName, setDentistName] = useState<string>('');
  const user = useSelector((state: RootState) => state.user);

  useEffect(() => {
    if (user.role === ROLES.DENTIST) {
      setDentistID(user._id);
    }
    if (user.role === ROLES.EMPLOYEE) {
      setDentistID(user.dentist || '');
    }
  }, [user]);

  return (
    <ContentBlock>
      <CustomTile title="Listagem de estoque" />
      {user.role === ROLES.ADMIN && (
        <CustomDescription title="Selecione abaixo o profissional ao qual deseja ver a listagem de estoque." />
      )}
      <Card elevation={0}>
        {user.role === ROLES.ADMIN && dentistName === '' && (
          <TableListProfessional
            onDentistChangeSetID={setDentistID}
            onDentistChangeSetName={setDentistName}
            confirmationModalMessage="Deseja ver o estoque do dentista selecionado (a)?"
            isEdit
          />
        )}

        {user.role === ROLES.ADMIN && dentistName !== '' && (
          <DentistWrapper>
            Dentista Selecionado: {dentistName}
            <ButtonWrapper
              variant="outlined"
              onClick={() => {
                setDentistID('');
                setDentistName('');
              }}
            >
              trocar dentista <FaExchangeAlt />
            </ButtonWrapper>
          </DentistWrapper>
        )}

        {((user.role === ROLES.ADMIN && dentistName !== '') ||
          user.role === ROLES.DENTIST ||
          user.role === ROLES.EMPLOYEE) && (
          <TableListStock
            isAdmin={user.role === ROLES.ADMIN}
            dentistID={dentistID}
          />
        )}
      </Card>
    </ContentBlock>
  );
};

export default ListStock;
