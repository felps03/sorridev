import React, { useState } from 'react';

import { Box } from '@material-ui/core';
import { FaExchangeAlt } from 'react-icons/fa';
import { useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';

import ContentBlock from '../../../components/ContentBlock';
import CustomDescription from '../../../components/CustomDescription';
import CustomTile from '../../../components/CustomTitle';
import RegisterStockForm from '../../../components/Form/RegisterStockForm';
import TableListProfessional from '../../../components/Tables/TableListProfessional';
import TableListStock from '../../../components/Tables/TableListStock';
import { MESSAGES, ROLES } from '../../../constants';
import { RootState } from '../../../store/configure-store';
import { UpdateStockRequest } from '../../../store/ducks/Dashboard/services';
import CallToaster from '../../../utils/callToaster';
import { ButtonWrapper, DentistWrapper } from './styles';

const EditStock = () => {
  const history = useHistory();
  const [loading, setLoading] = useState<boolean>(false);
  const [dentistID, setDentistID] = useState<string>('');
  const [service, setService] = useState<any>();
  const [dentistName, setDentistName] = useState<string>('');
  const user = useSelector((state: RootState) => state.user);

  const UpdateStock = async (values: any) => {
    setLoading(true);

    if (user.role === 'ADMIN') values.dentist = dentistID;
    if (user.role === 'EMPLOYEE') values.dentist = user.dentist;
    const response = await UpdateStockRequest(service._id, values);

    if (response.status === 200) {
      CallToaster({
        type: 'success',
        title: MESSAGES.SUCCESS_UPDATE_EMPLOYEE.title,
        description: MESSAGES.SUCCESS_UPDATE_EMPLOYEE.description
      });
      history.push('/dashboard/estoque/lista');
    }

    if (response.status === 409) {
      CallToaster({
        type: 'error',
        title: MESSAGES.CONFLICT_UPDATE_EMPLOYEE.title,
        description: MESSAGES.CONFLICT_UPDATE_EMPLOYEE.description
      });
    }
    if (response.status === 500) {
      CallToaster({
        type: 'error',
        title: MESSAGES.INTERNAL_SERVER_REGISTER.title,
        description: MESSAGES.INTERNAL_SERVER_REGISTER.description
      });
    }

    setLoading(false);
  };

  return (
    <ContentBlock>
      <CustomTile title="Editar itens de estoque" />
      {user.role === ROLES.ADMIN && (
        <CustomDescription title="Selecione abaixo o profissional ao qual deseja editar um item de estoque." />
      )}
      {user.role === ROLES.ADMIN && dentistName === '' && (
        <TableListProfessional
          onDentistChangeSetID={setDentistID}
          onDentistChangeSetName={setDentistName}
          confirmationModalMessage="Deseja ver o estoque do dentista selecionado (a)?"
          isEdit
        />
      )}

      {user.role === ROLES.ADMIN && dentistName !== '' && (
        <DentistWrapper>
          Dentista Selecionado: {dentistName}
          <ButtonWrapper
            variant="outlined"
            onClick={() => {
              setDentistID('');
              setDentistName('');
            }}
          >
            trocar dentista <FaExchangeAlt />
          </ButtonWrapper>
        </DentistWrapper>
      )}

      {((user.role === ROLES.DENTIST && !service) ||
        (user.role === ROLES.EMPLOYEE && !service) ||
        (!service && dentistName !== '')) && (
        <TableListStock
          confirmationModalMessage="Deseja editar este item de estoque?"
          onSelected={setService}
          isAdmin={user.role === ROLES.ADMIN}
          dentistID={dentistID}
          isEdit
        />
      )}

      {((user.role === ROLES.DENTIST && service) ||
        (user.role === ROLES.EMPLOYEE && service) ||
        (service && dentistName !== '')) && (
        <Box paddingTop={2}>
          <RegisterStockForm
            isLoading={loading}
            isEdit
            stockData={service}
            onClickSendButton={(values: any) => UpdateStock(values)}
          />
        </Box>
      )}
    </ContentBlock>
  );
};

export default EditStock;
