import { ROLES } from '../constants';

const DashboardRoutes = [
  {
    path: '/dashboard',
    icon: 'BiHomeAlt',
    name: 'Home',
    permission: [ROLES.ADMIN, ROLES.DENTIST, ROLES.EMPLOYEE]
  },
  {
    path: '/dashboard/pacientes',
    icon: 'BsPeople',
    name: 'Pacientes',
    permission: [ROLES.ADMIN, ROLES.DENTIST, ROLES.EMPLOYEE],
    subMenu: [
      {
        path: '/dashboard/pacientes/lista',
        icon: 'BiHomeAlt',
        name: 'Lista Pacientes',
        permission: [ROLES.ADMIN, ROLES.DENTIST, ROLES.EMPLOYEE]
      },
      {
        path: '/dashboard/pacientes/criar',
        icon: 'BiHomeAlt',
        name: 'Adicionar Pacientes',
        permission: [ROLES.ADMIN, ROLES.DENTIST, ROLES.EMPLOYEE]
      },
      {
        path: '/dashboard/pacientes/editar',
        icon: 'BiHomeAlt',
        name: 'Editar Pacientes',
        permission: [ROLES.ADMIN, ROLES.DENTIST, ROLES.EMPLOYEE]
      },
      {
        path: '/dashboard/pacientes/excluir',
        icon: 'BiHomeAlt',
        name: 'Excluir Pacientes',
        permission: [ROLES.ADMIN, ROLES.DENTIST, ROLES.EMPLOYEE]
      }
    ]
  },
  {
    path: '/dashboard/agenda/agendamentos',
    icon: 'BsFolder',
    name: 'Agenda',
    permission: [ROLES.ADMIN, ROLES.DENTIST, ROLES.USER, ROLES.EMPLOYEE]
  },
  {
    path: '/dashboard/profissionais',
    icon: 'RiCustomerService2Line',
    name: 'Profissionais',
    permission: [ROLES.ADMIN],
    subMenu: [
      {
        path: '/dashboard/profissionais/lista',
        icon: 'BiHomeAlt',
        name: 'Ver lista de profissionais',
        permission: [ROLES.ADMIN]
      },
      {
        path: '/dashboard/profissionais/adicionar',
        icon: 'BiHomeAlt',
        name: 'Adicionar profissionais',
        permission: [ROLES.ADMIN]
      },
      {
        path: '/dashboard/profissionais/editar',
        icon: 'BiHomeAlt',
        name: 'Editar profissionais',
        permission: [ROLES.ADMIN]
      },
      {
        path: '/dashboard/profissionais/excluir',
        icon: 'BiHomeAlt',
        name: 'Excluir profissionais',
        permission: [ROLES.ADMIN]
      }
    ]
  },
  {
    path: '/dashboard/funcionario',
    icon: 'RiCustomerService2Line',
    name: 'Funcionários',
    permission: [ROLES.ADMIN, ROLES.DENTIST, ROLES.PROFESSIONAL],
    subMenu: [
      {
        path: '/dashboard/funcionario/lista',
        icon: 'BiHomeAlt',
        name: 'Ver lista de funcionario',
        permission: [ROLES.ADMIN, ROLES.DENTIST, ROLES.PROFESSIONAL]
      },
      {
        path: '/dashboard/funcionario/adicionar',
        icon: 'BiHomeAlt',
        name: 'Adicionar funcionario',
        permission: [ROLES.ADMIN, ROLES.DENTIST, ROLES.PROFESSIONAL]
      },
      {
        path: '/dashboard/funcionario/editar',
        icon: 'BiHomeAlt',
        name: 'Editar funcionario',
        permission: [ROLES.ADMIN, ROLES.DENTIST, ROLES.PROFESSIONAL]
      },
      {
        path: '/dashboard/funcionario/excluir',
        icon: 'BiHomeAlt',
        name: 'Excluir funcionario',
        permission: [ROLES.ADMIN, ROLES.DENTIST, ROLES.PROFESSIONAL]
      }
    ]
  },
  {
    path: '/dashboard/financeiro',
    icon: 'FaRegMoneyBillAlt',
    name: 'Financeiro',
    permission: [ROLES.ADMIN, ROLES.DENTIST, ROLES.EMPLOYEE],
    subMenu: [
      {
        path: '/dashboard/financeiro/lista',
        icon: 'BiHomeAlt',
        name: 'Ver fluxo de caixa',
        permission: [ROLES.ADMIN, ROLES.DENTIST, ROLES.EMPLOYEE]
      },
      {
        path: '/dashboard/financeiro/cadastrar',
        icon: 'BiHomeAlt',
        name: 'Adicionar novo lançamento',
        permission: [ROLES.ADMIN, ROLES.DENTIST, ROLES.EMPLOYEE]
      },
      {
        path: '/dashboard/financeiro/editar',
        icon: 'BiHomeAlt',
        name: 'Editar lançamento',
        permission: [ROLES.ADMIN, ROLES.DENTIST, ROLES.EMPLOYEE]
      },
      {
        path: '/dashboard/financeiro/excluir',
        icon: 'BiHomeAlt',
        name: 'Excluir lançamento',
        permission: [ROLES.ADMIN, ROLES.DENTIST, ROLES.EMPLOYEE]
      }
    ]
  },
  {
    path: '/dashboard/servicos',
    icon: 'RiCustomerService2Line',
    name: 'Serviços Gerais',
    permission: [ROLES.ADMIN, ROLES.DENTIST, ROLES.EMPLOYEE],
    subMenu: [
      {
        path: '/dashboard/servicos/lista',
        icon: 'BiHomeAlt',
        name: 'Ver lista de Serviços Gerais',
        permission: [ROLES.ADMIN, ROLES.DENTIST, ROLES.EMPLOYEE]
      },
      {
        path: '/dashboard/servicos/cadastrar',
        icon: 'BiHomeAlt',
        name: 'Cadastro de Serviços Gerais',
        permission: [ROLES.ADMIN, ROLES.DENTIST, ROLES.EMPLOYEE]
      },
      {
        path: '/dashboard/servicos/editar',
        icon: 'BiHomeAlt',
        name: 'Edição de Serviços Gerais',
        permission: [ROLES.ADMIN, ROLES.DENTIST, ROLES.EMPLOYEE]
      },
      {
        path: '/dashboard/servicos/excluir',
        icon: 'BiHomeAlt',
        name: 'Exclusão de Serviços Gerais',
        permission: [ROLES.ADMIN, ROLES.DENTIST, ROLES.EMPLOYEE]
      }
    ]
  },
  {
    path: '/dashboard/estoque',
    icon: 'RiCustomerService2Line',
    name: 'Estoque',
    permission: [ROLES.ADMIN, ROLES.DENTIST, ROLES.EMPLOYEE],
    subMenu: [
      {
        path: '/dashboard/estoque/lista',
        icon: 'BiHomeAlt',
        name: 'Ver estoque',
        permission: [ROLES.ADMIN, ROLES.DENTIST, ROLES.EMPLOYEE]
      },
      {
        path: '/dashboard/estoque/cadastrar',
        icon: 'BiHomeAlt',
        name: 'Adicionar nova entrada ao estoque',
        permission: [ROLES.ADMIN, ROLES.DENTIST, ROLES.EMPLOYEE]
      },
      {
        path: '/dashboard/estoque/editar',
        icon: 'BiHomeAlt',
        name: 'Editar item do estoque',
        permission: [ROLES.ADMIN, ROLES.DENTIST, ROLES.EMPLOYEE]
      },
      {
        path: '/dashboard/estoque/excluir',
        icon: 'BiHomeAlt',
        name: 'Excluir item do estoque',
        permission: [ROLES.ADMIN, ROLES.DENTIST, ROLES.EMPLOYEE]
      }
    ]
  },
  {
    path: '/dashboard/configurar',
    icon: 'BsFillGearFill',
    name: 'Configurar',
    permission: [ROLES.ADMIN, ROLES.DENTIST, ROLES.USER, ROLES.EMPLOYEE]
  }
];

export default Object.freeze(DashboardRoutes);
