const Specialties = [
  'Cirurgia e Traumatologia Buco-Maxilo-Faciais',
  'Dentística',
  'Disfunção Temporomandibular e Dor Orofacial',
  'Endodontia',
  'Estomatologia',
  'Radiologia Odontológica e Imaginologia',
  'Implantodontia',
  'Odontologia Legal',
  'Odontologia do Trabalho',
  'Odontologia para Pacientes com Necessidades Especiais',
  'Odontogeriatria',
  'Odontopediatria',
  'Ortodontia',
  'Ortopedia Funcional dos Maxilares',
  'Patologia Bucal',
  'Periodontia',
  'Prótese Buco-Maxilo-Facial',
  'Prótese Dentária',
  'Saúde Coletiva e da Família.'
];

export default Object.freeze(Specialties);
