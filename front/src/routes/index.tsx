import React, { Suspense } from 'react';

import { BrowserRouter, Switch } from 'react-router-dom';

import Dashboard from '../pages/Dashboard';
import Login from '../pages/Login';
import Register from '../pages/Register';
import { ProtectedRoute } from './permissions-route';
import { PublicRoute } from './public-route';

const Routes = () => (
  <BrowserRouter>
    <Suspense fallback={<h1>Loading...</h1>}>
      <Switch>
        <PublicRoute exact path="/login" component={Login} />
        <PublicRoute exact path="/cadastro" component={Register} />
        <Switch>
          <ProtectedRoute
            path={['/', '/dashboard', '/dashboard/*']}
            component={Dashboard}
            requiredRole={['USER', 'DENTIST', 'EMPLOYEE', 'ADMIN']}
          />
        </Switch>
      </Switch>
    </Suspense>
  </BrowserRouter>
);

export default Routes;
