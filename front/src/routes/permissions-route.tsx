/* eslint-disable react/require-default-props */
/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';

import { useSelector } from 'react-redux';
import { Redirect, Route } from 'react-router-dom';

import { RootState } from '../store/configure-store';

interface Teste {
  path: any;
  component: any;
  render?: any;
  requiredRole: any;
}

export const ProtectedRoute = ({
  path,
  component: Component,
  render,
  requiredRole,
  ...rest
}: Teste) => {
  const userRole = useSelector((state: RootState) => state.user.role);
  return (
    <Route
      path={path}
      {...rest}
      render={(props) => {
        if (!requiredRole.includes(userRole)) {
          return <Redirect push to="/login" />;
        }
        return Component ? <Component {...props} /> : render(props);
      }}
    />
  );
};

export default ProtectedRoute;
