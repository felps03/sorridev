/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';

import { useSelector } from 'react-redux';
import { Redirect, Route } from 'react-router-dom';

import { RootState } from '../store/configure-store';

export const PublicRoute = ({ ...props }) => {
  const user = useSelector((state: RootState) => state.user);

  return user.isAuthenticated ? (
    <Redirect to="/dashboard" />
  ) : (
    <Route {...props} />
  );
};
