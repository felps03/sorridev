import { Action } from '..';
import { IError } from '../Error';

export interface LoginValues {
  email: string;
  password: string;
}

export interface LoginAction extends Action {
  payload: LoginValues;
}

export interface UserState {
  _id: string;
  email: string;
  name: string;
  userMenu: any;
  photo?: string;
  role: string;
  isAuthenticating: boolean;
  isAuthenticated?: boolean;
  isLoginFailed?: boolean;
  dentist?: string;
}

export interface UserPayload extends Action {
  payload: UserState;
}

export interface UserJWT {
  _id: string;
  blockedAt: boolean;
  exp: number;
  iat: number;
  owner: string;
}

export interface RegisterValues {
  name: string;
  birthdate: string;
  cpf: string;
  rg: string;
  phone: string;
  gender: string;
  email: string;
  observation?: string;
  password: string;
  confirmPassword: string;
}

export interface RegisterDashValues {
  name: string;
  birthdate: string;
  cpf: string;
  rg: string;
  phone: string;
  gender: string;
  email: string;
  observation?: string;
}

export interface RegisterAction extends Action {
  payload: RegisterValues;
}

export interface UserRegisterState {
  isRegisterSuccess: boolean;
  isRegisterError: boolean;
  registerError: IError;
  isLoading: boolean;
}

type Address = {
  cep: string;
  street: string;
  number: string;
  neighborhood: string;
  complement: string;
  city: string;
  state: string;
  country: string;
};
export interface RegisterProfessionalValues {
  name: string;
  birthdate: string;
  cpf: string;
  rg: string;
  phone: string;
  gender: string;
  email: string;
  address: Address;
  observation: string;
  password: string;
  CRO: string;
  specialist: Array<string>;
}
