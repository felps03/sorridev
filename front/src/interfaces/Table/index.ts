type BodyType = {
  emptyDataSourceMessage: string;
  deleteText: string;
  addTooltip: string;
  deleteTooltip: string;
  editTooltip: string;
  filterRow: FilterRowType;
  editRow: EditRowType;
};

type GroupingType = {
  placeholder: string;
  groupedBy: string;
};

type HeaderType = {
  actions: string;
};

type PaginationType = {
  labelDisplayedRows: string;
  labelRowsSelect: string;
  labelRowsPerPage: string;
  firstAriaLabel: string;
  firstTooltip: string;
  previousAriaLabel: string;
  previousTooltip: string;
  nextAriaLabel: string;
  nextTooltip: string;
  lastAriaLabel: string;
  lastTooltip: string;
};

type ToolbarType = {
  addRemoveColumns: string;
  nRowsSelected: string;
  showColumnsTitle: string;
  showColumnsAriaLabel: string;
  exportTitle: string;
  exportAriaLabel: string;
  exportName: string;
  searchTooltip: string;
  searchPlaceholder: string;
};

type FilterRowType = {
  filterPlaceHolder: string;
  filterTooltip: string;
};

type EditRowType = {
  deleteText: string;
  cancelTooltip: string;
  saveTooltip: string;
};

export interface TableLocaleProps {
  body: BodyType;
  grouping: GroupingType;
  header: HeaderType;
  pagination: PaginationType;
  toolbar: ToolbarType;
}

export interface TableProps {
  columns: Array<any>;
  title?: string | React.ReactNode;
  localization?: TableLocaleProps;
  options?: any;
}
