import { IError } from '../Error';

type Fetching = {
  isFetching: boolean;
  isFetchSuccess: boolean;
  isFetchError: boolean;
  error: IError;
  isLoading: boolean;
  noContent: boolean;
};

type GenderData = {
  male: number;
  female: number;
  empty: number;
  total: number;
};

type AgeData = {
  _id: string;
  count: number;
};

type AgeAnalysis = {
  data: [AgeData];
  status: Fetching;
};

type MonthlyAnalysis = {
  data: any;
  status: Fetching;
};

type AttendanceAnalysis = {
  data: any;
  status: Fetching;
};

type GenderAnalysis = {
  data: GenderData;
  status: Fetching;
};

type AbsentAnalysis = {
  data: any;
  status: Fetching;
};

export interface DashboardState {
  gender: GenderAnalysis;
  age: AgeAnalysis;
  monthly: MonthlyAnalysis;
  attendance: AttendanceAnalysis;
  absent: AbsentAnalysis;
}
