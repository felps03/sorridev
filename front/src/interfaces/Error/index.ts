export interface IError {
  motive: string;
  key: string;
  message: string;
  statusCode: number;
}
