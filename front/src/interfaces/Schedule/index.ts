import { IError } from '../Error';

export interface ScheduleState {
  newUserThisMonth: number;
  newUserLastMonth: number;
  attendanceThisMonth: number;
  attendanceLastMonth: number;
  absenceThisMonth: number;
  absenceLastMonth: number;
  yesterday: number;
  today: number;
  tomorrow: number;
  fetching: boolean;
  fetched: boolean;
  fetchError: boolean;
  error: IError;
}
