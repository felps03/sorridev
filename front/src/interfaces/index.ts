/* eslint-disable @typescript-eslint/no-explicit-any */
import { Action as ReduxActionType } from 'redux';

declare global {
  interface Window {
    __REDUX_DEVTOOLS_EXTENSION_COMPOSE__: any;
  }
}

/**
 * Default Action Type
 *
 * Use esse type como padrão nas actions dos ducks.
 * O parâmetro payload deve conter todo dado relevante para o state da
 * aplicação.
 */
export interface Action extends ReduxActionType {
  payload?: any;
}
