require('dotenv/config');

const config = NODE_ENV => {
  const environment = {
    homolog: {
      MONGO_URL:
        'mongodb+srv://sorridev:ZH0nBrzYYcCe4qDY@cluster0.by7zs.mongodb.net/sorridevDEV?retryWrites=true&w=majority',
      APP_SECRET: 'sorrirAgenda',
      PASSWORD_SALT: 10
    },
    dev: {
      MONGO_URL: 'mongodb://localhost:27017/sorriDev',
      APP_SECRET: 'test',
      PASSWORD_SALT: 5
    },
    test: {
      MONGO_URL: 'mongodb://localhost:27017/sorriDevTest',
      APP_SECRET: 'test',
      PASSWORD_SALT: 1
    }
  };

  return environment[NODE_ENV];
};

module.exports = config(process.env.NODE_ENV);
