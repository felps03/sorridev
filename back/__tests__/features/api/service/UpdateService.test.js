const request = require('supertest');

const app = require('../../../../src/app');
const utils = require('../../../../src/helpers/utils');

const {
  ServiceDataFaker,
  EmployeeDataFaker,
  UserDataFaker,
  ProfessionalDataFaker
} = require('../../../support/dataFaker');

const { MongoSeed } = require('../../../support/insertDatabase');

const ROUTE = id => `/service/${id}`;
const COLLECTION = 'Service';

describe(`API :: PUT ${ROUTE}`, () => {
  describe('[SUCCESS] Should return :: 200 :: PROFESSIONAL', () => {
    let response = '';

    beforeEach(async () => {
      const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());
      let service = ServiceDataFaker(1, Professional);
      const [{ _id }] = await MongoSeed(COLLECTION, service);
      response = await request(app)
        .put(ROUTE(_id))
        .send({
          name: 'teste',
          phone: '00 0 00000-000',
          dentist: service.dentist
        })
        .set('Authorization', `Bearer ${utils.generateToken(Professional)}`);
    });

    it(`[SUCCESS] Should return ${COLLECTION}`, async () => {
      expect(response.status).toBe(200);
    });

    it('[SUCCESS] Check JSON', async () => {
      expect(response.body).toEqual({
        _id: expect.any(String),
        name: expect.any(String),
        phone: expect.any(String),
        supply: expect.any(String)
      });
    });

    it('[SUCCESS] Check Name and Phone', async () => {
      expect(response.body.name).toEqual('teste');
      expect(response.body.phone).toEqual('00 0 00000-000');
    });
  });

  describe('[SUCCESS] Should return :: 401', () => {
    let response = '';

    beforeEach(async () => {
      const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());
      let service = ServiceDataFaker(1, Professional);
      const [{ _id }] = await MongoSeed(COLLECTION, service);
      response = await request(app)
        .put(ROUTE(_id))
        .send({
          name: 'teste',
          phone: '00 0 00000-000',
          dentist: service.dentist
        });
    });

    it(`[SUCCESS] Should return one ${COLLECTION}`, async () => {
      expect(response.status).toBe(401);
    });

    it('[SUCCESS] Check JSON', async () => {
      expect(response.status).toBe(401);
      expect(response.body.key).toBe('UNAUTHORIZED');
      expect(response.body.message).toBe(
        'The request has not been applied because it lacks valid authentication credentials for the target resource.'
      );
      expect(response.body.detail.motive).toBe(
        'Oops, we notice that you are missing some items on your authentication. Please, try again.'
      );
    });
  });

  describe('[SUCCESS] Should return :: 403 :: BLOCK', () => {
    let response = '';
    beforeEach(async () => {
      const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());
      const user = EmployeeDataFaker(1, Professional);
      user.blockedAt = true;
      const [{ _id }] = await MongoSeed(COLLECTION, ServiceDataFaker(1, Professional));
      response = await request(app)
        .put(ROUTE(_id))
        .send({
          cash_accounting: 'teste',
          spending: 'teste'
        })
        .set('Authorization', `Bearer ${utils.generateToken(user)}`);
    });

    it(`[SUCCESS] Should return one ${COLLECTION}`, async () => {
      expect(response.status).toBe(403);
    });

    it('[SUCCESS] Check JSON', async () => {
      expect(response.status).toBe(403);
      expect(response.body.key).toBe('FORBIDDEN');
      expect(response.body.message).toBe('The request could not be made because the resource was blocked.');
      expect(response.body.detail.motive).toBe(
        'Oops, we encountered a problem with your account. Please contact the administrators'
      );
    });
  });

  describe('[SUCCESS] Should return :: 403 :: USER', () => {
    let response = '';
    beforeEach(async () => {
      const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());
      const [{ _id }] = await MongoSeed(COLLECTION, ServiceDataFaker(1, Professional));
      response = await request(app)
        .put(ROUTE(_id))
        .send({
          cash_accounting: 'teste',
          spending: 'teste'
        })
        .set('Authorization', `Bearer ${utils.generateToken(UserDataFaker(1, Professional))}`);
    });

    it(`[SUCCESS] Should return one ${COLLECTION}`, async () => {
      expect(response.status).toBe(403);
    });

    it('[SUCCESS] Check JSON', async () => {
      expect(response.status).toBe(403);
      expect(response.body.key).toBe('FORBIDDEN');
      expect(response.body.message).toBe('The request could not be made because the resource was blocked.');
      expect(response.body.detail.motive).toBe(
        'To access this area you should be either a Professional, Employee or Admin'
      );
    });
  });

  describe('[SUCCESS] Should return :: 404', () => {
    it(`[SUCCESS] Should not return any ${COLLECTION}`, async () => {
      const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());
      let service = ServiceDataFaker(1, Professional);
      const response = await request(app)
        .put(ROUTE('5fdf7078d435f53cb02bfe00'))
        .send({
          name: 'teste',
          phone: '00 0 00000-000',
          dentist: service.dentist
        })
        .set('Authorization', `Bearer ${utils.generateToken(Professional)}`);
      expect(response.status).toBe(404);
      expect(response.body.key).toBe('NOT_FOUND');
      expect(response.body.message).toBe(
        "The request couldn't be accepted because the requested resource couldn't be founded."
      );
      expect(response.body.detail.motive).toBe(
        'Oops, we faced a problem trying to updating your service. Please try again later.'
      );
    });
  });
});
