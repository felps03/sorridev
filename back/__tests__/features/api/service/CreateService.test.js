const request = require('supertest');

const app = require('../../../../src/app');
const utils = require('../../../../src/helpers/utils');

const {
  ServiceDataFaker,
  EmployeeDataFaker,
  UserDataFaker,
  ProfessionalDataFaker
} = require('../../../support/dataFaker');

const { MongoSeed } = require('../../../support/insertDatabase');

const ROUTE = '/service';
const COLLECTION = 'Service';

describe(`API :: POST ${ROUTE}`, () => {
  describe('[SUCCESS] Should return :: 201', () => {
    describe('PROFESSIONAL', () => {
      let response = '';
      beforeEach(async () => {
        const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());
        response = await request(app)
          .post(ROUTE)
          .send(ServiceDataFaker(1, Professional))
          .set('Authorization', `Bearer ${utils.generateToken(Professional)}`);
      });

      it(`[SUCCESS] Should return Status 201`, done => {
        expect(response.status).toBe(201);
        done();
      });

      it('[SUCCESS] Check JSON', done => {
        expect(response.body).toEqual({
          _id: expect.any(String),
          name: expect.any(String),
          phone: expect.any(String),
          supply: expect.any(String)
        });
        done();
      });
    });
  });

  describe('[SUCCESS] Should return :: 401', () => {
    let response = '';

    beforeEach(async () => {
      const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());
      response = await request(app)
        .post(ROUTE)
        .send(ServiceDataFaker(1, Professional));
    });

    it(`[SUCCESS] Should return one ${COLLECTION}`, async () => {
      expect(response.status).toBe(401);
    });

    it('[SUCCESS] Check JSON', async () => {
      expect(response.status).toBe(401);
      expect(response.body.key).toBe('UNAUTHORIZED');
      expect(response.body.message).toBe(
        'The request has not been applied because it lacks valid authentication credentials for the target resource.'
      );
      expect(response.body.detail.motive).toBe(
        'Oops, we notice that you are missing some items on your authentication. Please, try again.'
      );
    });
  });

  describe('[SUCCESS] Should return :: 403 :: BLOCK', () => {
    let response = '';

    beforeEach(async () => {
      const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());
      const user = EmployeeDataFaker(1, Professional);
      user.blockedAt = true;
      response = await request(app)
        .post(ROUTE)
        .send(ServiceDataFaker(1, Professional))
        .set('Authorization', `Bearer ${utils.generateToken(user)}`);
    });

    it(`[SUCCESS] Should return one ${COLLECTION}`, async () => {
      expect(response.status).toBe(403);
    });

    it('[SUCCESS] Check JSON', async () => {
      expect(response.status).toBe(403);
      expect(response.body.key).toBe('FORBIDDEN');
      expect(response.body.message).toBe('The request could not be made because the resource was blocked.');
      expect(response.body.detail.motive).toBe(
        'Oops, we encountered a problem with your account. Please contact the administrators'
      );
    });
  });

  describe('[SUCCESS] Should return :: 403 :: USER', () => {
    let response = '';

    beforeEach(async () => {
      const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());
      response = await request(app)
        .post(ROUTE)
        .send(ServiceDataFaker(1, Professional))
        .set('Authorization', `Bearer ${utils.generateToken(UserDataFaker(1, Professional))}`);
    });

    it(`[SUCCESS] Should return one ${COLLECTION}`, async () => {
      expect(response.status).toBe(403);
    });

    it('[SUCCESS] Check JSON', async () => {
      expect(response.status).toBe(403);
      expect(response.body.key).toBe('FORBIDDEN');
      expect(response.body.message).toBe('The request could not be made because the resource was blocked.');
      expect(response.body.detail.motive).toBe(
        'To access this area you should be either a Professional, Employee or Admin'
      );
    });
  });

  describe('[SUCCESS] Should return :: 500', () => {
    let response = '';

    beforeEach(async () => {
      const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());
      let service = ServiceDataFaker(1, Professional);
      delete service.name;
      response = await request(app)
        .post(ROUTE)
        .send(service)
        .set('Authorization', `Bearer ${utils.generateToken(Professional)}`);
    });

    it(`[SUCCESS] Should return one ${COLLECTION}`, async () => {
      expect(response.status).toBe(500);
    });

    it('[SUCCESS] Check JSON', async () => {
      expect(response.body.key).toBe('SERVER_INTERNAL_ERROR');
      expect(response.body.message).toBe("The request couldn't be fulfilled due to an Internal Server Error.");
      expect(response.body.detail.motive).toBe('Service validation failed: name: Path `name` is required.');
    });
  });
});
