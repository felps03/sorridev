const request = require('supertest');

const app = require('../../../../src/app');

const { ProfessionalDataFaker, UserDataFaker } = require('../../../support/dataFaker');

const { MongoSeed } = require('../../../support/insertDatabase');

const ROUTE = '/auth/google';
const COLLECTION = 'User';

describe(`API :: POST ${ROUTE}`, () => {
  describe('[SUCCESS] Should return :: 200', () => {
    describe('USER', () => {
      let response = '';

      beforeEach(async () => {
        const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());
        const user = UserDataFaker(1, Professional);
        const [{ name, email }] = await MongoSeed(COLLECTION, user);
        response = await request(app)
          .post(ROUTE)
          .send({
            name,
            email
          });
      });

      it(`[SUCCESS] Should return Status 200`, done => {
        expect(response.status).toBe(200);
        expect(response.body).not.toBeNull();
        expect(response.header).not.toBeNull();
        done();
      });

      it('[SUCCESS] Check JSON Body', done => {
        expect(response.body).toEqual({
          _id: expect.any(String),
          name: expect.any(String),
          email: expect.any(String)
        });
        done();
      });

      it('[SUCCESS] Check JSON Headers', done => {
        expect(Object.keys(response.headers)).toEqual(
          expect.arrayContaining([
            'access-control-allow-origin',
            'content-security-policy',
            'x-dns-prefetch-control',
            'expect-ct',
            'x-frame-options',
            'strict-transport-security',
            'x-download-options',
            'x-content-type-options',
            'x-permitted-cross-domain-policies',
            'referrer-policy',
            'x-xss-protection',
            'token',
            'access-control-expose-headers',
            'content-type',
            'content-length',
            'etag',
            'date',
            'connection'
          ])
        );
        done();
      });
    });
  });

  describe('[SUCCESS] Should return :: 400', () => {
    let response = '';

    beforeEach(async () => {
      response = await request(app)
        .post(ROUTE)
        .send({
          name: 'teste',
          email: '@gmail.com'
        });
    });

    it(`[SUCCESS] Should return Status 400`, done => {
      expect(response.status).toBe(400);
      done();
    });

    it('[SUCCESS] Check JSON Body', done => {
      expect(response.body.key).toBe('BAD_REQUEST_PARAMETER');
      expect(response.body.message).toBe("The request couldn't be accepted due malformed request syntax.");
      expect(response.body.detail).not.toBeNull();
      expect(response.body.detail.motive).toBe('Email is not valid');
      done();
    });
  });
});
