const request = require('supertest');

const app = require('../../../../src/app');

const {
  AdminDataFaker,
  EmployeeDataFaker,
  ProfessionalDataFaker,
  UserDataFaker
} = require('../../../support/dataFaker');

const { MongoSeed } = require('../../../support/insertDatabase');

const ROUTE = '/auth';
const COLLECTION = 'User';

describe(`API :: POST ${ROUTE}`, () => {
  describe('[SUCCESS] Should return :: 200', () => {
    describe('ADMIN', () => {
      let response = '';

      beforeEach(async () => {
        const Admin = AdminDataFaker();
        Admin.password = '$2a$10$ge2B03SjNrkkPwHVDdTi8e9aTuioJyul5NEPIb4lHiNl1SW9lth.m';
        const [{ email }] = await MongoSeed('Admin', Admin);
        response = await request(app)
          .post(ROUTE)
          .send({
            email,
            password: '123456'
          });
      });

      it(`[SUCCESS] Should return Status 200`, done => {
        expect(response.status).toBe(200);
        expect(response.body).not.toBeNull();
        expect(response.header).not.toBeNull();
        done();
      });

      it('[SUCCESS] Check JSON Body', done => {
        expect(response.body).toEqual({
          _id: expect.any(String),
          name: expect.any(String),
          email: expect.any(String)
        });
        done();
      });

      it('[SUCCESS] Check JSON Headers', done => {
        expect(Object.keys(response.headers)).toEqual(
          expect.arrayContaining([
            'access-control-allow-origin',
            'content-security-policy',
            'x-dns-prefetch-control',
            'expect-ct',
            'x-frame-options',
            'strict-transport-security',
            'x-download-options',
            'x-content-type-options',
            'x-permitted-cross-domain-policies',
            'referrer-policy',
            'x-xss-protection',
            'token',
            'access-control-expose-headers',
            'content-type',
            'content-length',
            'etag',
            'date',
            'connection'
          ])
        );
        done();
      });
    });

    describe('USER', () => {
      let response = '';

      beforeEach(async () => {
        const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());
        const user = UserDataFaker(1, Professional);
        const { email } = user;
        user.password = '$2a$10$ge2B03SjNrkkPwHVDdTi8e9aTuioJyul5NEPIb4lHiNl1SW9lth.m';
        await MongoSeed(COLLECTION, user);
        response = await request(app)
          .post(ROUTE)
          .send({
            email,
            password: '123456'
          });
      });

      it(`[SUCCESS] Should return Status 200`, done => {
        expect(response.status).toBe(200);
        expect(response.body).not.toBeNull();
        expect(response.header).not.toBeNull();
        done();
      });

      it('[SUCCESS] Check JSON Body', done => {
        expect(response.body).toEqual({
          _id: expect.any(String),
          name: expect.any(String),
          email: expect.any(String)
        });
        done();
      });

      it('[SUCCESS] Check JSON Headers', done => {
        expect(Object.keys(response.headers)).toEqual(
          expect.arrayContaining([
            'access-control-allow-origin',
            'content-security-policy',
            'x-dns-prefetch-control',
            'expect-ct',
            'x-frame-options',
            'strict-transport-security',
            'x-download-options',
            'x-content-type-options',
            'x-permitted-cross-domain-policies',
            'referrer-policy',
            'x-xss-protection',
            'token',
            'access-control-expose-headers',
            'content-type',
            'content-length',
            'etag',
            'date',
            'connection'
          ])
        );
        done();
      });
    });

    describe('PROFESSIONAL', () => {
      let response = '';

      beforeEach(async () => {
        const ProfessionalFaker = ProfessionalDataFaker();
        ProfessionalFaker.password = '$2a$10$ge2B03SjNrkkPwHVDdTi8e9aTuioJyul5NEPIb4lHiNl1SW9lth.m';
        const [Professional] = await MongoSeed('Professional', ProfessionalFaker);
        const { email } = Professional;

        response = await request(app)
          .post(ROUTE)
          .send({
            email,
            password: '123456'
          });
      });

      it(`[SUCCESS] Should return Status 200`, done => {
        expect(response.status).toBe(200);
        expect(response.body).not.toBeNull();
        expect(response.header).not.toBeNull();
        done();
      });

      it('[SUCCESS] Check JSON Body', done => {
        expect(response.status).toBe(200);
        expect(response.body).toEqual({
          _id: expect.any(String),
          name: expect.any(String),
          email: expect.any(String)
        });
        done();
      });

      it('[SUCCESS] Check JSON Headers', done => {
        expect(Object.keys(response.headers)).toEqual(
          expect.arrayContaining([
            'access-control-allow-origin',
            'content-security-policy',
            'x-dns-prefetch-control',
            'expect-ct',
            'x-frame-options',
            'strict-transport-security',
            'x-download-options',
            'x-content-type-options',
            'x-permitted-cross-domain-policies',
            'referrer-policy',
            'x-xss-protection',
            'token',
            'access-control-expose-headers',
            'content-type',
            'content-length',
            'etag',
            'date',
            'connection'
          ])
        );
        done();
      });
    });

    describe('EMPLOYEE', () => {
      let response = '';

      beforeEach(async () => {
        const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());
        const employee = EmployeeDataFaker(1, Professional);
        const { email } = employee;
        employee.password = '$2a$10$ge2B03SjNrkkPwHVDdTi8e9aTuioJyul5NEPIb4lHiNl1SW9lth.m';
        await MongoSeed('Employee', employee);
        response = await request(app)
          .post(ROUTE)
          .send({
            email,
            password: '123456'
          });
      });

      it(`[SUCCESS] Should return Status 200`, done => {
        expect(response.status).toBe(200);
        expect(response.body).not.toBeNull();
        expect(response.header).not.toBeNull();
        done();
      });

      it('[SUCCESS] Check JSON Body', done => {
        expect(response.body).toEqual({
          _id: expect.any(String),
          name: expect.any(String),
          email: expect.any(String),
          dentist: expect.any(String)
        });
        done();
      });

      it('[SUCCESS] Check JSON Headers', done => {
        expect(Object.keys(response.headers)).toEqual(
          expect.arrayContaining([
            'access-control-allow-origin',
            'content-security-policy',
            'x-dns-prefetch-control',
            'expect-ct',
            'x-frame-options',
            'strict-transport-security',
            'x-download-options',
            'x-content-type-options',
            'x-permitted-cross-domain-policies',
            'referrer-policy',
            'x-xss-protection',
            'token',
            'access-control-expose-headers',
            'content-type',
            'content-length',
            'etag',
            'date',
            'connection'
          ])
        );
        done();
      });
    });
  });

  describe('[SUCCESS] Should return :: 401', () => {
    let response = '';

    beforeEach(async () => {
      const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());
      const user = UserDataFaker(1, Professional);
      const { email } = user;
      user.password = '$2a$10$ge2B03SjNrkkPwHVDdTi8e9aTuioJyul5NEPIb4lHiNl1SW9lth.m';
      await MongoSeed(COLLECTION, user);
      response = await request(app)
        .post(ROUTE)
        .send({
          email,
          password: '123457'
        });
    });

    it(`[SUCCESS] Should return Status 401`, done => {
      expect(response.status).toBe(401);
      done();
    });

    it('[SUCCESS] Check JSON Body', done => {
      expect(response.body.key).toBe('UNAUTHORIZED');
      expect(response.body.message).toBe(
        'The request has not been applied because it lacks valid authentication credentials for the target resource.'
      );
      expect(response.body.detail).not.toBeNull();
      done();
    });
  });

  describe('[SUCCESS] Should return :: 404', () => {
    let response = '';

    beforeEach(async () => {
      const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());
      const user = UserDataFaker(1, Professional);
      user.password = '$2a$10$ge2B03SjNrkkPwHVDdTi8e9aTuioJyul5NEPIb4lHiNl1SW9lth.m';
      await MongoSeed(COLLECTION, user);
      response = await request(app)
        .post(ROUTE)
        .send({
          email: 'teste@gmail.com',
          password: '123456'
        });
    });

    it(`[SUCCESS] Should return Status 404`, done => {
      expect(response.status).toBe(404);
      done();
    });

    it('[SUCCESS] Check JSON Body', done => {
      expect(response.body.key).toBe('NOT_FOUND');
      expect(response.body.message).toBe(
        "The request couldn't be accepted because the requested resource couldn't be founded."
      );
      expect(response.body.detail).not.toBeNull();
      done();
    });
  });
});
