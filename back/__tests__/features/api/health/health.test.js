const request = require('supertest');

const app = require('../../../../src/app');

const ROUTE = '/health';

describe(`API :: GET ${ROUTE}`, () => {
  it('[SUCCESS]  Should health is OK', async () => {
    const response = await request(app).get('/health');
    expect(response.status).toBe(200);
    expect(response.body).toMatchObject({ message: 'Welcome to Sorri Dev' });
  });
});
