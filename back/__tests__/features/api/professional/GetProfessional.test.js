const request = require('supertest');

const app = require('../../../../src/app');
const utils = require('../../../../src/helpers/utils');

const { AdminDataFaker, ProfessionalDataFaker, UserDataFaker } = require('../../../support/dataFaker');

const { MongoSeed } = require('../../../support/insertDatabase');

const ROUTE = '/professional';
const COLLECTION = 'Professional';

describe(`API :: GET ${ROUTE}`, () => {
  describe('Should return :: 200', () => {
    describe('ADMIN', () => {
      describe('Empty Parameter', () => {
        let response = '';

        beforeEach(async () => {
          await MongoSeed(COLLECTION, ProfessionalDataFaker());
          response = await request(app)
            .get(ROUTE)
            .set('Authorization', `Bearer ${utils.generateToken(AdminDataFaker())}`);
        });

        it(`Should return status 200`, done => {
          expect(response.status).toBe(200);
          expect(response.body.total).toBe(1);
          expect(response.body.limit).toBe(100);
          expect(response.body.page).toBe(1);
          expect(response.body.pages).toBe(1);
          done();
        });

        it('Check JSON', done => {
          expect(response.body).toEqual({
            docs: [
              {
                _id: expect.any(String),
                address: {
                  cep: expect.any(String),
                  city: expect.any(String),
                  complement: expect.any(String),
                  country: expect.any(String),
                  neighborhood: expect.any(String),
                  number: expect.any(String),
                  state: expect.any(String),
                  street: expect.any(String)
                },
                birthdate: expect.any(String),
                cpf: expect.any(String),
                age: expect.any(Number),
                email: expect.any(String),
                gender: expect.any(String),
                name: expect.any(String),
                rg: expect.any(String),
                owner: expect.any(String),
                phone: expect.any(String),
                CRO: expect.any(String),
                specialist: expect.any(Array)
              }
            ],
            total: expect.any(Number),
            limit: expect.any(Number),
            page: expect.any(Number),
            pages: expect.any(Number)
          });
          done();
        });
      });

      describe('With Parameters', () => {
        describe('Search', () => {
          let response = '';

          beforeEach(async () => {
            const professional = ProfessionalDataFaker();
            professional.CRO = '123456';
            await MongoSeed(COLLECTION, professional);
            response = await request(app)
              .get(ROUTE)
              .query({ search: '123456' })
              .set('Authorization', `Bearer ${utils.generateToken(AdminDataFaker())}`);
          });

          it(`Should return status 200`, done => {
            expect(response.status).toBe(200);
            expect(response.body.total).toBe(1);
            expect(response.body.limit).toBe(100);
            expect(response.body.page).toBe(1);
            expect(response.body.pages).toBe(1);
            done();
          });

          it('Check JSON', done => {
            expect(response.body).toEqual({
              docs: [
                {
                  _id: expect.any(String),
                  address: {
                    cep: expect.any(String),
                    city: expect.any(String),
                    complement: expect.any(String),
                    country: expect.any(String),
                    neighborhood: expect.any(String),
                    number: expect.any(String),
                    state: expect.any(String),
                    street: expect.any(String)
                  },
                  birthdate: expect.any(String),
                  cpf: expect.any(String),
                  age: expect.any(Number),
                  email: expect.any(String),
                  gender: expect.any(String),
                  name: expect.any(String),
                  rg: expect.any(String),
                  owner: expect.any(String),
                  phone: expect.any(String),
                  CRO: expect.any(String),
                  specialist: expect.any(Array)
                }
              ],
              total: expect.any(Number),
              limit: expect.any(Number),
              page: expect.any(Number),
              pages: expect.any(Number)
            });
            done();
          });
        });
      });
    });
  });

  describe('Should return :: 204', () => {
    describe('ADMIN', () => {
      let response = '';

      beforeEach(async () => {
        const [Admin] = await MongoSeed('Admin', AdminDataFaker());
        response = await request(app)
          .get(ROUTE)
          .set('Authorization', `Bearer ${utils.generateToken(Admin)}`);
      });

      it(`Should return status 204`, done => {
        expect(response.status).toBe(204);
        done();
      });

      it('Check JSON', done => {
        expect(response.body).toEqual({});
        done();
      });
    });
  });

  describe('Should return :: 400', () => {
    describe('ADMIN', () => {
      describe('Validation Param Invalid', () => {
        let response = '';

        beforeEach(async () => {
          await MongoSeed(COLLECTION, ProfessionalDataFaker());
          response = await request(app)
            .get(ROUTE)
            .query({ dentist: '123456' })
            .set('Authorization', `Bearer ${utils.generateToken(AdminDataFaker())}`);
        });

        it(`Should return status 400`, done => {
          expect(response.status).toBe(400);
          done();
        });

        it('Check JSON', done => {
          expect(response.body.key).toBe('BAD_REQUEST_PARAMETER');
          expect(response.body.message).toBe("The request couldn't be accepted due malformed request syntax.");
          expect(response.body.detail.motive).toBe(
            'Oops, we faced a problem when trying to research this request. Check the ID and try again later.'
          );
          done();
        });
      });
    });
  });

  describe('Should return :: 401', () => {
    describe('UNAUTHORIZED', () => {
      let response = '';

      beforeEach(async () => {
        await MongoSeed(COLLECTION, ProfessionalDataFaker());
        response = await request(app).get(ROUTE);
      });

      it(`Should return status 401`, done => {
        expect(response.status).toBe(401);
        done();
      });

      it('Check JSON', done => {
        expect(response.body.key).toBe('UNAUTHORIZED');
        expect(response.body.message).toBe(
          'The request has not been applied because it lacks valid authentication credentials for the target resource.'
        );
        expect(response.body.detail.motive).toBe(
          'Oops, we notice that you are missing some items on your authentication. Please, try again.'
        );
        done();
      });
    });
  });

  describe('Should return :: 403', () => {
    describe('BLOCK', () => {
      let response = '';

      beforeEach(async () => {
        await MongoSeed(COLLECTION, ProfessionalDataFaker());
        const admin = AdminDataFaker();
        admin.blockedAt = true;
        response = await request(app)
          .get(ROUTE)
          .set('Authorization', `Bearer ${utils.generateToken(admin)}`);
      });

      it(`Should return status 403`, done => {
        expect(response.status).toBe(403);
        done();
      });

      it('Check JSON', done => {
        expect(response.body.key).toBe('FORBIDDEN');
        expect(response.body.message).toBe('The request could not be made because the resource was blocked.');
        expect(response.body.detail.motive).toBe(
          'Oops, we encountered a problem with your account. Please contact the administrators'
        );
        done();
      });
    });

    describe('ADMIN', () => {
      let response = '';
      beforeEach(async () => {
        await MongoSeed(COLLECTION, ProfessionalDataFaker());
        const professional = ProfessionalDataFaker();
        response = await request(app)
          .get(ROUTE)
          .set('Authorization', `Bearer ${utils.generateToken(professional)}`);
      });

      it(`Should return status 403`, done => {
        expect(response.status).toBe(403);
        done();
      });

      it('Check JSON', done => {
        expect(response.body.key).toBe('FORBIDDEN');
        expect(response.body.message).toBe('The request could not be made because the resource was blocked.');
        expect(response.body.detail.motive).toBe('To access this area you should be either a Admin');
        done();
      });
    });
  });
});
