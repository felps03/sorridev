const request = require('supertest');

const app = require('../../../../src/app');
const utils = require('../../../../src/helpers/utils');

const { AdminDataFaker, ProfessionalDataFaker, UserDataFaker } = require('../../../support/dataFaker');

const { MongoSeed } = require('../../../support/insertDatabase');

const ROUTE = '/professional/morePatients';
const COLLECTION = 'Professional';

describe(`API :: GET ${ROUTE}`, () => {
  describe('Should return :: 200', () => {
    describe('ADMIN', () => {
      let response = '';

      beforeEach(async () => {
        const Professional = await MongoSeed(COLLECTION, ProfessionalDataFaker());
        await MongoSeed('User', UserDataFaker(1, Professional));
        response = await request(app)
          .get(ROUTE)
          .set('Authorization', `Bearer ${utils.generateToken(AdminDataFaker())}`);
      });

      it(`Should return status 200`, done => {
        expect(response.status).toBe(200);
        done();
      });

      it('Check JSON', done => {
        expect(response.body).toEqual([
          {
            name: expect.any(String),
            count: expect.any(Number)
          }
        ]);
        done();
      });
    });
  });

  describe('Should return :: 401', () => {
    describe('UNAUTHORIZED', () => {
      let response = '';

      beforeEach(async () => {
        await MongoSeed(COLLECTION, ProfessionalDataFaker());
        response = await request(app).get(ROUTE);
      });

      it(`Should return status 401`, done => {
        expect(response.status).toBe(401);
        done();
      });

      it('Check JSON', done => {
        expect(response.body.key).toBe('UNAUTHORIZED');
        expect(response.body.message).toBe(
          'The request has not been applied because it lacks valid authentication credentials for the target resource.'
        );
        expect(response.body.detail.motive).toBe(
          'Oops, we notice that you are missing some items on your authentication. Please, try again.'
        );
        done();
      });
    });
  });

  describe('Should return :: 403', () => {
    describe('BLOCK', () => {
      let response = '';

      beforeEach(async () => {
        await MongoSeed(COLLECTION, ProfessionalDataFaker());
        const admin = AdminDataFaker();
        admin.blockedAt = true;
        response = await request(app)
          .get(ROUTE)
          .set('Authorization', `Bearer ${utils.generateToken(admin)}`);
      });

      it(`Should return status 403`, done => {
        expect(response.status).toBe(403);
        done();
      });

      it('Check JSON', done => {
        expect(response.body.key).toBe('FORBIDDEN');
        expect(response.body.message).toBe('The request could not be made because the resource was blocked.');
        expect(response.body.detail.motive).toBe(
          'Oops, we encountered a problem with your account. Please contact the administrators'
        );
        done();
      });
    });
  });
});
