const request = require('supertest');

const app = require('../../../../src/app');
const utils = require('../../../../src/helpers/utils');

const { UserDataFaker, ProfessionalDataFaker, AdminDataFaker } = require('../../../support/dataFaker');

const { MongoSeed } = require('../../../support/insertDatabase');

const ROUTE = id => `/professional/${id}`;
const COLLECTION = 'Professional';

describe(`API :: GET ${ROUTE}`, () => {
  let response = '';

  describe('[SUCCESS] Should return :: 200', () => {

    beforeEach(async () => {
      const [{_id}] = await MongoSeed(COLLECTION, ProfessionalDataFaker());
      const [Admin] = await MongoSeed('Admin', AdminDataFaker());
      response = await request(app)
        .get(ROUTE(_id))
        .set('Authorization', `Bearer ${utils.generateToken(Admin)}`);
    });

    it('[SUCCESS] Should return Service', async () => {      
      expect(response.status).toBe(200);
    });

    it('[SUCCESS] Check JSON', async () => {
      expect(response.body).toEqual({
        _id: expect.any(String),
        address: {
          cep: expect.any(String),
          city: expect.any(String),
          complement: expect.any(String),
          country: expect.any(String),
          neighborhood: expect.any(String),
          number: expect.any(String),
          state: expect.any(String),
          street: expect.any(String)
        },
        birthdate: expect.any(String),
        cpf: expect.any(String),
        email: expect.any(String),
        gender: expect.any(String),
        name: expect.any(String),
        rg: expect.any(String),
        owner: expect.any(String),
        phone: expect.any(String),
        CRO: expect.any(String),
        specialist: expect.any(Array),
        age: expect.any(Number)
      });
    });
  });

  describe('[SUCCESS] Should return :: 204', () => {
    it(`[SUCCESS] Should not return any ${COLLECTION}`, async () => {
      const [Admin] = await MongoSeed('Admin', AdminDataFaker());
      const response = await request(app)
        .get(ROUTE('5fdf7078d435f53cb02bfe00'))
        .set('Authorization', `Bearer ${utils.generateToken(Admin)}`);
      expect(response.status).toBe(204);
      expect(response.body).toEqual({});
    });
  });

  describe('[SUCCESS] Should return :: 401', () => {
    let response = '';

    beforeEach(async () => {
      const [{ _id }] = await MongoSeed(COLLECTION, ProfessionalDataFaker());
      response = await request(app).get(ROUTE(_id));
    });

    it(`[SUCCESS] Should return one ${COLLECTION}`, async () => {
      expect(response.status).toBe(401);
    });

    it('[SUCCESS] Check JSON', async () => {
      expect(response.status).toBe(401);
      expect(response.body.key).toBe('UNAUTHORIZED');
      expect(response.body.message).toBe(
        'The request has not been applied because it lacks valid authentication credentials for the target resource.'
      );
      expect(response.body.detail.motive).toBe(
        'Oops, we notice that you are missing some items on your authentication. Please, try again.'
      );
    });
  });

  describe('[SUCCESS] Should return :: 403', () => {
    let response = '';
    beforeEach(async () => {
      const [professional] = await MongoSeed(COLLECTION, ProfessionalDataFaker());
      const user = UserDataFaker(1, professional);
      user.blockedAt = true;
      const { _id } = professional;
      response = await request(app)
        .get(ROUTE(_id))
        .set('Authorization', `Bearer ${utils.generateToken(user)}`);
    });

    it(`[SUCCESS] Should return one ${COLLECTION}`, async () => {
      expect(response.status).toBe(403);
    });

    it('[SUCCESS] Check JSON', async () => {
      expect(response.status).toBe(403);
      expect(response.body.key).toBe('FORBIDDEN');
      expect(response.body.message).toBe('The request could not be made because the resource was blocked.');
      expect(response.body.detail.motive).toBe(
        'Oops, we encountered a problem with your account. Please contact the administrators'
      );
    });
  });
});
