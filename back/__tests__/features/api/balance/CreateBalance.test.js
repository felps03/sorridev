const request = require('supertest');

const app = require('../../../../src/app');

const utils = require('../../../../src/helpers/utils');

const {
  BalanceDataFaker,
  UserDataFaker,
  ProfessionalDataFaker,
  AdminDataFaker
} = require('../../../support/dataFaker');

const { MongoSeed } = require('../../../support/insertDatabase');

const ROUTE = '/balance';

describe(`API :: CREATE ${ROUTE}`, () => {
  describe('Should return :: 201', () => {
    describe('PROFESSIONAL', () => {
      let response = '';
      beforeEach(async () => {
        const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());
        response = await request(app)
          .post(ROUTE)
          .send(BalanceDataFaker(1, Professional))
          .set('Authorization', `Bearer ${utils.generateToken(Professional)}`);
      });

      it(`Should return Status 201`, done => {
        expect(response.status).toBe(201);
        done();
      });

      it('Check JSON', done => {
        expect(response.body).toEqual({
          _id: expect.any(String),
          cash_accounting: expect.any(String),
          spending: expect.any(String),
          number_of_appointments: expect.any(Number)
        });
        done();
      });
    });
  });

  describe('Should return :: 400', () => {
    describe('PROFESSIONAL', () => {
      describe('Validation Param Required', () => {
        describe('cash_accounting', () => {
          let response = '';

          beforeEach(async () => {
            const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());
            let balance = BalanceDataFaker(1, Professional);
            delete balance.cash_accounting;
            response = await request(app)
              .post(ROUTE)
              .send(balance)
              .set('Authorization', `Bearer ${utils.generateToken(Professional)}`);
          });

          it(`Should return Status 400`, done => {
            expect(response.status).toBe(400);
            done();
          });

          it('Check JSON', done => {
            expect(response.body.key).toBe('BAD_REQUEST_PARAMETER');
            expect(response.body.message).toBe("The request couldn't be accepted due malformed request syntax.");
            expect(response.body.detail.motive).toBe('"cash_accounting" is required');
            done();
          });
        });
      });

      describe('Validation Param Invalid', () => {
        let response = '';
        beforeEach(async () => {
          const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());
          response = await request(app)
            .post(ROUTE)
            .send(BalanceDataFaker(1, { _id: '12345678' }))
            .set('Authorization', `Bearer ${utils.generateToken(Professional)}`);
        });

        it(`Should return Status 400`, done => {
          expect(response.status).toBe(400);
          done();
        });

        it('Check JSON', done => {
          expect(response.body.key).toBe('BAD_REQUEST_PARAMETER');
          expect(response.body.message).toBe("The request couldn't be accepted due malformed request syntax.");
          expect(response.body.detail.motive).toBe(
            'Oops, we faced a problem when trying to research this request. Check the ID and try again later.'
          );
          done();
        });
      });
    });
  });

  describe('Should return :: 401', () => {
    describe('UNAUTHORIZED', () => {
      let response = '';
      beforeEach(async () => {
        const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());
        response = await request(app)
          .post(ROUTE)
          .send(BalanceDataFaker(1, Professional));
      });

      it(`Should return Status 401`, done => {
        expect(response.status).toBe(401);
        done();
      });

      it('Check JSON', done => {
        expect(response.body.key).toBe('UNAUTHORIZED');
        expect(response.body.message).toBe(
          'The request has not been applied because it lacks valid authentication credentials for the target resource.'
        );
        expect(response.body.detail.motive).toBe(
          'Oops, we notice that you are missing some items on your authentication. Please, try again.'
        );
        done();
      });
    });
  });

  describe('Should return :: 403', () => {
    describe('BLOCK', () => {
      let response = '';

      beforeEach(async () => {
        const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());
        Professional.blockedAt = true;
        response = await request(app)
          .post(ROUTE)
          .send(BalanceDataFaker(1, Professional))
          .set('Authorization', `Bearer ${utils.generateToken(Professional)}`);
      });

      it(`Should return Status 403`, done => {
        expect(response.status).toBe(403);
        done();
      });

      it('Check JSON', done => {
        expect(response.body.key).toBe('FORBIDDEN');
        expect(response.body.message).toBe('The request could not be made because the resource was blocked.');
        expect(response.body.detail.motive).toBe(
          'Oops, we encountered a problem with your account. Please contact the administrators'
        );
        done();
      });
    });

    describe('USER', () => {
      let response = '';

      beforeEach(async () => {
        const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());
        response = await request(app)
          .post(ROUTE)
          .send(BalanceDataFaker(1, Professional))
          .set('Authorization', `Bearer ${utils.generateToken(UserDataFaker(1, Professional))}`);
      });

      it(`Should return Status 403`, done => {
        expect(response.status).toBe(403);
        done();
      });

      it('Check JSON', done => {
        expect(response.body.key).toBe('FORBIDDEN');
        expect(response.body.message).toBe('The request could not be made because the resource was blocked.');
        expect(response.body.detail.motive).toBe(
          'To access this area you should be either a Professional, Employee or Admin'
        );
        done();
      });
    });
  });

  describe('Should return :: 404', () => {
    describe('ADMIN', () => {
      describe('Validation Param Invalid', () => {
        let response = '';
        beforeEach(async () => {
          const [ADMIN] = await MongoSeed('Admin', AdminDataFaker());
          response = await request(app)
            .post(ROUTE)
            .send(BalanceDataFaker(1, ADMIN))
            .set('Authorization', `Bearer ${utils.generateToken(ADMIN)}`);
        });

        it(`Should return Status 404`, done => {
          expect(response.status).toBe(404);
          done();
        });

        it('Check JSON', done => {
          expect(response.body.key).toBe('NOT_FOUND');
          expect(response.body.message).toBe(
            "The request couldn't be accepted because the requested resource couldn't be founded."
          );
          expect(response.body.detail.motive).toBe(
            'Oops, it looks like the professional was not found. Please check your data and try again'
          );
          done();
        });
      });
    });
  });
});
