const request = require('supertest');

const app = require('../../../../src/app');

const utils = require('../../../../src/helpers/utils');

const { BalanceDataFaker, UserDataFaker, ProfessionalDataFaker } = require('../../../support/dataFaker');

const { MongoSeed } = require('../../../support/insertDatabase');

const ROUTE = '/balance';
const COLLECTION = 'Balance';

describe(`API :: GET ${ROUTE}`, () => {
  describe('Should return :: 200', () => {
    describe('PROFESSIONAL', () => {
      let response = '';

      beforeEach(async () => {
        const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());
        await MongoSeed(COLLECTION, BalanceDataFaker(1, Professional));
        response = await request(app)
          .get(ROUTE)
          .set('Authorization', `Bearer ${utils.generateToken(Professional)}`);
      });

      it(`Should return status 200`, done => {
        expect(response.status).toBe(200);
        expect(response.body.total).toBe(1);
        expect(response.body.limit).toBe(100);
        expect(response.body.page).toBe(1);
        expect(response.body.pages).toBe(1);
        done();
      });

      it('Check JSON', done => {
        expect(response.body).toEqual({
          docs: [
            {
              _id: expect.any(String),
              cash_accounting: expect.any(String),
              spending: expect.any(String),
              number_of_appointments: expect.any(Number)
            }
          ],
          total: expect.any(Number),
          limit: expect.any(Number),
          page: expect.any(Number),
          pages: expect.any(Number)
        });
        done();
      });
    });
  });

  describe('Should return :: 204', () => {
    describe('PROFESSIONAL', () => {
      let response = '';

      beforeEach(async () => {
        const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());
        response = await request(app)
          .get(ROUTE)
          .set('Authorization', `Bearer ${utils.generateToken(Professional)}`);
      });

      it(`Should return status 204`, done => {
        expect(response.status).toBe(204);
        done();
      });

      it('Check JSON', done => {
        expect(response.body).toEqual({});
        done();
      });
    });
  });

  describe('Should return :: 401', () => {
    describe('UNAUTHORIZED', () => {
      let response = '';

      beforeEach(async () => {
        response = await request(app).get(ROUTE);
      });

      it(`Should return status 401`, done => {
        expect(response.status).toBe(401);
        done();
      });

      it('Check JSON', done => {
        expect(response.body.key).toBe('UNAUTHORIZED');
        expect(response.body.message).toBe(
          'The request has not been applied because it lacks valid authentication credentials for the target resource.'
        );
        expect(response.body.detail.motive).toBe(
          'Oops, we notice that you are missing some items on your authentication. Please, try again.'
        );
        done();
      });
    });
  });

  describe('Should return :: 403', () => {
    describe('BLOCK', () => {
      let response = '';

      beforeEach(async () => {
        const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());
        Professional.blockedAt = true;
        await MongoSeed(COLLECTION, BalanceDataFaker(1, Professional));
        response = await request(app)
          .get(ROUTE)
          .set('Authorization', `Bearer ${utils.generateToken(Professional)}`);
      });

      it(`Should return status`, done => {
        expect(response.status).toBe(403);
        done();
      });

      it('Check JSON', done => {
        expect(response.body.key).toBe('FORBIDDEN');
        expect(response.body.message).toBe('The request could not be made because the resource was blocked.');
        expect(response.body.detail.motive).toBe(
          'Oops, we encountered a problem with your account. Please contact the administrators'
        );
        done();
      });
    });

    describe('USER', () => {
      let response = '';
      beforeEach(async () => {
        const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());
        await MongoSeed(COLLECTION, BalanceDataFaker(1, Professional));
        response = await request(app)
          .get(ROUTE)
          .set('Authorization', `Bearer ${utils.generateToken(UserDataFaker(1, Professional))}`);
      });

      it(`Should return status`, done => {
        expect(response.status).toBe(403);
        done();
      });

      it('Check JSON', done => {
        expect(response.body.key).toBe('FORBIDDEN');
        expect(response.body.message).toBe('The request could not be made because the resource was blocked.');
        expect(response.body.detail.motive).toBe(
          'To access this area you should be either a Professional, Employee or Admin'
        );
        done();
      });
    });
  });
});
