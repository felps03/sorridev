const request = require('supertest');

const app = require('../../../../src/app');

const utils = require('../../../../src/helpers/utils');

const {
  BalanceDataFaker,
  EmployeeDataFaker,
  UserDataFaker,
  ProfessionalDataFaker
} = require('../../../support/dataFaker');

const { MongoSeed } = require('../../../support/insertDatabase');

const ROUTE = id => `/balance/${id}`;
const COLLECTION = 'Balance';

describe(`API :: PUT ${ROUTE}`, () => {
  let response = '';

  describe('[SUCCESS] Should return :: 200 :: PROFESSIONAL', () => {
    beforeEach(async () => {
      const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());
      let balance = BalanceDataFaker(1, Professional);
      const [{ _id }] = await MongoSeed(COLLECTION, balance);
      response = await request(app)
        .put(ROUTE(_id))
        .send({
          cash_accounting: 'teste',
          spending: 'teste',
          dentist: balance.dentist
        })
        .set('Authorization', `Bearer ${utils.generateToken(Professional)}`);
    });

    it(`[SUCCESS] Should return ${COLLECTION}`, async () => {
      expect(response.status).toBe(200);
    });

    it('[SUCCESS] Check JSON', async () => {
      expect(response.body).toEqual({
        _id: expect.any(String),
        cash_accounting: expect.any(String),
        spending: expect.any(String),
        number_of_appointments: expect.any(Number)
      });
    });

    it('[SUCCESS] Check Cash Accounting and Spending', async () => {
      expect(response.body.cash_accounting).toEqual('teste');
      expect(response.body.spending).toEqual('teste');
    });
  });

  describe('[SUCCESS] Should return :: 401', () => {
    beforeEach(async () => {
      let balance = BalanceDataFaker(1, {
        _id: '5fdf7078d435f53cb02bfe00'
      });
      const [{ _id }] = await MongoSeed(COLLECTION, balance);
      response = await request(app)
        .put(ROUTE(_id))
        .send({
          cash_accounting: 'teste',
          spending: 'teste'
        });
    });

    it(`[SUCCESS] Should return one ${COLLECTION}`, async () => {
      expect(response.status).toBe(401);
    });

    it('[SUCCESS] Check JSON', async () => {
      expect(response.status).toBe(401);
      expect(response.body.key).toBe('UNAUTHORIZED');
      expect(response.body.message).toBe(
        'The request has not been applied because it lacks valid authentication credentials for the target resource.'
      );
      expect(response.body.detail.motive).toBe(
        'Oops, we notice that you are missing some items on your authentication. Please, try again.'
      );
    });
  });

  describe('[SUCCESS] Should return :: 403 :: BLOCK', () => {
    let response = '';
    beforeEach(async () => {
      const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());
      const balance = BalanceDataFaker(1, Professional);
      const user = EmployeeDataFaker(1, Professional);
      user.blockedAt = true;
      const [{ _id }] = await MongoSeed(COLLECTION, balance);
      response = await request(app)
        .put(ROUTE(_id))
        .send({
          cash_accounting: 'teste',
          spending: 'teste',
          dentist: balance.dentist
        })
        .set('Authorization', `Bearer ${utils.generateToken(user)}`);
    });

    it(`[SUCCESS] Should return one ${COLLECTION}`, async () => {
      expect(response.status).toBe(403);
    });

    it('[SUCCESS] Check JSON', async () => {
      expect(response.status).toBe(403);
      expect(response.body.key).toBe('FORBIDDEN');
      expect(response.body.message).toBe('The request could not be made because the resource was blocked.');
      expect(response.body.detail.motive).toBe(
        'Oops, we encountered a problem with your account. Please contact the administrators'
      );
    });
  });

  describe('[SUCCESS] Should return :: 403 :: USER', () => {
    let response = '';

    beforeEach(async () => {
      const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());
      let balance = BalanceDataFaker(1, Professional);
      const [{ _id }] = await MongoSeed(COLLECTION, balance);
      response = await request(app)
        .put(ROUTE(_id))
        .send({
          cash_accounting: 'teste',
          spending: 'teste',
          dentist: balance.dentist
        })
        .set('Authorization', `Bearer ${utils.generateToken(UserDataFaker(1, Professional))}`);
    });

    it(`[SUCCESS] Should return one ${COLLECTION}`, async () => {
      expect(response.status).toBe(403);
    });

    it('[SUCCESS] Check JSON', async () => {
      expect(response.status).toBe(403);
      expect(response.body.key).toBe('FORBIDDEN');
      expect(response.body.message).toBe('The request could not be made because the resource was blocked.');
      expect(response.body.detail.motive).toBe(
        'To access this area you should be either a Professional, Employee or Admin'
      );
    });
  });

  describe('[SUCCESS] Should return :: 404', () => {
    it(`[SUCCESS] Should not return any ${COLLECTION}`, async () => {
      const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());
      const balance = BalanceDataFaker(1, Professional);
      const response = await request(app)
        .put(ROUTE('5fdf7078d435f53cb02bfe00'))
        .send({
          cash_accounting: 'teste',
          spending: 'R$ 2.30',
          dentist: balance.dentist
        })
        .set('Authorization', `Bearer ${utils.generateToken(Professional)}`);
      expect(response.status).toBe(404);
      expect(response.body.key).toBe('NOT_FOUND');
      expect(response.body.message).toBe(
        "The request couldn't be accepted because the requested resource couldn't be founded."
      );
      expect(response.body.detail.motive).toBe(
        'Oops, we faced a problem trying to updating your balance. Please try again later.'
      );
    });
  });
});
