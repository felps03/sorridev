const request = require('supertest');

const app = require('../../../../src/app');
const utils = require('../../../../src/helpers/utils');

const { ScheduleDataFaker, UserDataFaker, ProfessionalDataFaker } = require('../../../support/dataFaker');

const { MongoSeed } = require('../../../support/insertDatabase');

const ROUTE = '/schedule/dashboard';
const COLLECTION = 'Schedule';

describe(`API :: GET ${ROUTE}`, () => {
  describe('[SUCCESS] Should return :: 204', () => {
    describe('PROFESSIONAL', () => {
      let response = '';
      beforeEach(async () => {
        const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());
        const [User] = await MongoSeed('User', UserDataFaker(1, Professional));
        await MongoSeed(COLLECTION, ScheduleDataFaker(1, Professional, User));
        response = await request(app)
          .get(ROUTE)
          .set('Authorization', `Bearer ${utils.generateToken(Professional)}`);
      });

      it(`[SUCCESS] Should return status 204`, done => {
        expect(response.status).toBe(204);
        done();
      });
    });
  });

  describe('[SUCCESS] Should return :: 401', () => {
    let response = '';

    beforeEach(async () => {
      const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());
      const [User] = await MongoSeed('User', UserDataFaker(1, Professional));
      await MongoSeed(COLLECTION, ScheduleDataFaker(1, Professional, User));
      response = await request(app).get(ROUTE);
    });

    it(`[SUCCESS] Should return status 401`, done => {
      expect(response.status).toBe(401);
      done();
    });

    it('[SUCCESS] Check JSON', done => {
      expect(response.body.key).toBe('UNAUTHORIZED');
      expect(response.body.message).toBe(
        'The request has not been applied because it lacks valid authentication credentials for the target resource.'
      );
      expect(response.body.detail.motive).toBe(
        'Oops, we notice that you are missing some items on your authentication. Please, try again.'
      );
      done();
    });
  });

  describe('[SUCCESS] Should return :: 403', () => {
    let response = '';
    beforeEach(async () => {
      const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());
      const [User] = await MongoSeed('User', UserDataFaker(1, Professional));
      await MongoSeed(COLLECTION, ScheduleDataFaker(1, Professional, User));
      User.blockedAt = true;
      response = await request(app)
        .get(ROUTE)
        .set('Authorization', `Bearer ${utils.generateToken(User)}`);
    });

    it(`[SUCCESS] Should return status 403`, done => {
      expect(response.status).toBe(403);
      done();
    });

    it('[SUCCESS] Check JSON', done => {
      expect(response.body.key).toBe('FORBIDDEN');
      expect(response.body.message).toBe('The request could not be made because the resource was blocked.');
      expect(response.body.detail.motive).toBe(
        'Oops, we encountered a problem with your account. Please contact the administrators'
      );
      done();
    });
  });
});
