const request = require('supertest');

const app = require('../../../../src/app');
const utils = require('../../../../src/helpers/utils');

const { ScheduleDataFaker, UserDataFaker, ProfessionalDataFaker } = require('../../../support/dataFaker');

const { MongoSeed } = require('../../../support/insertDatabase');

const ROUTE = '/schedule';
const COLLECTION = 'Schedule';

describe(`API :: POST ${ROUTE}`, () => {
  describe('[SUCCESS] Should return :: 201', () => {
    describe('PROFESSIONAL', () => {
      let response = '';

      beforeEach(async () => {
        const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());
        const [User] = await MongoSeed('User', UserDataFaker(1, Professional));
        response = await request(app)
          .post(ROUTE)
          .send(ScheduleDataFaker(1, Professional, User))
          .set('Authorization', `Bearer ${utils.generateToken(Professional)}`);
      });

      it(`[SUCCESS] Should return Status 201`, done => {
        expect(response.status).toBe(201);
        done();
      });

      it('[SUCCESS] Check JSON', done => {
        expect(response.body).toEqual({
          _id: expect.any(String),
          startTime: expect.any(String),
          endTime: expect.any(String),
          client: {
            _id: expect.any(String),
            cpf: expect.any(String),
            email: expect.any(String),
            name: expect.any(String),
            observation: expect.any(String),
            phone: expect.any(String)
          },
          dentist: {
            CRO: expect.any(String),
            _id: expect.any(String),
            cpf: expect.any(String),
            email: expect.any(String),
            name: expect.any(String),
            phone: expect.any(String)
          },
          health_insurance_name: expect.any(String),
          observation: expect.any(String),
          value: expect.any(String),
          status: expect.any(String),
          hasReturn: expect.any(Boolean)
        });
        done();
      });
    });
  });

  describe('[SUCCESS] Should return :: 401', () => {
    beforeEach(async () => {
      const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());
      const [User] = await MongoSeed('User', UserDataFaker(1, Professional));
      response = await request(app)
        .post(ROUTE)
        .send(ScheduleDataFaker(1, Professional, User));
    });

    it(`[SUCCESS] Should return Status 401`, done => {
      expect(response.status).toBe(401);
      done();
    });

    it('[SUCCESS] Check JSON', done => {
      expect(response.body.key).toBe('UNAUTHORIZED');
      expect(response.body.message).toBe(
        'The request has not been applied because it lacks valid authentication credentials for the target resource.'
      );
      expect(response.body.detail.motive).toBe(
        'Oops, we notice that you are missing some items on your authentication. Please, try again.'
      );
      done();
    });
  });

  describe('[SUCCESS] Should return :: 403', () => {
    let response = '';

    beforeEach(async () => {
      const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());
      const [User] = await MongoSeed('User', UserDataFaker(1, Professional));

      User.blockedAt = true;
      response = await request(app)
        .post(ROUTE)
        .send(ScheduleDataFaker(1, Professional, User))
        .set('Authorization', `Bearer ${utils.generateToken(User)}`);
    });

    it(`[SUCCESS] Should return one ${COLLECTION}`, async () => {
      expect(response.status).toBe(403);
    });

    it('[SUCCESS] Check JSON', async () => {
      expect(response.body.key).toBe('FORBIDDEN');
      expect(response.body.message).toBe('The request could not be made because the resource was blocked.');
      expect(response.body.detail.motive).toBe(
        'Oops, we encountered a problem with your account. Please contact the administrators'
      );
    });
  });

  describe('[SUCCESS] Should return :: 500', () => {
    let response = '';

    beforeEach(async () => {
      const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());
      const [User] = await MongoSeed('User', UserDataFaker(1, Professional));
      let schedule = ScheduleDataFaker(1, Professional, User);
      delete schedule.client;
      response = await request(app)
        .post(ROUTE)
        .send(schedule)
        .set('Authorization', `Bearer ${utils.generateToken(Professional)}`);
    });

    it(`[SUCCESS] Should return one ${COLLECTION}`, async () => {
      expect(response.status).toBe(500);
    });

    it('[SUCCESS] Check JSON', async () => {
      expect(response.body.key).toBe('SERVER_INTERNAL_ERROR');
      expect(response.body.message).toBe("The request couldn't be fulfilled due to an Internal Server Error.");
      expect(response.body.detail.motive).toBe('Schedule validation failed: client: Path `client` is required.');
    });
  });
});
