const request = require('supertest');

const app = require('../../../../src/app');
const utils = require('../../../../src/helpers/utils');

const { ScheduleDataFaker, UserDataFaker, ProfessionalDataFaker } = require('../../../support/dataFaker');

const { MongoSeed } = require('../../../support/insertDatabase');

const ROUTE = '/schedule';
const COLLECTION = 'Schedule';

describe(`API :: GET ${ROUTE}`, () => {
  let response = '';

  describe('[SUCCESS] Should return :: 200', () => {
    beforeEach(async () => {
      const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());
      const [User] = await MongoSeed('User', UserDataFaker(1, Professional));
      await MongoSeed(COLLECTION, ScheduleDataFaker(1, Professional, User));
      response = await request(app)
        .get(ROUTE)
        .set('Authorization', `Bearer ${utils.generateToken(Professional)}`);
    });

    it(`[SUCCESS] Should return one ${COLLECTION}`, async () => {
      expect(response.status).toBe(200);
      expect(response.body.total).toBe(1);
      expect(response.body.limit).toBe(100);
      expect(response.body.page).toBe(1);
      expect(response.body.pages).toBe(1);
    });

    it('[SUCCESS] Check JSON', async () => {
      expect(response.body).toEqual({
        docs: [
          {
            _id: expect.any(String),
            startTime: expect.any(String),
            endTime: expect.any(String),
            health_insurance_name: expect.any(String),
            client: {
              _id: expect.any(String),
              cpf: expect.any(String),
              email: expect.any(String),
              name: expect.any(String),
              observation: expect.any(String),
              phone: expect.any(String)
            },
            dentist: {
              CRO: expect.any(String),
              _id: expect.any(String),
              cpf: expect.any(String),
              email: expect.any(String),
              name: expect.any(String),
              phone: expect.any(String)
            },
            observation: expect.any(String),
            value: expect.any(String),
            status: expect.any(String),
            hasReturn: expect.any(Boolean)
          }
        ],
        total: expect.any(Number),
        limit: expect.any(Number),
        page: expect.any(Number),
        pages: expect.any(Number)
      });
    });
  });

  describe('[SUCCESS] Should return :: 204', () => {
    it(`[SUCCESS] Should not return any ${COLLECTION}`, async () => {
      const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());
      const response = await request(app)
        .get(ROUTE)
        .set('Authorization', `Bearer ${utils.generateToken(Professional)}`);
      expect(response.status).toBe(204);
      expect(response.body).toEqual({});
    });
  });

  describe('[SUCCESS] Should return :: 401', () => {
    let response = '';

    beforeEach(async () => {
      const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());
      const [User] = await MongoSeed('User', UserDataFaker(1, Professional));
      await MongoSeed(COLLECTION, ScheduleDataFaker(1, Professional, User));
      response = await request(app).get(ROUTE);
    });

    it(`[SUCCESS] Should return one ${COLLECTION}`, async () => {
      expect(response.status).toBe(401);
    });

    it('[SUCCESS] Check JSON', async () => {
      expect(response.status).toBe(401);
      expect(response.body.key).toBe('UNAUTHORIZED');
      expect(response.body.message).toBe(
        'The request has not been applied because it lacks valid authentication credentials for the target resource.'
      );
      expect(response.body.detail.motive).toBe(
        'Oops, we notice that you are missing some items on your authentication. Please, try again.'
      );
    });
  });

  describe('[SUCCESS] Should return :: 403', () => {
    let response = '';
    beforeEach(async () => {
      const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());
      const [User] = await MongoSeed('User', UserDataFaker(1, Professional));
      await MongoSeed(COLLECTION, ScheduleDataFaker(1, Professional, User));
      User.blockedAt = true;
      response = await request(app)
        .get(ROUTE)
        .set('Authorization', `Bearer ${utils.generateToken(User)}`);
    });

    it(`[SUCCESS] Should return one ${COLLECTION}`, async () => {
      expect(response.status).toBe(403);
    });

    it('[SUCCESS] Check JSON', async () => {
      expect(response.status).toBe(403);
      expect(response.body.key).toBe('FORBIDDEN');
      expect(response.body.message).toBe('The request could not be made because the resource was blocked.');
      expect(response.body.detail.motive).toBe(
        'Oops, we encountered a problem with your account. Please contact the administrators'
      );
    });
  });
});
