const request = require('supertest');

const app = require('../../../../src/app');
const utils = require('../../../../src/helpers/utils');

const { ScheduleDataFaker, UserDataFaker, ProfessionalDataFaker } = require('../../../support/dataFaker');

const { MongoSeed } = require('../../../support/insertDatabase');

const ROUTE = id => `/schedule/${id}`;
const COLLECTION = 'Schedule';

describe(`API :: PUT ${ROUTE}`, () => {
  let response = '';

  describe('[SUCCESS] Should return :: 200', () => {
    beforeEach(async () => {
      const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());
      const [User] = await MongoSeed('User', UserDataFaker(1, Professional)); 
      const schedule = ScheduleDataFaker(1, Professional, User);
      const [{ _id }] = await MongoSeed(COLLECTION, schedule);
      response = await request(app)
        .put(ROUTE(_id))
        .send({
          value: 'R$ 2.00',
          observation: 'teste',
          dentist: schedule.dentist
        })
        .set('Authorization', `Bearer ${utils.generateToken(Professional)}`);
    });

    it(`[SUCCESS] Should return status 200`, done => {
      expect(response.status).toBe(200);
      done();
    });

    it('[SUCCESS] Check JSON', done => {
      expect(response.body).toEqual({
        _id: expect.any(String),
        startTime: expect.any(String),
        endTime: expect.any(String),
        client: {
          _id: expect.any(String),
          cpf: expect.any(String),
          email: expect.any(String),
          name: expect.any(String),
          observation: expect.any(String),
          phone: expect.any(String)
        },
        dentist: {
          CRO: expect.any(String),
          _id: expect.any(String),
          cpf: expect.any(String),
          email: expect.any(String),
          name: expect.any(String),
          phone: expect.any(String)
        },
        health_insurance_name: expect.any(String),
        observation: expect.any(String),
        value: expect.any(String),
        status: expect.any(String),
        hasReturn: expect.any(Boolean)
      });
      done();
    });

    it('[SUCCESS] Check Value and Observation', done => {
      expect(response.body.value).toEqual('R$ 2.00');
      expect(response.body.observation).toEqual('teste');
      done();
    });
  });

  describe('[SUCCESS] Should return :: 401', () => {
    let response = '';

    beforeEach(async () => {
      const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());
      const [User] = await MongoSeed('User', UserDataFaker(1, Professional)); 
      const schedule = ScheduleDataFaker(1, Professional, User);
      const [{ _id }] = await MongoSeed(COLLECTION, schedule);
      response = await request(app)
        .put(ROUTE(_id))
        .send({
          value: 'R$ 2.00',
          observation: 'teste',
          dentist: schedule._id
        });
    });

    it(`[SUCCESS] Should return one ${COLLECTION}`, async () => {
      expect(response.status).toBe(401);
    });

    it('[SUCCESS] Check JSON', async () => {
      expect(response.status).toBe(401);
      expect(response.body.key).toBe('UNAUTHORIZED');
      expect(response.body.message).toBe(
        'The request has not been applied because it lacks valid authentication credentials for the target resource.'
      );
      expect(response.body.detail.motive).toBe(
        'Oops, we notice that you are missing some items on your authentication. Please, try again.'
      );
    });
  });

  describe('[SUCCESS] Should return :: 403', () => {
    let response = '';
    beforeEach(async () => {
      const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());
      const [User] = await MongoSeed('User', UserDataFaker(1, Professional)); 
      const schedule = ScheduleDataFaker(1, Professional, User);
      Professional.blockedAt = true;
      const [{ _id }] = await MongoSeed(COLLECTION, schedule);
      response = await request(app)
        .put(ROUTE(_id))
        .send({
          value: 'R$ 2.00',
          observation: 'teste',
          dentist: Professional._id
        })
        .set('Authorization', `Bearer ${utils.generateToken(Professional)}`);
    });

    it(`[SUCCESS] Should return one ${COLLECTION}`, async () => {
      expect(response.status).toBe(403);
    });

    it('[SUCCESS] Check JSON', async () => {
      expect(response.body.key).toBe('FORBIDDEN');
      expect(response.body.message).toBe('The request could not be made because the resource was blocked.');
      expect(response.body.detail.motive).toBe(
        'Oops, we encountered a problem with your account. Please contact the administrators'
      );
    });
  });

  describe('[SUCCESS] Should return :: 500', () => {
    it(`[SUCCESS] Should not return any ${COLLECTION}`, async () => {
      const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());
      const response = await request(app)
        .put(ROUTE('5fdf7078d435f53cb02bfe00'))
        .send({
          value: 'R$ 2.00',
          observation: 'teste',
          dentist: Professional._id
        })
        .set('Authorization', `Bearer ${utils.generateToken(Professional)}`);
      expect(response.status).toBe(500);
      expect(response.body.key).toBe('SERVER_INTERNAL_ERROR');
      expect(response.body.message).toBe("The request couldn't be fulfilled due to an Internal Server Error."
      );
      expect(response.body.detail.motive).toBe(
        "Schedule validation failed: client: Path `client` is required., endTime: Path `endTime` is required., startTime: Path `startTime` is required."
      );
    });
  });
});
