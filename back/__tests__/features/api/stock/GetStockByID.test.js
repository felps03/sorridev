const request = require('supertest');

const app = require('../../../../src/app');
const utils = require('../../../../src/helpers/utils');

const {
  AdminDataFaker,
  EmployeeDataFaker,
  ProfessionalDataFaker,
  StockDataFaker,
  UserDataFaker
} = require('../../../support/dataFaker');

const { MongoSeed } = require('../../../support/insertDatabase');

const ROUTE = id => `/stock/${id}`;
const COLLECTION = 'Stock';

describe(`API :: GET ${ROUTE}`, () => {
  let response = '';

  describe('[SUCCESS] Should return :: 200', () => {
    describe('PROFESSIONAL', () => {
      beforeEach(async () => {
        const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());
        const [{ _id }] = await MongoSeed(COLLECTION, StockDataFaker(1, Professional));
        response = await request(app)
          .get(ROUTE(_id))
          .set('Authorization', `Bearer ${utils.generateToken(Professional)}`);
      });

      it(`[SUCCESS] Should return Status 200`, done => {
        expect(response.status).toBe(200);
        done();
      });

      it('[SUCCESS] Check JSON', done => {
        expect(response.body).toEqual({
          _id: expect.any(String),
          price: expect.any(String),
          product: expect.any(String),
          provider: expect.any(String),
          quantity: expect.any(Number),
          dentist: {
            CRO: expect.any(String),
            _id: expect.any(String),
            email: expect.any(String),
            name: expect.any(String),
            phone: expect.any(String),
            specialist: expect.any(Array)
          }
        });
        done();
      });
    });

    describe('ADMIN', () => {
      beforeEach(async () => {
        const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());
        const [Admin] = await MongoSeed('Admin', AdminDataFaker());
        const [{ _id }] = await MongoSeed(COLLECTION, StockDataFaker(1, Professional));
        response = await request(app)
          .get(ROUTE(_id))
          .set('Authorization', `Bearer ${utils.generateToken(Admin)}`);
      });

      it(`[SUCCESS] Should return Status 200`, done => {
        expect(response.status).toBe(200);
        done();
      });

      it('[SUCCESS] Check JSON', done => {
        expect(response.body).toEqual({
          _id: expect.any(String),
          price: expect.any(String),
          product: expect.any(String),
          provider: expect.any(String),
          quantity: expect.any(Number),
          dentist: {
            CRO: expect.any(String),
            _id: expect.any(String),
            email: expect.any(String),
            name: expect.any(String),
            phone: expect.any(String),
            specialist: expect.any(Array)
          }
        });
        done();
      });
    });
  });

  describe('[SUCCESS] Should return :: 204', () => {
    it(`[SUCCESS] Should not return any ${COLLECTION}`, async () => {
      const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());
      const response = await request(app)
        .get(ROUTE('5fdf7078d435f53cb02bfe00'))
        .set('Authorization', `Bearer ${utils.generateToken(Professional)}`);
      expect(response.status).toBe(204);
      expect(response.body).toEqual({});
    });
  });

  describe('[SUCCESS] Should return :: 401', () => {
    describe('PROFESSIONAL NOT AUTHENTICATE', () => {
      beforeEach(async () => {
        const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());
        const [{ _id }] = await MongoSeed(COLLECTION, StockDataFaker(1, Professional));
        response = await request(app).get(ROUTE(_id));
      });

      it(`[SUCCESS] Should return Status 401`, done => {
        expect(response.status).toBe(401);
        done();
      });

      it('[SUCCESS] Check JSON', done => {
        expect(response.body.key).toBe('UNAUTHORIZED');
        expect(response.body.message).toBe(
          'The request has not been applied because it lacks valid authentication credentials for the target resource.'
        );
        expect(response.body.detail.motive).toBe(
          'Oops, we notice that you are missing some items on your authentication. Please, try again.'
        );
        done();
      });
    });

    describe('PROFESSIONAL NOT ALLOWED ACCESS', () => {
      beforeEach(async () => {
        const Professional = await MongoSeed('Professional', ProfessionalDataFaker(2));
        const [{ _id }] = await MongoSeed(COLLECTION, StockDataFaker(1, Professional[0]));
        response = await request(app)
          .get(ROUTE(_id))
          .set('Authorization', `Bearer ${utils.generateToken(Professional[1])}`);
      });

      it(`[SUCCESS] Should return Status 401`, done => {
        expect(response.status).toBe(401);
        done();
      });

      it('[SUCCESS] Check JSON', done => {
        expect(response.body.key).toBe('UNAUTHORIZED');
        expect(response.body.message).toBe(
          'The request has not been applied because it lacks valid authentication credentials for the target resource.'
        );
        expect(response.body.detail.motive).toBe("Oops, it looks like you don't have permission to view this user");
        done();
      });
    });

    describe('EMPLOYEE NOT ALLOWED ACCESS', () => {
      beforeEach(async () => {
        const Professional = await MongoSeed('Professional', ProfessionalDataFaker(2));
        const [{ _id }] = await MongoSeed(COLLECTION, StockDataFaker(1, Professional[0]));
        const [Employee] = await MongoSeed('Employee', EmployeeDataFaker(1, Professional[1]));
        response = await request(app)
          .get(ROUTE(_id))
          .set('Authorization', `Bearer ${utils.generateToken(Employee)}`);
      });

      it(`[SUCCESS] Should return Status 401`, done => {
        expect(response.status).toBe(401);
        done();
      });

      it('[SUCCESS] Check JSON', done => {
        expect(response.body.key).toBe('UNAUTHORIZED');
        expect(response.body.message).toBe(
          'The request has not been applied because it lacks valid authentication credentials for the target resource.'
        );
        expect(response.body.detail.motive).toBe("Oops, it looks like you don't have permission to view this user");
        done();
      });
    });
  });

  describe('[SUCCESS] Should return :: 403', () => {
    describe('BLOCK', () => {
      let response = '';
      beforeEach(async () => {
        const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());
        const user = EmployeeDataFaker(1, Professional);
        user.blockedAt = true;
        const [{ _id }] = await MongoSeed(COLLECTION, StockDataFaker(1, Professional));
        response = await request(app)
          .get(ROUTE(_id))
          .set('Authorization', `Bearer ${utils.generateToken(user)}`);
      });

      it(`[SUCCESS] Should return Status 403`, done => {
        expect(response.status).toBe(403);
        done();
      });

      it('[SUCCESS] Check JSON', done => {
        expect(response.body.key).toBe('FORBIDDEN');
        expect(response.body.message).toBe('The request could not be made because the resource was blocked.');
        expect(response.body.detail.motive).toBe(
          'Oops, we encountered a problem with your account. Please contact the administrators'
        );
        done();
      });
    });
    describe('USER', () => {
      let response = '';
      beforeEach(async () => {
        const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());
        const [{ _id }] = await MongoSeed(COLLECTION, StockDataFaker(1, Professional));
        response = await request(app)
          .get(ROUTE(_id))
          .set('Authorization', `Bearer ${utils.generateToken(UserDataFaker(1, Professional))}`);
      });

      it(`[SUCCESS] Should return Status 403`, done => {
        expect(response.status).toBe(403);
        done();
      });

      it('[SUCCESS] Check JSON', done => {
        expect(response.body.key).toBe('FORBIDDEN');
        expect(response.body.message).toBe('The request could not be made because the resource was blocked.');
        expect(response.body.detail.motive).toBe(
          'To access this area you should be either a Professional, Employee or Admin'
        );
        done();
      });
    });
  });
});
