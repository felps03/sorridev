const request = require('supertest');

const app = require('../../../../src/app');
const utils = require('../../../../src/helpers/utils');

const {
  StockDataFaker,
  EmployeeDataFaker,
  UserDataFaker,
  ProfessionalDataFaker
} = require('../../../support/dataFaker');

const { MongoSeed } = require('../../../support/insertDatabase');

const ROUTE = '/stock';
const COLLECTION = 'Stock';

describe(`API :: CREATE ${ROUTE}`, () => {
  describe('[SUCCESS] Should return :: 201', () => {
    describe('PROFESSIONAL', () => {
      let response = '';

      beforeEach(async () => {
        const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());
        response = await request(app)
          .post(ROUTE)
          .send(StockDataFaker(1, Professional))
          .set('Authorization', `Bearer ${utils.generateToken(Professional)}`);
      });

      it(`[SUCCESS] Should return Status 201`, done => {
        expect(response.status).toBe(201);
        done();
      });

      it('[SUCCESS] Check JSON', done => {
        expect(response.body).toEqual({
          _id: expect.any(String),
          price: expect.any(String),
          product: expect.any(String),
          provider: expect.any(String),
          dentist: expect.any(String),
          quantity: expect.any(Number)
        });
        done();
      });
    });
  });

  describe('[SUCCESS] Should return :: 401', () => {
    let response = '';

    beforeEach(async () => {
      const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());
      response = await request(app)
        .post(ROUTE)
        .send(StockDataFaker(1, Professional));
    });

    it(`[SUCCESS] Should return one ${COLLECTION}`, async () => {
      expect(response.status).toBe(401);
    });

    it('[SUCCESS] Check JSON', async () => {
      expect(response.status).toBe(401);
      expect(response.body.key).toBe('UNAUTHORIZED');
      expect(response.body.message).toBe(
        'The request has not been applied because it lacks valid authentication credentials for the target resource.'
      );
      expect(response.body.detail.motive).toBe(
        'Oops, we notice that you are missing some items on your authentication. Please, try again.'
      );
    });
  });

  describe('[SUCCESS] Should return :: 403', () => {
    describe('BLOCK', () => {
      let response = '';

      beforeEach(async () => {
        const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());
        let employee = EmployeeDataFaker(1, Professional);
        employee.blockedAt = true;

        response = await request(app)
          .post(ROUTE)
          .send(StockDataFaker(1, Professional))
          .set('Authorization', `Bearer ${utils.generateToken(employee)}`);
      });

      it(`[SUCCESS] Should return one ${COLLECTION}`, async () => {
        expect(response.status).toBe(403);
      });

      it('[SUCCESS] Check JSON', async () => {
        expect(response.status).toBe(403);
        expect(response.body.key).toBe('FORBIDDEN');
        expect(response.body.message).toBe('The request could not be made because the resource was blocked.');
        expect(response.body.detail.motive).toBe(
          'Oops, we encountered a problem with your account. Please contact the administrators'
        );
      });
    });

    describe('USER', () => {
      let response = '';

      beforeEach(async () => {
        const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());

        response = await request(app)
          .post(ROUTE)
          .send(StockDataFaker(1, Professional))
          .set('Authorization', `Bearer ${utils.generateToken(UserDataFaker(1, Professional))}`);
      });

      it(`[SUCCESS] Should return one ${COLLECTION}`, async () => {
        expect(response.status).toBe(403);
      });

      it('[SUCCESS] Check JSON', async () => {
        expect(response.status).toBe(403);
        expect(response.body.key).toBe('FORBIDDEN');
        expect(response.body.message).toBe('The request could not be made because the resource was blocked.');
        expect(response.body.detail.motive).toBe(
          'To access this area you should be either a Professional, Employee or Admin'
        );
      });
    });
  });

  describe('[SUCCESS] Should return :: 500', () => {
    let response = '';

    beforeEach(async () => {
      const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());
      let stock = StockDataFaker(1, Professional);
      delete stock.quantity;
      response = await request(app)
        .post(ROUTE)
        .send(stock)
        .set('Authorization', `Bearer ${utils.generateToken(Professional)}`);
    });

    it(`[SUCCESS] Should return Status 500`, done => {
      expect(response.status).toBe(500);
      done();
    });

    it('[SUCCESS] Check JSON', done => {
      expect(response.body.key).toBe('SERVER_INTERNAL_ERROR');
      expect(response.body.message).toBe("The request couldn't be fulfilled due to an Internal Server Error.");
      expect(response.body.detail.motive).toBe('Stock validation failed: quantity: Path `quantity` is required.');
      done();
    });
  });
});
