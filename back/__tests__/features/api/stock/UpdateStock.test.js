const request = require('supertest');

const app = require('../../../../src/app');
const utils = require('../../../../src/helpers/utils');

const {
  StockDataFaker,
  EmployeeDataFaker,
  UserDataFaker,
  ProfessionalDataFaker
} = require('../../../support/dataFaker');

const { MongoSeed } = require('../../../support/insertDatabase');

const ROUTE = id => `/stock/${id}`;
const COLLECTION = 'Stock';

describe(`API :: PUT ${ROUTE}`, () => {
  let response = '';

  describe('[SUCCESS] Should return :: 200 :: PROFESSIONAL', () => {
    beforeEach(async () => {
      const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());
      const stock = StockDataFaker(1, Professional)
      const [{ _id }] = await MongoSeed(COLLECTION, stock);
      response = await request(app)
        .put(ROUTE(_id))
        .send({
          product: 'teste',
          price: 'R$ 2.30',
          dentist: stock.dentist
        })
        .set('Authorization', `Bearer ${utils.generateToken(Professional)}`);
    });

    it(`[SUCCESS] Should return ${COLLECTION}`, async () => {
      expect(response.status).toBe(200);
    });

    it('[SUCCESS] Check JSON', async () => {
      expect(response.body).toEqual({
        _id: expect.any(String),
        price: expect.any(String),
        product: expect.any(String),
        provider: expect.any(String),
        dentist: expect.any(String),
        quantity: expect.any(Number)
      });
    });

    it('[SUCCESS] Check Product and Price', async () => {
      expect(response.body.product).toEqual('teste');
      expect(response.body.price).toEqual('R$ 2.30');
    });
  });

  describe('[SUCCESS] Should return :: 401', () => {
    let response = '';

    beforeEach(async () => {
      const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());
      let stock = StockDataFaker(1, Professional);
      const [{ _id }] = await MongoSeed(COLLECTION, stock);
      response = await request(app)
        .put(ROUTE(_id))
        .send({
          product: 'teste',
          price: 'R$ 2.30',
          dentist: stock.dentist
        });
    });

    it(`[SUCCESS] Should return one ${COLLECTION}`, async () => {
      expect(response.status).toBe(401);
    });

    it('[SUCCESS] Check JSON', async () => {
      expect(response.status).toBe(401);
      expect(response.body.key).toBe('UNAUTHORIZED');
      expect(response.body.message).toBe(
        'The request has not been applied because it lacks valid authentication credentials for the target resource.'
      );
      expect(response.body.detail.motive).toBe(
        'Oops, we notice that you are missing some items on your authentication. Please, try again.'
      );
    });
  });

  describe('[SUCCESS] Should return :: 403 :: BLOCK', () => {
    let response = '';
    beforeEach(async () => {
      const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());
      const user = EmployeeDataFaker(1, Professional);
      user.blockedAt = true;
      const [{ _id }] = await MongoSeed(COLLECTION, StockDataFaker(1, Professional));
      response = await request(app)
        .put(ROUTE(_id))
        .send({
          cash_accounting: 'teste',
          spending: 'teste'
        })
        .set('Authorization', `Bearer ${utils.generateToken(user)}`);
    });

    it(`[SUCCESS] Should return one ${COLLECTION}`, async () => {
      expect(response.status).toBe(403);
    });

    it('[SUCCESS] Check JSON', async () => {
      expect(response.status).toBe(403);
      expect(response.body.key).toBe('FORBIDDEN');
      expect(response.body.message).toBe('The request could not be made because the resource was blocked.');
      expect(response.body.detail.motive).toBe(
        'Oops, we encountered a problem with your account. Please contact the administrators'
      );
    });
  });

  describe('[SUCCESS] Should return :: 403 :: USER', () => {
    let response = '';

    beforeEach(async () => {
      const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());
      let stock = StockDataFaker(1, Professional);
      const [{ _id }] = await MongoSeed(COLLECTION, stock);
      response = await request(app)
        .put(ROUTE(_id))
        .send({
          product: 'teste',
          price: 'R$ 2.30'
        })
        .set('Authorization', `Bearer ${utils.generateToken(UserDataFaker(1, Professional))}`);
    });

    it(`[SUCCESS] Should return one ${COLLECTION}`, async () => {
      expect(response.status).toBe(403);
    });

    it('[SUCCESS] Check JSON', async () => {
      expect(response.status).toBe(403);
      expect(response.body.key).toBe('FORBIDDEN');
      expect(response.body.message).toBe('The request could not be made because the resource was blocked.');
      expect(response.body.detail.motive).toBe(
        'To access this area you should be either a Professional, Employee or Admin'
      );
    });
  });

  describe('[SUCCESS] Should return :: 404', () => {
    it(`[SUCCESS] Should not return any ${COLLECTION}`, async () => {
      const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());
      let stock = StockDataFaker(1, Professional);
      const response = await request(app)
        .put(ROUTE('5fdf7078d435f53cb02bfe00'))
        .send({
          product: 'teste',
          price: 'R$ 2.30',
          dentist: stock.dentist
        })
        .set('Authorization', `Bearer ${utils.generateToken(Professional)}`);

      expect(response.status).toBe(404);
      expect(response.body.key).toBe('NOT_FOUND');
      expect(response.body.message).toBe(
        "The request couldn't be accepted because the requested resource couldn't be founded."
      );
      expect(response.body.detail.motive).toBe(
        'Oops, we faced a problem trying to updating your stock. Please try again later.'
      );
    });
  });
});
