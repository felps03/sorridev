const request = require('supertest');

const app = require('../../../../src/app');
const utils = require('../../../../src/helpers/utils');

const {
  UserDataFaker,
  ProfessionalDataFaker,
  EmployeeDataFaker,
  ScheduleDataFaker
} = require('../../../support/dataFaker');

const { MongoSeed } = require('../../../support/insertDatabase');

const ROUTE = '/user/dashboard';
const COLLECTION = 'User';

describe(`API :: GET ${ROUTE}`, () => {
  describe('[SUCCESS] Should return :: 200', () => {
    describe('PROFESSIONAL', () => {
      let response = '';
      beforeEach(async () => {
        const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());
        await MongoSeed(COLLECTION, UserDataFaker(10, Professional));
        const [User] = await MongoSeed(COLLECTION, UserDataFaker(1, Professional));
        const schedule = ScheduleDataFaker(1, Professional, User);
        schedule.status = 'compareceu';
        schedule.startTime = new Date();
        await MongoSeed('Schedule', schedule);
        response = await request(app)
          .get(ROUTE)
          .set('Authorization', `Bearer ${utils.generateToken(Professional)}`);
      });

      it(`[SUCCESS] Should return status 200`, done => {
        expect(response.status).toBe(200);
        done();
      });
    });

    describe('EMPLOYEE', () => {
      let response = '';
      beforeEach(async () => {
        const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());
        const [Employee] = await MongoSeed('Employee', EmployeeDataFaker(1, Professional));
        await MongoSeed(COLLECTION, UserDataFaker(10, Professional));
        response = await request(app)
          .get(ROUTE)
          .set('Authorization', `Bearer ${utils.generateToken(Employee)}`);
      });

      it(`[SUCCESS] Should return status 200`, done => {
        expect(response.status).toBe(200);
        done();
      });
    });
  });

  describe('[SUCCESS] Should return :: 401', () => {
    describe('USER', () => {
      let response = '';

      beforeEach(async () => {
        const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());
        await MongoSeed(COLLECTION, UserDataFaker(1, Professional));
        response = await request(app).get(ROUTE);
      });

      it(`[SUCCESS] Should return status 401`, done => {
        expect(response.status).toBe(401);
        done();
      });

      it('[SUCCESS] Check JSON', done => {
        expect(response.body.key).toBe('UNAUTHORIZED');
        expect(response.body.message).toBe(
          'The request has not been applied because it lacks valid authentication credentials for the target resource.'
        );
        expect(response.body.detail.motive).toBe(
          'Oops, we notice that you are missing some items on your authentication. Please, try again.'
        );
        done();
      });
    });
  });

  describe('[SUCCESS] Should return :: 403', () => {
    describe('USER', () => {
      let response = '';
      beforeEach(async () => {
        const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());
        const user = UserDataFaker(1, Professional);
        user.blockedAt = true;
        await MongoSeed(COLLECTION, user);
        response = await request(app)
          .get(ROUTE)
          .set('Authorization', `Bearer ${utils.generateToken(user)}`);
      });

      it(`[SUCCESS] Should return status 403`, done => {
        expect(response.status).toBe(403);
        done();
      });

      it('[SUCCESS] Check JSON', done => {
        expect(response.body.key).toBe('FORBIDDEN');
        expect(response.body.message).toBe('The request could not be made because the resource was blocked.');
        expect(response.body.detail.motive).toBe(
          'Oops, we encountered a problem with your account. Please contact the administrators'
        );
        done();
      });
    });
  });
});
