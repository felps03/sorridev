const request = require('supertest');

const app = require('../../../../src/app');
const utils = require('../../../../src/helpers/utils');

const { UserDataFaker, ProfessionalDataFaker, AdminDataFaker } = require('../../../support/dataFaker');

const { MongoSeed } = require('../../../support/insertDatabase');

const ROUTE = id => `/user/${id}`;
const COLLECTION = 'User';

describe(`API :: GET ${ROUTE}`, () => {
  describe('[SUCCESS] Should return :: 200', () => {
    describe('PROFESSIONAL', () => {
      let response = '';
      beforeEach(async () => {
        const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());
        const [{ _id }] = await MongoSeed(COLLECTION, UserDataFaker(1, Professional));
        response = await request(app)
          .get(ROUTE(_id))
          .set('Authorization', `Bearer ${utils.generateToken(Professional)}`);
      });

      it(`[SUCCESS] Should return status 200`, done => {
        expect(response.status).toBe(200);
        done();
      });

      it('[SUCCESS] Check JSON', done => {
        expect(response.body).toEqual({
          _id: expect.any(String),
          cpf: expect.any(String),
          email: expect.any(String),
          gender: expect.any(String),
          name: expect.any(String),
          observation: expect.any(String),
          phone: expect.any(String),
          birthdate: expect.any(String),
          rg: expect.any(String),
          age: expect.any(Number)
        });
        done();
      });
    });

    describe('ADMIN', () => {
      let response = '';
      beforeEach(async () => {
        const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());
        const [Admin] = await MongoSeed('Admin', AdminDataFaker());
        const [{ _id }] = await MongoSeed(COLLECTION, UserDataFaker(1, Professional));
        response = await request(app)
          .get(ROUTE(_id))
          .set('Authorization', `Bearer ${utils.generateToken(Admin)}`);
      });

      it(`[SUCCESS] Should return Status 200`, done => {
        expect(response.status).toBe(200);
        done();
      });

      it('[SUCCESS] Check JSON', done => {
        expect(response.body).toEqual({
          _id: expect.any(String),
          cpf: expect.any(String),
          email: expect.any(String),
          gender: expect.any(String),
          birthdate: expect.any(String),
          rg: expect.any(String),
          name: expect.any(String),
          dentist: [
            {
              CRO: expect.any(String),
              _id: expect.any(String),
              email: expect.any(String),
              cpf: expect.any(String),
              name: expect.any(String),
              phone: expect.any(String),
              specialist: expect.any(Array)
            }
          ],
          observation: expect.any(String),
          phone: expect.any(String),
          age: expect.any(Number)
        });
        done();
      });
    });
  });

  describe('[SUCCESS] Should return :: 204', () => {
    describe('PROFESSIONAL', () => {
      let response = '';
      beforeEach(async () => {
        const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());
        response = await request(app)
          .get(ROUTE('5fdf7078d435f53cb02bfe00'))
          .set('Authorization', `Bearer ${utils.generateToken(Professional)}`);
      });

      it(`[SUCCESS] Should return Status 204`, done => {
        expect(response.status).toBe(204);
        done();
      });

      it('[SUCCESS] Check JSON', done => {
        expect(response.body).toEqual({});
        done();
      });
    });
  });

  describe('[SUCCESS] Should return :: 401', () => {
    let response = '';

    beforeEach(async () => {
      const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());
      const [{ _id }] = await MongoSeed(COLLECTION, UserDataFaker(1, Professional));
      response = await request(app).get(ROUTE(_id));
    });

    it(`[SUCCESS] Should return status 401`, done => {
      expect(response.status).toBe(401);
      done();
    });

    it('[SUCCESS] Check JSON', done => {
      expect(response.body.key).toBe('UNAUTHORIZED');
      expect(response.body.message).toBe(
        'The request has not been applied because it lacks valid authentication credentials for the target resource.'
      );
      expect(response.body.detail.motive).toBe(
        'Oops, we notice that you are missing some items on your authentication. Please, try again.'
      );
      done();
    });
  });

  describe('[SUCCESS] Should return :: 403', () => {
    let response = '';
    beforeEach(async () => {
      const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());
      const user = UserDataFaker(1, Professional);
      user.blockedAt = true;
      const [{ _id }] = await MongoSeed(COLLECTION, user);
      response = await request(app)
        .get(ROUTE(_id))
        .set('Authorization', `Bearer ${utils.generateToken(user)}`);
    });

    it(`[SUCCESS] Should return status 403`, done => {
      expect(response.status).toBe(403);
      done();
    });

    it('[SUCCESS] Check JSON', done => {
      expect(response.body.key).toBe('FORBIDDEN');
      expect(response.body.message).toBe('The request could not be made because the resource was blocked.');
      expect(response.body.detail.motive).toBe(
        'Oops, we encountered a problem with your account. Please contact the administrators'
      );
      done();
    });
  });
});
