const request = require('supertest');
const moment = require('moment');

const app = require('../../../../src/app');

const { UserDataFaker, ProfessionalDataFaker } = require('../../../support/dataFaker');

const { MongoSeed } = require('../../../support/insertDatabase');

const ROUTE = '/user';
const COLLECTION = 'User';

describe(`API :: POST ${ROUTE}`, () => {
  describe('[SUCCESS] Should return :: 201', () => {
    let response = '';

    beforeEach(async () => {
      const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());
      const User = UserDataFaker(1, Professional);
      delete User.owner;
      response = await request(app)
        .post(ROUTE)
        .send(User);
    });

    it(`[SUCCESS] Should return status 201`, done => {
      expect(response.status).toBe(201);
      done();
    });

    it('[SUCCESS] Check JSON', done => {
      expect(response.body).toEqual({
        _id: expect.any(String),
        cpf: expect.any(String),
        email: expect.any(String),
        gender: expect.any(String),
        name: expect.any(String),
        observation: expect.any(String),
        phone: expect.any(String),
        birthdate: expect.any(String),
        rg: expect.any(String),
        age: expect.any(Number)
      });
      done();
    });
  });

  describe('Should return :: 400', () => {
    describe('USER', () => {
      describe('Validation Param Required', () => {
        describe('Name', () => {
          let response = '';

          beforeEach(async () => {
            const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());
            const User = UserDataFaker(1, Professional);
            delete User.name;
            delete User.owner;

            response = await request(app)
              .post(ROUTE)
              .send(User);
          });

          it(`[SUCCESS] Should return status 400`, done => {
            expect(response.status).toBe(400);
            done();
          });

          it('[SUCCESS] Check JSON', done => {
            expect(response.body.key).toBe('BAD_REQUEST_PARAMETER');
            expect(response.body.message).toBe("The request couldn't be accepted due malformed request syntax.");
            expect(response.body.detail.motive).toBe('"name" is required');
            done();
          });
        });
      });

      describe('Validation Param Invalid', () => {
        describe('CPF', () => {
          let response = '';

          beforeEach(async () => {
            const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());
            const User = UserDataFaker(1, Professional);
            delete User.owner;
            User.cpf = '111.111.111-11';

            response = await request(app)
              .post(ROUTE)
              .send(User);
          });

          it(`[SUCCESS] Should return status 400`, done => {
            expect(response.status).toBe(400);
            done();
          });

          it('[SUCCESS] Check JSON', done => {
            expect(response.body.key).toBe('BAD_REQUEST_PARAMETER');
            expect(response.body.message).toBe("The request couldn't be accepted due malformed request syntax.");
            expect(response.body.detail.motive).toBe('CPF is not valid');
            done();
          });
        });

        describe('EMAIL', () => {
          let response = '';

          beforeEach(async () => {
            const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());
            const User = UserDataFaker(1, Professional);
            delete User.owner;
            User.email = 'gmail.com';

            response = await request(app)
              .post(ROUTE)
              .send(User);
          });

          it(`[SUCCESS] Should return status 400`, done => {
            expect(response.status).toBe(400);
            done();
          });

          it('[SUCCESS] Check JSON', done => {
            expect(response.body.key).toBe('BAD_REQUEST_PARAMETER');
            expect(response.body.message).toBe("The request couldn't be accepted due malformed request syntax.");
            expect(response.body.detail.motive).toBe('Email is not valid');
            done();
          });
        });

        describe('BIRTHDATE ', () => {
          describe('NOT VALID', () => {
            let response = '';

            beforeEach(async () => {
              const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());
              const User = UserDataFaker(1, Professional);
              delete User.owner;
              User.birthdate = '10/10/1010';

              response = await request(app)
                .post(ROUTE)
                .send(User);
            });

            it(`[SUCCESS] Should return status 400`, done => {
              expect(response.status).toBe(400);
              done();
            });

            it('[SUCCESS] Check JSON', done => {
              expect(response.body.key).toBe('BAD_REQUEST_PARAMETER');
              expect(response.body.message).toBe("The request couldn't be accepted due malformed request syntax.");
              expect(response.body.detail.motive).toBe('Birthdate is not valid');
              done();
            });
          });

          describe('TOMORROW', () => {
            let response = '';

            beforeEach(async () => {
              const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());
              const User = UserDataFaker(1, Professional);
              delete User.owner;
              User.birthdate = moment()
                .add(1, 'days')
                .format('YYYY-MM-DD');
              response = await request(app)
                .post(ROUTE)
                .send(User);
            });

            it(`[SUCCESS] Should return status 400`, done => {
              expect(response.status).toBe(400);
              done();
            });

            it('[SUCCESS] Check JSON', done => {
              expect(response.body.key).toBe('BAD_REQUEST_PARAMETER');
              expect(response.body.message).toBe("The request couldn't be accepted due malformed request syntax.");
              expect(response.body.detail.motive).toBe('Birthdate must be before or equal today');
              done();
            });
          });

          describe('IS AFTER 1900', () => {
            let response = '';

            beforeEach(async () => {
              const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());
              const User = UserDataFaker(1, Professional);
              delete User.owner;
              User.birthdate = '1899-12-31';
              response = await request(app)
                .post(ROUTE)
                .send(User);
            });

            it(`[SUCCESS] Should return status 400`, done => {
              expect(response.status).toBe(400);
              done();
            });

            it('[SUCCESS] Check JSON', done => {
              expect(response.body.key).toBe('BAD_REQUEST_PARAMETER');
              expect(response.body.message).toBe("The request couldn't be accepted due malformed request syntax.");
              expect(response.body.detail.motive).toBe('Birthdate is After 1900');
              done();
            });
          });
        });

        describe('DENTIST', () => {
          let response = '';

          beforeEach(async () => {
            const User = UserDataFaker(1, { _id: '1234567' });
            delete User.owner;
            response = await request(app)
              .post(ROUTE)
              .send(User);
          });

          it(`[SUCCESS] Should return status 400`, done => {
            expect(response.status).toBe(400);
            done();
          });

          it('[SUCCESS] Check JSON', done => {
            expect(response.body.key).toBe('BAD_REQUEST_PARAMETER');
            expect(response.body.message).toBe("The request couldn't be accepted due malformed request syntax.");
            expect(response.body.detail.motive).toBe(
              'Oops, we faced a problem when trying to research this request. Check the ID and try again later.'
            );
            done();
          });
        });

        describe('GENDER', () => {
          let response = '';

          beforeEach(async () => {
            const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());
            const User = UserDataFaker(1, Professional);
            delete User.owner;
            User.gender = '-';

            response = await request(app)
              .post(ROUTE)
              .send(User);
          });

          it(`[SUCCESS] Should return status 400`, done => {
            expect(response.status).toBe(400);
            done();
          });

          it('[SUCCESS] Check JSON', done => {
            expect(response.body.key).toBe('BAD_REQUEST_PARAMETER');
            expect(response.body.message).toBe("The request couldn't be accepted due malformed request syntax.");
            expect(response.body.detail.motive).toBe(
              'Oops, the current gender is not valid. Please review your input.'
            );
            done();
          });
        });

        describe('CEP', () => {
          let response = '';

          beforeEach(async () => {
            const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());
            const User = UserDataFaker(1, Professional);
            delete User.owner;
            User.address.cep = '38.408-092';

            response = await request(app)
              .post(ROUTE)
              .send(User);
          });

          it(`[SUCCESS] Should return status 400`, done => {
            expect(response.status).toBe(400);
            done();
          });

          it('[SUCCESS] Check JSON', done => {
            expect(response.body.key).toBe('BAD_REQUEST_PARAMETER');
            expect(response.body.message).toBe("The request couldn't be accepted due malformed request syntax.");
            expect(response.body.detail.motive).toBe('CEP number is not valid');
            done();
          });
        });
      });
    });
  });

  describe('[SUCCESS] Should return :: 404', () => {
    let response = '';

    beforeEach(async () => {
      const Professional = ProfessionalDataFaker();
      Professional._id = '5fdf7078d435f53cb02bfe00';
      const User = UserDataFaker(1, Professional);
      delete User.owner;
      response = await request(app)
        .post(ROUTE)
        .send(User);
    });

    it(`[SUCCESS] Should Status 404`, done => {
      expect(response.status).toBe(404);
      done();
    });

    it('[SUCCESS] Check JSON', done => {
      expect(response.body.key).toBe('NOT_FOUND');
      expect(response.body.message).toBe(
        "The request couldn't be accepted because the requested resource couldn't be founded."
      );
      expect(response.body.detail.motive).toBe(
        'Oops, it looks like the professional was not found. Please check your data and try again'
      );
      done();
    });
  });

  describe('[SUCCESS] Should return :: 409', () => {
    let response = '';

    beforeEach(async () => {
      const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());
      const User = UserDataFaker(1, Professional);
      MongoSeed(COLLECTION, User);
      delete User.owner;
      response = await request(app)
        .post(ROUTE)
        .send(User);
    });

    it(`[SUCCESS] Should return status 409`, done => {
      expect(response.status).toBe(409);
      done();
    });

    it('[SUCCESS] Check JSON', done => {
      expect(response.body.key).toBe('CONFLICT');
      expect(response.body.message).toBe(
        "The request couldn't be accepted due a conflict with current state of the server."
      );
      expect(response.body.detail.motive).toBe(
        'Sorry, already this data save. Please check your email and try again later'
      );
      done();
    });
  });

  describe('[SUCCESS] Should return :: 500', () => {
    let response = '';

    beforeEach(async () => {
      const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());
      const User = UserDataFaker(1, Professional);
      delete User.email;
      delete User.owner;
      response = await request(app)
        .post(ROUTE)
        .send(User);
    });

    it(`[SUCCESS] Should return status 500`, done => {
      expect(response.status).toBe(500);
      done();
    });

    it('[SUCCESS] Check JSON', done => {
      expect(response.body.key).toBe('SERVER_INTERNAL_ERROR');
      expect(response.body.message).toBe("The request couldn't be fulfilled due to an Internal Server Error.");
      expect(response.body.detail.motive).toBe('User validation failed: email: Path `email` is required.');
      done();
    });
  });
});
