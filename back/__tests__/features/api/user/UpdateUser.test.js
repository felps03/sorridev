const request = require('supertest');

const app = require('../../../../src/app');
const utils = require('../../../../src/helpers/utils');

const { UserDataFaker, ProfessionalDataFaker } = require('../../../support/dataFaker');

const { MongoSeed } = require('../../../support/insertDatabase');

const ROUTE = id => `/user/${id}`;
const COLLECTION = 'User';

describe(`API :: PUT ${ROUTE}`, () => {
  describe('[SUCCESS] Should return :: 200', () => {
    let response = '';

    beforeEach(async () => {
      const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());
      const [{ _id }] = await MongoSeed(COLLECTION, UserDataFaker(1, Professional));
      response = await request(app)
        .put(ROUTE(_id))
        .send({
          name: 'teste',
          email: 'teste@gmail.com',
          dentist: Professional._id
        })
        .set('Authorization', `Bearer ${utils.generateToken(Professional)}`);
    });

    it(`[SUCCESS] Should return ${COLLECTION}`, async () => {
      expect(response.status).toBe(200);
    });

    it('[SUCCESS] Check JSON', async () => {
      expect(response.body).toEqual({
        _id: expect.any(String),
        cpf: expect.any(String),
        email: expect.any(String),
        gender: expect.any(String),
        name: expect.any(String),
        observation: expect.any(String),
        phone: expect.any(String),
        birthdate: expect.any(String),
        rg: expect.any(String),
        age: expect.any(Number)
      });
    });

    it('[SUCCESS] Check Name and Email', async () => {
      expect(response.body.name).toEqual('teste');
      expect(response.body.email).toEqual('teste@gmail.com');
    });
  });

  describe('[SUCCESS] Should return :: 401', () => {
    let response = '';

    beforeEach(async () => {
      const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());
      const [{ _id }] = await MongoSeed(COLLECTION, UserDataFaker(1, Professional));

      response = await request(app)
        .put(ROUTE(_id))
        .send({
          name: 'teste',
          email: 'teste@gmail.com',
          dentist: Professional._id
        });
    });

    it(`[SUCCESS] Should return one ${COLLECTION}`, async () => {
      expect(response.status).toBe(401);
    });

    it('[SUCCESS] Check JSON', async () => {
      expect(response.status).toBe(401);
      expect(response.body.key).toBe('UNAUTHORIZED');
      expect(response.body.message).toBe(
        'The request has not been applied because it lacks valid authentication credentials for the target resource.'
      );
      expect(response.body.detail.motive).toBe(
        'Oops, we notice that you are missing some items on your authentication. Please, try again.'
      );
    });
  });

  describe('[SUCCESS] Should return :: 403', () => {
    let response = '';
    beforeEach(async () => {
      const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());
      const user = UserDataFaker(1, Professional);
      user.blockedAt = true;
      const [{ _id }] = await MongoSeed(COLLECTION, user);
      response = await request(app)
        .put(ROUTE(_id))
        .send({
          name: 'teste',
          email: 'teste@gmail.com',
          dentist: Professional._id
        })
        .set('Authorization', `Bearer ${utils.generateToken(user)}`);
    });

    it(`[SUCCESS] Should return one ${COLLECTION}`, async () => {
      expect(response.status).toBe(403);
    });

    it('[SUCCESS] Check JSON', async () => {
      expect(response.status).toBe(403);
      expect(response.body.key).toBe('FORBIDDEN');
      expect(response.body.message).toBe('The request could not be made because the resource was blocked.');
      expect(response.body.detail.motive).toBe(
        'Oops, we encountered a problem with your account. Please contact the administrators'
      );
    });
  });

  describe('[SUCCESS] Should return :: 404', () => {
    describe('PROFESSIONAL', () => {
      let response = '';

      beforeEach(async () => {
        const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());
        response = await request(app)
          .put(ROUTE('5fdf7078d435f53cb02bfe00'))
          .send({
            name: 'teste',
            email: 'teste@gmail.com',
            dentist: Professional._id
          })
          .set('Authorization', `Bearer ${utils.generateToken(Professional)}`);
      });

      it(`[SUCCESS] Should return status 404 ${COLLECTION}`, done => {
        expect(response.status).toBe(404);
        done();
      });

      it(`[SUCCESS] Should not return any ${COLLECTION}`, done => {
        expect(response.body.key).toBe('NOT_FOUND');
        expect(response.body.message).toBe(
          "The request couldn't be accepted because the requested resource couldn't be founded."
        );
        expect(response.body.detail.motive).toBe(
          'Oops, we faced a problem trying to updating your account. Please try again later.'
        );
        done();
      });
    });
  });
});
