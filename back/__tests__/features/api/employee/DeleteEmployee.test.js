const request = require('supertest');

const app = require('../../../../src/app');
const utils = require('../../../../src/helpers/utils');

const { EmployeeDataFaker, ProfessionalDataFaker, UserDataFaker } = require('../../../support/dataFaker');

const { MongoSeed } = require('../../../support/insertDatabase');

const ROUTE = id => `/employee/${id}`;
const COLLECTION = 'Employee';

describe(`API :: DELETE ${ROUTE}`, () => {
  describe('[SUCCESS] Should return :: 204', () => {
    it(`[SUCCESS] Should return ${COLLECTION}`, async () => {
      const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());
      const [{ _id }] = await MongoSeed(COLLECTION, EmployeeDataFaker(1, Professional));

      const response = await request(app)
        .delete(ROUTE(_id))
        .set('Authorization', `Bearer ${utils.generateToken(UserDataFaker(1, Professional))}`);
      expect(response.status).toBe(204);
      expect(response.body).toEqual({});
    });
  });

  describe('[SUCCESS] Should return :: 401', () => {
    let response = '';

    beforeEach(async () => {
      const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());
      const [{ _id }] = await MongoSeed(COLLECTION, EmployeeDataFaker(1, Professional));
      response = await request(app).delete(ROUTE(_id));
    });

    it(`[SUCCESS] Should return one ${COLLECTION}`, async () => {
      expect(response.status).toBe(401);
    });

    it('[SUCCESS] Check JSON', async () => {
      expect(response.status).toBe(401);
      expect(response.body.key).toBe('UNAUTHORIZED');
      expect(response.body.message).toBe(
        'The request has not been applied because it lacks valid authentication credentials for the target resource.'
      );
      expect(response.body.detail.motive).toBe(
        'Oops, we notice that you are missing some items on your authentication. Please, try again.'
      );
    });
  });

  describe('[SUCCESS] Should return :: 403', () => {
    let response = '';
    beforeEach(async () => {
      const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());
      const user = UserDataFaker(1, Professional);
      user.blockedAt = true;
      const [{ _id }] = await MongoSeed(COLLECTION, EmployeeDataFaker(1, Professional));
      response = await request(app)
        .delete(ROUTE(_id))
        .set('Authorization', `Bearer ${utils.generateToken(user)}`);
    });

    it(`[SUCCESS] Should return one ${COLLECTION}`, async () => {
      expect(response.status).toBe(403);
    });

    it('[SUCCESS] Check JSON', async () => {
      expect(response.status).toBe(403);
      expect(response.body.key).toBe('FORBIDDEN');
      expect(response.body.message).toBe('The request could not be made because the resource was blocked.');
      expect(response.body.detail.motive).toBe(
        'Oops, we encountered a problem with your account. Please contact the administrators'
      );
    });
  });

  describe('[SUCCESS] Should return :: 404', () => {
    it(`[SUCCESS] Should not return any ${COLLECTION}`, async () => {
      const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());
      const response = await request(app)
        .delete(ROUTE('5fdf7078d435f53cb02bfe00'))
        .set('Authorization', `Bearer ${utils.generateToken(UserDataFaker(1, Professional))}`);
      expect(response.status).toBe(404);
      expect(response.body.key).toBe('NOT_FOUND');
      expect(response.body.message).toBe(
        "The request couldn't be accepted because the requested resource couldn't be founded."
      );
      expect(response.body.detail.motive).toBe(
        'Oops, we faced a problem trying to deleting your employee. Please try again later.'
      );
    });
  });
});
