const request = require('supertest');
const moment = require('moment');

const app = require('../../../../src/app');
const utils = require('../../../../src/helpers/utils');

const { EmployeeDataFaker, ProfessionalDataFaker, UserDataFaker } = require('../../../support/dataFaker');

const { MongoSeed } = require('../../../support/insertDatabase');

const ROUTE = '/employee';

describe(`API :: POST ${ROUTE}`, () => {
  describe('Should return :: 201', () => {
    describe('PROFESSIONAL', () => {
      let response = '';

      beforeEach(async () => {
        const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());
        const Employee = EmployeeDataFaker(1, Professional);
        delete Employee.owner;
        response = await request(app)
          .post(ROUTE)
          .send(Employee)
          .set('Authorization', `Bearer ${utils.generateToken(Professional)}`);
      });

      it(`Should return status 201`, done => {
        expect(response.status).toBe(201);
        done();
      });

      it('Check JSON', done => {
        expect(response.body).toEqual({
          _id: expect.any(String),
          birthdate: expect.any(String),
          address: {
            cep: expect.any(String),
            city: expect.any(String),
            complement: expect.any(String),
            country: expect.any(String),
            neighborhood: expect.any(String),
            number: expect.any(String),
            state: expect.any(String),
            street: expect.any(String)
          },
          cpf: expect.any(String),
          email: expect.any(String),
          gender: expect.any(String),
          name: expect.any(String),
          rg: expect.any(String),
          owner: expect.any(String),
          dentist: expect.any(String),
          phone: expect.any(String),
          age: expect.any(Number)
        });
        done();
      });
    });
  });

  describe('Should return :: 400', () => {
    describe('PROFESSIONAL', () => {
      describe('Validation Param Required', () => {
        describe('Name', () => {
          let response = '';

          beforeEach(async () => {
            const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());
            const Employee = EmployeeDataFaker(1, Professional);
            delete Employee.owner;
            delete Employee.name;

            response = await request(app)
              .post(ROUTE)
              .send(Employee)
              .set('Authorization', `Bearer ${utils.generateToken(Professional)}`);
          });

          it(`Should return status 400`, done => {
            expect(response.status).toBe(400);
            done();
          });

          it('Check JSON', done => {
            expect(response.body.key).toBe('BAD_REQUEST_PARAMETER');
            expect(response.body.message).toBe("The request couldn't be accepted due malformed request syntax.");
            expect(response.body.detail.motive).toBe('"name" is required');
            done();
          });
        });
      });

      describe('Validation Param Invalid', () => {
        describe('CPF', () => {
          let response = '';

          beforeEach(async () => {
            const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());
            const Employee = EmployeeDataFaker(1, Professional);
            delete Employee.owner;
            Employee.cpf = '111.111.111-11';

            response = await request(app)
              .post(ROUTE)
              .send(Employee)
              .set('Authorization', `Bearer ${utils.generateToken(Professional)}`);
          });

          it(`Should return status 400`, done => {
            expect(response.status).toBe(400);
            done();
          });

          it('Check JSON', done => {
            expect(response.body.key).toBe('BAD_REQUEST_PARAMETER');
            expect(response.body.message).toBe("The request couldn't be accepted due malformed request syntax.");
            expect(response.body.detail.motive).toBe('CPF is not valid');
            done();
          });
        });

        describe('EMAIL', () => {
          let response = '';

          beforeEach(async () => {
            const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());
            const Employee = EmployeeDataFaker(1, Professional);
            delete Employee.owner;
            Employee.email = 'gmail.com';

            response = await request(app)
              .post(ROUTE)
              .send(Employee)
              .set('Authorization', `Bearer ${utils.generateToken(Professional)}`);
          });

          it(`Should return status 400`, done => {
            expect(response.status).toBe(400);
            done();
          });

          it('Check JSON', done => {
            expect(response.body.key).toBe('BAD_REQUEST_PARAMETER');
            expect(response.body.message).toBe("The request couldn't be accepted due malformed request syntax.");
            expect(response.body.detail.motive).toBe('Email is not valid');
            done();
          });
        });

        describe('BIRTHDATE ', () => {
          describe('NOT VALID', () => {
            let response = '';

            beforeEach(async () => {
              const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());
              const Employee = EmployeeDataFaker(1, Professional);
              delete Employee.owner;
              Employee.birthdate = '10/10/1010';

              response = await request(app)
                .post(ROUTE)
                .send(Employee)
                .set('Authorization', `Bearer ${utils.generateToken(Professional)}`);
            });

            it(`Should return status 400`, done => {
              expect(response.status).toBe(400);
              done();
            });

            it('Check JSON', done => {
              expect(response.body.key).toBe('BAD_REQUEST_PARAMETER');
              expect(response.body.message).toBe("The request couldn't be accepted due malformed request syntax.");
              expect(response.body.detail.motive).toBe('Birthdate is not valid');
              done();
            });
          });

          describe('TOMORROW', () => {
            let response = '';

            beforeEach(async () => {
              const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());
              const Employee = EmployeeDataFaker(1, Professional);
              delete Employee.owner;
              (Employee.birthdate = moment()
                .add(1, 'days')
                .format('YYYY-MM-DD')),
                (response = await request(app)
                  .post(ROUTE)
                  .send(Employee)
                  .set('Authorization', `Bearer ${utils.generateToken(Professional)}`));
            });

            it(`Should return status 400`, done => {
              expect(response.status).toBe(400);
              done();
            });

            it('Check JSON', done => {
              expect(response.body.key).toBe('BAD_REQUEST_PARAMETER');
              expect(response.body.message).toBe("The request couldn't be accepted due malformed request syntax.");
              expect(response.body.detail.motive).toBe('Birthdate must be before or equal today');
              done();
            });
          });
        });

        describe('BIRTHDATE ', () => {
          describe('NOT VALID', () => {
            let response = '';

            beforeEach(async () => {
              const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());
              const Employee = EmployeeDataFaker(1, Professional);
              delete Employee.owner;
              Employee.birthdate = '10/10/1010';

              response = await request(app)
                .post(ROUTE)
                .send(Employee)
                .set('Authorization', `Bearer ${utils.generateToken(Professional)}`);
            });

            it(`Should return status 400`, done => {
              expect(response.status).toBe(400);
              done();
            });

            it('Check JSON', done => {
              expect(response.body.key).toBe('BAD_REQUEST_PARAMETER');
              expect(response.body.message).toBe("The request couldn't be accepted due malformed request syntax.");
              expect(response.body.detail.motive).toBe('Birthdate is not valid');
              done();
            });
          });

          describe('DENTIST', () => {
            let response = '';

            beforeEach(async () => {
              const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());
              const Employee = EmployeeDataFaker(1, { _id: '1234567' });
              delete Employee.owner;
              response = await request(app)
                .post(ROUTE)
                .send(Employee)
                .set('Authorization', `Bearer ${utils.generateToken(Professional)}`);
            });

            it(`Should return status 400`, done => {
              expect(response.status).toBe(400);
              done();
            });

            it('Check JSON', done => {
              expect(response.body.key).toBe('BAD_REQUEST_PARAMETER');
              expect(response.body.message).toBe("The request couldn't be accepted due malformed request syntax.");
              expect(response.body.detail.motive).toBe(
                'Oops, we faced a problem when trying to research this request. Check the ID and try again later.'
              );
              done();
            });
          });
        });

        describe('GENDER', () => {
          let response = '';

          beforeEach(async () => {
            const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());
            const Employee = EmployeeDataFaker(1, Professional);
            delete Employee.owner;
            Employee.gender = '-';

            response = await request(app)
              .post(ROUTE)
              .send(Employee)
              .set('Authorization', `Bearer ${utils.generateToken(Professional)}`);
          });

          it(`Should return status 400`, done => {
            expect(response.status).toBe(400);
            done();
          });

          it('Check JSON', done => {
            expect(response.body.key).toBe('BAD_REQUEST_PARAMETER');
            expect(response.body.message).toBe("The request couldn't be accepted due malformed request syntax.");
            expect(response.body.detail.motive).toBe(
              'Oops, the current gender is not valid. Please review your input.'
            );
            done();
          });
        });

        describe('CEP', () => {
          let response = '';

          beforeEach(async () => {
            const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());
            const Employee = EmployeeDataFaker(1, Professional);
            delete Employee.owner;
            Employee.address.cep = '38.408-092';

            response = await request(app)
              .post(ROUTE)
              .send(Employee)
              .set('Authorization', `Bearer ${utils.generateToken(Professional)}`);
          });

          it(`Should return status 400`, done => {
            expect(response.status).toBe(400);
            done();
          });

          it('Check JSON', done => {
            expect(response.body.key).toBe('BAD_REQUEST_PARAMETER');
            expect(response.body.message).toBe("The request couldn't be accepted due malformed request syntax.");
            expect(response.body.detail.motive).toBe('CEP number is not valid');
            done();
          });
        });

        describe('DENTIST', () => {
          let response = '';

          beforeEach(async () => {
            const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());
            const Employee = EmployeeDataFaker(1, Professional);
            delete Employee.owner;
            Employee.dentist = '123456';

            response = await request(app)
              .post(ROUTE)
              .send(Employee)
              .set('Authorization', `Bearer ${utils.generateToken(Professional)}`);
          });

          it(`Should return status 400`, done => {
            expect(response.status).toBe(400);
            done();
          });

          it('Check JSON', done => {
            expect(response.body.key).toBe('BAD_REQUEST_PARAMETER');
            expect(response.body.message).toBe("The request couldn't be accepted due malformed request syntax.");
            expect(response.body.detail.motive).toBe(
              'Oops, we faced a problem when trying to research this request. Check the ID and try again later.'
            );
            done();
          });
        });
      });
    });
  });

  describe('Should return :: 401', () => {
    describe('PROFESSIONAL', () => {
      let response = '';

      beforeEach(async () => {
        const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());
        const Employee = EmployeeDataFaker(1, Professional);
        delete Employee.owner;
        response = await request(app)
          .post(ROUTE)
          .send(Employee);
      });

      it(`Should return status 401`, done => {
        expect(response.status).toBe(401);
        done();
      });

      it('Check JSON', done => {
        expect(response.body.key).toBe('UNAUTHORIZED');
        expect(response.body.message).toBe(
          'The request has not been applied because it lacks valid authentication credentials for the target resource.'
        );
        expect(response.body.detail.motive).toBe(
          'Oops, we notice that you are missing some items on your authentication. Please, try again.'
        );
        done();
      });
    });
  });

  describe('Should return :: 403', () => {
    describe('PROFESSIONAl', () => {
      let response = '';

      beforeEach(async () => {
        const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());
        const Employee = EmployeeDataFaker(1, Professional);
        delete Employee.owner;

        const user = UserDataFaker(1, Professional);
        user.blockedAt = true;
        response = await request(app)
          .post(ROUTE)
          .send(Employee)
          .set('Authorization', `Bearer ${utils.generateToken(user)}`);
      });

      it(`Should return status 403`, done => {
        expect(response.status).toBe(403);
        done();
      });

      it('Check JSON', done => {
        expect(response.body.key).toBe('FORBIDDEN');
        expect(response.body.message).toBe('The request could not be made because the resource was blocked.');
        expect(response.body.detail.motive).toBe(
          'Oops, we encountered a problem with your account. Please contact the administrators'
        );
        done();
      });
    });
  });
});
