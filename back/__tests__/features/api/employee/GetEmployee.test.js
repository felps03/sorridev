const request = require('supertest');

const app = require('../../../../src/app');
const utils = require('../../../../src/helpers/utils');

const { EmployeeDataFaker, ProfessionalDataFaker, UserDataFaker } = require('../../../support/dataFaker');

const { MongoSeed } = require('../../../support/insertDatabase');

const ROUTE = '/employee';
const COLLECTION = 'Employee';

describe(`API :: GET ${ROUTE}`, () => {
  describe('Should return :: 200', () => {
    describe('PROFESSIONAL', () => {
      describe('Empty Parameter', () => {

        describe('List One Emploee', () => {
          let response = '';
  
          beforeEach(async () => {
            const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());
            await MongoSeed(COLLECTION, EmployeeDataFaker(1, Professional));
            response = await request(app)
              .get(ROUTE)
              .set('Authorization', `Bearer ${utils.generateToken(Professional)}`);
          });
  
          it(`Should return status 200`, done => {
            expect(response.status).toBe(200);
            expect(response.body.total).toBe(1);
            expect(response.body.limit).toBe(100);
            expect(response.body.page).toBe(1);
            expect(response.body.pages).toBe(1);
            done();
          });
  
          it('Check JSON', done => {
            expect(response.body).toEqual({
              docs: [
                {
                  _id: expect.any(String),
                  address: {
                    cep: expect.any(String),
                    city: expect.any(String),
                    complement: expect.any(String),
                    country: expect.any(String),
                    neighborhood: expect.any(String),
                    number: expect.any(String),
                    state: expect.any(String),
                    street: expect.any(String)
                  },
                  age: expect.any(Number),
                  birthdate: expect.any(String),
                  cpf: expect.any(String),
                  email: expect.any(String),
                  gender: expect.any(String),
                  name: expect.any(String),
                  rg: expect.any(String),
                  owner: expect.any(String),
                  phone: expect.any(String),
                  dentist: expect.any(String)
                }
              ],
              total: expect.any(Number),
              limit: expect.any(Number),
              page: expect.any(Number),
              pages: expect.any(Number)
            });
            done();
          });
        });
  
        describe('List Eleven Emploee', () => {
          let response = '';
  
          beforeEach(async () => {
            const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());
            await MongoSeed(COLLECTION, EmployeeDataFaker(110, Professional));
            response = await request(app)
              .get(ROUTE)
              .set('Authorization', `Bearer ${utils.generateToken(Professional)}`);
          });
  
          it(`Should return status 200`, done => {
            expect(response.status).toBe(200);
            expect(response.body.total).toBe(110);
            expect(response.body.limit).toBe(100);
            expect(response.body.page).toBe(1);
            expect(response.body.pages).toBe(2);
            done();
          });
        });

      });

      describe('With Parameters', () => {
        describe('Search', () => {

          describe('List One Emploee', () => {

            let response = '';    
            beforeEach(async () => {
              const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());
              const Emploee = EmployeeDataFaker(1, Professional);
              Emploee.name = 'Emploee Junior'
              await MongoSeed(COLLECTION, Emploee);
              response = await request(app)
                .get(ROUTE)
                .query({ search: 'Emploee' })
                .set('Authorization', `Bearer ${utils.generateToken(Professional)}`);
            });
    
            it(`Should return status 200`, done => {
              expect(response.status).toBe(200);
              expect(response.body.total).toBe(1);
              expect(response.body.limit).toBe(100);
              expect(response.body.page).toBe(1);
              expect(response.body.pages).toBe(1);
              done();
            });
    
            it('Check JSON', done => {
              expect(response.body).toEqual({
                docs: [
                  {
                    _id: expect.any(String),
                    address: {
                      cep: expect.any(String),
                      city: expect.any(String),
                      complement: expect.any(String),
                      country: expect.any(String),
                      neighborhood: expect.any(String),
                      number: expect.any(String),
                      state: expect.any(String),
                      street: expect.any(String)
                    },
                    age: expect.any(Number),
                    birthdate: expect.any(String),
                    cpf: expect.any(String),
                    email: expect.any(String),
                    gender: expect.any(String),
                    name: expect.any(String),
                    rg: expect.any(String),
                    owner: expect.any(String),
                    phone: expect.any(String),
                    dentist: expect.any(String)
                  }
                ],
                total: expect.any(Number),
                limit: expect.any(Number),
                page: expect.any(Number),
                pages: expect.any(Number)
              });
              done();
            });
          });

        });
      });
    });
  });

  describe('Should return :: 204', () => {
    describe('PROFESSIONAl', () => {
      let response = '';

      beforeEach(async () => {
        const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());
        response = await request(app)
          .get(ROUTE)
          .set('Authorization', `Bearer ${utils.generateToken(Professional)}`);
      });

      it(`Should return status 204`, done => {
        expect(response.status).toBe(204);
        done();
      });

      it('Check JSON', done => {
        expect(response.body).toEqual({});
        done();
      });
    });
  });

  describe('Should return :: 400', () => {
    describe('PROFESSIONAL', () => {
      let response = '';

      beforeEach(async () => {
        const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());
        await MongoSeed(COLLECTION, EmployeeDataFaker(1, Professional));
        response = await request(app)
          .get(ROUTE)
          .query({ dentist: '123456' })
          .set('Authorization', `Bearer ${utils.generateToken(Professional)}`);
      });

      it(`Should return status 400`, done => {
        expect(response.status).toBe(400);
        done();
      });

      it('Check JSON', done => {
        expect(response.body.key).toBe('BAD_REQUEST_PARAMETER');
        expect(response.body.message).toBe("The request couldn't be accepted due malformed request syntax.");
        expect(response.body.detail.motive).toBe(
          'Oops, we faced a problem when trying to research this request. Check the ID and try again later.'
        );
        done();
      });

      describe('Create Eleven Emploee', () => {
        let response = '';

        beforeEach(async () => {
          const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());
          await MongoSeed(COLLECTION, EmployeeDataFaker(110, Professional));
          response = await request(app)
            .get(ROUTE)
            .set('Authorization', `Bearer ${utils.generateToken(Professional)}`);
        });

        it(`Should return status 200`, done => {
          expect(response.status).toBe(200);
          expect(response.body.total).toBe(110);
          expect(response.body.limit).toBe(100);
          expect(response.body.page).toBe(1);
          expect(response.body.pages).toBe(2);
          done();
        });
      });
    });
  });

  describe('Should return :: 401', () => {
    describe('PROFESSIONAl', () => {
      let response = '';

      beforeEach(async () => {
        const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());
        await MongoSeed(COLLECTION, EmployeeDataFaker(1, Professional));
        response = await request(app).get(ROUTE);
      });

      it(`Should return one ${COLLECTION}`, done => {
        expect(response.status).toBe(401);
        done();
      });

      it('Check JSON', done => {
        expect(response.body.key).toBe('UNAUTHORIZED');
        expect(response.body.message).toBe(
          'The request has not been applied because it lacks valid authentication credentials for the target resource.'
        );
        expect(response.body.detail.motive).toBe(
          'Oops, we notice that you are missing some items on your authentication. Please, try again.'
        );
        done();
      });
    });
  });

  describe('Should return :: 403', () => {
    describe('USER', () => {
      let response = '';
      beforeEach(async () => {
        const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());
        const user = UserDataFaker(1, Professional);
        user.blockedAt = true;
        await MongoSeed(COLLECTION, EmployeeDataFaker(1, Professional));
        response = await request(app)
          .get(ROUTE)
          .set('Authorization', `Bearer ${utils.generateToken(user)}`);
      });

      it(`Should return status`, done => {
        expect(response.status).toBe(403);
        done();
      });

      it('Check JSON', done => {
        expect(response.body.key).toBe('FORBIDDEN');
        expect(response.body.message).toBe('The request could not be made because the resource was blocked.');
        expect(response.body.detail.motive).toBe(
          'Oops, we encountered a problem with your account. Please contact the administrators'
        );
        done();
      });
    });
  });
});
