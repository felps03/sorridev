const request = require('supertest');

const app = require('../../../../src/app');
const utils = require('../../../../src/helpers/utils');

const { EmployeeDataFaker, ProfessionalDataFaker, UserDataFaker } = require('../../../support/dataFaker');

const { MongoSeed } = require('../../../support/insertDatabase');

const ROUTE = id => `/employee/${id}`;
const COLLECTION = 'Employee';

describe(`API :: PUT ${ROUTE}`, () => {
  describe('[SUCCESS] Should return :: 200', () => {
    let response = '';

    beforeEach(async () => {
      const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());
      const [{ _id }] = await MongoSeed(COLLECTION, EmployeeDataFaker(1, Professional));
      response = await request(app)
        .put(ROUTE(_id))
        .send({
          name: 'teste',
          phone: '00 0 00000-000'
        })
        .set('Authorization', `Bearer ${utils.generateToken(Professional)}`);
    });

    it(`[SUCCESS] Should return Status 200`, done => {
      expect(response.status).toBe(200);
      done();
    });

    it('[SUCCESS] Check JSON', done => {
      expect(response.body).toEqual({
        _id: expect.any(String),
        address: {
          cep: expect.any(String),
          city: expect.any(String),
          complement: expect.any(String),
          country: expect.any(String),
          neighborhood: expect.any(String),
          number: expect.any(String),
          state: expect.any(String),
          street: expect.any(String)
        },
        age: expect.any(Number),
        birthdate: expect.any(String),
        cpf: expect.any(String),
        dentist: expect.any(String),
        email: expect.any(String),
        gender: expect.any(String),
        name: expect.any(String),
        rg: expect.any(String),
        owner: expect.any(String),
        phone: expect.any(String)
      });
      done();
    });

    it('[SUCCESS] Check Name and Phone', done => {
      expect(response.body.name).toEqual('teste');
      expect(response.body.phone).toEqual('00 0 00000-000');
      done();
    });
  });

  describe('[SUCCESS] Should return :: 401', () => {
    let response = '';

    beforeEach(async () => {
      const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());
      const [{ _id }] = await MongoSeed(COLLECTION, EmployeeDataFaker(1, Professional));
      response = await request(app)
        .put(ROUTE(_id))
        .send({
          name: 'teste',
          phone: '00 0 00000-000'
        });
    });

    it(`[SUCCESS] Should return one ${COLLECTION}`, async () => {
      expect(response.status).toBe(401);
    });

    it('[SUCCESS] Check JSON', async () => {
      expect(response.body.key).toBe('UNAUTHORIZED');
      expect(response.body.message).toBe(
        'The request has not been applied because it lacks valid authentication credentials for the target resource.'
      );
      expect(response.body.detail.motive).toBe(
        'Oops, we notice that you are missing some items on your authentication. Please, try again.'
      );
    });
  });

  describe('[SUCCESS] Should return :: 403', () => {
    let response = '';
    beforeEach(async () => {
      const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());
      const user = UserDataFaker(1, Professional);
      user.blockedAt = true;
      const [{ _id }] = await MongoSeed(COLLECTION, EmployeeDataFaker(1, Professional));
      response = await request(app)
        .put(ROUTE(_id))
        .send({
          cash_accounting: 'teste',
          spending: 'teste'
        })
        .set('Authorization', `Bearer ${utils.generateToken(user)}`);
    });

    it(`[SUCCESS] Should return one ${COLLECTION}`, async () => {
      expect(response.status).toBe(403);
    });

    it('[SUCCESS] Check JSON', async () => {
      expect(response.body.key).toBe('FORBIDDEN');
      expect(response.body.message).toBe('The request could not be made because the resource was blocked.');
      expect(response.body.detail.motive).toBe(
        'Oops, we encountered a problem with your account. Please contact the administrators'
      );
    });
  });

  describe('[SUCCESS] Should return :: 404', () => {
    it(`[SUCCESS] Should not return any ${COLLECTION}`, async () => {
      const [Professional] = await MongoSeed('Professional', ProfessionalDataFaker());
      const response = await request(app)
        .put(ROUTE('5fdf7078d435f53cb02bfe00'))
        .set('Authorization', `Bearer ${utils.generateToken(UserDataFaker(1, Professional))}`);
      expect(response.status).toBe(404);
      expect(response.body.key).toBe('NOT_FOUND');
      expect(response.body.message).toBe(
        "The request couldn't be accepted because the requested resource couldn't be founded."
      );
      expect(response.body.detail.motive).toBe(
        'Oops, we faced a problem trying to updating your employee. Please try again later.'
      );
    });
  });
});
