const moment = require('moment');
const dataFaker = require('./dataFaker');
const { GenderType } = require('../../../src/helpers/enums');

const generateUserJSON = (count = 1, { _id }) => {
  const objs = [];
  for (let index = 0; index < count; index++) {
    objs.push({
      name: dataFaker.name(),
      birthdate: moment(dataFaker.birthday()).format('YYYY-MM-DD'),
      rg: dataFaker.string(),
      cpf: dataFaker.cpf(),
      phone: dataFaker.cellPhone(),
      gender: dataFaker.array(GenderType),
      email: dataFaker.email(),
      address: {
        cep: dataFaker.array(['54774-520', '77001-276', '29152-827']),
        street: dataFaker.name(),
        number: dataFaker.string(),
        neighborhood: dataFaker.string(),
        complement: dataFaker.string(),
        city: dataFaker.string(),
        state: dataFaker.string(),
        country: dataFaker.string()
      },
      observation: dataFaker.string(),
      owner: 'USER',
      password: dataFaker.word({ length: 9 }),
      blockedAt: false,
      dentist: _id
    });
  }
  return count === 1 ? objs[0] : objs;
};

module.exports = generateUserJSON;
