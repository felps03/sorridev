const Chance = require('chance');
const dataFaker = new Chance();

Object.assign(dataFaker, {
  array: array => array[dataFaker.integer({ min: 0, max: array.length - 1 })],
  string: () => new Chance().string({ alpha: true }),
  cellPhone: () => new Chance().string({ length: 11, numeric: true }).replace(/(\d{2})(\d{5})(\d{4})/, '($1) $2-$3')
});

module.exports = dataFaker;
