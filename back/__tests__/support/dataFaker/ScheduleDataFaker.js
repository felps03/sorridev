const dataFaker = require('./dataFaker');

const generateScheduleJSON = (count = 1, { _id }, user) => {
  const objs = [];
  for (let index = 0; index < count; index++) {
    objs.push({
        date: dataFaker.birthday(),
        startTime: dataFaker.birthday(),
        endTime: dataFaker.birthday(),
        health_insurance_name: dataFaker.string(),
        observation: dataFaker.string(),
        value: dataFaker.string(),
        status: dataFaker.string(),
        hasReturn: dataFaker.bool(),
        dentist: _id,
        client: user._id
    });
  }
  return count === 1 ? objs[0] : objs;
};

module.exports = generateScheduleJSON;
