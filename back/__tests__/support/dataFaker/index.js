const AdminDataFaker = require('./AdminDataFaker');
const BalanceDataFaker = require('./BalanceDataFaker');
const EmployeeDataFaker = require('./EmployeeDataFaker');
const ProfessionalDataFaker = require('./ProfessionalDataFaker');
const ScheduleDataFaker = require('./ScheduleDataFaker');
const ServiceDataFaker = require('./ServiceDataFaker');
const StockDataFaker = require('./StockDataFaker');
const UserDataFaker = require('./UserDataFaker');

module.exports = {
  AdminDataFaker,
  BalanceDataFaker,
  EmployeeDataFaker,
  ProfessionalDataFaker,
  ScheduleDataFaker,
  ServiceDataFaker,
  StockDataFaker,
  UserDataFaker
};
