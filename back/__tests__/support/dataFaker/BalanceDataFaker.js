const dataFaker = require('./dataFaker');

const generateBalanceJSON = (count = 1, { _id }) => {
  const objs = [];
  for (let index = 0; index < count; index++) {
    objs.push({
      cash_accounting: dataFaker.string(),
      spending: dataFaker.string(),
      number_of_appointments: dataFaker.natural({min: 1, max: 10}),
      dentist: _id
    });
  }
  return count === 1 ? objs[0] : objs;
};

module.exports = generateBalanceJSON;
