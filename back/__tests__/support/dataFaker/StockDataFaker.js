const dataFaker = require('./dataFaker');

const generateStockJSON = (count = 1, { _id }) => {
  const objs = [];
  for (let index = 0; index < count; index++) {
    objs.push({
      product: dataFaker.string(),
      price: dataFaker.string(),
      quantity: dataFaker.natural({ min: 1, max: 10 }),
      provider: dataFaker.string(),
      dentist: _id
    });
  }
  return count === 1 ? objs[0] : objs;
};

module.exports = generateStockJSON;
