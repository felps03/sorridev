const dataFaker = require('./dataFaker');

const generateServiceJSON = (count = 1, { _id }) => {
  const objs = [];
  for (let index = 0; index < count; index++) {
    objs.push({
      name: dataFaker.name(),
      phone: dataFaker.cellPhone(),
      supply: dataFaker.name(),
      dentist: _id
    });
  }
  return count === 1 ? objs[0] : objs;
};

module.exports = generateServiceJSON;
