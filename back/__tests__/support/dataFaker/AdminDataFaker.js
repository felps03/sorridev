const dataFaker = require('./dataFaker');
const { GenderType } = require('../../../src/helpers/enums');

const generateAdminJSON = (count = 1) => {
  const objs = [];
  for (let index = 0; index < count; index++) {
    objs.push({
      name: dataFaker.name(),
      birthdate: dataFaker.birthday(),
      rg: dataFaker.string(),
      cpf: dataFaker.cpf(),
      phone: dataFaker.cellPhone(),
      gender: dataFaker.array(GenderType),
      email: dataFaker.email(),
      address: {
        cep: dataFaker.string(),
        street: dataFaker.name(),
        number: dataFaker.string(),
        neighborhood: dataFaker.string(),
        complement: dataFaker.string(),
        city: dataFaker.string(),
        state: dataFaker.string(),
        country: dataFaker.string()
      },
      observation: dataFaker.string(),
      owner: 'ADMIN',
      password: dataFaker.string(),
      blockedAt: false
    });
  }
  return count === 1 ? objs[0] : objs;
};

module.exports = generateAdminJSON;
