const database = require('../../src/infra/database/mongo');

module.exports = {
  async MongoSeed(collection, generator) {
    const db = (await database).model(collection);
    return await db.insertMany(generator);
  }
};
