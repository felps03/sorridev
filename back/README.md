<h1> Sorri Dev - Back </h1>

> Status do Projeto: ✔️ 🚧 ( em desenvolvimento)

### Tópicos

🔵 [Descrição do Módulo](#descrição-do-projeto)

🔵 [Funcionalidades](#funcionalidades)

🔵 [Pré-requisitos](#pré-requisitos)

🔵 [Estrutura do Projeto](#estrutura-do-projeto)

🔵 [Como rodar a aplicação](#como-rodar-a-aplicação)

🔵 [Como rodar os testes](#como-rodar-os-testes)

🔵 [Dependências e bibliotecas utilizadas](#dependências-e-bibliotecas-utilizadas)

🔵 [Dependências](#dependências)

🔵 [Dependências de Desenvolvimento](#dependências-de-desenvolvimento)

🔵 [Desenvolvedores](#desenvolvedores)

## Descrição do projeto

  <p align="justify">
    Este módulo compõem a parte de ###### e do ato de ###### de usuários do projeto Sis Digital. Foi construido em Nodejs, Banco de Dados MongoDB e testes realizados com Jest.
  </p>

## Funcionalidades

✅ Cadastro de ######

✅ Listagem de todas as ######

✅ Listagem de ###### por id

✅ Busca de ###### por nome, id

✅ Desativação de ######

✅ Atualização de ######

## Pré-requisitos

❗ [Node](https://nodejs.org/en/download/)
❗ [Docker](https://www.docker.com/)
❗ [MongoDB](https://www.mongodb.com/)
❗ [Postman](https://www.postman.com/)

## Estrutura do projeto

```
  |-- __tests__
  |-- node_modules
    |-- coverage
      |-- [+] lcov-report
      |-- lcov.info
    |-- features
      |-- [+] api
    |-- integration
      |-- SorriDev.postman_collection.json
      |-- SorriDev.postman_environment.json
    |-- support
      |-- [+] dataFaker
      |-- cleanDatabase.js
      |-- insertDatabase.js
    |-- unit
      |-- setupDB
  |-- src
    |-- app
      |-- [+] controllers
      |-- [+] middlewares
      |-- [+] models
      |-- [+] repository
      |-- [+] schemas
      |-- [+] serializer
      |-- [+] serice
      |-- [+] validators
    |-- config
      |-- auth.js
      |-- startUpSeed.js
    |-- helpers
      |-- [+] enums
      |-- [+] errors
      |-- logger
      |-- regularExpressions
      |-- utils
    |-- infra
      |-- [+] database
    |-- lib
      |-- ExtendSchema.js
    |-- routes
      |-- auth.routes.js
      |-- balance.routes.js
      |-- employee.routes.js
      |-- health.routes.js
      |-- index.routes.js
      |-- professional.routes.js
      |-- schedule.routes.js
      |-- service.routes.js
      |-- stock.routes.js
      |-- swagger.routes.js
      |-- user.routes.js
    server.js
    app.js
    bootstrap.js
|-- tmp
|-- swagger
    |-- openapi.yaml
    |-- swagger.json
.editorconfig
.env
.env.example
.env.homolog
.env.test
.eslintignore
.eslintrc
.gitignore
.prettierrc
.sequelizerc
app.log
commitlint.config.js
Dockerfile
Jenkinsfile
jest.config.js
nodemon.json
package-lock.json
package.json
READEME.md
```

## Como rodar a aplicação

🚨 É necessário ter uma versão do MongoDB superior ou igual a v4.2.6

🚨 Para realização de qualquer teste e/ou execução do projeto, é necessário ter em mãos o api-access-token
caso você não o tenha, por favor contate a equipe SIS Digital via startupsisdigital@gmail.com

### Instalando o projeto

```bash
# Clone o repositório
$ git clone https://felps003@bitbucket.org/felps003/sorridev.git

# Entre na pasta
$ cd sorridev/back

# Instale todas as dependências do projeto
$ npm i

## (opcional) Caso não tenha o MongoDB localmente em sua máquian, é possível roda-lo via docker, para isto:

$ docker run --name mongo_sorridev -p 27017:27017 -d -t mongo

# Se tudo correu bem, agora rodamos a aplicação 😆
$ npm run dev
```

## Executando a aplicação

1. Como pré-requisito, possuir [docker](https://www.docker.com/).
2. Baixar o arquivo docker-compose.yml deste repositório e executar o comando: docker-compose up

- Usuário: admin@pidecor.com.br
- Senha: 123456

## Como rodar os testes

🚨 Para rodar o ambiente de testes é necessário termos o MongoDB rodando em nossos containers.

Estando com ambos rodando, realizamos o seguinte comando:

```bash
$ npm test

npx jest --testPathPattern __tests__/features/api/user/CreateUser.test.js
npx jest --testPathPattern __tests__/features/api/stock/CreateStock.test.js
npx jest --testPathPattern __tests__/features/api/user/UpdateUser.test.js
npx jest --testPathPattern __tests__/features/api/schedule/DeleteSchedule.test.js
npx jest --testPathPattern __tests__/features/api/user/GetUserSearch.test.js

```

Após a execução, será gerado um log dentro da pasta `coverage` contendo uma explicação das branch, funções e linhas que foram cobertas pelo caso de teste.

Já na janela do terminal, também teremos a demonstração da porcentagem de cobertura dos testes e caso venha a acontencer, quais testes estão falhando.

A aplicação tem por objetivo sempre alcançar 100% em todos os teste e coberturas.

## Dependências e bibliotecas utilizadas

### Dependências

- [dotenv](https://www.npmjs.com/package/dotenv) - É um módulo que carrega variáveis de ambiente de um arquivo .env para process.env.
- [express](https://www.npmjs.com/package/express) - Framework web rápido, flexível e minimalista para Node.js.
- [express-async-errors](https://www.npmjs.com/package/express-async-errors) - É um middleware destinado ao tramento de erro try/catch de forma assíncrona.
- [lodash](https://www.npmjs.com/package/lodash) - Biblioteca para manipulação de dados.
- [moment](https://www.npmjs.com/package/moment) - Biblioteca para manipulação de datas e horários.
- [moment-timezone](https://www.npmjs.com/package/moment-timezone) - Biblioteca para manipulação de fuso horário.
- [mongoose](https://www.npmjs.com/package/mongoose) - É uma ferramenta para modelagem de objeto do MongoDB projetada para funcionar em ambientes assíncrono.
- [mongoose-paginate](https://www.npmjs.com/package/mongoose-paginate) - É uma ferramenta que auxilia o mongoose na paginação.
- [winston](https://www.npmjs.com/package/winston) - Logger para NodeJS.
- [@hapi/joi](https://www.npmjs.com/package/@hapi/joi) - Validador de schemas.
- [youch](https://www.npmjs.com/package/youch) - Biblioteca para formatação e reportagem de erros em node.

### Dependências de Desenvolvimento

- [@commitlint/cli](https://www.npmjs.com/package/@commitlint/cli) - Biblioteca destinada a realizar lint nos commits convencionais.
- [@commitlint/config-conventional](https://www.npmjs.com/package/@commitlint/config-conventional)
- [core-js](https://www.npmjs.com/package/core-js) - Biblioteca padrão modular para JavaScript. Inclui polyfills para ECMAScript até 2019: promessas, símbolos, coleções, iteradores, matrizes digitadas, muitos outros recursos, propostas ECMAScript, alguns recursos WHATWG / W3C de plataforma cruzada e propostas como URL.
- [cross-env](https://www.npmjs.com/package/cross-env) - Permite utilização enviroment em diferentes SO.
- [eslint](https://www.npmjs.com/package/eslint) - Dsitinado a padronização e regras de codificação.
- [eslint-config-airbnb-base](https://www.npmjs.com/package/eslint-config-airbnb-base) - Biblioteca que fornece suporte as configurações do AirBnB
- [eslint-config-prettier](https://www.npmjs.com/package/eslint-config-prettier)- Biblioteca que fornece suporte as configurações do prettier
- [eslint-plugin-import](https://www.npmjs.com/package/eslint-plugin-import)- Plugin que fornece suporte suporte à sintaxe de importação / exportação do ES2015 + (ES6 +) afim de evitar problemas com erros de ortografia de caminhos de arquivos e nomes de importação.
- [eslint-plugin-import-helpers](https://www.npmjs.com/package/eslint-plugin-import-helpers)- Plugin que fornece suporte complementar as regras fornecidas pelo eslint-plugin-import.
- [eslint-plugin-prettier](https://www.npmjs.com/package/eslint-plugin-prettier)- Executa o Prettier como uma regra do ESLint e relata as diferenças como problemas individuais do ESLint.
- [husky](https://www.npmjs.com/package/husky) - Biblioteca destinada ao controle de push, commit, e entre outros comandos do git que possam vir a afetar as branchs do projeto.
- [jest](https://www.npmjs.com/package/jest) - Biblioteca para realização de testes automatizados.
- [nodemon](https://www.npmjs.com/package/nodemon) - É um utilitário que monitora qualquer alteração nos seus arquivos e reinicia automaticamente o servidor.
- [prettier](https://www.npmjs.com/package/prettier) - Biblioteca destina a formatação de código. Impondo um estilo consistente ao analisar seu código e reimprimi-lo com suas próprias regras, que levam em consideração o comprimento máximo da linha, agrupando o código quando necessário.
- [pretty-quick](https://www.npmjs.com/package/pretty-quick) - Biblioteca que é executada em conjunto com [prettier]
- [supertest](https://www.npmjs.com/package/supertest) - Biblioteca que fornece uma abstração de alto nível para realização de teste fazendo uso de HTTP

## Desenvolvedores

"Just checking if git is working properly..." 😁

[<img src="https://avatars3.githubusercontent.com/u/12463786?s=460&u=b207ef729d05bef11262e4f11f26c11248284e46&v=4" width=115><br><sub>Felipe Santos</sub>](https://github.com/Felps03) |
| :-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------: | :------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------: |


## Licença

Copyright :copyright: 2020 - Sis Digital
