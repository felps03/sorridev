const moment = require('moment-timezone');

/**
 * Data formart in YYYY-MM-DD
 * @param {*} data
 */
const getStartOfTheDay = data => {
  return moment
    .utc(data)
    .startOf('day')
    .toDate();
};

/**
 * Data formart in YYYY-MM-DD
 * @param {*} data
 */
const getEndOfTheDay = data => {
  return moment
    .utc(data)
    .endOf('day')
    .toDate();
};

const getStartOfTheMonth = () => {
  return moment()
    .startOf('month')
    .toDate();
};

const getEndOfTheMonth = () => {
  return moment()
    .endOf('month')
    .toDate();
};

const getYesterday = () => {
  return moment()
    .subtract(1, 'days')
    .format('YYYY-MM-DD');
};

const getToday = () => {
  return moment.utc().toDate();
};

const getTomorrow = () => {
  return moment()
    .add(1, 'days')
    .format('YYYY-MM-DD');
};

const getLastYear = () => {
  return moment()
    .subtract(1, 'years')
    .toDate();
};

const getFirstDayOfLastMonth = () => {
  return moment()
    .subtract(1, 'months')
    .startOf('month')
    .toDate();
};

const getLastDayOfLastMonth = () => {
  return moment()
    .subtract(1, 'months')
    .endOf('month')
    .toDate();
};

const isDateOlderThanToday = date => {
  const formatedDate = moment(date).format('YYYY-MM-DD');

  const MAX_DATE = moment()
    .add(1, 'day')
    .format('YYYY-MM-DD');

  if (moment(formatedDate).isSameOrAfter(MAX_DATE)) return false;

  return true;
};
/**
 * @param {*} date
 * @return {*} ("dd-MM-yyyy'T'HH:mm:ss'Z'")
 */
const formatedDate = date => {
  return moment(date).format();
};

const validateIsAfterDate = date => {
  return moment(date).isAfter('1900-01-01');
};

module.exports = {
  getStartOfTheDay,
  getEndOfTheDay,
  getStartOfTheMonth,
  getEndOfTheMonth,
  getToday,
  getYesterday,
  getTomorrow,
  getLastYear,
  getFirstDayOfLastMonth,
  getLastDayOfLastMonth,
  isDateOlderThanToday,
  formatedDate,
  validateIsAfterDate
};
