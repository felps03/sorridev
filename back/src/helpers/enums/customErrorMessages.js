const CustomMessages = {
  VALIDATION_ID: 'Oops, we faced a problem when trying to research this request. Check the ID and try again later.',
  MONGODB_ERROR: 'Sorry, we are facing an internal error. Please, verify your data and try again later.',
  GENDER_INVALID: 'Oops, the current gender is not valid. Please review your input.',
  MONGODB_CONFLICT: 'Sorry, already this data save. Please check your email and try again later',
  DELETE_USER_NOT_FOUND: 'Oops, we faced a problem trying to deleting your account. Please try again later.',
  UNAUTHORIZED_USER_NOT_FOUND: "Oops, it looks like you don't have permission to view this user",
  UPDATE_USER_NOT_FOUND: 'Oops, we faced a problem trying to updating your account. Please try again later.',
  DELETE_BALANCE_NOT_FOUND: 'Oops, we faced a problem trying to deleting your balance. Please try again later.',
  UPDATE_EMPLOYEE_NOT_FOUND: 'Oops, we faced a problem trying to updating your employee. Please try again later.',
  DELETE_EMPLOYEE_NOT_FOUND: 'Oops, we faced a problem trying to deleting your employee. Please try again later.',
  UPDATE_BALANCE_NOT_FOUND: 'Oops, we faced a problem trying to updating your balance. Please try again later.',
  DELETE_SCHEDULE_NOT_FOUND: 'Oops, we faced a problem trying to deleting your schedule. Please try again later.',
  UPDATE_SCHEDULE_NOT_FOUND: 'Oops, we faced a problem trying to updating your schedule. Please try again later.',
  CREATE_SCHEDULE_EXISTENT: 'Oops, is not possible to book the appointment in this hour. Please try another time.',
  TOKEN_INVALID: 'Oops, we had a problem with your authentication. Please, try again later.',
  TOKEN_MISSING: 'Oops, we notice that you are missing some items on your authentication. Please, try again.',
  DELETE_STOCK_NOT_FOUND: 'Oops, we faced a problem trying to deleting your stock. Please try again later.',
  UPDATE_STOCK_NOT_FOUND: 'Oops, we faced a problem trying to updating your stock. Please try again later.',
  DELETE_PROFESSIONAL_NOT_FOUND:
    'Oops, we faced a problem trying to deleting your professional. Please try again later.',
  UPDATE_PROFESSIONAL_NOT_FOUND:
    'Oops, we faced a problem trying to updating your professional. Please try again later.',
  DELETE_SERVICE_NOT_FOUND: 'Oops, we faced a problem trying to deleting your service. Please try again later.',
  UPDATE_SERVICE_NOT_FOUND: 'Oops, we faced a problem trying to updating your service. Please try again later.',
  USER_NOT_FOUND: 'Oops, you are not in our system yet. Please sing up.',
  PROFESSIONAL_NOT_EXIST: 'Oops, it looks like the professional was not found. Please check your data and try again',
  PASSWORD_NOT_MATCH:
    'Oops, we checked on our system and your password or email were not found. Please, check your credentials and try again.'
};

module.exports = Object.freeze(CustomMessages);
