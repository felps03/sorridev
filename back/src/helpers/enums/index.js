const CustomErrorMessages = require('./customErrorMessages');
const GenderType = require('./genderTypeEnum');
const HttpStatusCode = require('./httpCodes');
const Owner = require('./owner');

module.exports = {
  CustomErrorMessages,
  GenderType,
  HttpStatusCode,
  Owner
};
