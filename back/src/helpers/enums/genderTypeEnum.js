const GenderTypeEnum = ['MASCULINO', 'FEMININO', 'NÃO INFORMADO'];

module.exports = Object.freeze(GenderTypeEnum);
