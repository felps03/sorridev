const emailRegex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
const cpfRegex = /^(\d{3}.\d{3}.\d{3}\-\d{2})|(\d{11})$/;
const dataRegex = /^\d{4}\-(0?[1-9]|1[012])\-(0?[1-9]|[12][0-9]|3[01])*$/;
const letterRegex = /^[A-Za-zÀ-ÿ &]*$/;
const numberRegex = /^[0-9]*$/;
const emailDomainRegex = /^@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
const cepRegex = /^\d{5}-\d{3}$/;
const letterAndNumberRegex = /^[a-z0-9]*$/;
const cellPhoneRegex = /^\s\(\d{2}|\d{0}\)[- ]?(\d{5})[- ]?(\d{4})[- ]?\s*$/;
const phoneRegex = /^\s\(\d{2}|\d{0}\)[- ]?(\d{4})[- ]?(\d{4})[- ]?\s*$/;
const numbers1To9Regex = /^([1-9])$/;
const nameWithSpecialCharactersRegex = /^[A-Za-zÀ-ÿ-0-9\- \( \) \[ \]&]*$/;

module.exports = {
  emailRegex,
  cpfRegex,
  dataRegex,
  letterRegex,
  numberRegex,
  emailDomainRegex,
  cepRegex,
  letterAndNumberRegex,
  cellPhoneRegex,
  phoneRegex,
  numbers1To9Regex,
  nameWithSpecialCharactersRegex
};
