const jwt = require('jsonwebtoken');
const moment = require('moment-timezone');

const authConfig = require('../config/auth');
const {
  HttpStatusCode: { SERVER_INTERNAL_ERROR },
  CustomErrorMessages: { MONGODB_ERROR, MONGODB_CONFLICT }
} = require('./enums');
const { Exception, ErrorDefinitions } = require('./errors');
const { cpfRegex, emailRegex, dataRegex, cepRegex, cellPhoneRegex, phoneRegex } = require('./regularExpressions');

const utils = {
  mongoBrazilianTimezone() {
    return moment
      .tz(Date.now(), 'America/Sao_Paulo')
      .subtract(3, 'hour')
      .format('YYYY-MM-DD HH:mm:ss');
  },

  brazilianTimeZone() {
    return moment.tz(Date.now(), 'America/Sao_Paulo').format('YYYY-MM-DD HH:mm:ss');
  },

  getStatusCode(error) {
    return error.status || SERVER_INTERNAL_ERROR;
  },

  formatError(error) {
    if (error.name === 'MongoError') {
      if (error.code === 11000) {
        return Exception.raise({
          ...ErrorDefinitions(error.status),
          detail: { motive: MONGODB_CONFLICT }
        });
      }
      return Exception.raise({
        ...ErrorDefinitions(error.status),
        detail: { motive: MONGODB_ERROR }
      });
    }
    if (error.name === 'RequestError') {
      return Exception.raise({
        ...ErrorDefinitions(error.status),
        detail: { motive: error.message, options: error.options }
      });
    }

    if (error.name === 'JsonWebTokenError') {
      return Exception.raise({ ...ErrorDefinitions(error.status || 401), detail: { motive: error.message } });
    }
    // ValidationError
    return Exception.raise({
      ...ErrorDefinitions(error.status),
      detail: { errorType: error.type, motive: error.message, extra: error.details }
    });
  },

  escapeRegExp(string) {
    return string.replace(/[.*+?@#&¨%\!\-='^${}()|[\]\\]/g, '\\$&');
  },

  cleanEmpty(obj) {
    /* eslint no-return-assign: */
    if (Array.isArray(obj)) {
      return obj.map(v => (v && typeof v === 'object' ? utils.cleanEmpty(v) : v)).filter(v => !(v == null));
    }
    return Object.entries(obj)
      .map(([k, v]) => [k, v && typeof v === 'object' ? utils.cleanEmpty(v) : v])
      .reduce((a, [k, v]) => (v == null ? a : ((a[k] = v), a)), {});
    /* eslint no-return-assign: */
  },

  capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
  },

  generateToken(payload) {
    const { _id, owner, blockedAt } = payload;
    return jwt.sign({ _id, owner, blockedAt }, authConfig.secret, {
      expiresIn: authConfig.expiresIn
    });
  },

  formatLogger(error) {
    const msgErro = `Status: ${error.status ? error.status : '500'} | Message: ${error.message}`;
    let options = '';
    if (error.options) {
      options = ` | Option: ${error.options}`;
    }
    return msgErro + options;
  },

  getOnlyFormattedTime(datetime) {
    return moment(moment.utc(new Date(datetime)).format('HH:mm'), 'HH:mm');
  },

  cpfValidation(cpf) {
    cpf = cpf.replace(/\D/g, '');

    if (cpf.toString().length !== 11 || /^(\d)\1{10}$/.test(cpf)) return false;

    let result = true;
    [9, 10].forEach(j => {
      let sum = 0;
      let rest;
      cpf
        .split(/(?=)/)
        .splice(0, j)
        .forEach((e, i) => {
          sum += parseInt(e, 10) * (j + 2 - (i + 1));
        });
      rest = sum % 11;
      rest = rest < 2 ? 0 : 11 - rest;

      if (rest !== parseInt(cpf.substring(j, j + 1), 10)) result = false;
    });
    return result;
  },

  validateEmail(email) {
    return emailRegex.test(email);
  },

  isDateValid(date) {
    // regex formate obtained = require( https://stackoverflow.com/questions/15491894/regex-to-validate-date-format-dd-mm-yyyy/26972181#26972181
    return dataRegex.test(date);
  },

  cepValidation(cepNumber) {
    return cepRegex.test(cepNumber);
  },

  cellphoneValidation(phoneNumber) {
    return cellPhoneRegex.test(phoneNumber);
  },

  phoneValidation(phoneNumber) {
    return phoneRegex.test(phoneNumber);
  },

  isCPF(param) {
    return cpfRegex.test(param);
  }
};

module.exports = utils;
