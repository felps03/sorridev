/* eslint no-shadow: 0 */
const moment = require('moment-timezone');
const winston = require('winston');

const { combine, timestamp, printf } = winston.format;

const removeFileName = message => message.replace(`${message.split(' ')[0]} - `, '');

const getFileName = message => `${message.split(' ')[0]}.js`;

const myFormat = printf(({ level, message, timestamp }) => {
  return `${timestamp} [${getFileName(message)}] ${level}: ${removeFileName(message)}`;
});

const logger = winston.createLogger({
  level: 'silly',
  transports: [new winston.transports.Console(), new winston.transports.File({ filename: 'app.log' })],
  format: combine(
    timestamp({
      format: moment.tz(Date.now(), 'America/Sao_Paulo').format('DD-MM-YYYY HH:mm:ss')
    }),
    myFormat
  )
});

module.exports = logger;
/* eslint no-shadow: 0 */
