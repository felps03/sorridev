const mongoose = require('mongoose');
require('dotenv/config');

const { MONGO_URL } = require('../../../../config');

const connectionOptions = {
  useNewUrlParser: true,
  useFindAndModify: false,
  useUnifiedTopology: true,
  useCreateIndex: true
};
class Database {
  constructor() {
    this.connect();
  }

  connect() {
    return mongoose.connect(MONGO_URL, connectionOptions);
  }
}

module.exports = new Database().connect();
