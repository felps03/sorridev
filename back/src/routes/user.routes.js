const UserController = require('../app/controllers/UserController');
const AuthMiddleware = require('../app/middlewares/authMiddleware');
const BlockMiddleware = require('../app/middlewares/blockMiddleware');
const ValidateCreate = require('../app/validators/user/create');
const ValidateGetAll = require('../app/validators/user/getAll');
const ValidateID = require('../app/validators/validateParams/validateID');

module.exports = (server, routes, prefix = '/user') => {
  routes.post('/', ValidateCreate, UserController.create);

  routes.use(AuthMiddleware, BlockMiddleware);

  routes.get('/', ValidateGetAll, UserController.listAll);

  routes.get('/search', UserController.search);
  routes.get('/analysis/age', UserController.analysisAge);
  routes.get('/analysis/gender', UserController.analysisGender);

  routes.get('/dashboard', UserController.dashboard);

  routes.get('/dashboard/attendance', UserController.dashboardAttendance);

  routes
    .route('/:id')
    .get(ValidateID, UserController.find)
    .put(ValidateID, UserController.update)
    .delete(ValidateID, UserController.delete);

  server.use(prefix, routes);
};
