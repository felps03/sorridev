const ProfessionalController = require('../app/controllers/ProfessionalController');
const AdminMiddleware = require('../app/middlewares/adminMiddleware');
const AuthMiddleware = require('../app/middlewares/authMiddleware');
const BlockMiddleware = require('../app/middlewares/blockMiddleware');
const ValidateCreate = require('../app/validators/professional/create');
const ValidateGetAll = require('../app/validators/professional/getAll');
const ValidateID = require('../app/validators/validateParams/validateID');

module.exports = (server, routes, prefix = '/professional') => {
  routes.use(AuthMiddleware, BlockMiddleware, AdminMiddleware);

  routes
    .route('/')
    .post(ValidateCreate, ProfessionalController.create)
    .get(ValidateGetAll, ProfessionalController.listAll);

  routes.route('/morePatients').get(ProfessionalController.professionalWithMorePatients);

  routes
    .route('/:id')
    .get(ValidateID, ProfessionalController.find)
    .put(ValidateID, ProfessionalController.update)
    .delete(ValidateID, ProfessionalController.delete)
    .patch(ValidateID, ProfessionalController.patch);

  server.use(prefix, routes);
};
