const ScheduleController = require('../app/controllers/ScheduleController');
const AuthMiddleware = require('../app/middlewares/authMiddleware');
const BlockMiddleware = require('../app/middlewares/blockMiddleware');
const ValidateID = require('../app/validators/validateParams/validateID');

module.exports = (server, routes, prefix = '/schedule') => {
  routes.use(AuthMiddleware, BlockMiddleware);

  routes
    .route('/')
    .post(ScheduleController.create)
    .get(ScheduleController.listAll);

  routes.get('/search', ScheduleController.search);

  routes.get('/servicecount', ScheduleController.serviceCount);

  routes.get('/dashboard', ScheduleController.dashboard);

  routes
    .route('/:id')
    .get(ValidateID, ScheduleController.find)
    .put(ValidateID, ScheduleController.update)
    .delete(ValidateID, ScheduleController.delete);

  server.use(prefix, routes);
};
