const BalanceController = require('../app/controllers/BalanceController');
const AuthMiddleware = require('../app/middlewares/authMiddleware');
const BlockMiddleware = require('../app/middlewares/blockMiddleware');
const BusinessMiddleware = require('../app/middlewares/businessMiddleware');
const PostValidate = require('../app/validators/balance/PostValidate');
const ValidateID = require('../app/validators/validateParams/validateID');

module.exports = (server, routes, prefix = '/balance') => {
  routes.use(AuthMiddleware, BlockMiddleware, BusinessMiddleware);

  routes
    .route('/')
    .post(PostValidate, BalanceController.create)
    .get(BalanceController.listAll);

  routes
    .route('/:id')
    .get(ValidateID, BalanceController.find)
    .put(ValidateID, BalanceController.update)
    .delete(ValidateID, BalanceController.delete);

  server.use(prefix, routes);
};
