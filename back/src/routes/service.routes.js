const ServiceController = require('../app/controllers/ServiceController');
const AuthMiddleware = require('../app/middlewares/authMiddleware');
const BlockMiddleware = require('../app/middlewares/blockMiddleware');
const BusinessMiddleware = require('../app/middlewares/businessMiddleware');
const ValidateID = require('../app/validators/validateParams/validateID');

module.exports = (server, routes, prefix = '/service') => {
  routes.use(AuthMiddleware, BlockMiddleware, BusinessMiddleware);

  routes
    .route('/')
    .post(ServiceController.create)
    .get(ServiceController.listAll);

  routes
    .route('/:id')
    .get(ValidateID, ServiceController.find)
    .put(ValidateID, ServiceController.update)
    .delete(ValidateID, ServiceController.delete);

  server.use(prefix, routes);
};
