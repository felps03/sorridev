const EmployeeController = require('../app/controllers/EmployeeController');
const AuthMiddleware = require('../app/middlewares/authMiddleware');
const BlockMiddleware = require('../app/middlewares/blockMiddleware');
const ValidateCreate = require('../app/validators/employee/create');
const ValidateGetAll = require('../app/validators/employee/getAll');
const ValidateID = require('../app/validators/validateParams/validateID');

module.exports = (server, routes, prefix = '/employee') => {
  routes.use(AuthMiddleware, BlockMiddleware);

  routes
    .route('/')
    .post(ValidateCreate, EmployeeController.create)
    .get(ValidateGetAll, EmployeeController.listAll);

  routes
    .route('/:id')
    .get(ValidateID, EmployeeController.find)
    .put(ValidateID, EmployeeController.update)
    .delete(ValidateID, EmployeeController.delete);

  server.use(prefix, routes);
};
