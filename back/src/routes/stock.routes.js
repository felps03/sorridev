const StockController = require('../app/controllers/StockController');
const AuthMiddleware = require('../app/middlewares/authMiddleware');
const BlockMiddleware = require('../app/middlewares/blockMiddleware');
const BusinessMiddleware = require('../app/middlewares/businessMiddleware');
const ValidateID = require('../app/validators/validateParams/validateID');

module.exports = (server, routes, prefix = '/stock') => {
  routes.use(AuthMiddleware, BlockMiddleware, BusinessMiddleware);

  routes
    .route('/')
    .post(StockController.create)
    .get(StockController.listAll);

  routes
    .route('/:id')
    .get(ValidateID, StockController.find)
    .put(ValidateID, StockController.update)
    .delete(ValidateID, StockController.delete);

  server.use(prefix, routes);
};
