const ExpressBrute = require('express-brute');

const AuthController = require('../app/controllers/AuthController');
const AuthMiddleware = require('../app/middlewares/authMiddleware');
const ValidateCreateGoogle = require('../app/validators/auth/authGoogle');
const ValidateResetPassword = require('../app/validators/auth/resetPassword');

const store = new ExpressBrute.MemoryStore();
const bruteforce = new ExpressBrute(store);

module.exports = (server, routes, prefix = '/auth') => {
  if (['test', 'dev'].includes(process.env.NODE_ENV)) {
    routes.post('/', AuthController.login);
  } else {
    routes.post('/', bruteforce.prevent, AuthController.login);
  }
  routes.post('/google', ValidateCreateGoogle, AuthController.google);
  routes.patch('/resetPassword', AuthMiddleware, ValidateResetPassword, AuthController.resetPassword);
  server.use(prefix, routes);
};
