const { Router } = require('express');

const auth = require('./auth.routes');
const balance = require('./balance.routes');
const employee = require('./employee.routes');
const health = require('./health.routes');
const professional = require('./professional.routes');
const schedule = require('./schedule.routes');
const service = require('./service.routes');
const stock = require('./stock.routes');
const swagger = require('./swagger.routes');
const user = require('./user.routes');

module.exports = server => {
  server.use((req, res, next) => {
    auth(server, new Router());
    balance(server, new Router());
    health(server, new Router());
    professional(server, new Router());
    schedule(server, new Router());
    stock(server, new Router());
    swagger(server, new Router());
    service(server, new Router());
    employee(server, new Router());
    user(server, new Router());
    next();
  });
};
