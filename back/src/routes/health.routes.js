const HealthController = require('../app/controllers/HealthController');

module.exports = (server, routes, prefix = '/health') => {
  routes.get('/', HealthController.index);

  server.use(prefix, routes);
};
