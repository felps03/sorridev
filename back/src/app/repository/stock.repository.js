const Stock = require('../schemas/Stock');
const Repository = require('./repository');

const populate = {
  path: 'dentist',
  select: ['name', 'email', 'phone', 'specialist', 'CRO']
};

class StockRepository extends Repository {
  constructor() {
    super(Stock);
  }

  get(_id) {
    return super.get(_id, populate);
  }

  getAll(query) {
    if (!query.search) {
      query.populate = populate;

      return super.getAll({ ...query });
    }

    const dbQuery = {
      $and: []
    };

    dbQuery.$and.push({ $text: { $search: query.search } });
    dbQuery.$and.push({ dentist: query.dentist });

    delete query.search;
    delete query.dentist;

    return super.getAll({ ...dbQuery, ...query });
  }
}

module.exports = new StockRepository();
