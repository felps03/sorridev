const AdminRepository = require('./admin.repository');
const BalanceRepository = require('./balance.repository');
const EmployeeRepository = require('./employee.repository');
const ProfessionalRepository = require('./professional.repository');
const ScheduleRepository = require('./schedule.repository');
const ServiceRepository = require('./service.repository');
const StockRepository = require('./stock.repository');
const UserRepository = require('./user.repository');

module.exports = {
  AdminRepository,
  BalanceRepository,
  ProfessionalRepository,
  ScheduleRepository,
  ServiceRepository,
  StockRepository,
  EmployeeRepository,
  UserRepository
};
