const Employee = require('../schemas/Employee');
const Repository = require('./repository');

class EmployeeRepository extends Repository {
  constructor() {
    super(Employee);
    this.schema = Employee;
  }

  create(payload) {
    payload.owner = 'EMPLOYEE';
    return super.create(payload);
  }

  getAll(query) {
    if (!query.search) return super.getAll({ ...query });

    const dbQuery = {
      $and: []
    };

    dbQuery.$and.push({ $text: { $search: query.search } });

    delete query.search;

    return super.getAll({ ...dbQuery, ...query });
  }

  get(_id) {
    const options = {
      path: 'dentist',
      select: ['name', 'email', 'phone', 'specialist', 'CRO']
    };
    return super.get(_id, options);
  }

  emailExistsGetPassword(email) {
    return this.schema.findOne({ email }).select(['+password', '+blockedAt']);
  }
}

module.exports = new EmployeeRepository();
