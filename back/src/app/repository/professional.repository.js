const Professional = require('../schemas/Professional');
const Repository = require('./repository');

class ProfessionalRepository extends Repository {
  constructor() {
    super(Professional);
    this.schema = Professional;
  }

  create(payload) {
    payload.owner = 'DENTIST';
    return super.create(payload);
  }

  getAll(query) {
    if (!query.search) return super.getAll({ ...query });

    const dbQuery = {
      $and: []
    };

    dbQuery.$and.push({ $text: { $search: query.search } });

    delete query.search;

    return super.getAll({ ...dbQuery, ...query });
  }

  emailExistsGetPassword(email) {
    return this.schema.findOne({ email }).select(['+password', '+blockedAt']);
  }
}

module.exports = new ProfessionalRepository();
