const mongoose = require('mongoose');

const User = require('../schemas/User');
const dashboardAggregate = require('./query/dashboardQuery');
const Repository = require('./repository');

const populate = [
  {
    path: 'dentist',
    select: ['name', 'cpf', 'phone', 'email', 'observation', 'CRO', 'specialist']
  }
];

class UserRepository extends Repository {
  constructor() {
    super(User);
    this.schema = User;
  }

  create(payload) {
    payload.owner = 'USER';
    return super.create(payload);
  }

  getAll(query) {
    if (!query.search) {
      query.populate = populate;

      return super.getAll({ ...query });
    }

    const dbQuery = {
      $and: []
    };

    dbQuery.$and.push({ $text: { $search: query.search } });
    dbQuery.$and.push({ $or: [{ dentist: { $exists: true, $size: 0 } }, { dentist: query.dentist }] });

    delete query.search;
    delete query.dentist;

    return super.getAll({ ...dbQuery, ...query });
  }

  get(_id) {
    return super.get(_id, populate);
  }

  emailExistsGetPassword(email) {
    return this.schema.findOne({ email }).select(['+password', '+blockedAt']);
  }

  dashboard({ TODAY, YEAR_BEFORE, dentist }) {
    const pipeline = [];

    pipeline.push({
      $match: {
        dentist: new mongoose.Types.ObjectId(dentist),
        createdAt: { $gte: YEAR_BEFORE, $lte: TODAY }
      }
    });

    pipeline.push({
      $group: {
        _id: { year_month: { $substrCP: ['$createdAt', 0, 7] } },
        count: { $sum: 1 }
      }
    });

    pipeline.push(...dashboardAggregate({ TODAY, YEAR_BEFORE }));

    return super.aggregate(pipeline);
  }

  newUser({ startOfMonth, endOfMonth, dentist }) {
    const payload = {
      dentist,
      createdAt: {
        $gte: startOfMonth,
        $lt: endOfMonth
      }
    };
    return super.count(payload);
  }

  analysisGender({ dentist }) {
    return this.schema.aggregate([
      {
        $match: {
          dentist: new mongoose.Types.ObjectId(dentist)
        }
      },
      {
        $project: {
          male: { $cond: [{ $eq: ['$gender', 'MASCULINO'] }, 1, 0] },
          female: { $cond: [{ $eq: ['$gender', 'FEMININO'] }, 1, 0] },
          empty: { $cond: [{ $eq: ['$gender', 'NÃO INFORMADO'] }, 1, 0] }
        }
      },
      {
        $group: {
          _id: null,
          male: { $sum: '$male' },
          female: { $sum: '$female' },
          empty: { $sum: '$empty' },
          total: { $sum: 1 }
        }
      }
    ]);
  }

  analysisAge({ dentist }) {
    return this.schema.aggregate([
      {
        $match: {
          dentist: new mongoose.Types.ObjectId(dentist)
        }
      },
      {
        $project: {
          _id: 0,
          age: {
            $divide: [
              {
                $subtract: [new Date(), { $ifNull: ['$birthdate', new Date()] }]
              },
              1000 * 86400 * 365
            ]
          }
        }
      },
      {
        $project: {
          range: {
            $concat: [
              { $cond: [{ $lte: ['$age', 0] }, 'Unknown', ''] },
              { $cond: [{ $and: [{ $gt: ['$age', 0] }, { $lt: ['$age', 10] }] }, 'Under 10', ''] },
              { $cond: [{ $and: [{ $gt: ['$age', 10] }, { $lt: ['$age', 18] }] }, '11 - 18', ''] },
              { $cond: [{ $and: [{ $gte: ['$age', 18] }, { $lt: ['$age', 25] }] }, '18 - 24', ''] },
              { $cond: [{ $and: [{ $gte: ['$age', 25] }, { $lt: ['$age', 31] }] }, '25 - 30', ''] },
              { $cond: [{ $and: [{ $gte: ['$age', 31] }, { $lt: ['$age', 41] }] }, '31 - 40', ''] },
              { $cond: [{ $and: [{ $gte: ['$age', 41] }, { $lt: ['$age', 51] }] }, '41 - 50', ''] },
              { $cond: [{ $gte: ['$age', 51] }, 'Over 50', ''] }
            ]
          }
        }
      },
      {
        $group: {
          _id: '$range',
          count: {
            $sum: 1
          }
        }
      }
    ]);
  }

  professionalWithMorePatients(limit = 10) {
    const pipeline = [
      {
        $lookup: {
          from: 'professionals',
          let: { dentist: '$dentist' },
          pipeline: [
            {
              $match: {
                $expr: {
                  $and: [{ $in: ['$_id', '$$dentist'] }, { $not: { $eq: ['$_id', 1] } }]
                }
              }
            },
            {
              $project: {
                name: 1,
                email: 1,
                phone: 1
              }
            }
          ],
          as: 'dentist'
        }
      },
      { $group: { _id: '$dentist', count: { $sum: 1 } } },
      { $sort: { count: -1 } },
      { $limit: Number(limit) }
    ];

    return super.aggregate(pipeline);
  }
}

module.exports = new UserRepository();
