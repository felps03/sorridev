const Admin = require('../schemas/Admin');
const Repository = require('./repository');

class EmployeeRepository extends Repository {
  constructor() {
    super(Admin);
    this.schema = Admin;
  }

  create(payload) {
    payload.owner = 'ADMIN';
    return super.create(payload);
  }

  emailExistsGetPassword(email) {
    return this.schema.findOne({ email }).select(['+password', '+blockedAt']);
  }
}

module.exports = new EmployeeRepository();
