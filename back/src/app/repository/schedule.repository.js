const mongoose = require('mongoose');

const { dataUtils } = require('../../helpers');
const Schedule = require('../schemas/Schedule');
const dashboardAggregate = require('./query/dashboardQuery');
const Repository = require('./repository');

const populate = [
  {
    path: 'client',
    select: ['name', 'cpf', 'phone', 'email', 'observation']
  },
  {
    path: 'dentist',
    select: ['name', 'cpf', 'phone', 'email', 'observation', 'CRO']
  }
];

class ScheduleRepository extends Repository {
  constructor() {
    super(Schedule);
    this.schema = Schedule;
  }

  search({ startTime, endTime, dentist }) {
    const payload = { dentist };
    if (startTime && endTime) {
      payload.startTime = {
        $gte: dataUtils.getStartOfTheDay(startTime),
        $lte: dataUtils.getEndOfTheDay(endTime)
      };
    } else if (startTime) {
      payload.startTime = {
        $gte: dataUtils.getStartOfTheDay(startTime),
        $lte: dataUtils.getEndOfTheDay(startTime)
      };
    }

    payload.sort = { startTime: 1 };

    payload.populate = populate;

    return super.getAll(payload);
  }

  getAll(payload) {
    payload.populate = populate;

    return super.getAll(payload);
  }

  get(id) {
    return super.get(id, populate);
  }

  serviceCount({ day, dentist, status }) {
    const payload = { dentist, status };
    if (!status) delete payload.status;

    payload.startTime = {
      $gte: dataUtils.getStartOfTheDay(day),
      $lte: dataUtils.getEndOfTheDay(day)
    };
    return super.count(payload);
  }

  dashboard({ status, TODAY, YEAR_BEFORE, dentist }) {
    const pipeline = [];

    pipeline.push({
      $match: {
        status,
        dentist: new mongoose.Types.ObjectId(dentist),
        startTime: { $gte: YEAR_BEFORE, $lte: TODAY }
      }
    });

    pipeline.push({
      $group: {
        _id: { year_month: { $substrCP: ['$startTime', 0, 7] } },
        count: { $sum: 1 }
      }
    });

    pipeline.push(...dashboardAggregate({ TODAY, YEAR_BEFORE }));

    return super.aggregate(pipeline);
  }

  attendance({ startOfMonth, endOfMonth, dentist }) {
    const payload = {
      dentist,
      status: { $ne: 'ausente' },
      startTime: {
        $gte: startOfMonth,
        $lt: endOfMonth
      }
    };

    return super.count(payload);
  }

  absence({ startOfMonth, endOfMonth, dentist }) {
    const payload = {
      dentist,
      status: 'ausente',
      startTime: {
        $gte: startOfMonth,
        $lt: endOfMonth
      }
    };

    return super.count(payload);
  }

  update(_id, payload) {
    return super.update(_id, payload, populate);
  }
}

module.exports = new ScheduleRepository();
