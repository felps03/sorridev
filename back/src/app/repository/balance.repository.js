const Balance = require('../schemas/Balance');
const Repository = require('./repository');

class BalanceRepository extends Repository {
  constructor() {
    super(Balance);
  }
  
  getAll(query) {
    if (!query.search) {
      return super.getAll({ ...query });
    }

    const dbQuery = {
      $and: []
    };

    dbQuery.$and.push({ $text: { $search: query.search } });
    dbQuery.$and.push({ dentist: query.dentist });

    delete query.search;
    delete query.dentist;

    return super.getAll({ ...dbQuery, ...query });
  }
}

module.exports = new BalanceRepository();
