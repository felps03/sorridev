const { utils } = require('../../helpers');

class Repository {
  constructor(schema) {
    this.schema = schema;
  }

  create(payload) {
    return this.schema.create(utils.cleanEmpty(payload));
  }

  delete(_id) {
    return this.schema.findByIdAndDelete(_id);
  }

  update(_id, payload, options) {
    const newPayload = utils.cleanEmpty(payload);
    return this.schema
      .findByIdAndUpdate(_id, { ...newPayload, updatedAt: utils.brazilianTimeZone() }, { new: true })
      .populate(options);
  }

  search(payload, options) {
    return this.schema.findOne(payload).populate(options);
  }

  get(_id, options) {
    return this.schema.findById({ _id }).populate(options);
  }

  getAll(queryParam) {
    utils.cleanEmpty(queryParam);
    const { page = 1, limit = 100, sort, populate, ...query } = queryParam;
    return this.schema.paginate(
      { ...query },
      {
        populate,
        sort,
        limit: Number(limit),
        skip: (Number(page) - 1) * Number(limit),
        page: Number(page)
      }
    );
  }

  count(payload) {
    return this.schema.countDocuments(payload);
  }

  aggregate(pipeline) {
    return this.schema.aggregate(pipeline);
  }
}

module.exports = Repository;
