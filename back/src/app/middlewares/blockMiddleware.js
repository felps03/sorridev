module.exports = async (req, res, next) => {
  if (req.userBlock === false) next();
  else
    res.status(403).send({
      key: 'FORBIDDEN',
      statusCode: 403,
      message: 'The request could not be made because the resource was blocked.',
      detail: {
        motive: 'Oops, we encountered a problem with your account. Please contact the administrators'
      }
    });
};
