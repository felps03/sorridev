module.exports = async (req, res, next) => {
  if (req.userOwner === 'ADMIN' || req.userOwner === 'DENTIST' || req.userOwner === 'EMPLOYEE') next();
  else
    res.status(403).send({
      key: 'FORBIDDEN',
      statusCode: 403,
      message: 'The request could not be made because the resource was blocked.',
      detail: {
        motive: 'To access this area you should be either a Professional, Employee or Admin'
      }
    });
};

// https://stackoverflow.com/a/58301800
// Olhar como funciona acesso de token
