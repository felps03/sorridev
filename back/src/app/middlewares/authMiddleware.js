const jwt = require('jsonwebtoken');
const { isEmpty } = require('lodash');
const { promisify } = require('util');

const authConfig = require('../../config/auth');
const {
  logger,
  utils,
  error: { JwtError },
  enums: {
    HttpStatusCode: { UNAUTHORIZED },
    CustomErrorMessages: { TOKEN_MISSING }
  }
} = require('../../helpers');

module.exports = async (req, res, next) => {
  try {
    const authHeader = req.headers.authorization;

    if (!isEmpty(authHeader)) {
      const [, token] = authHeader.split(' ');
      const { _id, owner, blockedAt } = await promisify(jwt.verify)(token, authConfig.secret);
      req.userId = _id;
      req.userOwner = owner;
      req.userBlock = blockedAt;
    } else {
      throw new JwtError(TOKEN_MISSING, UNAUTHORIZED);
    }

    return next();
  } catch (error) {
    logger.error('AUTH - FAIL', error);
    return res.status(utils.getStatusCode(error)).json(utils.formatError(error));
  }
};
