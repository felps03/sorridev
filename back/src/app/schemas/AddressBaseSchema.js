const mongoose = require('mongoose');

const AddressBaseSchema = new mongoose.Schema({
  address: {
    cep: {
      type: String
    },
    street: {
      type: String
    },
    number: {
      type: String
    },
    neighborhood: {
      type: String
    },
    complement: {
      type: String
    },
    city: {
      type: String
    },
    state: {
      type: String
    },
    country: {
      type: String
    }
  }
});

module.exports = AddressBaseSchema;
