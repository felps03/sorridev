const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');

const { constructSchema } = require('../../lib/ExtendSchema');
const AccessControlSchema = require('./AccessControlSchema');

const Schemas = [AccessControlSchema];
const StockSchema = constructSchema(Schemas, {
  product: {
    type: String,
    required: true
  },
  price: {
    type: String,
    required: true
  },
  quantity: {
    type: Number,
    required: true
  },
  provider: {
    type: String,
    required: true
  },
  dentist: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: 'Professional'
  }
});


StockSchema.index(
  {
    product: 'text',
    provider: 'text'
  },
  {
    weights: {
      product: 10,
      provider: 7
    }
  }
);

StockSchema.plugin(mongoosePaginate);
const Stock = mongoose.model('Stock', StockSchema);

module.exports = Stock;
