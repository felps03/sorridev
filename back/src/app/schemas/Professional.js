const bcrypt = require('bcryptjs');
const moment = require('moment');
const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');

const { constructSchema } = require('../../lib/ExtendSchema');
const AuthBaseSchema = require('./AuthBaseSchema');
const UserBaseSchema = require('./UserBaseSchema');

const Schemas = [AuthBaseSchema, UserBaseSchema];
const ProfessionalSchema = constructSchema(Schemas, {
  CRO: {
    type: String,
    required: true
  },
  specialist: {
    type: Array
  }
});

ProfessionalSchema.index(
  {
    name: 'text',
    cpf: 'text',
    CRO: 'text',
    phone: 'text'
  },
  {
    weights: {
      CRO: 10,
      cpf: 9,
      name: 8,
      phone: 3
    }
  }
);

ProfessionalSchema.virtual('age').get(function() {
  return moment().diff(this.birthdate, 'years', false);
});

ProfessionalSchema.pre('save', async function(next) {
  const hash = await bcrypt.hash(this.password, Number(process.env.PASSWORD_SALT));
  this.password = hash;

  next();
});

ProfessionalSchema.plugin(mongoosePaginate);
const Professional = mongoose.model('Professional', ProfessionalSchema);

module.exports = Professional;
