const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');

const { constructSchema } = require('../../lib/ExtendSchema');
const AccessControlSchema = require('./AccessControlSchema');

const Schemas = [AccessControlSchema];
const SchemaSchedule = constructSchema(Schemas, {
  startTime: {
    type: Date,
    required: true
  },
  endTime: {
    type: Date,
    required: true
  },
  client: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: 'User'
  },
  dentist: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: 'Professional'
  },
  health_insurance_name: {
    type: String
  },
  observation: {
    type: String
  },
  value: {
    type: String
  },
  status: {
    type: String
  },
  hasReturn: {
    type: Boolean
  }
});

SchemaSchedule.plugin(mongoosePaginate);
const Schedule = mongoose.model('Schedule', SchemaSchedule);

module.exports = Schedule;
