const bcrypt = require('bcryptjs');
const moment = require('moment');
const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');

const { constructSchema } = require('../../lib/ExtendSchema');
const AuthBaseSchema = require('./AuthBaseSchema');
const UserBaseSchema = require('./UserBaseSchema');

const Schemas = [AuthBaseSchema, UserBaseSchema];
const UserSchema = constructSchema(Schemas, {
  observation: {
    type: String
  },
  dentist: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Professional'
    }
  ],
  password: {
    type: String,
    select: false
  }
});

UserSchema.index(
  {
    name: 'text',
    cpf: 'text',
    phone: 'text'
  },
  {
    weights: {
      cpf: 10,
      name: 7,
      phone: 3
    }
  }
);

UserSchema.virtual('age').get(function() {
  return moment().diff(this.birthdate, 'years', false);
});

UserSchema.pre('save', async function(next) {
  if (this.password) {
    const hash = await bcrypt.hash(this.password, Number(process.env.PASSWORD_SALT));
    this.password = hash;
  }
  next();
});

UserSchema.plugin(mongoosePaginate);
const User = mongoose.model('User', UserSchema);

module.exports = User;
