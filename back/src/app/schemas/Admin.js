const bcrypt = require('bcryptjs');
const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');

const { constructSchema } = require('../../lib/ExtendSchema');
const AuthBaseSchema = require('./AuthBaseSchema');
const UserBaseSchema = require('./UserBaseSchema');

const Schemas = [AuthBaseSchema, UserBaseSchema];
const AdminSchema = constructSchema(Schemas, {});

AdminSchema.index(
  {
    name: 'text'
  },
  {
    weights: {
      name: 10
    }
  }
);

AdminSchema.pre('save', async function(next) {
  const hash = await bcrypt.hash(this.password, Number(process.env.PASSWORD_SALT));
  this.password = hash;

  next();
});

AdminSchema.plugin(mongoosePaginate);
const Admin = mongoose.model('Admin', AdminSchema);

module.exports = Admin;
