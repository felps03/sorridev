const mongoose = require('mongoose');

const {
  utils,
  enums: { GenderType, Owner }
} = require('../../helpers');
const { constructSchema } = require('../../lib/ExtendSchema');
const AccessControlSchema = require('./AccessControlSchema');
const AddressBaseSchema = require('./AddressBaseSchema');

const UserBaseSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  cpf: {
    type: String,
    unique: true,
    sparse: true,
    required: false
  },
  email: {
    type: String,
    unique: true,
    required: true
  },
  birthdate: {
    type: Date,
    required: false
  },
  gender: {
    type: String,
    required: false,
    uppercase: true,
    enum: GenderType
  },
  owner: {
    type: String,
    required: false,
    uppercase: true,
    enum: Owner,
    default: 'USER'
  },
  rg: {
    type: String
  },
  phone: {
    type: String
  },
  accessedAt: {
    type: Date,
    select: false,
    default: utils.mongoBrazilianTimezone()
  },
  blockedAt: {
    type: Boolean,
    select: false,
    default: false
  },
  deactivatedAt: {
    type: Date
  },
  photo: {
    type: String
  },
  provider: {
    type: String
  }
});

const Schemas = [AccessControlSchema, UserBaseSchema, AddressBaseSchema];
const SchemaUserBase = constructSchema(Schemas, {});

module.exports = SchemaUserBase;
