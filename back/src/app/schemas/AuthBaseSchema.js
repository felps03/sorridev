const mongoose = require('mongoose');

const AuthBaseSchema = new mongoose.Schema({
  password: {
    type: String,
    required: true,
    select: false
  },
  passwordResetToken: {
    type: String,
    select: false
  },
  passwordResetExpires: {
    type: Date,
    select: false
  },
  resetDataToken: {
    type: Date,
    select: false
  },
  resetPasswordToken: {
    type: String,
    select: false
  }
});

module.exports = AuthBaseSchema;
