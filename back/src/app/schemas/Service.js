const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');

const { constructSchema } = require('../../lib/ExtendSchema');
const AccessControlSchema = require('./AccessControlSchema');

const Schemas = [AccessControlSchema];
const ServiceSchema = constructSchema(Schemas, {
  name: {
    type: String,
    required: true
  },
  phone: {
    type: String,
    required: true
  },
  supply: {
    type: String
  },
  dentist: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: 'Professional'
  }
});


ServiceSchema.index(
  {
    name: 'text'
  },
  {
    weights: {
      name: 10
    }
  }
);

ServiceSchema.plugin(mongoosePaginate);
const Service = mongoose.model('Service', ServiceSchema);

module.exports = Service;
