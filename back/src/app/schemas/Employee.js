const bcrypt = require('bcryptjs');
const moment = require('moment');
const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');

const { constructSchema } = require('../../lib/ExtendSchema');
const AuthBaseSchema = require('./AuthBaseSchema');
const UserBaseSchema = require('./UserBaseSchema');

const Schemas = [AuthBaseSchema, UserBaseSchema];
const EmployeeSchema = constructSchema(Schemas, {
  dentist: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: 'Professional'
  }
});

EmployeeSchema.index(
  {
    name: 'text',
    cpf: 'text',
    phone: 'text'
  },
  {
    weights: {
      cpf: 10,
      name: 8,
      phone: 3
    }
  }
);

EmployeeSchema.virtual('age').get(function() {
  return moment().diff(this.birthdate, 'years', false);
});

EmployeeSchema.pre('save', async function(next) {
  const hash = await bcrypt.hash(this.password, Number(process.env.PASSWORD_SALT));
  this.password = hash;

  next();
});

EmployeeSchema.plugin(mongoosePaginate);
const Employee = mongoose.model('Employee', EmployeeSchema);

module.exports = Employee;
