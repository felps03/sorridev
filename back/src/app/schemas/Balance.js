const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');

const { constructSchema } = require('../../lib/ExtendSchema');
const AccessControlSchema = require('./AccessControlSchema');

const Schemas = [AccessControlSchema];
const BalanceSchema = constructSchema(Schemas, {
  cash_accounting: {
    type: String,
    required: true
  },
  spending: {
    type: String,
    required: true
  },
  number_of_appointments: {
    type: Number,
    required: true
  },
  dentist: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: 'Professional'
  }
});


BalanceSchema.index(
  {
    spending: 'text',
    number_of_appointments: 'text'
  },
  {
    weights: {
      spending: 10,
      name: 8
    }
  }
);

BalanceSchema.plugin(mongoosePaginate);
const Balance = mongoose.model('Balance', BalanceSchema);

module.exports = Balance;
