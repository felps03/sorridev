const mongoose = require('mongoose');

const { utils } = require('../../helpers');

const AccessControlSchema = new mongoose.Schema({
  createdAt: {
    type: Date,
    select: false,
    default: utils.mongoBrazilianTimezone()
  },
  updatedAt: {
    type: Date,
    select: false,
    default: utils.mongoBrazilianTimezone()
  }
});

module.exports = AccessControlSchema;
