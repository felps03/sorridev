const serialize = ({ _id, name, phone, supply }) => {
  return { _id, name, phone, supply };
};

const paginatedSerialize = ({ docs, total, limit, page, pages }) => {
  return {
    docs: docs.map(serialize),
    total,
    limit,
    page: Number(page),
    pages: Number(pages)
  };
};

module.exports = {
  serialize,
  paginatedSerialize
};
