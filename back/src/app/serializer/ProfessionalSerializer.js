const serialize = ({ _id, name, birthdate, age, cpf, rg, owner, specialist, phone, gender, email, CRO, address }) => {
  return { _id, name, birthdate, age, cpf, rg, owner, specialist, phone, gender, email, CRO, address };
};

const paginatedSerialize = ({ docs, total, limit, page, pages }) => {
  return {
    docs: docs.map(serialize),
    total,
    limit,
    page: Number(page),
    pages: Number(pages)
  };
};

const morePatientsSerialize = payload => {
  const newData = [];

  payload.map(({ _id, count }) => {
    if (_id[0] === undefined) {
      newData.push({
        name: 'Nenhum Profissional Atribuído',
        count
      });
    } else {
      newData.push({
        _id: _id[0]._id,
        name: _id[0].name,
        phone: _id[0].phone,
        email: _id[0].email,
        count
      });
    }
    return newData;
  });

  return newData;
};

module.exports = {
  serialize,
  paginatedSerialize,
  morePatientsSerialize
};
