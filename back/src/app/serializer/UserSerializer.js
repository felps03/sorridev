const serialize = (
  { _id, name, age, cpf, phone, gender, email, observation, dentist, provider, photo, rg, birthdate },
  userOwner
) => {
  if (userOwner === 'ADMIN')
    return { _id, name, age, cpf, phone, gender, email, observation, dentist, provider, photo, rg, birthdate };

  return { _id, name, age, cpf, phone, gender, email, observation, photo, rg, birthdate };
};

const paginatedSerialize = ({ docs, total, limit, page, pages }, userOwner) => {
  return {
    docs: docs.map(user => serialize(user, userOwner)),
    total,
    limit,
    page: Number(page),
    pages: Number(pages)
  };
};

const dashboardAttendanceSerialize = ({
  newUserThisMonth,
  newUserLastMonth,
  attendanceThisMonth,
  attendanceLastMonth,
  absenceThisMonth,
  absenceLastMonth
}) => {
  return {
    newUserThisMonth,
    newUserLastMonth,
    attendanceThisMonth,
    attendanceLastMonth,
    absenceThisMonth,
    absenceLastMonth
  };
};

const dashboardSerialize = ([{ data }]) => {
  return data;
};

const analysisGenderSerialize = ([{ male, female, empty, total }]) => {
  return { male, female, empty, total };
};

const analysisAgeSerialize = data => {
  return data;
};

module.exports = {
  serialize,
  paginatedSerialize,
  dashboardAttendanceSerialize,
  dashboardSerialize,
  analysisGenderSerialize,
  analysisAgeSerialize
};
