const serialize = ({ _id, owner, name, birthdate, age, rg, cpf, phone, gender, email, dentist, address }) => {
  return { _id, owner, name, birthdate, age, rg, cpf, phone, gender, email, dentist, address };
};

const paginatedSerialize = ({ docs, total, limit, page, pages }) => {
  return {
    docs: docs.map(serialize),
    total,
    limit,
    page: Number(page),
    pages: Number(pages)
  };
};

module.exports = {
  serialize,
  paginatedSerialize
};
