const { formatedDate } = require('../../helpers/dataUtils');

const serialize = ({
  _id,
  startTime,
  endTime,
  client,
  dentist,
  health_insurance_name,
  observation,
  value,
  status,
  hasReturn
}) => {
  return {
    _id,
    startTime: formatedDate(startTime),
    endTime: formatedDate(endTime),
    client,
    dentist,
    health_insurance_name,
    observation,
    value,
    status,
    hasReturn
  };
};

const paginatedSerialize = ({ docs, total, limit, page, pages }) => {
  return {
    docs: docs.map(serialize),
    total,
    limit,
    page: Number(page),
    pages: Number(pages)
  };
};

const dashboardSerialize = ([{ data }]) => {
  return data;
};

const serviceCountSerialize = ({ yesterday, today, tomorrow }) => {
  return { yesterday, today, tomorrow };
};

module.exports = {
  serialize,
  paginatedSerialize,
  dashboardSerialize,
  serviceCountSerialize
};
