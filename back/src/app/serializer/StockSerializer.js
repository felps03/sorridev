const serialize = ({ _id, product, price, quantity, provider, dentist }) => {
  return { _id, product, price, quantity, provider, dentist };
};

const paginatedSerialize = ({ docs, total, limit, page, pages }) => {
  return {
    docs: docs.map(serialize),
    total,
    limit,
    page: Number(page),
    pages: Number(pages)
  };
};

module.exports = {
  serialize,
  paginatedSerialize
};
