const serialize = ({ _id, cash_accounting, spending, number_of_appointments }) => {
  return { _id, cash_accounting, spending, number_of_appointments };
};

const paginatedSerialize = ({ docs, total, limit, page, pages }) => {
  return {
    docs: docs.map(serialize),
    total,
    limit,
    page: Number(page),
    pages: Number(pages)
  };
};

module.exports = {
  serialize,
  paginatedSerialize
};
