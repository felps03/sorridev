const {
  utils,
  logger,
  error: { RequestError },
  enums: {
    HttpStatusCode: { OK, NO_CONTENT, CREATED }
  }
} = require('../../helpers');
const { serialize, paginatedSerialize } = require('../serializer/BalanceSerializer');
const BalanceService = require('../service/BalanceService');

const classControllerName = 'BalanceController';

class BalanceController {
  async create(req, res) {
    try {
      const balance = await BalanceService.create({
        body: req.body,
        userOwner: req.userOwner,
        userId: req.userId
      });
      return res.status(CREATED).json(serialize(balance));
    } catch (error) {
      logger.error(`${classControllerName} - create - [${utils.formatLogger(error)}]`);
      return res.status(utils.getStatusCode(error)).json(utils.formatError(error));
    }
  }

  async delete(req, res) {
    try {
      await BalanceService.delete(req.params.id);
      throw new RequestError(null, NO_CONTENT);
    } catch (error) {
      logger.error(`${classControllerName} - delete - [${utils.formatLogger(error)}]`);
      return res.status(utils.getStatusCode(error)).json(utils.formatError(error));
    }
  }

  async update(req, res) {
    try {
      const balance = await BalanceService.update({
        id: req.params.id,
        body: req.body,
        userOwner: req.userOwner,
        userId: req.userId
      });
      return res.status(OK).json(serialize(balance));
    } catch (error) {
      logger.error(`${classControllerName} - update - [${utils.formatLogger(error)}]`);
      return res.status(utils.getStatusCode(error)).json(utils.formatError(error));
    }
  }

  async find(req, res) {
    try {
      const balance = await BalanceService.find({
        id: req.params.id,
        userOwner: req.userOwner,
        userId: req.userId
      });
      return res.status(OK).json(serialize(balance));
    } catch (error) {
      logger.error(`${classControllerName} - find - [${utils.formatLogger(error)}]`);
      return res.status(utils.getStatusCode(error)).json(utils.formatError(error));
    }
  }

  async listAll(req, res) {
    try {
      const balance = await BalanceService.listAll({
        userOwner: req.userOwner,
        query: req.query,
        userId: req.userId
      });
      return res.status(OK).json(paginatedSerialize(balance));
    } catch (error) {
      logger.error(`${classControllerName} - listAll - [${utils.formatLogger(error)}]`);
      return res.status(utils.getStatusCode(error)).json(utils.formatError(error));
    }
  }
}

module.exports = new BalanceController();
