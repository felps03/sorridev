const {
  utils,
  logger,
  error: { RequestError },
  enums: {
    HttpStatusCode: { OK, NO_CONTENT, CREATED }
  }
} = require('../../helpers');
const {
  serialize,
  paginatedSerialize,
  dashboardAttendanceSerialize,
  dashboardSerialize,
  analysisGenderSerialize,
  analysisAgeSerialize
} = require('../serializer/UserSerializer');
const UserService = require('../service/UserService');

const classControllerName = 'UserController';

class UserController {
  async create(req, res) {
    try {
      const user = await UserService.create({
        body: req.body,
        userOwner: req.userOwner,
        userId: req.userId
      });
      return res.status(CREATED).json(serialize(user, req.userOwner));
    } catch (error) {
      logger.error(`${classControllerName} - create - [${utils.formatLogger(error)}]`);
      return res.status(utils.getStatusCode(error)).json(utils.formatError(error));
    }
  }

  async delete(req, res) {
    try {
      await UserService.delete(req.params.id);
      throw new RequestError(null, NO_CONTENT);
    } catch (error) {
      logger.error(`${classControllerName} - delete - [${utils.formatLogger(error)}]`);
      return res.status(utils.getStatusCode(error)).json(utils.formatError(error));
    }
  }

  async update(req, res) {
    try {
      const user = await UserService.update({
        id: req.params.id,
        body: req.body,
        userOwner: req.userOwner,
        userId: req.userId
      });
      return res.status(OK).json(serialize(user, req.userOwner));
    } catch (error) {
      logger.error(`${classControllerName} - update - [${utils.formatLogger(error)}]`);
      return res.status(utils.getStatusCode(error)).json(utils.formatError(error));
    }
  }

  async search(req, res) {
    try {
      const user = await UserService.search({
        query: req.query,
        userOwner: req.userOwner,
        userId: req.userId
      });
      return res.status(OK).json(serialize(user, req.userOwner));
    } catch (error) {
      logger.error(`${classControllerName} - find - [${utils.formatLogger(error)}]`);
      return res.status(utils.getStatusCode(error)).json(utils.formatError(error));
    }
  }

  async find(req, res) {
    try {
      const user = await UserService.find({
        id: req.params.id,
        userOwner: req.userOwner,
        userId: req.userId
      });
      return res.status(OK).json(serialize(user, req.userOwner));
    } catch (error) {
      logger.error(`${classControllerName} - find - [${utils.formatLogger(error)}]`);
      return res.status(utils.getStatusCode(error)).json(utils.formatError(error));
    }
  }

  async listAll(req, res) {
    try {
      const user = await UserService.listAll({
        userOwner: req.userOwner,
        query: req.query,
        userId: req.userId
      });
      return res.status(OK).json(paginatedSerialize(user, req.userOwner));
    } catch (error) {
      logger.error(`${classControllerName} - listAll - [${utils.formatLogger(error)}]`);
      return res.status(utils.getStatusCode(error)).json(utils.formatError(error));
    }
  }

  async dashboard(req, res) {
    try {
      const user = await UserService.dashboard({
        query: req.query,
        userOwner: req.userOwner,
        userId: req.userId
      });
      return res.status(OK).json(dashboardSerialize(user));
    } catch (error) {
      logger.error(`${classControllerName} - dashboard - [${utils.formatLogger(error)}]`);
      return res.status(utils.getStatusCode(error)).json(utils.formatError(error));
    }
  }

  async dashboardAttendance(req, res) {
    try {
      const user = await UserService.dashboardAttendance({
        query: req.query,
        userOwner: req.userOwner,
        userId: req.userId
      });
      return res.status(OK).json(dashboardAttendanceSerialize(user));
    } catch (error) {
      logger.error(`${classControllerName} - dashboardAttendance - [${utils.formatLogger(error)}]`);
      return res.status(utils.getStatusCode(error)).json(utils.formatError(error));
    }
  }

  async analysisGender(req, res) {
    try {
      const user = await UserService.analysisGender({
        query: req.query,
        userOwner: req.userOwner,
        userId: req.userId
      });

      if (user.length === 0) throw new RequestError(null, NO_CONTENT);

      return res.status(OK).json(analysisGenderSerialize(user));
    } catch (error) {
      logger.error(`${classControllerName} - analysisGender - [${utils.formatLogger(error)}]`);
      return res.status(utils.getStatusCode(error)).json(utils.formatError(error));
    }
  }

  async analysisAge(req, res) {
    try {
      const user = await UserService.analysisAge({
        query: req.query,
        userOwner: req.userOwner,
        userId: req.userId
      });

      if (user.length === 0) throw new RequestError(null, NO_CONTENT);

      return res.status(OK).json(analysisAgeSerialize(user));
    } catch (error) {
      logger.error(`${classControllerName} - analysisGender - [${utils.formatLogger(error)}]`);
      return res.status(utils.getStatusCode(error)).json(utils.formatError(error));
    }
  }
}

module.exports = new UserController();
