const {
  utils,
  logger,
  error: { RequestError },
  enums: {
    HttpStatusCode: { OK, NO_CONTENT, CREATED }
  }
} = require('../../helpers');
const { serialize, paginatedSerialize, morePatientsSerialize } = require('../serializer/ProfessionalSerializer');
const ProfessionalService = require('../service/ProfessionalService');

const classControllerName = 'ProfessionalController';

class ProfessionalController {
  async create(req, res) {
    try {
      const professional = await ProfessionalService.create(req.body);
      return res.status(CREATED).json(serialize(professional));
    } catch (error) {
      logger.error(`${classControllerName} - create - [${utils.formatLogger(error)}]`);
      return res.status(utils.getStatusCode(error)).json(utils.formatError(error));
    }
  }

  async delete(req, res) {
    try {
      await ProfessionalService.delete(req.params.id);
      throw new RequestError(null, NO_CONTENT);
    } catch (error) {
      logger.error(`${classControllerName} - delete - [${utils.formatLogger(error)}]`);
      return res.status(utils.getStatusCode(error)).json(utils.formatError(error));
    }
  }

  async update(req, res) {
    try {
      const professional = await ProfessionalService.update({
        id: req.params.id,
        body: req.body
      });
      return res.status(OK).json(serialize(professional));
    } catch (error) {
      logger.error(`${classControllerName} - update - [${utils.formatLogger(error)}]`);
      return res.status(utils.getStatusCode(error)).json(utils.formatError(error));
    }
  }

  async patch(req, res) {
    try {
      const professional = await ProfessionalService.update({
        id: req.params.id,
        body: req.body
      });
      return res.status(OK).json(serialize(professional));
    } catch (error) {
      logger.error(`${classControllerName} - patch - [${utils.formatLogger(error)}]`);
      return res.status(utils.getStatusCode(error)).json(utils.formatError(error));
    }
  }

  async find(req, res) {
    try {
      const professional = await ProfessionalService.find({
        id: req.params.id,
        userOwner: req.userOwner,
        userId: req.userId
      });
      return res.status(OK).json(serialize(professional));
    } catch (error) {
      logger.error(`${classControllerName} - find - [${utils.formatLogger(error)}]`);
      return res.status(utils.getStatusCode(error)).json(utils.formatError(error));
    }
  }

  async listAll(req, res) {
    try {
      const professional = await ProfessionalService.listAll({
        query: req.query
      });
      return res.status(OK).json(paginatedSerialize(professional));
    } catch (error) {
      logger.error(`${classControllerName} - listAll - [${utils.formatLogger(error)}]`);
      return res.status(utils.getStatusCode(error)).json(utils.formatError(error));
    }
  }

  async professionalWithMorePatients(req, res) {
    try {
      const professional = await ProfessionalService.professionalWithMorePatients({
        query: req.query
      });
      return res.status(OK).json(morePatientsSerialize(professional));
    } catch (error) {
      logger.error(`${classControllerName} - professionalWithMorePatients - [${utils.formatLogger(error)}]`);
      return res.status(utils.getStatusCode(error)).json(utils.formatError(error));
    }
  }
}

module.exports = new ProfessionalController();
