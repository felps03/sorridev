const {
  utils,
  logger,
  enums: {
    HttpStatusCode: { OK }
  }
} = require('../../helpers');
const AuthService = require('../service/AuthService');

const classControllerName = 'AuthController';

class AuthController {
  async login(req, res) {
    try {
      const { token, body } = await AuthService.login(req.body);
      return res
        .status(OK)
        .set('Token', token)
        .set('Access-Control-Expose-Headers', 'Token')
        .send(body);
    } catch (error) {
      logger.error(`${classControllerName} - login - [${utils.formatLogger(error)}]`);
      return res.status(utils.getStatusCode(error)).json(utils.formatError(error));
    }
  }

  async google(req, res) {
    try {
      const { token, body } = await AuthService.googleAuth(req.body);
      return res
        .status(OK)
        .set('Token', token)
        .set('Access-Control-Expose-Headers', 'Token')
        .send(body);
    } catch (error) {
      logger.error(`${classControllerName} - google - [${utils.formatLogger(error)}]`);
      return res.status(utils.getStatusCode(error)).json(utils.formatError(error));
    }
  }

  async resetPassword(req, res) {
    try {
      const data = await AuthService.resetPassword({
        body: req.body,
        userOwner: req.userOwner,
        userId: req.userId
      });
      return res.status(OK).send(data);
    } catch (error) {
      logger.error(`${classControllerName} - google - [${utils.formatLogger(error)}]`);
      return res.status(utils.getStatusCode(error)).json(utils.formatError(error));
    }
  }
}

module.exports = new AuthController();
