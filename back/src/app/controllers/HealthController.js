const {
  HttpStatusCode: { OK }
} = require('../../helpers/enums');

class HealthController {
  async index(req, res) {
    return res
      .status(OK)
      .json({ message: 'Welcome to Sorri Dev' })
      .end();
  }
}

module.exports = new HealthController();
