const {
  utils,
  logger,
  error: { RequestError },
  enums: {
    HttpStatusCode: { OK, NO_CONTENT, CREATED }
  }
} = require('../../helpers');
const { serialize, paginatedSerialize } = require('../serializer/EmployeeSerializer');
const EmployeeService = require('../service/EmployeeService');

const classControllerName = 'EmployeeController';

class EmployeeController {
  async create(req, res) {
    try {
      const employee = await EmployeeService.create({
        body: req.body,
        userOwner: req.userOwner,
        userId: req.userId
      });
      return res.status(CREATED).json(serialize(employee));
    } catch (error) {
      logger.error(`${classControllerName} - create - [${utils.formatLogger(error)}]`);
      return res.status(utils.getStatusCode(error)).json(utils.formatError(error));
    }
  }

  async delete(req, res) {
    try {
      await EmployeeService.delete(req.params.id);
      throw new RequestError(null, NO_CONTENT);
    } catch (error) {
      logger.error(`${classControllerName} - delete - [${utils.formatLogger(error)}]`);
      return res.status(utils.getStatusCode(error)).json(utils.formatError(error));
    }
  }

  async update(req, res) {
    try {
      const employee = await EmployeeService.update({
        id: req.params.id,
        body: req.body,
        userOwner: req.userOwner,
        userId: req.userId
      });
      return res.status(OK).json(serialize(employee));
    } catch (error) {
      logger.error(`${classControllerName} - update - [${utils.formatLogger(error)}]`);
      return res.status(utils.getStatusCode(error)).json(utils.formatError(error));
    }
  }

  async find(req, res) {
    try {
      const employee = await EmployeeService.find({
        id: req.params.id,
        userOwner: req.userOwner,
        userId: req.userId
      });
      return res.status(OK).json(serialize(employee));
    } catch (error) {
      logger.error(`${classControllerName} - find - [${utils.formatLogger(error)}]`);
      return res.status(utils.getStatusCode(error)).json(utils.formatError(error));
    }
  }

  async listAll(req, res) {
    try {
      const employee = await EmployeeService.listAll({
        userOwner: req.userOwner,
        query: req.query,
        userId: req.userId
      });
      return res.status(OK).json(paginatedSerialize(employee));
    } catch (error) {
      logger.error(`${classControllerName} - listAll - [${utils.formatLogger(error)}]`);
      return res.status(utils.getStatusCode(error)).json(utils.formatError(error));
    }
  }
}

module.exports = new EmployeeController();
