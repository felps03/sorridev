const {
  utils,
  logger,
  error: { RequestError },
  enums: {
    HttpStatusCode: { OK, NO_CONTENT, CREATED }
  }
} = require('../../helpers');
const {
  serialize,
  paginatedSerialize,
  dashboardSerialize,
  serviceCountSerialize
} = require('../serializer/ScheduleSerializer');
const ScheduleService = require('../service/ScheduleService');

const classControllerName = 'ScheduleController';

class ScheduleController {
  async create(req, res) {
    try {
      const schedule = await ScheduleService.create({
        body: req.body,
        userOwner: req.userOwner,
        userId: req.userId
      });
      return res.status(CREATED).json(serialize(schedule));
    } catch (error) {
      logger.error(`${classControllerName} - create - [${utils.formatLogger(error)}]`);
      return res.status(utils.getStatusCode(error)).json(utils.formatError(error));
    }
  }

  async delete(req, res) {
    try {
      await ScheduleService.delete(req.params.id);
      throw new RequestError(null, NO_CONTENT);
    } catch (error) {
      logger.error(`${classControllerName} - delete - [${utils.formatLogger(error)}]`);
      return res.status(utils.getStatusCode(error)).json(utils.formatError(error));
    }
  }

  async update(req, res) {
    try {
      const schedule = await ScheduleService.update({
        id: req.params.id,
        body: req.body,
        userOwner: req.userOwner,
        userId: req.userId
      });
      return res.status(OK).json(serialize(schedule));
    } catch (error) {
      logger.error(`${classControllerName} - update - [${utils.formatLogger(error)}]`);
      return res.status(utils.getStatusCode(error)).json(utils.formatError(error));
    }
  }

  async find(req, res) {
    try {
      const schedule = await ScheduleService.find({
        id: req.params.id,
        userOwner: req.userOwner,
        userId: req.userId
      });
      return res.status(OK).json(serialize(schedule));
    } catch (error) {
      logger.error(`${classControllerName} - find - [${utils.formatLogger(error)}]`);
      return res.status(utils.getStatusCode(error)).json(utils.formatError(error));
    }
  }

  async listAll(req, res) {
    try {
      const schedule = await ScheduleService.listAll({
        userOwner: req.userOwner,
        query: req.query,
        userId: req.userId
      });
      return res.status(OK).json(paginatedSerialize(schedule));
    } catch (error) {
      logger.error(`${classControllerName} - listAll - [${utils.formatLogger(error)}]`);
      return res.status(utils.getStatusCode(error)).json(utils.formatError(error));
    }
  }

  async search(req, res) {
    try {
      const schedule = await ScheduleService.search({
        query: req.query,
        userOwner: req.userOwner,
        userId: req.userId
      });
      return res.status(OK).json(paginatedSerialize(schedule));
    } catch (error) {
      logger.error(`${classControllerName} - search - [${utils.formatLogger(error)}]`);
      return res.status(utils.getStatusCode(error)).json(utils.formatError(error));
    }
  }

  async serviceCount(req, res) {
    try {
      const schedule = await ScheduleService.serviceCount({
        query: req.query,
        userOwner: req.userOwner,
        userId: req.userId
      });
      return res.status(OK).json(serviceCountSerialize(schedule));
    } catch (error) {
      logger.error(`${classControllerName} - serviceCount - [${utils.formatLogger(error)}]`);
      return res.status(utils.getStatusCode(error)).json(utils.formatError(error));
    }
  }

  async dashboard(req, res) {
    try {
      const schedule = await ScheduleService.dashboard({
        query: req.query,
        userOwner: req.userOwner,
        userId: req.userId
      });
      return res.status(OK).json(dashboardSerialize(schedule));
    } catch (error) {
      logger.error(`${classControllerName} - dashboard - [${utils.formatLogger(error)}]`);
      return res.status(utils.getStatusCode(error)).json(utils.formatError(error));
    }
  }
}

module.exports = new ScheduleController();
