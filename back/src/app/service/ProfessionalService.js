const {
  error: { RequestError },
  enums: {
    HttpStatusCode: { NOT_FOUND, NO_CONTENT, CONFLICT },
    CustomErrorMessages: { DELETE_PROFESSIONAL_NOT_FOUND, UPDATE_PROFESSIONAL_NOT_FOUND, MONGODB_CONFLICT }
  }
} = require('../../helpers');
const { ProfessionalRepository, UserRepository } = require('../repository');

class ProfessionalService {
  async create(payload) {
    try {
      return await ProfessionalRepository.create(payload);
    } catch (error) {
      if (error.code === 11000)
        throw new RequestError(MONGODB_CONFLICT, CONFLICT, JSON.stringify({ email: payload.email, cpf: payload.cpf }));
      throw error;
    }
  }

  async update(payload) {
    const { id, body } = payload;
    try {
      const professional = await ProfessionalRepository.update(id, body);
      if (!professional) throw new RequestError(UPDATE_PROFESSIONAL_NOT_FOUND, NOT_FOUND);
      return professional;
    } catch (error) {
      throw error;
    }
  }

  async delete(id) {
    try {
      const professional = await ProfessionalRepository.delete(id);
      if (!professional) throw new RequestError(DELETE_PROFESSIONAL_NOT_FOUND, NOT_FOUND);
      return professional;
    } catch (error) {
      throw error;
    }
  }

  async find(payload) {
    const { id } = payload;
    try {
      const professional = await ProfessionalRepository.get(id);
      if (!professional) throw new RequestError(null, NO_CONTENT);
      return professional;
    } catch (error) {
      throw error;
    }
  }

  async listAll(payload) {
    const { query } = payload;
    try {
      const professional = await ProfessionalRepository.getAll(query);
      if (professional.total === 0) throw new RequestError(null, NO_CONTENT);
      return professional;
    } catch (error) {
      throw error;
    }
  }

  async professionalWithMorePatients(payload) {
    const {
      query: { limit }
    } = payload;
    try {
      const professional = await UserRepository.professionalWithMorePatients(limit);
      if (professional.total === 0) throw new RequestError(null, NO_CONTENT);
      return professional;
    } catch (error) {
      throw error;
    }
  }
}

module.exports = new ProfessionalService();
