const bcrypt = require('bcryptjs');

const {
  utils,
  error: { RequestError },
  enums: {
    HttpStatusCode: { UNAUTHORIZED, NOT_FOUND },
    CustomErrorMessages: { USER_NOT_FOUND, PASSWORD_NOT_MATCH }
  }
} = require('../../helpers');
const { AdminRepository, UserRepository, EmployeeRepository, ProfessionalRepository } = require('../repository');

class AuthService {
  async login(payload) {
    try {
      const { email, password } = payload;
      const userExists = await Promise.all([
        AdminRepository.emailExistsGetPassword(email),
        UserRepository.emailExistsGetPassword(email),
        EmployeeRepository.emailExistsGetPassword(email),
        ProfessionalRepository.emailExistsGetPassword(email)
      ]);

      if (userExists.some(el => !!el)) {
        const [user] = userExists.filter(el => el != null);
        if (!(await bcrypt.compare(password, user.password))) throw new RequestError(PASSWORD_NOT_MATCH, UNAUTHORIZED);

        const { _id, name, owner, dentist } = user;

        const newPayload = {
          accessedAt: utils.mongoBrazilianTimezone(),
          accountStatus: 'ACTIVE'
        };

        if (owner === 'USER') await UserRepository.update(_id, newPayload);
        if (owner === 'EMPLOYEE') await EmployeeRepository.update(_id, newPayload);
        if (owner === 'DENTIST') await ProfessionalRepository.update(_id, newPayload);

        const payloadBody = {
          _id,
          name,
          email
        };

        if (owner === 'EMPLOYEE') payloadBody.dentist = dentist;

        return {
          token: utils.generateToken(user),
          body: payloadBody
        };
      }
      throw new RequestError(USER_NOT_FOUND, NOT_FOUND);
    } catch (error) {
      throw error;
    }
  }

  async resetPassword(payload) {
    const { body, userId } = payload;

    try {
      const newPassword = await bcrypt.hash(body.password, Number(process.env.PASSWORD_SALT));

      const userExists = await Promise.all([
        AdminRepository.get(userId),
        UserRepository.get(userId),
        EmployeeRepository.get(userId),
        ProfessionalRepository.get(userId)
      ]);

      if (userExists.some(el => !!el)) {
        const [{ email, owner }] = userExists.filter(el => el != null);

        if (owner === 'ADMIN') {
          const { _id, password } = await AdminRepository.emailExistsGetPassword(email);

          if (!(await bcrypt.compare(body.currentPassword, password)))
            throw new RequestError(PASSWORD_NOT_MATCH, UNAUTHORIZED);

          const admin = await AdminRepository.update(_id, { password: newPassword });

          return admin;
        }

        if (owner === 'DENTIST') {
          const { _id, password } = await ProfessionalRepository.emailExistsGetPassword(email);

          if (!(await bcrypt.compare(body.currentPassword, password)))
            throw new RequestError(PASSWORD_NOT_MATCH, UNAUTHORIZED);

          const dentist = await ProfessionalRepository.update(_id, { password: newPassword });

          return dentist;
        }

        if (owner === 'EMPLOYEE') {
          const { _id, password } = await EmployeeRepository.emailExistsGetPassword(email);

          if (!(await bcrypt.compare(body.currentPassword, password)))
            throw new RequestError(PASSWORD_NOT_MATCH, UNAUTHORIZED);

          const employee = await EmployeeRepository.update(_id, { password: newPassword });

          return employee;
        }

        if (owner === 'USER') {
          const { _id, password } = await UserRepository.emailExistsGetPassword(email);

          if (!(await bcrypt.compare(body.currentPassword, password)))
            throw new RequestError(PASSWORD_NOT_MATCH, UNAUTHORIZED);

          const user = await UserRepository.update(_id, { password: newPassword });

          return user;
        }
      }
      throw new RequestError(USER_NOT_FOUND, NOT_FOUND);
    } catch (error) {
      throw error;
    }
  }

  async googleAuth(payload) {
    const { name, email, photo, provider } = payload;
    try {
      const userExists = await UserRepository.emailExistsGetPassword(email);
      if (userExists) {
        const { _id } = userExists;
        return {
          token: utils.generateToken(userExists),
          body: {
            _id,
            name,
            email
          }
        };
      }
      const newUser = await UserRepository.create({
        name,
        email,
        provider,
        photo
      });
      return {
        token: utils.generateToken(newUser),
        body: {
          _id: newUser._id,
          name: newUser.name,
          email: newUser.email
        }
      };
    } catch (error) {
      throw error;
    }
  }
}

module.exports = new AuthService();
