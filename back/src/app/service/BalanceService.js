const {
  error: { RequestError },
  enums: {
    HttpStatusCode: { NOT_FOUND, NO_CONTENT },
    CustomErrorMessages: { DELETE_BALANCE_NOT_FOUND, UPDATE_BALANCE_NOT_FOUND }
  }
} = require('../../helpers');
const { BalanceRepository } = require('../repository');
const ConfigurationService = require('./configuration/ConfigurationService');

class BalanceService {
  async create(payload) {
    const { body, userOwner, userId } = payload;
    try {
      body.dentist = await ConfigurationService.getIDDentist(body, userOwner, userId);
      return await BalanceRepository.create(body);
    } catch (error) {
      throw error;
    }
  }

  async update(payload) {
    const { id, body, userOwner, userId } = payload;
    try {
      body.dentist = await ConfigurationService.getIDDentist(body, userOwner, userId);
      const balance = await BalanceRepository.update(id, body);
      if (!balance) throw new RequestError(UPDATE_BALANCE_NOT_FOUND, NOT_FOUND);
      return balance;
    } catch (error) {
      throw error;
    }
  }

  async delete(id) {
    try {
      const balance = await BalanceRepository.delete(id);
      if (!balance) throw new RequestError(DELETE_BALANCE_NOT_FOUND, NOT_FOUND);
      return balance;
    } catch (error) {
      throw error;
    }
  }

  async find(payload) {
    const { id, userOwner, userId } = payload;
    try {
      const balance = await BalanceRepository.get(id);
      if (balance) {
        await ConfigurationService.authorized(balance.dentist._id, userOwner, userId);
      }
      if (!balance) throw new RequestError(null, NO_CONTENT);
      return balance;
    } catch (error) {
      throw error;
    }
  }

  async listAll(payload) {
    const { query, userOwner, userId } = payload;
    try {
      query.dentist = await ConfigurationService.getIDDentist(query, userOwner, userId);
      const balance = await BalanceRepository.getAll(query);
      if (balance.total === 0) throw new RequestError(null, NO_CONTENT);
      return balance;
    } catch (error) {
      throw error;
    }
  }
}

module.exports = new BalanceService();
