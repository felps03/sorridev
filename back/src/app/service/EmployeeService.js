const {
  error: { RequestError },
  enums: {
    HttpStatusCode: { NOT_FOUND, NO_CONTENT },
    CustomErrorMessages: { UPDATE_EMPLOYEE_NOT_FOUND, DELETE_EMPLOYEE_NOT_FOUND }
  }
} = require('../../helpers');
const { EmployeeRepository } = require('../repository');
const ConfigurationService = require('./configuration/ConfigurationService');

class EmployeeService {
  async create(payload) {
    const { body, userOwner, userId } = payload;
    try {
      body.dentist = await ConfigurationService.getIDDentist(body, userOwner, userId);
      return await EmployeeRepository.create(body);
    } catch (error) {
      throw error;
    }
  }

  async update(payload) {
    const { id, body, userOwner, userId } = payload;
    try {
      body.dentist = await ConfigurationService.getIDDentist(body, userOwner, userId);
      const employee = await EmployeeRepository.update(id, body);
      if (!employee) throw new RequestError(UPDATE_EMPLOYEE_NOT_FOUND, NOT_FOUND);
      return employee;
    } catch (error) {
      throw error;
    }
  }

  async delete(id) {
    try {
      const employee = await EmployeeRepository.delete(id);
      if (!employee) throw new RequestError(DELETE_EMPLOYEE_NOT_FOUND, NOT_FOUND);
      return employee;
    } catch (error) {
      throw error;
    }
  }

  async find(payload) {
    const { id, userOwner, userId } = payload;
    try {
      const employee = await EmployeeRepository.get(id);
      if (employee) {
        await ConfigurationService.authorized(employee.dentist._id, userOwner, userId);
      }
      if (!employee) throw new RequestError(null, NO_CONTENT);
      return employee;
    } catch (error) {
      throw error;
    }
  }

  async listAll(payload) {
    const { query, userOwner, userId } = payload;
    try {
      query.dentist = await ConfigurationService.getIDDentist(query, userOwner, userId);
      const employee = await EmployeeRepository.getAll(query);
      if (employee.total === 0) throw new RequestError(null, NO_CONTENT);
      return employee;
    } catch (error) {
      throw error;
    }
  }
}

module.exports = new EmployeeService();
