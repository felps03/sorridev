const moment = require('moment');

const {
  error: { RequestError },
  enums: {
    HttpStatusCode: { NOT_FOUND, NO_CONTENT, CONFLICT },
    CustomErrorMessages: { CREATE_SCHEDULE_EXISTENT, DELETE_SCHEDULE_NOT_FOUND, UPDATE_SCHEDULE_NOT_FOUND }
  }
} = require('../../helpers');
const { getOnlyFormattedTime } = require('../../helpers/utils');
const { ScheduleRepository } = require('../repository');
const ConfigurationService = require('./configuration/ConfigurationService');

class ScheduleService {
  async create(payload) {
    const { body, userOwner, userId } = payload;

    try {
      body.dentist = await ConfigurationService.getIDDentist(body, userOwner, userId);

      const getAppointment = await ScheduleRepository.search(body);
      if (getAppointment.total === 0) {
        const { _id } = await ScheduleRepository.create(body);
        const appointment = await ScheduleRepository.get(_id);

        return appointment;
      }

      await this.checkSchedule(getAppointment, body);

      const { _id } = await ScheduleRepository.create(body);

      const appointment = await ScheduleRepository.get(_id);

      return appointment;
    } catch (error) {
      throw error;
    }
  }

  async update(payload) {
    const { id, body, userOwner, userId } = payload;
    try {
      body.dentist = await ConfigurationService.getIDDentist(body, userOwner, userId);

      const getAppointment = await ScheduleRepository.search(body);
      if (getAppointment.total === 0) {
        return await ScheduleRepository.create(body);
      }

      await this.checkSchedule(getAppointment, body, id);

      const schedule = await ScheduleRepository.update(id, body);
      if (!schedule) throw new RequestError(UPDATE_SCHEDULE_NOT_FOUND, NOT_FOUND);
      return schedule;
    } catch (error) {
      throw error;
    }
  }

  async delete(id) {
    try {
      const schedule = await ScheduleRepository.delete(id);
      if (!schedule) throw new RequestError(DELETE_SCHEDULE_NOT_FOUND, NOT_FOUND);
      return schedule;
    } catch (error) {
      throw error;
    }
  }

  async find(payload) {
    const { id, userOwner, userId } = payload;
    try {
      const schedule = await ScheduleRepository.get(id);
      if (schedule) {
        await ConfigurationService.authorized(schedule.dentist._id, userOwner, userId);
      }
      if (!schedule) throw new RequestError(null, NO_CONTENT);
      return schedule;
    } catch (error) {
      throw error;
    }
  }

  async listAll(payload) {
    const { query, userOwner, userId } = payload;
    try {
      query.dentist = await ConfigurationService.getIDDentist(query, userOwner, userId);

      if (userOwner === 'USER' && !query.dentist) {
        delete query.dentist;
        query.client = userId;
      }

      const schedule = await ScheduleRepository.getAll(query);
      if (schedule.total === 0) throw new RequestError(null, NO_CONTENT);
      return schedule;
    } catch (error) {
      throw error;
    }
  }

  async search(payload) {
    const { query, userOwner, userId } = payload;
    try {
      query.dentist = await ConfigurationService.getIDDentist(query, userOwner, userId);

      if (!query.startTime) query.startTime = moment();

      const schedule = await ScheduleRepository.search(query);
      if (schedule.total === 0) throw new RequestError(null, NO_CONTENT);
      return schedule;
    } catch (error) {
      throw error;
    }
  }

  async serviceCount(payload) {
    const { query, userOwner, userId } = payload;
    try {
      query.dentist = await ConfigurationService.getIDDentist(query, userOwner, userId);
      let yesterdayCount = 0;
      let todayCount = 0;
      let tomorrowCount = 0;

      if (!query.day) {
        query.day = moment()
          .utc()
          .toDate();
      }

      const now = query.day;

      todayCount = await ScheduleRepository.serviceCount(query);

      query.day = moment(now)
        .utc()
        .add(1, 'days');

      tomorrowCount = await ScheduleRepository.serviceCount(query);

      query.day = moment(now)
        .utc()
        .subtract(1, 'days');

      query.status = 'compareceu';
      yesterdayCount = await ScheduleRepository.serviceCount(query);

      return {
        yesterday: yesterdayCount,
        today: todayCount,
        tomorrow: tomorrowCount
      };
    } catch (error) {
      throw error;
    }
  }

  async checkSchedule(appointments, { startTime, endTime }, options) {
    const newBookingStart = getOnlyFormattedTime(startTime);
    const newBookingEnd = getOnlyFormattedTime(endTime);

    const bookings = appointments.docs.find(booked => {
      const bookedStart = getOnlyFormattedTime(booked.startTime);
      const bookedEnd = getOnlyFormattedTime(booked.endTime);

      if (newBookingStart.isSameOrAfter(bookedStart) && newBookingStart.isSameOrBefore(bookedEnd)) {
        if (String(booked._id) !== options) {
          return true;
        }
      }
      if (newBookingStart.isSameOrBefore(bookedEnd)) {
        if (newBookingEnd.isSameOrAfter(bookedStart)) {
          if (String(booked._id) !== options) {
            return true;
          }
        }
      }

      return false;
    });

    if (bookings) throw new RequestError(CREATE_SCHEDULE_EXISTENT, CONFLICT);

    return bookings;
  }

  async dashboard(payload) {
    const { query, userOwner, userId } = payload;
    try {
      const dentist = await ConfigurationService.getIDDentist(query, userOwner, userId);

      const schedule = await ScheduleRepository.dashboard({
        status: query.status || 'compareceu',
        TODAY: moment()
          .utc()
          .toDate(),
        YEAR_BEFORE: moment()
          .subtract(1, 'years')
          .utc()
          .toDate(),
        dentist
      });

      if (schedule.length === 0) throw new RequestError(null, NO_CONTENT);

      return schedule;
    } catch (error) {
      throw error;
    }
  }
}

module.exports = new ScheduleService();
