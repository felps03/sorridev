const {
  error: { RequestError },
  enums: {
    HttpStatusCode: { NOT_FOUND, NO_CONTENT },
    CustomErrorMessages: { DELETE_SERVICE_NOT_FOUND, UPDATE_SERVICE_NOT_FOUND }
  }
} = require('../../helpers');
const { ServiceRepository } = require('../repository');
const ConfigurationService = require('./configuration/ConfigurationService');

class ServiceService {
  async create(payload) {
    const { body, userOwner, userId } = payload;
    try {
      body.dentist = await ConfigurationService.getIDDentist(body, userOwner, userId);
      return await ServiceRepository.create(body);
    } catch (error) {
      throw error;
    }
  }

  async update(payload) {
    const { id, body, userOwner, userId } = payload;
    try {
      body.dentist = await ConfigurationService.getIDDentist(body, userOwner, userId);
      const service = await ServiceRepository.update(id, body);
      if (!service) throw new RequestError(UPDATE_SERVICE_NOT_FOUND, NOT_FOUND);
      return service;
    } catch (error) {
      throw error;
    }
  }

  async delete(id) {
    try {
      const service = await ServiceRepository.delete(id);
      if (!service) throw new RequestError(DELETE_SERVICE_NOT_FOUND, NOT_FOUND);
      return service;
    } catch (error) {
      throw error;
    }
  }

  async find(payload) {
    const { id, userOwner, userId } = payload;
    try {
      const service = await ServiceRepository.get(id);
      if (!service) throw new RequestError(null, NO_CONTENT);
      if (service) {
        await ConfigurationService.authorized(service.dentist._id, userOwner, userId);
      }
      return service;
    } catch (error) {
      throw error;
    }
  }

  async listAll(payload) {
    const { query, userOwner, userId } = payload;
    try {
      query.dentist = await ConfigurationService.getIDDentist(query, userOwner, userId);
      const service = await ServiceRepository.getAll(query);
      if (service.total === 0) throw new RequestError(null, NO_CONTENT);
      return service;
    } catch (error) {
      throw error;
    }
  }
}

module.exports = new ServiceService();
