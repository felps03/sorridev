const mongoose = require('mongoose');

const {
  error: { RequestError },
  enums: {
    HttpStatusCode: { UNAUTHORIZED, NOT_FOUND },
    CustomErrorMessages: { UNAUTHORIZED_USER_NOT_FOUND, PROFESSIONAL_NOT_EXIST }
  }
} = require('../../../helpers');
const { EmployeeRepository, ProfessionalRepository } = require('../../repository');

class ConfigurationService {
  async authorized(dentist, userOwner, userId, options) {
    let ObjectIdDentist = '';
    if (typeof dentist === 'string') {
      ObjectIdDentist = mongoose.Types.ObjectId(dentist);
    } else {
      ObjectIdDentist = dentist;
    }

    if (userOwner === 'DENTIST') {
      if (!options && !ObjectIdDentist.equals(userId)) {
        throw new RequestError(UNAUTHORIZED_USER_NOT_FOUND, UNAUTHORIZED);
      }
      if (options && !dentist.find(professional => professional._id.equals(userId))) {
        throw new RequestError(UNAUTHORIZED_USER_NOT_FOUND, UNAUTHORIZED);
      }
    }

    if (userOwner === 'EMPLOYEE') {
      const employee = await EmployeeRepository.get(userId);
      if (!employee.dentist._id.equals(ObjectIdDentist)) {
        throw new RequestError(UNAUTHORIZED_USER_NOT_FOUND, UNAUTHORIZED);
      }
    }
  }

  async getIDDentist(query, userOwner, userId) {
    if (userOwner === 'DENTIST') return userId;
    if (userOwner === 'EMPLOYEE') {
      const {
        dentist: { _id }
      } = await EmployeeRepository.get(userId);
      return String(_id);
    }

    if (query.dentist && typeof query.dentist !== 'string') {
      await Promise.all(
        query.dentist.map(async dentist => {
          if (!(await ProfessionalRepository.get(dentist))) {
            throw new RequestError(PROFESSIONAL_NOT_EXIST, NOT_FOUND, dentist);
          }
        })
      );
    } else if (query.dentist && !(await ProfessionalRepository.get(query.dentist))) {
      throw new RequestError(PROFESSIONAL_NOT_EXIST, NOT_FOUND, query.dentist);
    }

    return query.dentist;
  }
}

module.exports = new ConfigurationService();
