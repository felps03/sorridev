const {
  dataUtils,
  error: { RequestError },
  enums: {
    HttpStatusCode: { CONFLICT, NOT_FOUND, NO_CONTENT },
    CustomErrorMessages: { DELETE_USER_NOT_FOUND, MONGODB_CONFLICT, UPDATE_USER_NOT_FOUND, PROFESSIONAL_NOT_EXIST }
  }
} = require('../../helpers');
const { UserRepository, ProfessionalRepository, ScheduleRepository } = require('../repository');
const ConfigurationService = require('./configuration/ConfigurationService');

class UserService {
  async create(payload) {
    const { body } = payload;
    try {
      if (body.dentist && !(await ProfessionalRepository.get(body.dentist)))
        throw new RequestError(PROFESSIONAL_NOT_EXIST, NOT_FOUND, body.dentist);

      return await UserRepository.create(body);
    } catch (error) {
      if (error.code === 11000)
        throw new RequestError(MONGODB_CONFLICT, CONFLICT, JSON.stringify({ email: payload.email, cpf: payload.cpf }));
      throw error;
    }
  }

  async update(payload) {
    const { id, body, userOwner, userId } = payload;
    try {
      body.dentist = await ConfigurationService.getIDDentist(body, userOwner, userId);
      const user = await UserRepository.update(id, body);
      if (!user) throw new RequestError(UPDATE_USER_NOT_FOUND, NOT_FOUND);
      return user;
    } catch (error) {
      throw error;
    }
  }

  async delete(id) {
    try {
      const user = await UserRepository.delete(id);
      if (!user) throw new RequestError(DELETE_USER_NOT_FOUND, NOT_FOUND);
      return user;
    } catch (error) {
      throw error;
    }
  }

  async search(payload) {
    const { query } = payload;
    try {
      const user = await UserRepository.search(query);
      if (!user) throw new RequestError(null, NO_CONTENT);
      return user;
    } catch (error) {
      throw error;
    }
  }

  async find(payload) {
    const { id, userOwner, userId } = payload;
    try {
      const user = await UserRepository.get(id);
      if (user) {
        await ConfigurationService.authorized(user.dentist, userOwner, userId, true);
      }
      if (!user) throw new RequestError(null, NO_CONTENT);
      return user;
    } catch (error) {
      throw error;
    }
  }

  async listAll(payload) {
    const { query, userOwner, userId } = payload;
    try {
      query.dentist = await ConfigurationService.getIDDentist(query, userOwner, userId);
      if (query.dentist === undefined) delete query.dentist;
      const user = await UserRepository.getAll(query);
      if (user.total === 0) throw new RequestError(null, NO_CONTENT);
      return user;
    } catch (error) {
      throw error;
    }
  }

  async dashboard(payload) {
    const { query, userOwner, userId } = payload;
    try {
      const dentist = await ConfigurationService.getIDDentist(query, userOwner, userId);

      const schedule = await UserRepository.dashboard({
        TODAY: dataUtils.getToday(),
        YEAR_BEFORE: dataUtils.getLastYear(),
        dentist
      });

      if (schedule.length === 0) throw new RequestError(null, NO_CONTENT);

      return schedule;
    } catch (error) {
      throw error;
    }
  }

  async dashboardAttendance(payload) {
    const { query, userOwner, userId } = payload;

    try {
      const dentist = await ConfigurationService.getIDDentist(query, userOwner, userId);

      const newUserThisMonth = await UserRepository.newUser({
        startOfMonth: dataUtils.getStartOfTheMonth(),
        endOfMonth: dataUtils.getEndOfTheMonth(),
        dentist
      });

      const newUserLastMonth = await UserRepository.newUser({
        startOfMonth: dataUtils.getFirstDayOfLastMonth(),
        endOfMonth: dataUtils.getLastDayOfLastMonth(),
        dentist
      });

      const attendanceThisMonth = await ScheduleRepository.attendance({
        startOfMonth: dataUtils.getStartOfTheMonth(),
        endOfMonth: dataUtils.getToday(),
        dentist
      });

      const attendanceLastMonth = await ScheduleRepository.attendance({
        startOfMonth: dataUtils.getFirstDayOfLastMonth(),
        endOfMonth: dataUtils.getLastDayOfLastMonth(),
        dentist
      });

      const absenceThisMonth = await ScheduleRepository.absence({
        startOfMonth: dataUtils.getStartOfTheMonth(),
        endOfMonth: dataUtils.getToday(),
        dentist
      });

      const absenceLastMonth = await ScheduleRepository.absence({
        startOfMonth: dataUtils.getFirstDayOfLastMonth(),
        endOfMonth: dataUtils.getLastDayOfLastMonth(),
        dentist
      });

      return {
        newUserThisMonth,
        newUserLastMonth,
        attendanceThisMonth,
        attendanceLastMonth,
        absenceThisMonth,
        absenceLastMonth
      };
    } catch (error) {
      throw error;
    }
  }

  async analysisGender(payload) {
    const { query, userOwner, userId } = payload;
    try {
      const dentist = await ConfigurationService.getIDDentist(query, userOwner, userId);
      if (!dentist) throw new RequestError(PROFESSIONAL_NOT_EXIST, NOT_FOUND, dentist);

      const user = await UserRepository.analysisGender({ dentist });

      return user;
    } catch (error) {
      throw error;
    }
  }

  async analysisAge(payload) {
    const { query, userOwner, userId } = payload;
    try {
      const dentist = await ConfigurationService.getIDDentist(query, userOwner, userId);
      if (!dentist) throw new RequestError(PROFESSIONAL_NOT_EXIST, NOT_FOUND, dentist);

      const user = await UserRepository.analysisAge({ dentist });

      return user;
    } catch (error) {
      throw error;
    }
  }
}

module.exports = new UserService();
