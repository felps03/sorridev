const {
  error: { RequestError },
  enums: {
    HttpStatusCode: { NOT_FOUND, NO_CONTENT },
    CustomErrorMessages: { DELETE_STOCK_NOT_FOUND, UPDATE_STOCK_NOT_FOUND }
  }
} = require('../../helpers');
const { StockRepository } = require('../repository');
const ConfigurationService = require('./configuration/ConfigurationService');

class StockService {
  async create(payload) {
    const { body, userOwner, userId } = payload;
    try {
      body.dentist = await ConfigurationService.getIDDentist(body, userOwner, userId);
      return await StockRepository.create(body);
    } catch (error) {
      throw error;
    }
  }

  async update(payload) {
    const { id, body, userOwner, userId } = payload;
    try {
      await ConfigurationService.authorized(body.dentist, userOwner, userId);
      const stock = await StockRepository.update(id, body);
      if (!stock) throw new RequestError(UPDATE_STOCK_NOT_FOUND, NOT_FOUND);
      return stock;
    } catch (error) {
      throw error;
    }
  }

  async delete(id) {
    try {
      const stock = await StockRepository.delete(id);
      if (!stock) throw new RequestError(DELETE_STOCK_NOT_FOUND, NOT_FOUND);
      return stock;
    } catch (error) {
      throw error;
    }
  }

  async find(payload) {
    const { id, userOwner, userId } = payload;
    try {
      const stock = await StockRepository.get(id);
      if (stock) {
        await ConfigurationService.authorized(stock.dentist._id, userOwner, userId);
      }
      if (!stock) throw new RequestError(null, NO_CONTENT);
      return stock;
    } catch (error) {
      throw error;
    }
  }

  async listAll(payload) {
    const { query, userOwner, userId } = payload;
    try {
      query.dentist = await ConfigurationService.getIDDentist(query, userOwner, userId);
      const stock = await StockRepository.getAll(query);
      if (stock.total === 0) throw new RequestError(null, NO_CONTENT);
      return stock;
    } catch (error) {
      throw error;
    }
  }
}

module.exports = new StockService();
