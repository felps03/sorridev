const Joi = require('joi');

const {
  logger,
  utils,
  error: { ValidationError },
  enums: {
    CustomErrorMessages: { VALIDATION_ID }
  },
  regularExpressions: { letterAndNumberRegex }
} = require('../../../helpers');

/* eslint no-return-assign: "error" */
module.exports = async (req, res, next) => {
  try {
    const schema = Joi.object().keys({
      search: Joi.string().trim(),
      dentist: Joi.string()
        .min(24)
        .max(24)
        .pattern(letterAndNumberRegex)
        .trim()
        .error(errors => {
          errors.forEach(err => (err.message = VALIDATION_ID));
          return errors;
        }),
      limit: Joi.number()
        .integer()
        .min(1),
      page: Joi.number()
        .integer()
        .min(1)
    });

    const { error } = await schema.validate(req.query, { abortEarly: true });
    if (error) throw new ValidationError(error, 400);
    return next();
  } catch (err) {
    logger.error('getAll - Validation Fails - ERROR: ', err);
    return res.status(utils.getStatusCode(err)).json(utils.formatError(err));
  }
};
