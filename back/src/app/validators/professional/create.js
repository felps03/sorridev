const Joi = require('joi');

const {
  logger,
  utils,
  dataUtils,
  enums: {
    GenderType,
    CustomErrorMessages: { GENDER_INVALID }
  },
  error: { ValidationError }
} = require('../../../helpers');

/* eslint no-return-assign: "error" */
module.exports = async (req, res, next) => {
  try {
    const schema = Joi.object().keys({
      blockedAt: Joi.boolean(),
      name: Joi.string()
        .trim()
        .required(),
      cpf: Joi.string()
        .max(14)
        .min(14)
        .trim()
        .custom((value, helper) => {
          if (!utils.cpfValidation(value)) {
            return helper.message('CPF is not valid');
          }
          return true;
        }),
      email: Joi.string()
        .trim()
        .custom((value, helper) => {
          if (!utils.validateEmail(value)) {
            return helper.message('Email is not valid');
          }
          return true;
        }),
      birthdate: Joi.string().custom((value, helper) => {
        if (!utils.isDateValid(value)) {
          return helper.message('Birthdate is not valid');
        }
        if (!dataUtils.isDateOlderThanToday(value)) {
          return helper.message('Birthdate must be before or equal today');
        }
        return true;
      }),
      gender: Joi.string()
        .uppercase()
        .valid(...GenderType)
        .trim()
        .required()
        .error(errors => {
          errors.forEach(err => (err.message = GENDER_INVALID));
          return errors;
        }),
      rg: Joi.string().trim(),
      phone: Joi.string().trim(),
      photo: Joi.string().trim(),
      provider: Joi.string().trim(),
      observation: Joi.string().trim(),
      CRO: Joi.string(),
      specialist: Joi.array(),
      password: Joi.string()
        .min(6)
        .max(20)
        .trim(),
      address: Joi.object({
        cep: Joi.string()
          .trim()
          .custom((value, helper) => {
            if (!utils.cepValidation(value)) {
              return helper.message('CEP number is not valid');
            }
            return true;
          }),
        street: Joi.string().trim(),
        number: Joi.string().trim(),
        neighborhood: Joi.string().trim(),
        complement: Joi.string().trim(),
        city: Joi.string().trim(),
        state: Joi.string().trim(),
        country: Joi.string().trim()
      })
    });

    const { error } = await schema.validate(req.body, { abortEarly: true });
    if (error) throw new ValidationError(error, 400);
    return next();
  } catch (err) {
    logger.error('getAll - Validation Fails - ERROR: ', err);
    return res.status(utils.getStatusCode(err)).json(utils.formatError(err));
  }
};
