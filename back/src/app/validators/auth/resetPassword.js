const Joi = require('joi');

const {
  logger,
  utils,
  error: { ValidationError }
} = require('../../../helpers');

/* eslint no-return-assign: "error" */
module.exports = async (req, res, next) => {
  try {
    const schema = Joi.object().keys({
      currentPassword: Joi.string()
        .trim()
        .required(),
      password: Joi.string()
        .trim()
        .required(),
      confirmPassword: Joi.string()
        .valid(Joi.ref('password'))
        .when('password', {
          is: String,
          then: Joi.string()
            .min(3)
            .max(20)
            .required(),
          otherwise: Joi.optional()
        })
        .required()
    });

    const { error } = await schema.validate(req.body, { abortEarly: true });
    if (error) throw new ValidationError(error, 400);
    return next();
  } catch (err) {
    logger.error('getAll - Validation Fails - ERROR: ', err);
    return res.status(utils.getStatusCode(err)).json(utils.formatError(err));
  }
};
