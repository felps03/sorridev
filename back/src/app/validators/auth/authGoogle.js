const Joi = require('joi');

const {
  logger,
  utils,
  error: { ValidationError }
} = require('../../../helpers');

/* eslint no-return-assign: "error" */
module.exports = async (req, res, next) => {
  try {
    const schema = Joi.object().keys({
      name: Joi.string()
        .trim()
        .required(),
      email: Joi.string()
        .trim()
        .custom((value, helper) => {
          if (!utils.validateEmail(value)) {
            return helper.message('Email is not valid');
          }
          return true;
        }),
      photo: Joi.string().trim(),
      provider: Joi.string().trim()
    });

    const { error } = await schema.validate(req.body, { abortEarly: true });
    if (error) throw new ValidationError(error, 400);
    return next();
  } catch (err) {
    logger.error('getAll - Validation Fails - ERROR: ', err);
    return res.status(utils.getStatusCode(err)).json(utils.formatError(err));
  }
};
