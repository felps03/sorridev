const Joi = require('joi');

const {
  logger,
  utils,
  error: { ValidationError },
  enums: {
    CustomErrorMessages: { VALIDATION_ID }
  },
  regularExpressions: { letterAndNumberRegex }
} = require('../../../helpers');

/* eslint no-return-assign: "error" */
module.exports = async (req, res, next) => {
  try {
    const schema = Joi.object().keys({
      cash_accounting: Joi.string()
        .trim()
        .required(),
      spending: Joi.string()
        .trim()
        .required(),
      number_of_appointments: Joi.number()
        .integer()
        .required(),
      dentist: Joi.string()
        .min(24)
        .max(24)
        .pattern(letterAndNumberRegex)
        .trim()
        .error(errors => {
          errors.forEach(err => (err.message = VALIDATION_ID));
          return errors;
        })
    });

    const { error } = await schema.validate(req.body, { abortEarly: true });
    if (error) throw new ValidationError(error, 400);
    return next();
  } catch (err) {
    logger.error('PostBalance - Validation Fail - ERROR: ', err);
    return res.status(utils.getStatusCode(err)).json(utils.formatError(err));
  }
};
