const Joi = require('joi');

const {
  logger,
  utils,
  error: { ValidationError },
  enums: {
    CustomErrorMessages: { VALIDATION_ID }
  },
  regularExpressions: { letterAndNumberRegex }
} = require('../../../helpers');

/* eslint no-return-assign: "error" */
module.exports = async (req, res, next) => {
  try {
    const schema = Joi.object().keys({
      id: Joi.string()
        .min(24)
        .max(24)
        .pattern(letterAndNumberRegex)
        .trim()
        .error(errors => {
          errors.forEach(err => (err.message = VALIDATION_ID));
          return errors;
        })
        .required()
    });

    const { error } = await schema.validate(req.params, { abortEarly: true });
    if (error) throw new ValidationError(error, 400);
    return next();
  } catch (err) {
    logger.error('validateID - Validation Fails - ERROR: ', err);
    return res.status(utils.getStatusCode(err)).json(utils.formatError(err));
  }
};
