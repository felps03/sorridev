require('dotenv/config');
const cluster = require('cluster');
const os = require('os');

const app = require('./app');

if (cluster.isMaster && process.env.NODE_ENV !== 'dev') {
  for (let index = 0; index < os.cpus().length; index++) {
    cluster.fork();
  }
} else {
  app.listen(process.env.PORT || 3001);
}
