const mongoose = require('mongoose');

const extend = (Schema, obj) => new mongoose.Schema(Object.assign({}, Schema.obj, obj));

const constructSchema = (listOfSchemas, extraContent) => {
  let UserSchema;
  listOfSchemas.forEach((schema, index) => {
    if (index === 0) UserSchema = extend(schema, extraContent);
    UserSchema = extend(schema, UserSchema.obj);
  });
  return UserSchema;
};

module.exports = {
  extend,
  constructSchema
};
