const { APP_SECRET } = require('../../config');

module.exports = {
  secret: APP_SECRET,
  expiresIn: '7d'
};
