require('dotenv/config');
const mongoose = require('mongoose');

const { MONGO_URL } = require('../../config');
const Admin = require('../app/schemas/Admin');

const StartUpDatabase = async () => {
  mongoose.connect(MONGO_URL, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true
  });

  const NewAdminUser = new Admin({
    name: 'SorriDev Administrator',
    birthdate: '2020-12-27',
    cpf: '000-000-000-00',
    owner: 'ADMIN',
    phone: '53 9 0000-0000',
    gender: 'masculino',
    email: 'sorridevadmin@gmail.com',
    password: '$2a$10$ge2B03SjNrkkPwHVDdTi8e9aTuioJyul5NEPIb4lHiNl1SW9lth.m'
  });

  NewAdminUser.collection
    .insertOne(NewAdminUser)
    .then(() => mongoose.disconnect())
    .finally(() => {
      console.log('====================================');
      console.log('===  ADMIN INSERIDO COM SUCESSO  ===');
      console.log('====================================');
    });
};
StartUpDatabase();
