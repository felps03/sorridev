require('./bootstrap');

require('dotenv/config');
require('express-async-errors');
const cors = require('cors');
const express = require('express');
const RateLimit = require('express-rate-limit');
const helmet = require('helmet');
const Youch = require('youch');

const routes = require('./routes/index.routes');
require('./infra/database/mongo');

class App {
  constructor() {
    this.server = express();
    this.middlewares();
    this.routes();
    this.exceptionHandler();
  }

  middlewares() {
    this.server.use(cors());
    this.server.use(express.json());
    this.server.use(helmet());

    if (!['dev', 'test'].includes(process.env.NODE_ENV)) {
      this.server.use(
        new RateLimit({
          windowMs: 1000 * 60 * 15, // 15 minutos de espera se ultrapassar o valor max.
          max: 100 // Máximo de requisições
        })
      );
    }
  }

  routes() {
    routes(this.server);
  }

  exceptionHandler() {
    this.server.use(async (err, req, res, next) => {
      if (process.env.NODE_ENV === 'dev') {
        const errors = await new Youch(err, req).toJSON();

        return res.status(500).json(errors);
      }

      return res.status(500).json({ error: 'Internal server error' });
    });
  }
}

module.exports = new App().server;
